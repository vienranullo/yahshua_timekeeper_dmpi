package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class LeaveType implements Parcelable
{
    private int id;
    private String name;
    @SerializedName("is_deductible")
    private boolean isDeductible;


    public LeaveType(){}


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isDeductible()
    {
        return isDeductible;
    }

    public void setDeductible(boolean deductible)
    {
        isDeductible = deductible;
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeByte((byte) (isDeductible ? 1 : 0));
    }

    protected LeaveType(Parcel in)
    {
        id = in.readInt();
        name = in.readString();
        isDeductible = in.readByte() != 0;
    }

    public static final Creator<LeaveType> CREATOR = new Creator<LeaveType>()
    {
        @Override
        public LeaveType createFromParcel(Parcel in)
        {
            return new LeaveType(in);
        }

        @Override
        public LeaveType[] newArray(int size)
        {
            return new LeaveType[size];
        }
    };


    public static ArrayList<LeaveType> read(Context context, String searchQuery)
    {
        ArrayList<LeaveType> leaveTypeArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            if (searchQuery != null) {
                selectQuery = "SELECT * FROM leaveType " + searchQuery;
            } else {
                selectQuery = "SELECT * FROM leaveType";
            }

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    LeaveType leaveType = new LeaveType();

                    leaveType.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    leaveType.setName(cursor.getString(cursor.getColumnIndex("name")));
                    leaveType.setDeductible(cursor.getInt(cursor.getColumnIndex("isDeductible")) > 0);

                    leaveTypeArrayList.add(leaveType);

                } while (cursor.moveToNext());
            }

            cursor.close();
            DatabaseAdapter.close();

            return leaveTypeArrayList;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("LeaveType read error "+errorMessage);
            if(errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return leaveTypeArrayList;
        }
    }

    public  boolean save(DbAdapter dbAdapter)
    {
        boolean isSuccess;
        try
        {
            ContentValues contentValues = new ContentValues();

            contentValues.put("id", getId());
            contentValues.put("name", getName());
            contentValues.put("isDeductible",isDeductible());

            dbAdapter.getDb().insertOrThrow("leaveType", null, contentValues);

            isSuccess = true;

        } catch (Exception err)
        {
            ErrorLogs.save("LeaveType batchSave error "+err.getMessage(), dbAdapter);
            isSuccess = false;
        }

        return isSuccess;
    }

    public static void deleteAll(DbAdapter dbAdapter) throws SQLException
    {
        dbAdapter.getDb().delete("leaveType", "", new String[]{});
    }
}