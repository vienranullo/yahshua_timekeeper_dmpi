package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;

import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.ErrorDialogFragment;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.OpenSettingsDialog;
import com.yahshua.yahshuatimekeeperdmpi.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public final class Utility
{
    private static final String TAG = "TIMEKEEPER 3S";

    static SimpleDateFormat dateDisplayFormat = new SimpleDateFormat("MMM. dd, yyyy", Locale.US);


    public static String dateFromDateToDisplay(Date dateFrom, Date dateTo)
    {
        SimpleDateFormat dateFromDisplayFormat = new SimpleDateFormat("MMM. dd", Locale.US);

        return "From " + dateFromDisplayFormat.format(dateFrom) + " to " + dateDisplayFormat.format(dateTo.getTime());
    }

    public static String dateDisplay(Date date)
    {
        return dateDisplayFormat.format(date.getTime());
    }

    public static String getAndroidId(Context context)
    {
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    public static void sendHandlerMessage(Handler handler, int what, String handlerMessage)
    {
        Message message = handler.obtainMessage(what, handlerMessage);
        message.sendToTarget();
    }

    public static void showError(FragmentManager fragmentManager, String errorMessage)
    {
        ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        Bundle bundle = new Bundle();

        if (errorMessage != null) bundle.putString("ERROR_MESSAGE", errorMessage);

        errorDialogFragment.setArguments(bundle);
        errorDialogFragment.show(fragmentManager, "ErrorDialog");
    }

    public static boolean isNfcSupported(Context context)
    {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
        return (nfcAdapter != null);
    }

    public static boolean dateAndTimeDetection(Context context, FragmentManager fragmentManager){


        if (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 0
                && Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) == 0)
        {
            openSettingsDialog( context.getString(R.string.error_automatic_date_time), "Date and time is off", fragmentManager);
            return false;
        }
        else if (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 0)
        {
            openSettingsDialog( context.getString(R.string.make_sure_to_turn_on_auto_date), "Auto date & time is off", fragmentManager);
            return false;
        }
        else if (Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) == 0)
        {
            openSettingsDialog( context.getString(R.string.make_sure_to_turn_on_time_zone), "Time zone is off", fragmentManager);
            return false;
        } else {
            return true;
        }
    }

    public static void openSettingsDialog(String message, String title, FragmentManager fragmentManager){

        Bundle bundle = new Bundle();
        bundle.putString("MESSAGE", message);
        bundle.putString("TITLE", title);

        OpenSettingsDialog customAlertDialog = new OpenSettingsDialog();
        customAlertDialog.setArguments(bundle);
        customAlertDialog.show(fragmentManager,"frag");
    }

    public static boolean haveNetworkConnection(Context context, FragmentManager fragmentManager)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo;

        if (connectivityManager != null) networkInfo = connectivityManager.getActiveNetworkInfo();
        else networkInfo = null;

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        }else{
            openSettingsDialog( context.getString(R.string.set_network_message), context.getString(R.string.set_network_title), fragmentManager);
            return false;
        }
    }


    public static boolean haveNetworkConnection(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo;

        if (connectivityManager != null) networkInfo = connectivityManager.getActiveNetworkInfo();
        else networkInfo = null;

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static void showNoNetworkConnectionDialog(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("There's no network connection")
                .setMessage("Make sure you're connected to a WI-FI or mobile network and try again.")
                .setPositiveButton("OK", null);
        builder.show();
    }

    public static void showFailedToConnectToServerDialog(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Failed to connect to the server")
                .setMessage("Please check your internet connection or contact admin.")
                .setPositiveButton("Close", null);
        builder.show();
    }

    public static void showServerErrorDialog(Context context, String error)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Server Error")
                .setMessage(error)
                .setPositiveButton("Close", null);
        builder.show();
    }
}