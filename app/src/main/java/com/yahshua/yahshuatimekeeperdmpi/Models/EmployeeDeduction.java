package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class EmployeeDeduction implements Parcelable
{
    private int id;
    @SerializedName("deduction_type")
    private String deductionType;
    private double amount;
    @SerializedName("date_requested")
    private String dateRequested;
    private String remarks;
    private String status;
    private String term;
    private double amountPerDeduct;
    private String dateEnd;
    private String dateStart;
    private boolean isApproved;
    @SerializedName("deduction_id")
    private int deductionTypeId;
    @SerializedName("regular_id")
    private int regularId;
    @SerializedName("employee_id")
    private int employeeId;
    @SerializedName("paymentterm")
    private String paymentTerm;
    private boolean synced;
    private DeductionType deductType;
    private String lastSyncedDate;
    private Employee employee;

    public EmployeeDeduction()
    {

    }

    public Employee getEmployee()
    {
        return employee;
    }

    public void setEmployee(Employee employee)
    {
        this.employee = employee;
    }

    public String getLastSyncedDate()
    {
        return lastSyncedDate;
    }

    public void setLastSyncedDate(String lastSyncedDate)
    {
        this.lastSyncedDate = lastSyncedDate;
    }

    public DeductionType getDeductType()
    {
        return deductType;
    }

    public void setDeductType(DeductionType deductType)
    {
        this.deductType = deductType;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getDeductionTypeId()
    {
        return deductionTypeId;
    }

    public void setDeductionTypeId(int deductionTypeId)
    {
        this.deductionTypeId = deductionTypeId;
    }

    public String getTerm()
    {
        return term;
    }

    public void setTerm(String term)
    {
        this.term = term;
    }

    public double getAmountPerDeduct()
    {
        return amountPerDeduct;
    }

    public void setAmountPerDeduct(double amountPerDeduct)
    {
        this.amountPerDeduct = amountPerDeduct;
    }

    public String getDateEnd()
    {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd)
    {
        this.dateEnd = dateEnd;
    }

    public String getDateStart()
    {
        return dateStart;
    }

    public void setDateStart(String dateStart)
    {
        this.dateStart = dateStart;
    }

    public boolean isApproved()
    {
        return isApproved;
    }

    public void setApproved(boolean approved)
    {
        isApproved = approved;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public String getDeductionType()
    {
        return deductionType;
    }

    public void setDeductionType(String deductionType)
    {
        this.deductionType = deductionType;
    }

    public String getDateRequested()
    {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested)
    {
        this.dateRequested = dateRequested;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public int getRegularId() {
        return regularId;
    }

    public void setRegularId(int regularId) {
        this.regularId = regularId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public static ArrayList<EmployeeDeduction> read(Context context, int employeeId, boolean forSyncing, String searchQuery)
    {
        ArrayList<EmployeeDeduction> employeeDeductionArrayList = new ArrayList<>();
        try
        {
            String selectQuery = "SELECT ";
                selectQuery += "employeeDeductions.id as employeeDeductionId," ;
                selectQuery += "employeeDeductions.deduction_type_id as deductionTypeId, ";
                selectQuery += "employeeDeductions.employee_id as employeeDeductId, ";
                selectQuery += "employeeDeductions.is_synced as employeeDeductIsSynced, ";
                selectQuery += "employeeDeductions.amount, ";
                selectQuery += "employeeDeductions.remarks, ";
                selectQuery += "employeeDeductions.amount_per_deduction, ";
                selectQuery += "employeeDeductions.date_requested, ";
                selectQuery += "employeeDeductions.date_start, ";
                selectQuery += "employeeDeductions.date_end, ";
                selectQuery += "employeeDeductions.lastSyncedDate, ";
                selectQuery += "employeeDeductions.is_synced, ";
                selectQuery += "employeeDeductions.term, ";
                selectQuery += "employee.firstname as firstName, ";
                selectQuery += "employee.lastname as lastName, ";
                selectQuery += "deductionTypes.name as deductionTypesName ";
                selectQuery += "FROM employeeDeductions ";
                selectQuery += "LEFT JOIN employee ON employeeDeductions.employee_id = employee.ypo_id " ;
                selectQuery += "LEFT JOIN deductionTypes ON employeeDeductions.deduction_type_id = deductionTypes.deduction_type_id ";

            if(employeeId != 0)
            {
                selectQuery +=  "WHERE employee.ypo_id=" + employeeId;

                if (searchQuery != null)
                {
                    selectQuery += " AND (remarks LIKE '%" + searchQuery + "%' OR deductionTypesName LIKE '%" + searchQuery + "%' OR amount LIKE '%" + searchQuery + "%' OR amount_per_deduction LIKE '%" + searchQuery + "%')";
                }

                if (forSyncing)
                {
                    selectQuery += " AND is_synced = 0";
                }
            }
            else
            {
                selectQuery += " WHERE is_synced = 0";
            }


            selectQuery += " ORDER BY employeeDeductionId DESC";

            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst())
            {
                do
                {
                    EmployeeDeduction employeeDeduction = new EmployeeDeduction();
                    employeeDeduction.setDeductType(new DeductionType());
                    employeeDeduction.setEmployee(new Employee());

                    employeeDeduction.setId(Converter.ObjectInt(cursor, "employeeDeductionId"));
                    employeeDeduction.setDeductionTypeId(Converter.ObjectInt(cursor, "deductionTypeId"));
                    employeeDeduction.setEmployeeId(Converter.ObjectInt(cursor, "employeeDeductId"));
                    employeeDeduction.setAmount(Converter.ObjectToDouble(cursor, "amount"));
                    employeeDeduction.setAmountPerDeduct(Converter.ObjectToDouble(cursor, "amount_per_deduction"));
                    employeeDeduction.setDateRequested(Converter.ObjectToString(cursor, "date_requested"));
                    employeeDeduction.setDateStart(Converter.ObjectToString(cursor, "date_start"));
                    employeeDeduction.setDateEnd(Converter.ObjectToString(cursor, "date_end"));
                    employeeDeduction.setSynced(Converter.ObjectToBoolean(cursor, "employeeDeductIsSynced"));
                    employeeDeduction.setStatus(employeeDeduction.isSynced() ? "Synced" : "Unsynced");
                    employeeDeduction.setTerm(Converter.ObjectToString(cursor, "term"));
                    employeeDeduction.setRemarks(Converter.ObjectToString(cursor, "remarks"));
                    employeeDeduction.getDeductType().setName(Converter.ObjectToString(cursor, "deductionTypesName"));
                    employeeDeduction.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));
                    employeeDeduction.getEmployee().setFirstname(Converter.ObjectToString(cursor, "firstName"));
                    employeeDeduction.getEmployee().setLastname(Converter.ObjectToString(cursor, "lastName"));

                    employeeDeductionArrayList.add(employeeDeduction);
                } while (cursor.moveToNext());
            }

            return employeeDeductionArrayList;
        } catch (SQLiteException err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeDeduction save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return employeeDeductionArrayList;
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeDeduction save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return employeeDeductionArrayList;
        }
    }

    public void delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            db.delete("employeeDeductions","id = ?", new String[]{String.valueOf(getId())});
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeDeduction delete error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
        }
    }

    public boolean setToSynced(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("is_synced", 1);
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));

            db.open();
            return db.update(contentValues, "employeeDeductions", "id=?", new String[]{getId()+""});

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeDeduction setToSynced error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return  false;
        }
    }

    public boolean save(Context context, int id)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("deduction_type_id", getDeductionTypeId());
            contentValues.put("amount", getAmount());
            contentValues.put("date_requested", getDateRequested());
            contentValues.put("remarks", getRemarks());
            contentValues.put("term", getTerm());
            contentValues.put("amount_per_deduction", getAmountPerDeduct());
            contentValues.put("date_start", getDateStart());
            contentValues.put("date_end", getDateEnd());
            contentValues.put("employee_id", getEmployeeId());
            contentValues.put("regular_id", getRegularId());
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (id > 0)
            {
                db.open();
                db.update(contentValues, "employeeDeductions", " id = ?", new String[]{String.valueOf(id)});
            }
            else
            {
                db.save(contentValues, "employeeDeductions");
            }

            return true;
        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeDeduction save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    protected EmployeeDeduction(Parcel in)
    {
        id = in.readInt();
        lastSyncedDate = in.readString();
        deductionType = in.readString();
        amount = in.readDouble();
        dateRequested = in.readString();
        remarks = in.readString();
        status = in.readString();
        term = in.readString();
        amountPerDeduct = in.readDouble();
        dateEnd = in.readString();
        dateStart = in.readString();
        isApproved = in.readByte() != 0;
        deductionTypeId = in.readInt();
        regularId = in.readInt();
        employeeId = in.readInt();
        paymentTerm = in.readString();
        synced = in.readByte() != 0;
        deductType = in.readParcelable(DeductionType.class.getClassLoader());
        employee = in.readParcelable(Employee.class.getClassLoader());
    }

    public static final Creator<EmployeeDeduction> CREATOR = new Creator<EmployeeDeduction>()
    {
        @Override
        public EmployeeDeduction createFromParcel(Parcel in)
        {
            return new EmployeeDeduction(in);
        }

        @Override
        public EmployeeDeduction[] newArray(int size)
        {
            return new EmployeeDeduction[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(lastSyncedDate);
        dest.writeString(deductionType);
        dest.writeDouble(amount);
        dest.writeString(dateRequested);
        dest.writeString(remarks);
        dest.writeString(status);
        dest.writeString(term);
        dest.writeDouble(amountPerDeduct);
        dest.writeString(dateEnd);
        dest.writeString(dateStart);
        dest.writeByte((byte) (isApproved ? 1 : 0));
        dest.writeInt(deductionTypeId);
        dest.writeInt(regularId);
        dest.writeInt(employeeId);
        dest.writeString(paymentTerm);
        dest.writeByte((byte) (synced ? 1 : 0));
        dest.writeParcelable(deductType, flags);
        dest.writeParcelable(employee, flags);
    }
}
