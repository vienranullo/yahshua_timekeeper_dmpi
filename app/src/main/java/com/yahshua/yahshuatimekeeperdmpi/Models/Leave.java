package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;


public class Leave extends Application implements Parcelable
{
    private String day = "", leaveTypeName, timeFrom, timeTo;
    private Date dateFrom, dateTo, halfDayDate;
    private int leaveTypeId;
    @SerializedName("days_leave")
    private double daysLeave;

    static private SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-M-d HH:mm:ss", Locale.US);


    public Leave() {}


    public String getDay()
    {
        return day;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

    public String getLeaveTypeName()
    {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName)
    {
        this.leaveTypeName = leaveTypeName;
    }

    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    public double getDaysLeave()
    {
        return daysLeave;
    }

    public void setDaysLeave(double daysLeave)
    {
        this.daysLeave = daysLeave;
    }

    public int getLeaveTypeId()
    {
        return leaveTypeId;
    }

    public void setLeaveTypeId(int leaveTypeId)
    {
        this.leaveTypeId = leaveTypeId;
    }

    public Date getHalfDayDate()
    {
        return halfDayDate;
    }

    public void setHalfDayDate(Date halfDayDate)
    {
        this.halfDayDate = halfDayDate;
    }

    public String getTimeFrom()
    {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom)
    {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo()
    {
        return timeTo;
    }

    public void setTimeTo(String timeTo)
    {
        this.timeTo = timeTo;
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(day);
        dest.writeString(leaveTypeName);
        dest.writeInt(leaveTypeId);
        dest.writeDouble(daysLeave);
    }

    protected Leave(Parcel in) {
        day = in.readString();
        leaveTypeName = in.readString();
        leaveTypeId = in.readInt();
        daysLeave = in.readDouble();
    }

    public static final Creator<Leave> CREATOR = new Creator<Leave>() {
        @Override
        public Leave createFromParcel(Parcel in) {
            return new Leave(in);
        }

        @Override
        public Leave[] newArray(int size) {
            return new Leave[size];
        }
    };


    public static ArrayList<Leave> load(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<Leave> leaveArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM leave";

        try {
            if (searchQuery != null) selectQuery += " " + searchQuery ;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    Leave leave = new Leave();

                    Date dateFrom, dateTo, halfDayDate, requestedDate;
                    dateFrom = dateTo = halfDayDate = requestedDate = new Date();

                    leave.setDay(Converter.ObjectToString(cursor, "day"));

                    if (leave.getDay().equals("whole")) {

                        dateFrom = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("dateFrom")));
                        dateTo = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("dateTo")));

                        leave.setDateFrom(dateFrom);
                        leave.setDateTo(dateTo);

                    } else if (leave.getDay().equals("half")) {

                        halfDayDate = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("halfDayDate")));

                        leave.setHalfDayDate(halfDayDate);
                        Debugger.logD(requestedDate + " ");
                        Debugger.logD(Converter.ObjectToString(cursor, "timeFrom"));
                        leave.setTimeFrom(cursor.getString(cursor.getColumnIndex("timeFrom")));
                        leave.setTimeTo(cursor.getString(cursor.getColumnIndex("timeTo")));
                    }

                    try {
                        requestedDate = datetimeFormat.parse(cursor.getString(cursor.getColumnIndex("requestedDate"))); // Naay Eyyor
                    } catch (ParseException err) {
                        Debugger.logD(err.toString());
                    }

                    Debugger.logD(requestedDate + " ");

                    leave.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    leave.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    leave.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    leave.setDay(cursor.getString(cursor.getColumnIndex("day")));
                    leave.setDaysLeave(cursor.getDouble(cursor.getColumnIndex("daysLeave")));
                    leave.setLeaveTypeId(cursor.getInt(cursor.getColumnIndex("leaveTypeId")));
                    leave.setLeaveTypeName(cursor.getString(cursor.getColumnIndex("leaveTypeName")));
                    leave.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    leave.setRequestedDate(requestedDate);
                    leave.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    leave.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    leave.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    leaveArrayList.add(leave);

                } while (cursor.moveToNext());
            }

            return leaveArrayList;
        } catch (SQLiteException err)
        {
            Toasty.warning(context, context.getString(R.string.label_database_error) + " : LVR").show();
            return leaveArrayList;
        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Leave load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return leaveArrayList;
        }
    }

    public static int count(Context context, String countQuery)
    {
        int count = 0;

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            Cursor cursor = db.read(countQuery);

            if (cursor.moveToNext()) count = cursor.getInt(cursor.getColumnIndex("count"));

            return count;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Leave count error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return count;
        }
    }

    public boolean save(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();
            String dateFromStr, dateToStr, halfDayDateStr;

            switch (getDay()) {
                case "whole":
                    dateFromStr = DateToString.dbFormat(getDateFrom());
                    dateToStr = DateToString.dbFormat(getDateTo());

                    contentValues.put("dateFrom", dateFromStr);
                    contentValues.put("dateTo", dateToStr);

                    contentValues.put("halfDayDate", "");
                    contentValues.put("timeFrom", "");
                    contentValues.put("timeTo", "");

                    break;

                case "half":
                    halfDayDateStr = DateToString.dbFormat(getHalfDayDate());

                    contentValues.put("halfDayDate", halfDayDateStr);
                    contentValues.put("timeFrom", getTimeFrom());
                    contentValues.put("timeTo", getTimeTo());

                    contentValues.put("dateFrom", "");
                    contentValues.put("dateTo", "");

                    break;
            }

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("employeeName", getEmployeeName());
            contentValues.put("day", getDay());
            contentValues.put("daysLeave", getDaysLeave());
            contentValues.put("leaveTypeId", getLeaveTypeId());
            contentValues.put("leaveTypeName", getLeaveTypeName());
            contentValues.put("reason", getReason());
            contentValues.put("isSync", isSync());
            contentValues.put("status", getStatus());
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (getId() > 0) {
                db.open();
                db.update(contentValues, "leave", " id = ?", new String[]{ String.valueOf( getId()) });
            } else {
                db.save(contentValues, "leave");
            }

            return true;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Leave save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public boolean changeSyncStatus(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("isSync", "1");
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));

            return db.update(contentValues, "leave", "id="+this.getId(),null);

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("changeSyncStatus error " + errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public void delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            if (getId() > 0) db.delete("leave","id = ?", new String[]{ String.valueOf(getId()) });
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("delete error " + errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
        }
    }

    public static int getLeaveCount(Context context)
    {
        try {
            Leave leave = new Leave();
            String selectQuery = "SELECT COUNT(id) AS count FROM leave WHERE isSync = " + leave.getEmployeeId();
            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            return count;
        }catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("getLeaveCount error " + errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return 0;
        }
    }

}