package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.util.Base64;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLeaveBalance;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.Models.ErrorLogs;
import com.yahshua.yahshuatimekeeperdmpi.Models.Leave;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveType;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveTypeBalances;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.Models.Undertime;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnRestday;
import com.yahshua.yahshuatimekeeperdmpi.Models.Workdays;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class Syncer
{

    public static void uploadAll(final Context context, final boolean notify)
    {
        try
        {
            // Will be used if we need to Upload Data Silently
            // Pass False if silent background upload
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            JSONObject jsonObject = new JSONObject(); // Initialize Json Object to Be Converted to String Entity

            // Read All Data Needed to Be Uploaded
            final ArrayList<EmployeeLogs> employeeLogs = EmployeeLogs.read(context,true,0, 20);
            final ArrayList<Leave> leaveArrayList = Leave.load(context, "WHERE isSync = 0");
            final ArrayList<Overtime> overtimeArrayList = Overtime.load(context,"WHERE isSync = 0");
            final ArrayList<Undertime> undertimeArrayList = Undertime.load(context,"WHERE isSync = 0");
            final ArrayList<WorkOnHoliday> workOnHolidayArrayList = WorkOnHoliday.load(context,"WHERE isSync = 0");
            final ArrayList<WorkOnRestday> workOnRestdayArrayList = WorkOnRestday.load(context,"WHERE isSync = 0");

            // Convert to JSON and add to jsonObject
            jsonObject.put("employee_logs", new Gson().toJson(employeeLogs));
            jsonObject.put("leave", new Gson().toJson(leaveArrayList));
            jsonObject.put("overtime", new Gson().toJson(overtimeArrayList));
            jsonObject.put("undertime", new Gson().toJson(undertimeArrayList));
            jsonObject.put("workOnHoliday", new Gson().toJson(workOnHolidayArrayList));
            jsonObject.put("workOnRestday", new Gson().toJson(workOnRestdayArrayList));

            StringEntity stringEntity = new StringEntity(jsonObject.toString()); // Convert to String Entity

            // Execute HTTP POST, Example URL (upload_all/)
            HttpProvider.post(context, "upload_all/", stringEntity, true, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if (notify) Toasty.success(context, "Upload Success").show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    try
                    {
                        String str = new String(responseBody, "UTF-8");
                        throw new Exception(str);
                    } catch (Exception err)
                    {
                        if (notify)
                        {
                            Toasters.HideLoadingSpinner();
                            Toasters.ShowToast(context, "Sync Failed " + err.toString());
                        }
                    }
                }
            });

        } catch (Exception err)
        {
            if (notify)
            {
                Toasty.error(context, err.toString()).show();
            }
        }
    }

    public static void syncEmployee(final Context context, final boolean notify)
    {
        try
        {
            if(notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            HttpProvider.post(context, "get-employees/", null, true, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    DbAdapter dbAdapter = new DbAdapter(context);
                    dbAdapter.open();
                    try
                    {
                        Employee.delete(dbAdapter);
                        EmployeeLeaveBalance.delete(dbAdapter);

                        ArrayList<Employee> employeeArrayList = new Gson().fromJson(response.getJSONArray("emp_list").toString(), new TypeToken<List<Employee>>(){}.getType());
                        Debugger.logD(employeeArrayList.size() +" size");

                        for (Employee employee : employeeArrayList)
                        {
                            employee.save(context, dbAdapter);

                            for (LeaveTypeBalances leaveTypeBalances: employee.getLeaveTypeBalances())
                            {
                                EmployeeLeaveBalance employeeLeaveBalance = new EmployeeLeaveBalance();

                                employeeLeaveBalance.setEmployeeId(employee.getId());
                                employeeLeaveBalance.setLeaveTypeId(leaveTypeBalances.getId());
                                employeeLeaveBalance.setLeaveTypeName(leaveTypeBalances.getName());
                                employeeLeaveBalance.setTotal(leaveTypeBalances.getTotal());

                                employeeLeaveBalance.save(dbAdapter);
                            }
                        }

                        // Leave Types
                        ArrayList<LeaveType> leaveTypeList = new Gson().fromJson(response.getJSONArray("leave_types").toString(), new TypeToken<List<LeaveType>>(){}.getType());

                        LeaveType.deleteAll(dbAdapter);
//                        LeaveType.batchSave(dbAdapter, leaveTypeList);

                        // Employee Schedule Workdays
                        JSONArray workdaysJSONArray = response.getJSONArray("emp_workday_list");

                        for (int i = 0 ; i < workdaysJSONArray.length(); i++)
                        {
                            JSONObject workdayJSON = workdaysJSONArray.getJSONObject(i);
                            Workdays workdays = new Workdays();

                            workdays.setMonday(Boolean.parseBoolean(workdayJSON.getString("0")));
                            workdays.setTuesday(Boolean.parseBoolean(workdayJSON.getString("1")));
                            workdays.setWednesday(Boolean.parseBoolean(workdayJSON.getString("2")));
                            workdays.setThursday(Boolean.parseBoolean(workdayJSON.getString("3")));
                            workdays.setFriday(Boolean.parseBoolean(workdayJSON.getString("4")));
                            workdays.setSaturday(Boolean.parseBoolean(workdayJSON.getString("5")));
                            workdays.setSunday(Boolean.parseBoolean(workdayJSON.getString("6")));

                            workdays.setEmployeeId(Integer.parseInt(workdayJSON.getString("employee")));
                            workdays.setScheduleId(Integer.parseInt(workdayJSON.getString("schedule")));

                            workdays.save(dbAdapter);
                        }

                        if(notify)
                        {
                            Toasters.HideLoadingSpinner();
                            Toasty.success(context, "Sync Complete").show();
                        }

                        dbAdapter.close();
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE","SYNC","SUCCESS","ALL");

                    } catch (Exception err)
                    {
                        ErrorLogs.save("Sync employee onSuccess error ", dbAdapter);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasters.ShowToast(context, "Sync Failed 101 : " + responseString);
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE","SYNC","FAILED","ALL");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    if (notify) Toasters.HideLoadingSpinner();
                    String errorMessaage = errorResponse != null ? errorResponse.toString() : " Check Internet Connection";
                    AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE","SYNC","FAILED","ALL");
                    Toasters.ShowToast(context, "Sync Failed 102 : \n" + errorMessaage);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                    }
                    AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE","SYNC","FAILED","ALL");
                    Toasters.ShowToast(context, "Sync Failed 103 : ");
                }
            });

        } catch (Exception err)
        {
            if (notify)
                Toasters.HideLoadingSpinner();

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync employee error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, err.toString()).show();
                Debugger.logD("Sync employee error " + err.toString());
            }
        }
    }

    public static void syncIndividualLogs(final Context context, final EmployeeLogs employeeLog, AsyncHttpResponseHandler asyncHttpResponseHandler)
    {
        try
        {
            Toasters.ShowLoadingSpinner(context);

            JSONArray jsonArray = new JSONArray();


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("employee", employeeLog.getEmployeeId());
            jsonObject.put("log_time", employeeLog.getLogTime());
            jsonObject.put("log_type", employeeLog.getLogType());
            jsonObject.put("sync_id", employeeLog.getSyncId());
            jsonObject.put("date", employeeLog.getDate());

            // Check first if image file exists
            boolean imgExists = false;
            int index = employeeLog.getLogImg().lastIndexOf('/');
            String imageFilename = employeeLog.getLogImg().substring(index+1);
            if (ImageHandler.fileExists(context, imageFilename)) imgExists = true;

            // Convert File image as Base 64 if files exists
            if (imgExists)
            {
                String image = Base64.encodeToString(ImageHandler.getImageByPath(employeeLog.getLogImg()), Base64.DEFAULT);
                jsonObject.put("log_img", image);
            }

            jsonArray.put(jsonObject);

            String json = jsonArray.toString();
            StringEntity entity = new StringEntity(json);

            HttpProvider.post(context, "sync-logs2/", entity, false, asyncHttpResponseHandler);

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync individual logs error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync individual logs error " + err.toString());
            }
        }
    }

    public static void loadLeaveRequests(final Context context, final boolean notify, boolean isNotSync)
    {
        try
        {
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            String searchQuery = "WHERE isSync = 0";
            final ArrayList<Leave> leaveArrayList = Leave.load(context, searchQuery);

            if (leaveArrayList.size() == 0) throw new Exception("No Leave Applications To Sync");
            uploadLeaveRequests(leaveArrayList, 0, isNotSync, context, notify);

        } catch (Exception err)
        {
            if (notify)
            {
                Toasters.HideLoadingSpinner();
                Toasters.ShowToast(context, err.getMessage());
            }

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync leave applications error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync leave applications error" + err.toString());
            }

        }
    }

    private static void uploadLeaveRequests(final ArrayList<Leave> leaveArrayList, final int idx, final boolean isNotSync, final Context context, final boolean notify) throws Exception
    {
        try
        {
            if (idx == leaveArrayList.size())
            {
                if (isNotSync)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasty.success(context, "Syncing Leave Applications Success!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasters.HideLoadingSpinner();
                    Toasty.success(context,"Done with Leave Applications").show();
                }

                AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","SUCCESS","NONE");
            }
            else
            {
                // Convert to JSONObject
                final Leave leave = leaveArrayList.get(idx);
                JSONObject leaveJson = new JSONObject();

                if (leave.getDay().equals("whole")) {
                    leaveJson.put("date_from", DateToString.dbFormat(leave.getDateFrom()));
                    leaveJson.put("date_to", DateToString.dbFormat(leave.getDateTo()));
                } else {
                    leaveJson.put("half_day_date", DateToString.dbFormat(leave.getHalfDayDate()));
                    leaveJson.put("time_from", leave.getTimeFrom());
                    leaveJson.put("time_to", leave.getTimeTo());
                }

                leaveJson.put("employee", leave.getEmployeeId());
                leaveJson.put("day", leave.getDay());
                leaveJson.put("days_leave", leave.getDaysLeave());
                leaveJson.put("leave_type2", leave.getLeaveTypeId());
                leaveJson.put("reason", leave.getReason());
                leaveJson.put("requested_date", DateToString.dateTimeFormat(leave.getRequestedDate()));
                leaveJson.put("status", leave.getStatus());

                // Upload
                String json = leaveJson.toString();
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            leave.changeSyncStatus(context);
                            uploadLeaveRequests(leaveArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            if (notify)
                            {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, err.getMessage());
                            }

                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("Sync leave applications error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("Sync leave applications error" + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            String str = "";

                            if (responseBody != null && responseBody.length > 0)
                            {
                                Debugger.logD(new String(responseBody));
                                str = new String(responseBody, "UTF-8");
                            }

                            if (notify)
                            {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, "Sync Failed " + str);
                            }


                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");

                        } catch (Exception err) {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler leave applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Toasty.error(context, err.toString()).show();
                                Debugger.logD("AsyncHttpResponseHandler leave applications onFailure error " + err.toString());
                            }
                        }
                    }

                };

                AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            leave.changeSyncStatus(context);
                            uploadLeaveRequests(leaveArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            if (notify)
                            {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, err.getMessage());
                            }

                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("Sync leave applications error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("Sync leave applications error" + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        if (responseBody != null) Debugger.logD(new String(responseBody));
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");
                    }

                };

                if (isNotSync)
                {
                    HttpProvider.postSync(context, "leave-application/save/", entity, false, syncHttpResponseHandler);
                } else
                {
                    HttpProvider.post(context, "leave-application/save/", entity, false, asyncHttpResponseHandler );
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading requests: " + err.toString());
        }
    }

    public static void loadOvertimeRequests(final Context context, final boolean notify, boolean isNotSync)
    {
        try
        {
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            String searchQuery = "WHERE isSync = 0 AND company_id = " + UserSession.getCompanyId(context);
            ArrayList<Overtime> overtimeArrayList = Overtime.load(context, searchQuery);

            // Tempo Fix
            if (overtimeArrayList.size() == 0)
            {
                 searchQuery = "WHERE isSync = 0";
                 overtimeArrayList = Overtime.load(context, searchQuery);
            }

            if (overtimeArrayList.size() == 0) throw new Exception("No Overtime Applications To Sync");

            uploadOvertimeRequests(overtimeArrayList, 0, isNotSync, context, notify);

//            final ArrayList<Overtime> overtimeArrayListFinal = overtimeArrayList;
//
//            // Called in Manual Syncing
//            for (Overtime overtime : overtimeArrayListFinal)
//            {
//                overtime.setDateString(DateToString.dbFormat(overtime.getDate()));
//                overtime.setDateRequestedString(DateToString.dbFormat(overtime.getDate()));
//            }
//
//            String json = new Gson().toJson(overtimeArrayList);
//            StringEntity entity = new StringEntity(json);
//
//            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                    for(Overtime overtime : overtimeArrayListFinal)
//                    {
//                        overtime.changeSyncStatus(context);
//                    }
//
//                    if (notify)
//                    {
//                        Toasters.HideLoadingSpinner();
//                        Toasty.success(context, "Syncing Overtime Applications Success!", Toast.LENGTH_LONG).show();
//                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","SUCCESS","NONE");
//                    }
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                    try {
//                        String str = "";
//
//                        if (responseBody!= null && responseBody.length > 0)
//                        {
//                            str = new String(responseBody, "UTF-8");
//                        }
//
//                        Toasters.HideLoadingSpinner();
//                        Toasters.ShowToast(context, "Sync Failed " + str);
//
//                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
//
//                    } catch (Exception err) {
//                        ErrorLogs errorLogs = new ErrorLogs();
//                        String errorMessage = err.toString();
//                        errorLogs.setName("AsyncHttpResponseHandler overtime applications onFailure error "+errorMessage);
//                        if(errorLogs.save(context)){
//                            Debugger.logD("AsyncHttpResponseHandler overtime applications onFailure error " + err.toString());
//                        }
//                    }
//                }
//
//            };
//
//            AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                    Toasty.success(context,"Done with Overtime Applications").show();
//                    AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","SUCCESS","NONE");
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                    AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
//                }
//
//            };
//
//            if (isNotSync)
//            {
//                HttpProvider.postSync(context, "sync-ot-applications/", entity, false,syncHttpResponseHandler);
//            } else
//            {
//                HttpProvider.post(context, "sync-ot-applications/", entity, false,asyncHttpResponseHandler);
//            }

        } catch (Exception err)
        {
            if (notify)
            {
                Toasters.HideLoadingSpinner();
                Toasters.ShowToast(context, err.getMessage());
            }

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync overtime applications error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync overtime applications error " + err.toString());
            }
        }
    }

    private static void uploadOvertimeRequests(final ArrayList<Overtime> overtimeArrayList, final int idx, final boolean isNotSync, final Context context, final boolean notify) throws Exception
    {
        try
        {
            if (idx == overtimeArrayList.size())
            {
                if (isNotSync)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasty.success(context, "Syncing Overtime Applications Success!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasters.HideLoadingSpinner();
                    Toasty.success(context,"Done with Overtime Applications").show();
                }

                AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","SUCCESS","NONE");
            }
            else
            {
                Overtime overtime = overtimeArrayList.get(idx);
                final Overtime overtime2 = overtime;
                overtime.setDateString(DateToString.dbFormat(overtime.getDate()));
                overtime.setDateRequestedString(DateToString.dateTimeFormat(overtime.getRequestedDate()));
                overtime.setEmployeeName("");

                String json = new Gson().toJson(overtime);
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try
                        {
                            overtime2.changeSyncStatus(context);
                            uploadOvertimeRequests(overtimeArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            if (notify)
                            {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, err.getMessage());
                            }

                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("Sync overtime applications error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("Sync overtime applications error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            String str = "";

                            if (responseBody!= null && responseBody.length > 0)
                            {
                                str = new String(responseBody, "UTF-8");
                            }

                            Debugger.logD(str);
                            Toasters.HideLoadingSpinner();
                            Toasters.ShowToast(context, "Sync Failed " + str);

                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");

                        } catch (Exception err) {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler overtime applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler overtime applications onFailure error " + err.toString());
                            }
                        }
                    }

                };

                AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            overtime2.changeSyncStatus(context);
                            uploadOvertimeRequests(overtimeArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            if (notify)
                            {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, err.getMessage());
                            }

                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("Sync overtime applications error "+errorMessage);
                            if (errorLogs.save(context)){
                                Debugger.logD("Sync overtime applications error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
                    }

                };

                if (isNotSync)
                {
                    HttpProvider.postSync(context, "overtime-application/save/", entity, false,syncHttpResponseHandler);
                } else
                {
                    HttpProvider.post(context, "overtime-application/save/", entity, false,asyncHttpResponseHandler);
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading overtime requests: " + err.toString());
        }
    }

    public static void loadUnderTimeRequests(final Context context, final boolean notify, boolean isNotSync)
    {
        try
        {
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            String searchQuery = "WHERE isSync = 0";
            final ArrayList<Undertime> undertimeArrayList = Undertime.load(context, searchQuery);

            if (undertimeArrayList.size() == 0) throw new Exception("No Undertime Applications To Sync");

            uploadUnderTimeRequests(undertimeArrayList, 0, isNotSync, context, notify);

        } catch (Exception err) {
            if (notify)
            {
                Toasters.HideLoadingSpinner();
                Toasters.ShowToast(context, err.getMessage());
            }

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync undertime applications error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync undertime applications error " + err.toString());
            }

        }
    }

    private static void uploadUnderTimeRequests(final ArrayList<Undertime> undertimeArrayList, final int idx, final boolean isNotSync, final Context context, final boolean notify) throws Exception
    {
        try
        {
            if (idx == undertimeArrayList.size())
            {
                if (isNotSync)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasty.success(context, "Syncing Undertime Applications Success!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasters.HideLoadingSpinner();
                    Toasty.success(context,"Done with Undertime Applications").show();
                }

                AuditLog.saveAuditLog(context,"MANUAL SYNCING - UNDERTIME","SYNC","SUCCESS","NONE");
            }
            else
            {
                Undertime undertime = undertimeArrayList.get(idx);
                final Undertime undertime2 = undertime;
                undertime.setDateString(DateToString.dbFormat(undertime.getDate()));
                undertime.setDateRequestedString(DateToString.dateTimeFormat(undertime.getRequestedDate()));
                undertime.setEmployeeName("");

                String json = new Gson().toJson(undertime);
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            undertime2.changeSyncStatus(context);
                            uploadUnderTimeRequests(undertimeArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler undertime applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler undertime applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            String str = "";

                            if (responseBody!= null && responseBody.length > 0)
                            {
                                str = new String(responseBody, "UTF-8");
                            }

                            Toasters.HideLoadingSpinner();
                            Toasters.ShowToast(context, "Sync Failed " + str);
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - UNDERTIME","SYNC","FAILED","NONE");

                        } catch (Exception err) {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler undertime applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler undertime applications onFailure error " + err.toString());
                            }
                        }
                    }

                };

                AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            undertime2.changeSyncStatus(context);
                            uploadUnderTimeRequests(undertimeArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler undertime applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler undertime applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - UNDERTIME","SYNC","FAILED","NONE");
                    }

                };

                if (isNotSync) {
                    HttpProvider.postSync(context, "undertime-application/save/", entity,false,syncHttpResponseHandler);
                } else {
                    HttpProvider.post(context, "undertime-application/save/", entity,false,asyncHttpResponseHandler);
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading under time requests: " + err.toString());
        }
    }

    public static void loadWorkOnRestDayRequests(final Context context, final boolean notify, boolean isNotSync)
    {
        try
        {
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            String searchQuery = "WHERE isSync = 0";
            final ArrayList<WorkOnRestday> workOnRestdayArrayList = WorkOnRestday.load(context, searchQuery);

            if (workOnRestdayArrayList.size() == 0) throw new Exception("No Work on Restday Applications To Sync");

            uploadWorkOnRestDayRequests(workOnRestdayArrayList, 0, isNotSync, context, notify);

        } catch (Exception err) {

            if (notify)
            {
                Toasters.HideLoadingSpinner();
                Toasters.ShowToast(context, err.getMessage());
            }

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync work on restdays error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync work on restdays error " + err.toString());
            }
        }
    }

    private static void uploadWorkOnRestDayRequests(final ArrayList<WorkOnRestday> workOnRestDayArrayList, final int idx, final boolean isNotSync, final Context context, final boolean notify) throws Exception
    {
        try
        {
            if (idx == workOnRestDayArrayList.size())
            {
                if (isNotSync)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasty.success(context, "Syncing Work on Restday Applications Success!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasters.HideLoadingSpinner();
                    Toasty.success(context,"Done with WorkOnRestdays Application").show();
                }

                AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON REST DAY","SYNC","SUCCESS","NONE");
            }
            else
            {
                final WorkOnRestday workOnRestday = workOnRestDayArrayList.get(idx);
                JSONObject workOnRestDayJsonObject = new JSONObject();

                workOnRestDayJsonObject.put("employee", workOnRestday.getEmployeeId());
                workOnRestDayJsonObject.put("date", DateToString.dbFormat(workOnRestday.getDate()));
                workOnRestDayJsonObject.put("hours", workOnRestday.getHours());
                workOnRestDayJsonObject.put("reason", workOnRestday.getReason());
                workOnRestDayJsonObject.put("requested_date", DateToString.dateTimeFormat(workOnRestday.getRequestedDate()));
                workOnRestDayJsonObject.put("status", workOnRestday.getStatus());

//                JSONArray workOnRestdayJsonArray = new JSONArray();
//
//                for (WorkOnRestday workOnRestday : workOnRestDayArrayList)
//                {
//                    JSONObject workOnRestdayJson = new JSONObject();
//
//                    workOnRestdayJson.put("employee", workOnRestday.getEmployeeId());
//                    workOnRestdayJson.put("date", DateToString.dbFormat(workOnRestday.getDate()));
//                    workOnRestdayJson.put("hours", workOnRestday.getHours());
//                    workOnRestdayJson.put("reason", workOnRestday.getReason());
//                    workOnRestdayJson.put("requested_date", DateToString.dateTimeFormat(workOnRestday.getRequestedDate()));
//                    workOnRestdayJson.put("status", workOnRestday.getStatus());
//
//                    workOnRestdayJsonArray.put(workOnRestdayJson);
//                }

                String json = workOnRestDayJsonObject.toString();
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            workOnRestday.changeSyncStatus(context);
                            uploadWorkOnRestDayRequests(workOnRestDayArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on restday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on restday applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            String str = "";

                            if (responseBody!= null && responseBody.length > 0)
                            {
                                str = new String(responseBody, "UTF-8");
                            }

                            if (notify) {
                                Toasters.HideLoadingSpinner();
                                Toasters.ShowToast(context, "Sync Failed " + str);
                            }

                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON REST DAY","SYNC","FAILED","NONE");
                        } catch (Exception err) {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on restday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on restday applications onFailure error " + err.toString());
                            }
                        }
                    }

                };

                AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            workOnRestday.changeSyncStatus(context);
                            uploadWorkOnRestDayRequests(workOnRestDayArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on restday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on restday applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON REST DAY","SYNC","FAILED","NONE");
                    }

                };

                if (isNotSync)
                {
                    HttpProvider.postSync(context, "workon-restday-application/save/", entity, false,syncHttpResponseHandler);
                } else {
                    HttpProvider.post(context, "workon-restday-application/save/", entity, false,asyncHttpResponseHandler);
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading work on rest day requests: " + err.toString());
        }
    }

    public static void loadWorkOnHolidayRequests(final Context context, final boolean notify, boolean isNotSync)
    {
        try
        {
            if (notify)
            {
                Toasters.ShowLoadingSpinner(context);
            }

            String searchQuery = "WHERE isSync = 0";
            final ArrayList<WorkOnHoliday> workOnHolidayArrayList = WorkOnHoliday.load(context, searchQuery);

            if (workOnHolidayArrayList.size() == 0) throw new Exception("No Work on Holiday Applications To Sync");


            uploadWorkOnHolidayRequests(workOnHolidayArrayList, 0, isNotSync, context, notify);

        } catch (Exception err)
        {
            if (notify) {
                Toasters.HideLoadingSpinner();
                Toasters.ShowToast(context, err.getMessage());
            }

            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Sync work on holidays error "+errorMessage);
            if(errorLogs.save(context)){
                Debugger.logD("Sync work on holidays error " + err.toString());
            }
        }
    }

    private static void uploadWorkOnHolidayRequests(final ArrayList<WorkOnHoliday> workOnHolidayArrayList, final int idx, final boolean isNotSync, final Context context, final boolean notify) throws Exception
    {
        try
        {
            if (idx == workOnHolidayArrayList.size())
            {
                if (isNotSync)
                {
                    if (notify)
                    {
                        Toasters.HideLoadingSpinner();
                        Toasty.success(context, "Syncing Work on Holiday Applications Success!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasters.HideLoadingSpinner();
                    Toasty.success(context,"Done with WorkOnHolidays").show();
                }

                AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON HOLIDAY","SYNC","SUCCESS","NONE");
            }
            else
            {
                final WorkOnHoliday workOnHoliday = workOnHolidayArrayList.get(idx);
                JSONObject workOnHolidayJsonObject = new JSONObject();

                workOnHolidayJsonObject.put("employee", workOnHoliday.getEmployeeId());
                workOnHolidayJsonObject.put("date", DateToString.dbFormat(workOnHoliday.getDate()));
                workOnHolidayJsonObject.put("hours", workOnHoliday.getHours());
                workOnHolidayJsonObject.put("reason", workOnHoliday.getReason());
                workOnHolidayJsonObject.put("requested_date", DateToString.dateTimeFormat(workOnHoliday.getRequestedDate()));
                workOnHolidayJsonObject.put("status", workOnHoliday.getStatus());

//                JSONArray workOnHolidayJsonArray = new JSONArray();
//
//                for (WorkOnHoliday workOnHoliday : workOnHolidayArrayList)
//                {
//                    JSONObject workOnHolidayJson = new JSONObject();
//
//                    workOnHolidayJson.put("employee", workOnHoliday.getEmployeeId());
//                    workOnHolidayJson.put("date", DateToString.dbFormat(workOnHoliday.getDate()));
//                    workOnHolidayJson.put("hours", workOnHoliday.getHours());
//                    workOnHolidayJson.put("reason", workOnHoliday.getReason());
//                    workOnHolidayJson.put("requested_date", DateToString.dateTimeFormat(workOnHoliday.getRequestedDate()));
//                    workOnHolidayJson.put("status", workOnHoliday.getStatus());
//
//                    workOnHolidayJsonArray.put(workOnHolidayJson);
//                }

                String json = workOnHolidayJsonObject.toString();
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try
                        {
                            workOnHoliday.changeSyncStatus(context);
                            uploadWorkOnHolidayRequests(workOnHolidayArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on holiday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on holiday applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            String str = "";

                            if (responseBody!= null && responseBody.length > 0)
                            {
                                str = new String(responseBody, "UTF-8");
                            }

                            Toasters.HideLoadingSpinner();
                            Toasters.ShowToast(context, "Sync Failed " + str);

                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON HOLIDAY","SYNC","FAILED","NONE");

                        } catch (Exception err) {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on holiday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on holiday applications onFailure error " + err.toString());
                            }
                        }
                    }
                };

                AsyncHttpResponseHandler syncHttpResponseHandler = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try
                        {
                            workOnHoliday.changeSyncStatus(context);
                            uploadWorkOnHolidayRequests(workOnHolidayArrayList, idx + 1, isNotSync, context, notify);
                        }
                        catch (Exception err)
                        {
                            ErrorLogs errorLogs = new ErrorLogs();
                            String errorMessage = err.toString();
                            errorLogs.setName("AsyncHttpResponseHandler work on holiday applications onFailure error "+errorMessage);
                            if(errorLogs.save(context)){
                                Debugger.logD("AsyncHttpResponseHandler work on holiday applications onFailure error " + err.toString());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON HOLIDAY","SYNC","FAILED","NONE");
                    }

                };

                if (isNotSync) {
                    HttpProvider.postSync(context, "workon-holiday-application/save/", entity, false, syncHttpResponseHandler);
                } else {
                    HttpProvider.post(context, "workon-holiday-application/save/", entity, false, asyncHttpResponseHandler);
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading work on holiday requests: " + err.toString());
        }
    }
}
