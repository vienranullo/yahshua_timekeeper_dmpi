package com.yahshua.yahshuatimekeeperdmpi.DBClasses;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import com.yahshua.yahshuatimekeeperdmpi.services.UpdateDeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import java.util.ArrayList;
import java.util.Iterator;

import es.dmoral.toasty.Toasty;


public class DatabaseHelper extends SQLiteOpenHelper
{
    private Context context;
    public static final String DATABASE_NAME = "db_session";
    private static final int DATABASE_VERSION = 1; // See onUpgrade function

    private String employee, employeeLogs,
                    createLeaveTableStmt, createEmployeeLeaveBalanceTableStmt, createLeaveTypeTableStmt,
                    createOvertimeTableStmt, createUndertimeTableStmt,
                    createWorkOnRestdayTableStmt, createWorkOnHolidayTableStmt,
                    createWorkdaysTableStmt, createErrorLogsTable, createAuditLogsTable, createEmployeeDeduction,
                    createDeductionType;

    private static String generateCreateStatement(String tableName, ArrayList<String> fields)
    {
        String query = "CREATE TABLE IF NOT EXISTS " + tableName;

        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append(query);
        strBuilder.append(" (");
        strBuilder.append("id INTEGER PRIMARY KEY AUTOINCREMENT,");

        for (Iterator<String> i = fields.iterator(); i.hasNext();)
        {
            String item = i.next();
            strBuilder.append(item);
            strBuilder.append(i.hasNext() ? "," : ")");
        }

        return strBuilder.toString();
    }

    private void createTables2(Context _context)
    {
        context = _context;

        try
        {
            employee                        = generateCreateStatement("employee", DatabaseFields.employeeFields);
            employeeLogs                    = generateCreateStatement("employee_logs", DatabaseFields.employeeLogFields);
            createUndertimeTableStmt        = generateCreateStatement("undertime", DatabaseFields.undertimeFields);
            createWorkOnRestdayTableStmt    = generateCreateStatement("workOnRestday", DatabaseFields.workOnRestdayFields);
            createWorkOnHolidayTableStmt    = generateCreateStatement("workOnHoliday", DatabaseFields.workOnHolidayFields);
            createWorkdaysTableStmt         = generateCreateStatement("workDays", DatabaseFields.scheduleWorkdaysFields);

        } catch (Exception err)
        {
            Toasty.warning(_context, "Database table generation failed").show();
        }
    }

    private void createTables()
    {
        createLeaveTableStmt = "CREATE TABLE IF NOT EXISTS leave (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "employeeId INT, employeeName TEXT, day TEXT, dateFrom DATE, " +
                                "dateTo DATE, halfDayDate DATE, timeFrom TEXT, timeTo TEXT, " +
                                "daysLeave DOUBLE, leaveTypeId INT, leaveTypeName TEXT, reason TEXT, " +
                                "requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), isSync BOOLEAN, company_id INT, " +
                                "status TEXT, lastSyncedDate DATE)";

        createEmployeeLeaveBalanceTableStmt = "CREATE TABLE IF NOT EXISTS employeeLeaveBalance (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                        "employeeId INT, leaveTypeId INT, company_id INT, leaveTypeName TEXT, total DOUBLE)";

        createLeaveTypeTableStmt = "CREATE TABLE IF NOT EXISTS leaveType (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                    "name TEXT, company_id INT, isDeductible BOOLEAN)";

        createOvertimeTableStmt = "CREATE TABLE IF NOT EXISTS overtime (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                    "employeeId INT, employeeName TEXT, date DATE, hours DOUBLE, " +
                                    "requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), " +
                                    "reason TEXT, isSync BOOLEAN, company_id INT, status TEXT, remarks TEXT, lastSyncedDate DATE)";

        createErrorLogsTable = "CREATE TABLE IF NOT EXISTS errorLogs (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "errorlogsId INT, error_logs TEXT, date DATE) ";

        createAuditLogsTable = "CREATE TABLE IF NOT EXISTS auditLog (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "module TEXT, dateTime DATETIME DEFAULT (datetime('now', 'localtime')), " +
                                "action TEXT, employeeId INT, employeeName TEXT, status TEXT, errorMessage TEXT, " +
                                "previousValue TEXT, newValue TEXT)";

        createEmployeeDeduction = "CREATE TABLE IF NOT EXISTS employeeDeductions (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "deduction_type_id INT, amount NUMERIC, date_requested DATE, remarks TEXT, status TEXT, term TEXT," +
                                "amount_per_deduction NUMERIC, date_start DATE, date_end DATE, regular_id INT, employee_id INT, is_synced INT DEFAULT 0, company_id INT, lastSyncedDate DATE)";

        createDeductionType = "CREATE TABLE IF NOT EXISTS deductionTypes (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                "deduction_type_id INT, name TEXT, company_id INT)";

    }

    public DatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory)
    {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
        createTables2(context);
        createTables();
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        try
        {
                db.execSQL(employee);
                db.execSQL(employeeLogs);
                db.execSQL(createLeaveTableStmt);
                db.execSQL(createEmployeeLeaveBalanceTableStmt);
                db.execSQL(createLeaveTypeTableStmt);
                db.execSQL(createOvertimeTableStmt);
                db.execSQL(createUndertimeTableStmt);
                db.execSQL(createWorkOnRestdayTableStmt);
                db.execSQL(createWorkOnHolidayTableStmt);
                db.execSQL(createWorkdaysTableStmt);
                db.execSQL(createErrorLogsTable);
                db.execSQL(createAuditLogsTable);
                db.execSQL(createEmployeeDeduction);
                db.execSQL(createDeductionType);

        } catch (SQLException err) {
            Debugger.logD(err.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion)
    {
        Debugger.logD("_oldVersion: "+_oldVersion +" _newVersion "+_newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    private boolean columnExist(SQLiteDatabase db, String tableName, String columnName)
    {
        Cursor cursor = db.rawQuery("PRAGMA table_info("+ tableName +")", null);

        if (cursor != null)
        {
            while (cursor.moveToNext())
            {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                if (columnName.equalsIgnoreCase(name)) return true;
            }

            cursor.close();
        }

        return false;
    }
}