package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import com.yahshua.yahshuatimekeeperdmpi.Interfaces.DialogAlertInterfaces;


/**
 * Created by AlfredVanz on 8/2/2017.
 */

public class PopUpProvider {

    public static void getItemPopUp(Context context, View view){

        PopupMenu popup = new android.support.v7.widget.PopupMenu(context, view);
//        popup.getMenuInflater().inflate(R.menu.popup_session_exercises, popup.getMenu());

//        popup.setOnMenuItemClickListener(new android.support.v7.widget.PopupMenu.OnMenuItemClickListener() {
//            public boolean onMenuItemClick(MenuItem item) {
//
//                switch(item.getItemId()){
//
//                    case R.id.action_edit_item : break;
//                    case R.id.action_remove_item : removeSessionExercise(); break;
//
//                }
//
//                return true;
//            }
//        });

        popup.show();

    }

    public static void buildConfirmationDialog(Context context, DialogInterface.OnClickListener dialogInterface, String message, String yesMessage, String noMessage)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(yesMessage, dialogInterface)
                .setNegativeButton(noMessage, dialogInterface).show();
    }

    public static void getConfirmDialog(Context mContext,String title, String msg, String positiveBtnCaption, String negativeBtnCaption, boolean isCancelable, final DialogAlertInterfaces target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        int imageResource = android.R.drawable.ic_dialog_alert;
//        Drawable image = mContext.getResources().getDrawable(imageResource);

        builder.setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(positiveBtnCaption, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                target.PositiveMethod(dialog, id);
            }
        }).setNegativeButton(negativeBtnCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                target.NegativeMethod(dialog, id);
            }
        });

        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
        if (isCancelable) {
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface arg0) {
                    target.NegativeMethod(null, 0);
                }
            });
        }
    }

}
