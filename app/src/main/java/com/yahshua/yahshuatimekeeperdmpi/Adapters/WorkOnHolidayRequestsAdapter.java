package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnHolidayApprovalRequestActivity;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;


public class WorkOnHolidayRequestsAdapter extends RecyclerView.Adapter<WorkOnHolidayRequestsAdapter.ViewHolder>
{
    private ArrayList<WorkOnHoliday> workOnHolidayArrayList;
    private Context context;


    public WorkOnHolidayRequestsAdapter(Context context, ArrayList<WorkOnHoliday> workOnHolidayArrayList)
    {
        this.context = context;
        this.workOnHolidayArrayList = workOnHolidayArrayList;
    }

    @NonNull
    @Override
    public WorkOnHolidayRequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.work_on_holiday_request_item, parent, false);
        return new ViewHolder(contactView);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEmployeeName, tvDate, tvHours, tvReason;
        CardView cvWorkOnHolidayRequest;

        ViewHolder(View itemView)
        {
            super(itemView);

            tvEmployeeName = itemView.findViewById(R.id.tvEmployeeName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvHours = itemView.findViewById(R.id.tvHours);
            tvReason = itemView.findViewById(R.id.tvReason);
            cvWorkOnHolidayRequest = itemView.findViewById(R.id.cvWorkOnHolidayRequest);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        WorkOnHoliday workOnHoliday = workOnHolidayArrayList.get(position);

        holder.tvEmployeeName.setText(workOnHoliday.getEmployeeName());
        holder.tvDate.setText(DateToString.displayFormat(workOnHoliday.getDate()));
        holder.tvHours.setText(workOnHoliday.getHours() + " hrs.");
        holder.tvReason.setText(workOnHoliday.getReason());

        holder.cvWorkOnHolidayRequest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent coursesActivityIntent = new Intent(context, WorkOnHolidayApprovalRequestActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("WORK_ON_HOLIDAY", workOnHolidayArrayList.get(position));
                coursesActivityIntent.putExtras(bundle);

                context.startActivity(coursesActivityIntent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return workOnHolidayArrayList.size();
    }
}
