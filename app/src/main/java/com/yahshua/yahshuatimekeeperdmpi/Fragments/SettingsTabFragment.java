package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.yahshua.yahshuatimekeeperdmpi.Activities.ErrorLogsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.DialogAlertInterfaces;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.services.UpdateDeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Utils.PopUpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ToastNotification;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import es.dmoral.toasty.Toasty;

public class SettingsTabFragment extends Fragment
{
    // Context and Views
    private View view;
    private Context context;

    // Widgets
    private Spinner spLogInterval;
    private LinearLayout llEmailDbFile, llUpdateDeviceSettings, llShowErrorLogs, llClearSyncedLogs, llLogInterval;


    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view = inflater.inflate(R.layout.fragment_tab_settings, container, false);
        context = getContext();

        initializeUI();


        return view;
    }

    // Initialize component from a layout file
    private void initializeUI()
    {
        spLogInterval             = view.findViewById(R.id.spLogInterval);
        llEmailDbFile             = view.findViewById(R.id.llEmailDbFile);
        llUpdateDeviceSettings    = view.findViewById(R.id.llUpdateDeviceSettings);
        llShowErrorLogs           = view.findViewById(R.id.llShowErrorLogs);
        llClearSyncedLogs         = view.findViewById(R.id.llClearSyncedLogs);
        llLogInterval             = view.findViewById(R.id.llLogInterval);


        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(), R.array.pref_log_interval_values, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLogInterval.setAdapter(adapter2);

        spLogInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());  //context
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString("log_interval",spLogInterval.getSelectedItem().toString());
                prefEditor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });

        String log_interval=PreferenceManager
                .getDefaultSharedPreferences(getActivity().getBaseContext())
                .getString("log_interval","");

        for (int i = 0; i <= 2; i++)
            if(log_interval.equals(spLogInterval.getItemAtPosition(i).toString()))
            {
                spLogInterval.setSelection(i);
                break;
            }

        llLogInterval.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                spLogInterval.performClick();
            }
        });

        llClearSyncedLogs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                PopUpProvider.getConfirmDialog(context,"Are you sure?", "this will clear all Synced Logs", "Yes", "No", false,
                        new DialogAlertInterfaces()
                        {

                            @Override
                            public void PositiveMethod(final DialogInterface dialog, final int id)
                            {

                                EmployeeLogs.clearSyncedLogs(context);
                                Toasters.ShowSnackBar(getView(), "Synced Logs Cleared");
                                AuditLog.saveAuditLog(context,"SETTINGS","CLEAR LOGS","SUCCESS","NONE");

                            }

                            @Override
                            public void NegativeMethod(DialogInterface dialog, int id)
                            {
                            }

                        });
            }
        });

        llShowErrorLogs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v )
            {
                Intent intent = new Intent(context,ErrorLogsActivity.class);
                startActivity(intent);
            }
        });

        llUpdateDeviceSettings.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v )
            {
                // Show progress dialog
                final ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Please Wait..");
                progressDialog.setMessage("Updating device settings..");
                progressDialog.setCancelable(false);
                progressDialog.show();

                final Handler handler = new Handler(Looper.getMainLooper())
                {
                    @Override
                    public void handleMessage(Message message)
                    {
                        progressDialog.dismiss();

                        switch (message.what)
                        {
                            // Success
                            case 1: ToastNotification.success(context, "Device Settings Updated");
                                    break;
                            // Error
                            case 2: Utility.showError(getFragmentManager(), message.obj.toString());
                                    break;
                        }
                    }
                };

                ThreadGroup threadGroup = new ThreadGroup("downloadDeviceSettings");
                Runnable runnable = new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            UpdateDeviceSettings.downloadDeviceSettings(context, handler);
                        }
                        catch (Exception err)
                        {
                            Utility.sendHandlerMessage(handler, 2, "Error Runnable: \n" + err.toString());
                        }
                    }
                };

                new Thread(threadGroup, runnable, "downloadDeviceSettings", 3000000).start();
            }
        });

        llEmailDbFile.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (Utility.haveNetworkConnection(context))
                {
                    confirmUploadDb();
                }
                else
                {
                    ToastNotification.warning(context, context.getString(R.string.internet_required));
                }
            }
        });
    }

    private void confirmUploadDb()
    {
        PopUpProvider.getConfirmDialog(context, "Email Database", "Are you sure you want to email database ?", "Yes", "No",false,
                new DialogAlertInterfaces()
                {

                    @Override
                    public void PositiveMethod(final DialogInterface dialog, final int id)
                    {
                        new UploadDb().execute();
                    }

                    @Override
                    public void NegativeMethod(DialogInterface dialog, int id)
                    {
                    }
                });
    }

    private class UploadDb extends AsyncTask<String, Integer, String>
    {
        private String success = "";

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(getContext());

                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, String successMessage)
                    {

                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        success = errorMessage;
                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.SendToEmailDatabase();
            }
            catch (Exception err)
            {
                return success += "FAILED";
            }

            return success;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Sending database...", false);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            try
            {
                if(success.equals(""))
                {
                    Toasty.success(context, context.getString(R.string.db_sent)).show();
                }
                else
                {
                    Toasty.warning(context, success).show();
                }

                Toasters.HideLoadingSpinner();

            }catch (Exception err)
            {
                Debugger.logD("onPostExecute "+err.getMessage());
                Toasty.warning(context, context.getResources().getString(R.string.server_error)).show();
            }
        }
    }


    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}
