package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.MainActivity;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.Models.ErrorLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONObject;

import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class LogOutAuthenticationDialog extends DialogFragment
{
    // Context and Views
    private View view;
    private Context context;

    //Widgets
    private Button btnConfirm;
    private EditText etPassword;
    private ImageView imgClose;
    private ProgressBar progressBar;
    private TextInputLayout tiPassword;
    private TextView tvDescription, tvTitle;

    //Data
    private boolean isLogOut;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.dialog_authen_logout,container,false);
        context = getContext();

        if(getArguments() != null)
        {
            isLogOut = getArguments().getBoolean("IS_LOGOUT");
        }

        initializeUI();
        return view;
    }

    private void initializeUI()
    {
        imgClose    = view.findViewById(R.id.img_close);
        etPassword  = view.findViewById(R.id.et_password);
        btnConfirm  = view.findViewById(R.id.btn_confirm);
        progressBar = view.findViewById(R.id.pb_logout);
        tiPassword  = view.findViewById(R.id.tiPassword);
        tvDescription  = view.findViewById(R.id.tvDescription);
        tvTitle     = view.findViewById(R.id.tvTitle);

        progressBar.setVisibility(View.GONE);
        btnConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (validate())
                {
                    requestLogout(etPassword.getText().toString());
                    hideKeyboard();
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        etPassword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                tiPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        if (isLogOut)
        {
            tvTitle.setText(context.getString(R.string.authentication_logout));
        }
        else
        {
            tvTitle.setText(context.getString(R.string.update_app_auth));
            tvDescription.setVisibility(View.GONE);
        }
    }

    private boolean validate()
    {
        if(etPassword.getText().toString().equals(""))
        {
            tiPassword.requestFocus();
            tiPassword.setError(getString(R.string.error_field_required));

            return false;
        }
        else if(!Utility.haveNetworkConnection(context))
        {
            tiPassword.requestFocus();
            tiPassword.setError(getString(R.string.internet_required));
            return false;
        }

        return true;
    }

    public void hideKeyboard()
    {
        if (getDialog().getCurrentFocus() != null)
        {
            InputMethodManager inputManager = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getDialog().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void requestLogout(String password)
    {
        try
        {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("username", UserSession.getCompanyEmail(context));
            jsonParams.put("password", password);
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context,"api-auth/",entity,"application/json", new AsyncHttpResponseHandler()
            {
                @Override
                public void onStart()
                {
                    super.onStart();
                    progressBar.setVisibility(View.VISIBLE);
                    btnConfirm.setVisibility(View.GONE);
                }

                @Override
                public void onFinish()
                {
                    super.onFinish();
                    progressBar.setVisibility(View.GONE);
                    btnConfirm.setVisibility(View.VISIBLE);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    DbAdapter dbAdapter = new DbAdapter(context);
                    dbAdapter.open();
                    try
                    {
                        if(isLogOut)
                        {
                            UserSession.clearSession(context);
                            Employee.delete(dbAdapter);
                            EmployeeLogs.delete(dbAdapter);
                            ((MainActivity) context).checkUserSession();
                            Toasty.success(context, "Logout Successfully").show();
                        }
                        else
                        {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                            startActivity(intent);
                            dismiss();
                        }


                    }catch (Exception err)
                    {
                        Debugger.logD("onSuccess err "+err.getMessage());
                        ErrorLogs.save("requestLogout onSuccess err "+err.getMessage(),dbAdapter);
                    }

                    dbAdapter.close();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        String response = new String(responseBody);
                        if(response.contains("Invalid"))
                        {
                            displayError(getString(R.string.error_incorrect_password));
                        }
                        else
                        {
                            displayError(response);
                        }

                    }catch (Exception err)
                    {
                        displayError(err.getMessage());
                    }
                }
            });

        }catch (Exception err)
        {
            Debugger.logD("Exception requestLogout "+err.getMessage());
        }

    }

    private void displayError(String err)
    {
        tiPassword.setError(err);
        tiPassword.requestFocus();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }
}
