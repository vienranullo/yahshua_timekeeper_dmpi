package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.LeaveAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.LeaveAddEditDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLeaveBalance;
import com.yahshua.yahshuatimekeeperdmpi.Models.Leave;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveType;
import com.yahshua.yahshuatimekeeperdmpi.Models.Workdays;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;


public class LeaveApplicationsActivity extends BaseActivity
{
    // Context
    private Context context = this;

    // Widgets
    private ListView lvLeave;
    private MenuItem menuSync;

    // Adapter
    private LeaveAdapter leaveAdapter;

    // ArrayList and Data
    private ArrayList<EmployeeLeaveBalance> employeeLeaveBalanceArrayList = new ArrayList<>();
    private ArrayList<LeaveType> leaveTypeArrayList = new ArrayList<>();
    private ArrayList<Leave> leaveArrayList = new ArrayList<>();
    private Workdays employeeWorkdays = new Workdays();
    private Leave selectedLeave = new Leave();
    private String employeeName;
    private Employee employee;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_applications);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        employee = getIntent().getParcelableExtra("EMPLOYEE");

        String middleInitial = (employee.getMiddlename() != null &&  employee.getMiddlename().length() > 0) ? employee.getMiddlename().charAt(0) + ". " : "";
        employeeName = employee.getFirstname() + " " + middleInitial + employee.getLastname();

    }

    private void initializeData()
    {
        selectedLeave = new Leave();

        // Load Leave applications
        String searchQuery = "WHERE employeeId=" + employee.getId();
        leaveArrayList = Leave.load(this, searchQuery + " ORDER BY id DESC ");

        // Employee Work Days
        employeeWorkdays = Workdays.get(context, searchQuery);

        // Load Leave Balances
        searchQuery += " ORDER BY leaveTypeId";
        employeeLeaveBalanceArrayList = EmployeeLeaveBalance.read(getBaseContext(), searchQuery);

        // Load Leave Types
        leaveTypeArrayList = LeaveType.read(getBaseContext(), null);
        showSyncIcon();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync, menu);

        menuSync = menu.findItem(R.id.action_sync);
        showSyncIcon();

        initializeData();
        initializeUI();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_sync:
                uploadLeave();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSyncIcon()
    {
        try
        {
            menuSync.setVisible(Leave.load(context, "WHERE isSync = 0").size() > 0);
        }catch (Exception err)
        {
            Debugger.logD("Exception showSyncIcon err "+err);
        }
    }

    // Initialize component from a layout file
    private void initializeUI()
    {
        // Set Title on Action Bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle("Leave Applications");
        actionBar.setSubtitle(employeeName);

        // List View
        lvLeave = findViewById(R.id.lvLeave);

        leaveAdapter = new LeaveAdapter(this, leaveArrayList);

        lvLeave.setAdapter(leaveAdapter);

        lvLeave.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
            {
                selectedLeave = leaveAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Options");

                final String[] finalOptions;
                String[] optionUnSynced;
                String[] optionSynced;

                optionUnSynced = getResources().getStringArray(R.array.lv_option3);
                optionSynced = Arrays.copyOf(optionUnSynced, optionUnSynced.length - 1);

                finalOptions = selectedLeave.isSync() ? optionSynced : optionUnSynced;

                builder.setItems(finalOptions, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        switch (finalOptions[i])
                        {
                            case "View/Edit":
                                 showLeaveAddEditDialog();
                                break;
                            case "Upload":
                                uploadLeave();
                                break;
                            case "Delete":
                                confirmDelete();
                                break;
                        }
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        selectedLeave = new Leave();
                    }
                });

                builder.show();
            }
        });

        // Floating Action Button
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLeaveAddEditDialog();
            }
        });
    }


    // Leave Add/Edit Dialog
    private void showLeaveAddEditDialog()
    {
        try
        {
            LeaveAddEditDialog leaveAddEditDialog = new LeaveAddEditDialog();
            Bundle dialogArgs = new Bundle();

            dialogArgs.putInt("EMPLOYEE_ID", employee.getId());
            dialogArgs.putString("EMPLOYEE_NAME", employeeName);
            dialogArgs.putParcelableArrayList("EMPLOYEE_LEAVE_BALANCES", employeeLeaveBalanceArrayList);
            dialogArgs.putParcelableArrayList("LEAVE_TYPES", leaveTypeArrayList);
            dialogArgs.putParcelable("WORKDAYS", employeeWorkdays);

            if (selectedLeave.getId() > 0) dialogArgs.putParcelable("SELECTED_LEAVE", selectedLeave);

            leaveAddEditDialog.setArguments(dialogArgs);

            leaveAddEditDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    initializeData();
                    updateLeaveAdapter();
                }
            });

            leaveAddEditDialog.show(getSupportFragmentManager(), "Create/Edit Leave");

        } catch (Exception err) {
            Toasty.error(context, err.getMessage()).show();
        }
    }

    private void uploadLeave()
    {
        if (Utility.haveNetworkConnection(context))
        {
            if(Leave.load(context, "WHERE isSync = 0").size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                new LeaveApplicationsActivity.UploadLeaveApplication(context).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }

    private void updateLeaveAdapter()
    {
        leaveAdapter.clear();
        leaveAdapter.addAll(leaveArrayList);
        leaveAdapter.notifyDataSetChanged();
    }

    // Delete Leave
    private void confirmDelete()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                deleteRecord();
            }
        };

        String message = "Delete Leave Application on\n";

        if (selectedLeave.getDay().equals("whole")) {
            message += DateToString.displayFormat2(selectedLeave.getDateFrom()) + " to " + DateToString.displayFormat(selectedLeave.getDateTo()) + "?";
        } else if (selectedLeave.getDay().equals("half")) {
            message += DateToString.displayFormat(selectedLeave.getHalfDayDate()) + "?";
        }

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Confirm Delete");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, dialogClickListener);
        builder.setNegativeButton(android.R.string.no, null);

        builder.show();
    }

    private void deleteRecord()
    {
        selectedLeave.delete(context);
        AuditLog.saveAuditLog(context,"LEAVE","DELETE","SUCCESS",""+employeeName);
        initializeData();
        updateLeaveAdapter();
        Toasters.ShowToast(context, "Leave Application Deleted");
    }

    private class UploadLeaveApplication extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadLeaveApplication(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", true);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message += "success";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message +=  errorMessage;
                    }
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.loadLeaveRequests(employee.getId());

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, "Successfully uploaded").show();
            }
            else
            {
                Toasty.warning(context, "Failed to upload ").show();
            }

            Toasters.HideLoadingSpinner();
            initializeData();
            initializeUI();
        }
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End

}