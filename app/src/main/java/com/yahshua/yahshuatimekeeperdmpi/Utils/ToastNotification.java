package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;


public class ToastNotification
{
    public static void success(Context context, String message)
    {
        Toasty.success(context, "" + message, Toast.LENGTH_LONG).show();
    }

    public static void error(Context context, String message)
    {
        Toasty.error(context, "" + message, Toast.LENGTH_LONG).show();
    }

    public static void warning(Context context, String message)
    {
        Toasty.warning(context, "" + message, Toast.LENGTH_LONG).show();
    }

    public static void info(Context context, String message)
    {
        Toasty.info(context, "" + message, Toast.LENGTH_LONG).show();
    }
}
