package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class EmployeeLeaveBalance implements Parcelable
{
    private int id, employeeId, leaveTypeId;
    private String leaveTypeName;
    private double total;


    public EmployeeLeaveBalance(){}


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(int employeeId)
    {
        this.employeeId = employeeId;
    }

    public int getLeaveTypeId()
    {
        return leaveTypeId;
    }

    public void setLeaveTypeId(int leaveTypeId)
    {
        this.leaveTypeId = leaveTypeId;
    }

    public String getLeaveTypeName()
    {
        return leaveTypeName;
    }

    public void setLeaveTypeName(String leaveTypeName)
    {
        this.leaveTypeName = leaveTypeName;
    }

    public double getTotal()
    {
        return total;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }


    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeInt(employeeId);
        dest.writeInt(leaveTypeId);
        dest.writeString(leaveTypeName);
        dest.writeDouble(total);
    }

    protected EmployeeLeaveBalance(Parcel in)
    {
        id = in.readInt();
        employeeId = in.readInt();
        leaveTypeId = in.readInt();
        leaveTypeName = in.readString();
        total = in.readDouble();
    }

    public static final Creator<EmployeeLeaveBalance> CREATOR = new Creator<EmployeeLeaveBalance>()
    {
        @Override
        public EmployeeLeaveBalance createFromParcel(Parcel in)
        {
            return new EmployeeLeaveBalance(in);
        }

        @Override
        public EmployeeLeaveBalance[] newArray(int size)
        {
            return new EmployeeLeaveBalance[size];
        }
    };


    public static ArrayList<EmployeeLeaveBalance> read(Context context, String searchQuery)
    {
        ArrayList<EmployeeLeaveBalance> employeeLeaveBalanceArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            if (searchQuery != null) {
                selectQuery = "SELECT * FROM employeeLeaveBalance " + searchQuery;
            } else {
                selectQuery = "SELECT * FROM employeeLeaveBalance";
            }

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    EmployeeLeaveBalance employeeLeaveBalance = new EmployeeLeaveBalance();

                    employeeLeaveBalance.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    employeeLeaveBalance.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    employeeLeaveBalance.setLeaveTypeId(cursor.getInt(cursor.getColumnIndex("leaveTypeId")));
                    employeeLeaveBalance.setLeaveTypeName(cursor.getString(cursor.getColumnIndex("leaveTypeName")));
                    employeeLeaveBalance.setTotal(cursor.getDouble(cursor.getColumnIndex("total")));

                    employeeLeaveBalanceArrayList.add(employeeLeaveBalance);

                } while (cursor.moveToNext());
            }

            cursor.close();
            DatabaseAdapter.close();

            return employeeLeaveBalanceArrayList;

        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("EmployeeLeaveBalance read error " + errorMessage);
            if (errorLogs.save(context))  Toasty.error(context, "Database Error").show();
        }

        return employeeLeaveBalanceArrayList;
    }


    public boolean save(DbAdapter dbAdapter)
    {
        try
        {
            ContentValues contentValues = new ContentValues();

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("leaveTypeId", getLeaveTypeId());
            contentValues.put("leaveTypeName", getLeaveTypeName());
            contentValues.put("total", getTotal());

            dbAdapter.getDb().insertOrThrow("employeeLeaveBalance", null, contentValues);
            return true;
        }
        catch (Exception err)
        {
            ErrorLogs.save("EmployeeLeaveBalance save error "+err.getMessage(), dbAdapter);
            return false;
        }
    }

    public static void delete(DbAdapter dbAdapter) throws SQLException
    {
        dbAdapter.getDb().delete("employeeLeaveBalance", "", new String[]{});
    }
}