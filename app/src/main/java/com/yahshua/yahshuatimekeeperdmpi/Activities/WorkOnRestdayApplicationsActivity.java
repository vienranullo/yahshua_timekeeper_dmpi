package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.WorkOnRestdayAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.WorkOnRestdayAddEditDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnRestday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;


public class WorkOnRestdayApplicationsActivity extends BaseActivity
{
    // Context
    private Context context = this;

    // Widget
    private ListView             lvWorkOnRestday;
    private MenuItem             menuSync;

    // Adapter
    private WorkOnRestdayAdapter workOnRestdayAdapter;

    // ArrayList and Data
    private ArrayList<WorkOnRestday> workOnRestdayArrayList = new ArrayList<>();
    private WorkOnRestday selectedWorkOnRestday;
    private String employeeName;
    private Employee employee;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workonrestday_applications_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        employee = getIntent().getParcelableExtra("EMPLOYEE");

        String middleInitial = (employee.getMiddlename() != null &&  employee.getMiddlename().length() > 0) ? employee.getMiddlename().charAt(0) + ". " : "";
        employeeName = employee.getFirstname() + " " + middleInitial + employee.getLastname();
    }

    private void initializeData()
    {
        selectedWorkOnRestday = new WorkOnRestday();

        String searchQuery = "WHERE employeeId = " + employee.getId() + " ORDER BY id DESC ";
        workOnRestdayArrayList = WorkOnRestday.load(context, searchQuery);
        showSyncIcon();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_sync, menu);

        menuSync = menu.findItem(R.id.action_sync);

        initializeData();
        initializeUI();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_sync:
                uploadRestDay();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSyncIcon()
    {
        try
        {
            String searchQueryRestDay = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<WorkOnRestday> workOnRestdayArrayList = WorkOnRestday.load(context, searchQueryRestDay);

            menuSync.setVisible(workOnRestdayArrayList.size() > 0);

        }catch (Exception err)
        {
            Debugger.logD("Exception showSyncIcon err "+err);
        }
    }

    private void uploadRestDay()
    {
        if (Utility.haveNetworkConnection(context))
        {
            String searchQueryRestDay = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<WorkOnRestday> workOnRestdayArrayList = WorkOnRestday.load(context, searchQueryRestDay);

            if(workOnRestdayArrayList.size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                new WorkOnRestdayApplicationsActivity.UploadRestDay(context).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }

    // Initialize component from a layout file
    private void initializeUI()
    {
        // Set Title on Action Bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle("Work on Restday Applications");
        actionBar.setSubtitle(employeeName);

        // List View
        lvWorkOnRestday = findViewById(R.id.lvWorkOnRestday);
        workOnRestdayAdapter = new WorkOnRestdayAdapter(this, workOnRestdayArrayList);

        lvWorkOnRestday.setAdapter(workOnRestdayAdapter);

        lvWorkOnRestday.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
            {
                selectedWorkOnRestday = workOnRestdayAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Options");

                final String[] finalOptions;
                String[] optionUnSynced;
                String[] optionSynced;

                optionUnSynced = getResources().getStringArray(R.array.lv_option3);
                optionSynced = Arrays.copyOf(optionUnSynced, optionUnSynced.length - 1);

                finalOptions = selectedWorkOnRestday.isSync() ? optionSynced : optionUnSynced;

                builder.setItems(finalOptions, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        switch (finalOptions[i])
                        {
                            case "View/Edit":
                                showWorkOnRestdayAddEditDialog();
                                break;
                            case "Upload":
                                uploadRestDay();
                                break;
                            case "Delete":
                                confirmDelete();
                                break;
                        }
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        selectedWorkOnRestday = new WorkOnRestday();
                    }
                });

                builder.show();
            }
        });

        // Floating Action Button
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWorkOnRestdayAddEditDialog();
            }
        });
    }

    // Work on rest day add edit dialog
    private void showWorkOnRestdayAddEditDialog()
    {
        WorkOnRestdayAddEditDialog workOnRestdayAddEditDialog = new WorkOnRestdayAddEditDialog();
        Bundle dialogArgs = new Bundle();

        dialogArgs.putInt("EMPLOYEE_ID", employee.getId());
        dialogArgs.putString("EMPLOYEE_NAME", employeeName);

        if (selectedWorkOnRestday.getId() > 0) dialogArgs.putParcelable("SELECTED_WORKONRESTDAY", selectedWorkOnRestday);

        workOnRestdayAddEditDialog.setArguments(dialogArgs);

        workOnRestdayAddEditDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                initializeData();
                updateLeaveAdapter();
            }
        });

        workOnRestdayAddEditDialog.show(getSupportFragmentManager(), "Create/Edit Work on Restday");
    }

    private void updateLeaveAdapter()
    {
        workOnRestdayAdapter.clear();
        workOnRestdayAdapter.addAll(workOnRestdayArrayList);
        workOnRestdayAdapter.notifyDataSetChanged();
    }

    private void confirmDelete()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                deleteRecord();
            }
        };

        String message = "Delete Work on Restday Application on\n" + DateToString.displayFormat(selectedWorkOnRestday.getDate()) + "?";

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Confirm Delete");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, dialogClickListener);
        builder.setNegativeButton(android.R.string.no, null);

        builder.show();
    }

    private void deleteRecord()
    {
        selectedWorkOnRestday.delete(context);
        AuditLog.saveAuditLog(context,"WORK ON REST DAY","DELETE","SUCCESS",employeeName);
        initializeData();
        updateLeaveAdapter();
        Toasters.ShowToast(context, "Work on Restday Application Deleted");
    }

    private class UploadRestDay extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadRestDay(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", true);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message += "success";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message +=  errorMessage;
                    }
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.loadWorkOnRestDayRequests(employee.getId());

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, "Successfully uploaded").show();
            }
            else
            {
                Toasty.warning(context, "Failed to upload ").show();
            }

            Toasters.HideLoadingSpinner();
            initializeData();
            initializeUI();
            Debugger.logD(message+" message");
        }
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End
}
