package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.ErrorLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;

import java.util.ArrayList;

public class ErrorLogsAdapter extends ArrayAdapter<ErrorLogs>{

    private final int mResource;
    private ArrayList<ErrorLogs> errorLogsArrayList;
    private int position;
    private boolean isAutoComplete;


    public ErrorLogsAdapter(Context context, ArrayList<ErrorLogs> errorLogs)
    {
        super(context, 0, errorLogs);
        mResource=0;
    }
    public ErrorLogsAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<ErrorLogs> list)
    {
        super(context, resource, list);
        mResource=resource;
    }

    public ErrorLogsAdapter(@NonNull Context context, ArrayList<ErrorLogs> list, boolean isAutoComplete)
    {
        super(context,0, list);
        errorLogsArrayList = list;
        mResource = 0;
        this.isAutoComplete = isAutoComplete;
    }

    @Override
    public int getPosition(@Nullable ErrorLogs item)
    {
        int x=0;

        for(ErrorLogs errorLogs: errorLogsArrayList){
            if(errorLogs.getId()==item.getId()) return x;
            else x++;
        }

        return super.getPosition(item);
    }
    public void setPosition(int _position)
    {
        position = _position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        return createViewFromResource(LayoutInflater.from(getContext()), position, convertView, parent, mResource);
    }

    private @NonNull View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                                 @Nullable View convertView, @NonNull ViewGroup parent, int resource) {
        final TextView tvErrorMessage;
        final TextView tvDate;

        ErrorLogs errorLogs = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_error_logs, parent, false);
            //  Otherwise, find the TextView field within the layout
        }

            tvErrorMessage = convertView.findViewById(R.id.tv_error_message);
            tvDate = convertView.findViewById(R.id.tv_date);

            tvErrorMessage.setText(errorLogs.getName());
            tvDate.setText(DateTimeHandler.getTimeForError(errorLogs.getDate()));

            return convertView;
    }

}
