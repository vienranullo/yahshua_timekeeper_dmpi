package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.entity.StringEntity;


public class HttpProvider {


    private static AsyncHttpClient client = new AsyncHttpClient();
    private static SyncHttpClient syncHttpClient = new SyncHttpClient();

    public static void get(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        client.addHeader("Authorization", "Token " + UserSession.getToken(context));
        client.setConnectTimeout(50000);
        client.setTimeout(50000);
        client.setResponseTimeout(50000);
        client.get(getAbsoluteUrl(context, url), params, responseHandler);
    }

    public static void post(Context context, String url, StringEntity entity, boolean possibleLongOperation, AsyncHttpResponseHandler responseHandler)
    {
        client.addHeader("Authorization","Token " + UserSession.getToken(context));
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

        if (possibleLongOperation)
        {
            client.setConnectTimeout ( 1000000 );
            client.setTimeout ( 1000000 );
            client.setResponseTimeout ( 1000000 );
        }

        client.post(context,getAbsoluteUrl(context, url), entity , null, responseHandler);
    }

    public static void postSync(Context context, String url, StringEntity entity, boolean possibleLongOperation, AsyncHttpResponseHandler responseHandler)
    {
        syncHttpClient.addHeader("Authorization","Token " + UserSession.getToken(context));
        syncHttpClient.addHeader("Content-Type", "application/json;charset=UTF-8");
        syncHttpClient.addHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

        if (possibleLongOperation)
        {
            syncHttpClient.setConnectTimeout ( 1000000 );
            syncHttpClient.setTimeout ( 1000000 );
            syncHttpClient.setResponseTimeout ( 1000000 );
        }

        syncHttpClient.post(context,getAbsoluteUrl(context, url), entity , null, responseHandler);
    }

    public static void cancelRequest(Context context)
    {
        syncHttpClient.cancelRequests(context, true);
    }

    public static void post(Context ctx, String url, StringEntity entity, java.lang.String contentType, AsyncHttpResponseHandler responseHandler ){
        client.post(ctx, getAbsoluteUrl(ctx, url), entity,contentType,responseHandler);
    }

    private static String getAbsoluteUrl(Context context, String relativeUrl)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        String base_url = "https://delmontepayroll.com/api/"; // Live
//      String base_url = "http://192.168.0.8:8000/api/"; // Local
//        String base_url  = "http://35.240.178.4/api/"; // Staging 3

        if (settings.getString("SELECTED_API", "LIVE").equals("LOCAL"))
        {
            base_url = "http://" + settings.getString("LOCAL_API_URL", "") + "/api/";
        }

        return base_url + relativeUrl;
    }
}