package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.Base64;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceInformation;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmailCredential;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeDeduction;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLeaveBalance;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.Models.Leave;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveType;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveTypeBalances;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.Models.Undertime;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnRestday;
import com.yahshua.yahshuatimekeeperdmpi.Models.Workdays;
import com.yahshua.yahshuatimekeeperdmpi.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class TransactionSyncer
{
    private Context context;
    HttpRequestListener httpRequestListener;
    private Session session;
    private ArrayList<String> mFilePathList = new ArrayList<>();

    public TransactionSyncer(Context context)
    {
        this.context = context;
    }

    public void setOnRequestListener(HttpRequestListener httpRequestListener)
    {
        this.httpRequestListener = httpRequestListener;
    }

    public void loadLeaveRequests(int employeeId) throws Exception
    {
        try
        {
            String searchId = employeeId == 0 ? "" : " AND employeeId = "+employeeId;
            String searchQuery  = "WHERE isSync = 0 "+searchId;

            final ArrayList<Leave> leaveArrayList = Leave.load(context, searchQuery);

            if(leaveArrayList.size() == 0 )
            {
                httpRequestListener.onSuccess("leave", context.getResources().getString(R.string.no_leave_to_sync));
                httpRequestListener.onFinish("leave");
            }
            else
            {
                uploadLeaveRequests(leaveArrayList, 0);
            }
        }
        catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Leave Application request:" + err.toString());
        }
    }

    private void uploadLeaveRequests(final ArrayList<Leave> leaveArrayList, final int idx) throws Exception
    {
        try
        {
            if (idx == leaveArrayList.size())
            {
                AuditLog.saveAuditLog(context, "MANUAL SYNCING - LEAVE", "SYNC", "SUCCESS", "NONE");
                httpRequestListener.onSuccess("leave","");
            }
            else
            {
                // Convert to JSONObject
                final Leave leave = leaveArrayList.get(idx);
                JSONObject leaveJson = new JSONObject();

                if (leave.getDay().equals("whole"))
                {
                    leaveJson.put("date_from", DateToString.dbFormat(leave.getDateFrom()));
                    leaveJson.put("date_to", DateToString.dbFormat(leave.getDateTo()));
                }
                else
                {
                    leaveJson.put("half_day_date", DateToString.dbFormat(leave.getHalfDayDate()));
                    leaveJson.put("time_from", leave.getTimeFrom());
                    leaveJson.put("time_to", leave.getTimeTo());
                }

                leaveJson.put("employee", leave.getEmployeeId());
                leaveJson.put("day", leave.getDay());
                leaveJson.put("days_leave", leave.getDaysLeave());
                leaveJson.put("leave_type2", leave.getLeaveTypeId());
                leaveJson.put("reason", leave.getReason().replaceAll("[^a-zA-Z0-9\\ss+]", " x "));
                leaveJson.put("requested_date", DateToString.dateTimeFormat(leave.getRequestedDate()));
                leaveJson.put("status", leave.getStatus());

                // Upload
                String json = leaveJson.toString();
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
                {

                    @Override
                    public void onStart()
                    {
                        httpRequestListener.onStart("leave");
                    }

                    @Override
                    public void onFinish()
                    {
                        httpRequestListener.onFinish("leave");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            leave.changeSyncStatus(context);
                            uploadLeaveRequests(leaveArrayList, idx + 1);
                        }
                        catch (Exception err)
                        {
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");
                            httpRequestListener.onFailure(1, err.getMessage(),"leave");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        try
                        {
                            String responseString = new String(responseBody);

                            if (responseString.contains("html"))
                            {
                                httpRequestListener.onFailure(1,"Server encountered some errors","leave");
                                return;
                            }

                            httpRequestListener.onFailure(1,responseString,"leave");
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");
                        }
                        catch (Exception e)
                        {
                            httpRequestListener.onFailure(1,e.getMessage(),"leave");
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - LEAVE","SYNC","FAILED","NONE");
                        }
                    }
                };

                HttpProvider.postSync(context, "leave-application/save/", entity, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading requests: " + err.toString());
        }
    }

    public void loadOvertimeRequests(int employeeId) throws Exception
    {
        try
        {
            String searchId = employeeId == 0 ? "" : " AND employeeId = "+employeeId;
            String searchQuery = "WHERE isSync = 0 AND company_id = " + UserSession.getCompanyId(context) +searchId;

            ArrayList<Overtime> overtimeArrayList = Overtime.load(context, searchQuery);

            if(overtimeArrayList.size() == 0 )
            {
                httpRequestListener.onSuccess("overtime", context.getResources().getString(R.string.no_overtime_to_sync));
                httpRequestListener.onFinish("overtime");
            }
            else
            {
                // Called in Overtime Activity Nav Option button
                for (Overtime overtime : overtimeArrayList)
                {
                    overtime.setDateString(DateToString.dbFormat(overtime.getDate()));
                    overtime.setDateRequestedString(DateToString.dateTimeFormat(overtime.getRequestedDate()));
                }

                uploadOvertimeRequests(overtimeArrayList, 0);
            }
        }catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Undertime Application request:" + err.toString());
        }
    }

    private void uploadOvertimeRequests(final ArrayList<Overtime> overtimeArrayList, final int idx) throws Exception
    {
        try
        {
            if (idx == overtimeArrayList.size())
            {
                httpRequestListener.onSuccess("overtime","");
                AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","SUCCESS","NONE");
            }
            else
            {
                Overtime overtime = overtimeArrayList.get(idx);
                final Overtime overtime2 = overtime;
                overtime.setDateString(DateToString.dbFormat(overtime.getDate()));
                overtime.setDateRequestedString(DateToString.dateTimeFormat(overtime.getRequestedDate()));
                overtime.setEmployeeName("");

                String json = new Gson().toJson(overtime);
                StringEntity entity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onStart()
                    {
                        httpRequestListener.onStart("overtime");
                    }

                    @Override
                    public void onFinish()
                    {
                        httpRequestListener.onFinish("overtime");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            overtime2.changeSyncStatus(context);
                            uploadOvertimeRequests(overtimeArrayList, idx + 1);
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1,err.getMessage(),"overtime");
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        try
                        {
                            String responseString = new String(responseBody);

                            if (responseString.contains("html"))
                            {
                                httpRequestListener.onFailure(1,"Server encountered some errors","overtime");
                                return;
                            }

                            httpRequestListener.onFailure(1,responseString,"overtime");
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");

                            uploadOvertimeRequests(overtimeArrayList, idx + 1);
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1,err.getMessage(),"overtime");
                            AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");
                        }
                    }

                };

                HttpProvider.postSync(context, "overtime-application/save/", entity, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading overtime request: " + err.toString());
        }
    }

    public void loadUnderTimeRequests(int employeeId) throws Exception
    {
        try
        {
            String searchId = employeeId == 0 ? "" : " AND employeeId = "+employeeId;

            String  searchUnderTime = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context) +searchId;
            final ArrayList<Undertime> undertimeArrayList = Undertime.load(context, searchUnderTime);

            if(undertimeArrayList.size() == 0 )
            {
                httpRequestListener.onSuccess("undertime", context.getResources().getString(R.string.no_undertime_to_sync));
                httpRequestListener.onFinish("undertime");
            }
            else
            {
                uploadUnderTimeRequests(undertimeArrayList, 0);
            }
        }catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - UNDERTIME","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Undertime Application request:" + err.toString());
        }
    }

    private void uploadUnderTimeRequests(final ArrayList<Undertime> underTimeArrayList, final int idx) throws Exception
    {
        try
        {
            if (idx == underTimeArrayList.size())
            {
                httpRequestListener.onSuccess("undertime","");
                AuditLog.saveAuditLog(context, "MANUAL SYNCING - UNDERTIME", "SYNC", "SUCCESS", "NONE");
            }
            else
            {
                Undertime undertime = underTimeArrayList.get(idx);
                final Undertime undertime2 = undertime;
                undertime.setDateString(DateToString.dbFormat(undertime.getDate()));
                undertime.setDateRequestedString(DateToString.dateTimeFormat(undertime.getRequestedDate()));
                undertime.setEmployeeName("");

                String json = new Gson().toJson(undertime);
                StringEntity underTimeEntity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onStart()
                    {
                        httpRequestListener.onStart("undertime");
                    }

                    @Override
                    public void onFinish()
                    {
                        httpRequestListener.onFinish("undertime");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            undertime2.changeSyncStatus(context);
                            uploadUnderTimeRequests(underTimeArrayList, idx + 1);
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.toString(), "undertime");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - UNDERTIME", "SYNC", "FAILED", "NONE");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        try
                        {
                            String responseString = new String(responseBody);

                            if (responseString.contains("html"))
                            {
                                httpRequestListener.onFailure(1, "Server encountered some errors", "undertime");
                                return;
                            }

                            httpRequestListener.onFailure(1, responseString, "undertime");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - UNDERTIME", "SYNC", "FAILED", "NONE");
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.getMessage(), "undertime");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - UNDERTIME", "SYNC", "FAILED", "NONE");
                        }
                    }
                };

                HttpProvider.postSync(context, "undertime-application/save/", underTimeEntity, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading undertime requests: " + err.toString());
        }
    }

    public void loadWorkOnRestDayRequests(int employeeId) throws Exception
    {
        try
        {
            String searchId = employeeId == 0 ? "" : " AND employeeId = "+employeeId;
            String searchQueryRestDay = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context) +searchId;

            final ArrayList<WorkOnRestday> workOnRestdayArrayList = WorkOnRestday.load(context, searchQueryRestDay);

            if(workOnRestdayArrayList.size() == 0 )
            {
                httpRequestListener.onSuccess("Work on Rest Day", context.getResources().getString(R.string.no_restday_to_sync));
                httpRequestListener.onFinish("Work on Rest Day");
            }
            else
            {
                uploadWorkOnRestDayRequests(workOnRestdayArrayList, 0);
            }
        }catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON REST DAY","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Work on Rest Day Application request:" + err.toString());
        }
    }

    private void uploadWorkOnRestDayRequests(final ArrayList<WorkOnRestday> workOnRestDayArrayList, final int idx) throws Exception
    {
        try
        {
            if (idx == workOnRestDayArrayList.size())
            {
                httpRequestListener.onSuccess("Work on Rest Day","");
                AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON REST DAY", "SYNC", "SUCCESS", "NONE");
            }
            else
            {
                WorkOnRestday workOnRestday = workOnRestDayArrayList.get(idx);
                final WorkOnRestday workOnRestday2 = workOnRestday;
                workOnRestday.setDateString(DateToString.dbFormat(workOnRestday.getDate()));
                workOnRestday.setDateRequestedString(DateToString.dateTimeFormat(workOnRestday.getRequestedDate()));
                workOnRestday.setReason(workOnRestday.getReason().replaceAll("[^a-zA-Z0-9\\s+]", ""));
                workOnRestday.setEmployeeName("");

                String json = new Gson().toJson(workOnRestday);
                StringEntity restDayEntity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onStart()
                    {
                        httpRequestListener.onStart("Work on Rest Day");
                    }

                    @Override
                    public void onFinish()
                    {
                        httpRequestListener.onFinish("Work on Rest Day");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            uploadWorkOnRestDayRequests(workOnRestDayArrayList, idx + 1);
                            workOnRestday2.changeSyncStatus(context);
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.toString(), "Work on Rest Day");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON REST DAY", "SYNC", "FAILED", "NONE");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        try
                        {
                            String responseString = new String(responseBody);

                            if (responseString.contains("html"))
                            {
                                httpRequestListener.onFailure(1, "Server encountered some errors", "Work on Rest Day");
                                return;
                            }

                            httpRequestListener.onFailure(1, responseString, "Work on Rest Day");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON REST DAY", "SYNC", "FAILED", "NONE");

                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.getMessage(), "Work on Rest Day");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON REST DAY", "SYNC", "FAILED", "NONE");
                        }
                    }
                };

                HttpProvider.postSync(context, "workon-restday-application/save/", restDayEntity, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading work on rest day requests: " + err.toString());
        }
    }

    public void loadWorkOnHolidayRequests(int employeeId) throws Exception
    {
        try
        {
            String searchId = employeeId == 0 ? "" : " AND employeeId = "+employeeId;
            String searchQueryHoliday = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context) +searchId;

            final ArrayList<WorkOnHoliday> workOnHolidayArrayList = WorkOnHoliday.load(context, searchQueryHoliday);

            if(workOnHolidayArrayList.size() == 0 )
            {
                httpRequestListener.onSuccess("Work on Holiday", context.getResources().getString(R.string.no_restday_to_sync));
                httpRequestListener.onFinish("Work on Holiday");
            }
            else
            {
                uploadWorkOnHolidayRequests(workOnHolidayArrayList, 0);
                // Called in?
//                for (WorkOnHoliday workOnHoliday : workOnHolidayArrayList)
//                {
//                    workOnHoliday.setDateString(DateToString.dbFormat(workOnHoliday.getDate()));
//                    workOnHoliday.setDateRequestedString(DateToString.dateTimeFormat(workOnHoliday.getRequestedDate()));
//                }
//
//                String json = new Gson().toJson(workOnHolidayArrayList);
//                StringEntity HolidayJsonEntity = new StringEntity(json);
//
//                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
//                {
//                    @Override
//                    public void onStart()
//                    {
//                        httpRequestListener.onStart("Work on Holiday");
//                    }
//
//                    @Override
//                    public void onFinish()
//                    {
//                        httpRequestListener.onFinish("Work on Holiday");
//                    }
//
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
//                    {
//                        for (WorkOnHoliday workOnHoliday : workOnHolidayArrayList)
//                        {
//                            workOnHoliday.changeSyncStatus(context);
//                        }
//
//                        httpRequestListener.onSuccess("Work on Holiday","");
//                        AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "SUCCESS", "NONE");
//                    }
//
//                    @Override
//                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
//                    {
//                        try
//                        {
//                            String responseString = new String(responseBody);
//
//                            if (responseString.contains("html"))
//                            {
//                                httpRequestListener.onFailure(1, "Server encountered some errors", "Work on Holiday");
//                                return;
//                            }
//
//                            httpRequestListener.onFailure(1, responseString, "Work on Holiday");
//                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "FAILED", "NONE");
//                        }
//                        catch (Exception err)
//                        {
//                            httpRequestListener.onFailure(1, err.getMessage(), "Work on Holiday");
//                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "FAILED", "NONE");
//                        }
//                    }
//                };
//
//                HttpProvider.postSync(context, "sync-work-on-holidays/", HolidayJsonEntity, true, asyncHttpResponseHandler);
            }
        }catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - WORK ON HOLIDAY","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Work on Holiday Application request:" + err.toString());
        }
    }

    private void uploadWorkOnHolidayRequests(final ArrayList<WorkOnHoliday> workOnHolidayArrayList, final int idx) throws Exception
    {
        try
        {
            if (idx == workOnHolidayArrayList.size())
            {
                httpRequestListener.onSuccess("Work on Holiday","");
                AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "SUCCESS", "NONE");
            }
            else
            {
                WorkOnHoliday workOnHoliday = workOnHolidayArrayList.get(idx);
                final WorkOnHoliday workOnHoliday2 = workOnHoliday;
                workOnHoliday.setDateString(DateToString.dbFormat(workOnHoliday.getDate()));
                workOnHoliday.setDateRequestedString(DateToString.dateTimeFormat(workOnHoliday.getRequestedDate()));
                workOnHoliday.setEmployeeName("");

                String json = new Gson().toJson(workOnHoliday);
                StringEntity HolidayJsonEntity = new StringEntity(json);

                AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onStart()
                    {
                        httpRequestListener.onStart("Work on Holiday");
                    }

                    @Override
                    public void onFinish()
                    {
                        httpRequestListener.onFinish("Work on Holiday");
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        try
                        {
                            workOnHoliday2.changeSyncStatus(context);
                            uploadWorkOnHolidayRequests(workOnHolidayArrayList, idx + 1);
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.getMessage(), "Work on Holiday");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "FAILED", "NONE");
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        try
                        {
                            String responseString = new String(responseBody);

                            if (responseString.contains("html"))
                            {
                                httpRequestListener.onFailure(1, "Server encountered some errors", "Work on Holiday");
                                return;
                            }

                            httpRequestListener.onFailure(1, responseString, "Work on Holiday");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "FAILED", "NONE");
                        }
                        catch (Exception err)
                        {
                            httpRequestListener.onFailure(1, err.getMessage(), "Work on Holiday");
                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - WORK ON HOLIDAY", "SYNC", "FAILED", "NONE");
                        }
                    }
                };

                HttpProvider.postSync(context, "workon-holiday-application/save/", HolidayJsonEntity, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            throw new Exception("Error uploading work on holiday requests: " + err.toString());
        }
    }

    public void uploadEmployeeDeduction(int employeeId) throws Exception
    {
        try
        {
            final ArrayList<EmployeeDeduction> employeeDeductionArrayList = EmployeeDeduction.read(context, employeeId, true, null);

            if(employeeDeductionArrayList.size() == 0)
            {
                httpRequestListener.onSuccess("cash advance",context.getResources().getString(R.string.no_ca_to_sync));
                httpRequestListener.onFinish("cash advance");
            }
            else
            {
                int countSync = 0;
                for (final EmployeeDeduction employeeDeduction : employeeDeductionArrayList)
                {
                    countSync++;
                    JSONObject jsonObject = new JSONObject();

                    Date date = DateStringToDate.dateTimeDbFormat2(employeeDeduction.getDateStart());
                    String startDate = DateToString.dbQuery(date);

                    jsonObject.put("term", employeeDeduction.getTerm());
                    jsonObject.put("employee", employeeDeduction.getEmployeeId());
                    jsonObject.put("paymentterm", "regular");
                    jsonObject.put("daterequested", employeeDeduction.getDateRequested());
                    jsonObject.put("deduction_id", employeeDeduction.getDeductionTypeId());
                    jsonObject.put("amount", employeeDeduction.getAmount());
                    jsonObject.put("remarks", employeeDeduction.getRemarks());
                    jsonObject.put("datestart", startDate);
                    jsonObject.put("dateend",  employeeDeduction.getDateEnd());
                    jsonObject.put("amountperdeduct", employeeDeduction.getAmountPerDeduct());

                    StringEntity stringEntity  = new StringEntity(jsonObject.toString());

                    final int finalCountSync = countSync;

                    HttpProvider.postSync(context, "employee_deductions/save/", stringEntity, true, new AsyncHttpResponseHandler()
                    {
                        @Override
                        public void onStart()
                        {
                            if(finalCountSync == 1)
                            {
                                httpRequestListener.onStart("cash advance");
                            }
                        }

                        @Override
                        public void onFinish()
                        {
                            if(finalCountSync == employeeDeductionArrayList.size())
                            {
                                httpRequestListener.onFinish("cash advance");
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                        {
                            employeeDeduction.setToSynced(context);

                            if(finalCountSync == employeeDeductionArrayList.size())
                            {
                                httpRequestListener.onSuccess("cash advance", "");
                            }

                            AuditLog.saveAuditLog(context, "MANUAL SYNCING - CASH ADVANCE", "SYNC", "SUCCESS", employeeDeduction.getEmployee().getFullName());
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                        {
                            try
                            {
                                String responseString = new String(responseBody);

                                if (responseString.contains("html"))
                                {
                                    httpRequestListener.onFailure(1, "Server encountered some errors", "cash advance");
                                    return;
                                }

                                httpRequestListener.onFailure(1, responseString, "cash advance");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - CASH ADVANCE", "SYNC", "FAILED",  employeeDeduction.getEmployee().getFullName());

                            }
                            catch (Exception err)
                            {
                                httpRequestListener.onFailure(1, err.getMessage(), "cash advance");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - CASH ADVANCE", "SYNC", "FAILED",  employeeDeduction.getEmployee().getFullName());
                            }
                        }
                    });
                }
            }
        } catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - CASH ADVANCE","SYNC","FAILED","NONE");
            throw new Exception("Error on upload Cash Advance Application request:" + err.toString());
        }
    }

    public void uploadEmployeeLogs()throws Exception
    {
        try
        {
            final ArrayList<EmployeeLogs> employeeLogs = EmployeeLogs.read(context, true, 0, 20);

                for (final EmployeeLogs employeeLog : employeeLogs)
                {
                    String    image     = "";
                    JSONArray jsonArray = new JSONArray();
                    JSONObject o = new JSONObject();
                    o.put("employee", employeeLog.getEmployeeId());
                    o.put("log_time", employeeLog.getLogTime());
                    o.put("log_type", employeeLog.getLogType());
                    o.put("sync_id", employeeLog.getSyncId());
                    o.put("date", employeeLog.getDate());
                    o.put("is_selfie_app", true);
                    o.put("is_android", true);

                    // Check first if image file exists
                    boolean imgExists = false;
                    int index = employeeLog.getLogImg().lastIndexOf('/');
                    String imageFilename = employeeLog.getLogImg().substring(index + 1);
                    if (ImageHandler.fileExists(context, imageFilename)) imgExists = true;

                    // Convert File image as Base 64 if files exists
                    if (imgExists)
                    {
                        try
                        {
                            image = Base64.encodeToString(ImageHandler.getImageByPath(employeeLog.getLogImg()), Base64.DEFAULT);
                            o.put("log_img", image);
                        } catch (Exception err)
                        {
                            Debugger.logD(err.toString());
                        }
                    }

                    jsonArray.put(o);

                    String SyncEmployeeLogsJson = jsonArray.toString();
                    StringEntity SyncEmployeeLogsEntity = new StringEntity(SyncEmployeeLogsJson);

                    HttpProvider.postSync(context, "sync-logs2/", SyncEmployeeLogsEntity, true, new AsyncHttpResponseHandler()
                    {

                        @Override
                        public void onStart()
                        {
                            httpRequestListener.onStart("Employee Logs");
                        }

                        @Override
                        public void onFinish()
                        {
                            httpRequestListener.onFinish("Employee Logs");
                        }


                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                        {
                            try
                            {
                                Debugger.logD("onSuccess");
                                employeeLog.convertSyncStatus(context);

                                httpRequestListener.onSuccess("Employee Logs", "");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE LOGS", "SYNC", "SUCCESS", "NONE");

                            } catch (Exception err)
                            {
                                httpRequestListener.onFailure(1, err.getMessage(), "Employee Logs");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE LOGS", "SYNC", "FAILED", "NONE");
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                        {
                            try
                            {
                                Debugger.logD("onFailure");
                                String responseString = new String(responseBody);
                                Debugger.logD("responseString "+responseString);

                                if (statusCode == 404 || responseString.contains("html"))
                                {
                                    httpRequestListener.onFailure(1, context.getString(R.string.return_404), "Employee Logs");
                                } else
                                {
                                    httpRequestListener.onFailure(1, responseString, "Employee Logs");
                                }

                                httpRequestListener.onFailure(1, responseString, "Employee Logs");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE LOGS", "SYNC", "FAILED", "NONE");

                            } catch (Exception err)
                            {
                                httpRequestListener.onFailure(1, context.getString(R.string.internet_connection_problem), "Employee Logs");
                                AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE LOGS", "SYNC", "FAILED", "NONE");
                            }
                        }
                    });
                }
            } catch (Exception err)
            {
                AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE LOGS","SYNC","FAILED","NONE");
                throw new Exception("Error on upload Employee Logs Application request:" + err.toString());
            }
    }

    public void downloadEmployee(boolean isSync)
    {
        try
        {
            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {

                @Override
                public void onStart()
                {
                    httpRequestListener.showSpinner(true, "Employee");
                }

                @Override
                public void onFinish()
                {
                    httpRequestListener.showSpinner(false, "Employee");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    DbAdapter dbAdapter = new DbAdapter(context);
                    dbAdapter.open();

                    try
                    {
                        JSONObject response = new JSONObject(new String(responseBody));

                        Employee.delete(dbAdapter);
                        Workdays.deleteAll(dbAdapter);
                        LeaveType.deleteAll(dbAdapter);
                        EmployeeLeaveBalance.delete(dbAdapter);

                        int employeeCounter = 0;

                        ArrayList<Employee> employeeArrayList = new Gson().fromJson(response.getJSONArray("emp_list").toString(), new TypeToken<List<Employee>>() {}.getType());

                        for (Employee employee : employeeArrayList)
                        {
                            employee.save(context, dbAdapter);
                            employeeCounter++;
                            httpRequestListener.onUpdate(employeeArrayList.size(), employeeCounter,"Employee");
                        }

                        httpRequestListener.onSuccess("Employee", "");

                        dbAdapter.close();
                        AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE", "SYNC", "SUCCESS", "ALL");
                    }
                    catch (Exception err)
                    {
                        httpRequestListener.onFailure(1, err.getMessage(), "Employee");
                        AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE", "SYNC", "FAILED", "NONE");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        String responseString = new String(responseBody);

                        if (statusCode == 404 || responseString.contains("html"))
                        {
                            httpRequestListener.onFailure(1, context.getString(R.string.return_404),"Employee");
                        }
                        else if (statusCode == 408)
                        {
                            httpRequestListener.onFailure(1, context.getString(R.string.return_408),"Employee");
                        }
                        else
                        {
                            httpRequestListener.onFailure(1, responseString,"Employee");
                        }

                        AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE", "SYNC", "FAILED", "NONE");

                    }
                    catch (Exception err)
                    {
                        httpRequestListener.onFailure(1, err.getMessage(), "Employee");
                        AuditLog.saveAuditLog(context, "MANUAL SYNCING - EMPLOYEE", "SYNC", "FAILED", "NONE");
                    }
                }
            };

            if(isSync)
            {
                HttpProvider.postSync(context, "get-employees/", null, true, asyncHttpResponseHandler);
            }
            else
            {
                HttpProvider.post(context, "get-employees/", null, true, asyncHttpResponseHandler);
            }
        }
        catch (Exception err)
        {
            AuditLog.saveAuditLog(context,"MANUAL SYNCING - EMPLOYEE","SYNC","FAILED","NONE");
        }
    }

    @SuppressLint("MissingPermission")
    public void SendToEmailDatabase()
    {
        try
        {
            Debugger.logD("SendToEmailDatabase");

            //We zipped db file first
            String zipName = "zipped_db.zip";
            addFilesToZip();
            File fileOutput = getOutputZipFile(zipName, context);

            String zipFileName;
            if (fileOutput != null)
            {
                zipFileName = fileOutput.getAbsolutePath();
                if (mFilePathList.size() > 0)
                {
                    zip(zipFileName);
                }
            }
            //End

            Properties props = new Properties();

            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            //Creating a new session
            session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator()
                    {
                        //Authenticating the password
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication()
                        {
                            return new PasswordAuthentication(EmailCredential.EMAIL, EmailCredential.PASSWORD);
                        }
                    });

            //File Attachment
            File root = Environment.getDataDirectory();
            String pathToMyAttachedFile ="/data/" + context.getPackageName() + "/files/"+zipName;
            File file = new File(root, pathToMyAttachedFile);
            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            BodyPart messageBodyPart2 = new MimeBodyPart();
            DataSource source = new FileDataSource(file);

            //Device Details
            DeviceInformation deviceInformation = new DeviceInformation(context);
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;

            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(file.getName());
            messageBodyPart2.setText("COMPANY EMAIL: "+UserSession.getCompanyEmail(context)+"\n DEVICE ID: "+deviceInformation.getDeviceId()+ "\n BRAND: "+deviceInformation.getBrand()+"\n HOSTNAME: "+deviceInformation.getDeviceHostName()+ "\n MODEL: "+deviceInformation.getModel()+"\n APP VERSION: "+version+"\n TOKEN : "+UserSession.getToken(context));
            multipart.addBodyPart(messageBodyPart);
            multipart.addBodyPart(messageBodyPart2);


            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(session);

            //Setting sender address
            mm.setFrom(new InternetAddress(EmailCredential.EMAIL));
            //Adding receiver
            mm.setRecipients(Message.RecipientType.TO, InternetAddress.parse("yahshua.developers@gmail.com"));
            //Adding subject
            mm.setSubject("DATABASE DMPI TIMEKEEPER FROM "+ UserSession.getCompany(context));

            //Adding Attachment
            mm.setContent(multipart);

            //Sending email
            Transport.send(mm);

            httpRequestListener.onSuccess("Upload Db", "");


        }
        catch (Exception err)
        {
            Debugger.logD("SendToEmailDatabase  Exception \n" + err.toString());
            httpRequestListener.onFailure(1, err.getMessage(), "Upload Db");
        }
    }

    private static File getOutputZipFile(String fileName, Context context)
    {
        File root = Environment.getDataDirectory();
        String pathToMyAttachedFile ="/data/" + context.getPackageName() + "/files/";
        File filePath = new File(root, pathToMyAttachedFile);

        if (!filePath.exists())
        {
            if (!filePath.mkdirs())
            {
                return null;
            }
        }
        return new File(filePath.getPath() + File.separator + fileName);
    }

    private void addFilesToZip()
    {
        File root = Environment.getDataDirectory();
        String pathToMyAttachedFile ="/data/" + context.getPackageName() + "/databases/db_session";
        File file = new File(root, pathToMyAttachedFile);

        mFilePathList.add(file.getAbsolutePath());
    }

    private void zip(String zipFilePath)
    {
        try
        {
            BufferedInputStream origin;
            FileOutputStream dest = new FileOutputStream(zipFilePath);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

            byte data[] = new byte[2048];

            for (int i = 0; i < mFilePathList.size(); i++)
            {
                FileInputStream fi = new FileInputStream(mFilePathList.get(i));
                origin = new BufferedInputStream(fi, 2048);
                ZipEntry entry = new ZipEntry(mFilePathList.get(i).substring(mFilePathList.get(i).lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, 2048)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e)
        {
            e.printStackTrace();
            Debugger.logD("zip "+e.getMessage());
        }
    }
}

