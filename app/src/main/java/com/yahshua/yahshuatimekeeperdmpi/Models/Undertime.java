package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class Undertime extends Application implements Parcelable
{

    public Undertime() {}

    protected Undertime(Parcel in)
    {
        super.setId(in.readInt());
        super.setEmployeeId(in.readInt());
        super.setEmployeeName(in.readString());
        super.setReason(in.readString());
        super.setStatus(in.readString());
        super.setRemarks(in.readString());
        super.setHours(in.readDouble());
        super.setSync(in.readByte() != 0);
        super.setDate((java.util.Date) in.readSerializable());
        super.setRequestedDate((java.util.Date) in.readSerializable());
    }

    public static final Creator<Undertime> CREATOR = new Creator<Undertime>()
    {
        @Override
        public Undertime createFromParcel(Parcel in) { return new Undertime(in); }

        @Override
        public Undertime[] newArray(int size) {  return new Undertime[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(super.getId());
        dest.writeInt(super.getEmployeeId());
        dest.writeString(super.getEmployeeName());
        dest.writeString(super.getReason());
        dest.writeString(super.getStatus());
        dest.writeString(super.getRemarks());
        dest.writeDouble(super.getHours());
        dest.writeByte((byte) (super.isSync() ? 1 : 0));
        dest.writeSerializable(super.getDate());
        dest.writeSerializable(super.getRequestedDate());
    }


    public static ArrayList<Undertime> load(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<Undertime> undertimeArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM undertime";

        try {
            if (searchQuery != null) selectQuery += " " + searchQuery;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    Undertime undertime = new Undertime();
                    Date date, requestedDate;

                    date = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("date")));
                    requestedDate = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("requestedDate")));

                    undertime.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    undertime.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    undertime.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    undertime.setDate(date);
                    undertime.setHours(cursor.getDouble(cursor.getColumnIndex("hours")));
                    undertime.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    undertime.setRequestedDate(requestedDate);
                    undertime.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    undertime.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    undertime.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    undertimeArrayList.add(undertime);

                } while (cursor.moveToNext());
            }

            return undertimeArrayList;
        } catch (SQLiteException err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return undertimeArrayList;
        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return undertimeArrayList;
        }
    }

    public static int count(Context context, String countQuery) {
        int count = 0;

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            Cursor cursor = db.read(countQuery);

            if (cursor.moveToNext()) count = cursor.getInt(cursor.getColumnIndex("count"));

            return count;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime count error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return count;
        }
    }


    public boolean save(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ContentValues contentValues = new ContentValues();

        try {
            // Hours
            DecimalFormat decimalFormat = new DecimalFormat("#.######");
            contentValues.put("hours", Double.valueOf(decimalFormat.format(getHours())));

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("employeeName", getEmployeeName());
            contentValues.put("date", DateToString.dbFormat(getDate()));
            contentValues.put("reason", getReason());
            contentValues.put("isSync", false);
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (getId() > 0) {
                db.open();
                db.update(contentValues, "undertime", " id = ?", new String[]{ String.valueOf( getId()) });
            } else {
                contentValues.put("status", "pending");

                db.save(contentValues, "undertime");
            }

            return true;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public boolean changeSyncStatus(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("isSync", "1");
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));

            return db.update(contentValues, "undertime", "id=" + this.getId(),null);

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime changeSyncStatus error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }


    public void delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            if (getId() > 0) db.delete("undertime","id = ?", new String[]{ String.valueOf(getId()) });
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Undertime delete error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
        }
    }

    public static int getUndertimeCount(Context context)
    {
        try {
            Undertime undertime = new Undertime();
            String selectQuery = "SELECT COUNT(id) AS count FROM leave WHERE isSync = " + undertime.getEmployeeId();
            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            return count;
        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("getUndertimecount error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, "Database Error").show();
            }
            return 0;
        }

    }

}
