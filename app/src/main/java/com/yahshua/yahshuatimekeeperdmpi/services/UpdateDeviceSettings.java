package com.yahshua.yahshuatimekeeperdmpi.services;

import android.content.Context;
import android.os.Handler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Models.BreakIntervalSettingsList;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceInformation;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.SettingsHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class UpdateDeviceSettings
{
    public static void downloadDeviceSettings(final Context context, final Handler handler)
    {
        StringEntity stringEntity;

        try
        {
            JSONObject jsonObject = new JSONObject();
            DeviceInformation deviceInformation = new DeviceInformation(context);

            jsonObject.put("device_type", deviceInformation.getDeviceType());
            jsonObject.put("device_id", deviceInformation.getDeviceId());
            jsonObject.put("model", deviceInformation.getModel());
            jsonObject.put("brand", deviceInformation.getBrand());
            jsonObject.put("hostname", deviceInformation.getDeviceHostName());

            stringEntity = new StringEntity(jsonObject.toString());

            HttpProvider.postSync(context, "timekeeper-settings/", stringEntity, true, new JsonHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        saveDeviceSettings(response);
                        Utility.sendHandlerMessage(handler, 1, "");
                    }
                    catch (Exception err)
                    {
                        Utility.sendHandlerMessage(handler, 2, "Error onSuccess Handler: \n" + err.toString());
                    }
                }

                private void saveDeviceSettings(JSONObject response) throws Exception
                {
                    try
                    {
                        DeviceSettings deviceSettings = new DeviceSettings();
                        deviceSettings.setAutoDateTime(response.getJSONObject("device_settings").getBoolean("auto_datetime"));

                        JSONObject deviceSettingsJSON = response.getJSONObject("device_settings");
                        JSONObject visibleMenuItemJSON = deviceSettingsJSON.getJSONObject("visible_menu_items");
                        JSONObject applicationJSON = visibleMenuItemJSON.getJSONObject("applications");


                        DeviceSettings.visibleMenuItems visibleMenuItems = deviceSettings.new visibleMenuItems();
                        visibleMenuItems.setTimeIn(response.getJSONObject("device_settings").getJSONObject("visible_menu_items").getBoolean("time_in"));
                        visibleMenuItems.setTimeOut(response.getJSONObject("device_settings").getJSONObject("visible_menu_items").getBoolean("time_out"));
                        visibleMenuItems.setUploadAll(response.getJSONObject("device_settings").getJSONObject("visible_menu_items").getBoolean("upload_all"));
                        visibleMenuItems.setApplications(response.getJSONObject("device_settings").getJSONObject("visible_menu_items").getJSONObject("applications").getBoolean("is_visible"));

                        if (visibleMenuItemJSON.has("applications"))
                        {
                            if (applicationJSON.has("leave"))
                            {
                                visibleMenuItems.setLeave(applicationJSON.getBoolean("leave"));
                            }

                            if (applicationJSON.has("overtime"))
                            {
                                visibleMenuItems.setOvertime(applicationJSON.getBoolean("overtime"));
                            }

                            if (applicationJSON.has("undertime"))
                            {
                                visibleMenuItems.setUnderTime(applicationJSON.getBoolean("undertime"));
                            }

                            if (applicationJSON.has("work_on_rest_day"))
                            {
                                visibleMenuItems.setWorkOnRestDay(applicationJSON.getBoolean("work_on_rest_day"));
                            }

                            if (applicationJSON.has("work_on_holiday"))
                            {
                                visibleMenuItems.setWorkOnHoliday(applicationJSON.getBoolean("work_on_holiday"));
                            }
                        }

                        deviceSettings.setVisibleMenuItems(visibleMenuItems);
                        ArrayList<BreakIntervalSettingsList> breakIntervalSettingsLists = new Gson().fromJson(response.getJSONObject("device_settings").getJSONArray("break_interval_settings_list").toString(), new TypeToken<ArrayList<BreakIntervalSettingsList>>() {}.getType());
                        deviceSettings.setBreakIntervalSettingsListArrayList(breakIntervalSettingsLists);

                        SettingsHandler.isHaveBreakInterval(context, breakIntervalSettingsLists.size() != 0);


                        deviceSettings.save(context);
                    }
                    catch (Exception err)
                    {
                        throw new Exception("Error Setting Device Settings Fields: \n" + err.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    String errorMessage = (errorResponse != null) ? errorResponse.toString() : "";
                    Utility.sendHandlerMessage(handler, 2, "Downloading Device Settings Failed[1]: \n" + errorMessage);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    String errorMessage = (errorResponse != null) ? errorResponse.toString() : "";
                    Utility.sendHandlerMessage(handler, 2, "Downloading Device Settings Failed[2]: \n" + errorMessage);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    String errorMessage = (responseString != null) ? responseString : "";
                    Utility.sendHandlerMessage(handler, 2, "Downloading Device Settings Failed[3]: \n" + errorMessage);
                }
            });
        }
        catch (Exception err)
        {
            Utility.sendHandlerMessage(handler, 2, "Error Downloading Device Settings: \n" + err.toString());
        }
    }
}