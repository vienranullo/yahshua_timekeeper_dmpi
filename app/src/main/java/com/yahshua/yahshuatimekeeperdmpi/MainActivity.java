package com.yahshua.yahshuatimekeeperdmpi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yahshua.yahshuatimekeeperdmpi.Activities.AuditLogsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.BaseActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.IntroSigninActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.SettingsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.SettingsTabActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.SyncingActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.UnsycedOvertimeActivity;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseFields;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.ExportLogsDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.KioskSettingDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.LogOutAuthenticationDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.SystemIdDialog;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.EmployeeFragment;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.EmployeeLogFragments;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.HomeFragment;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.SelectRequestTypeFragment;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.SyncingFragment;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceInformation;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.KioskMode;
import com.yahshua.yahshuatimekeeperdmpi.services.DownloadDeductionTypeService;
import com.yahshua.yahshuatimekeeperdmpi.services.SyncingService;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DiagnosticClass;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener
{
    static { AppCompatDelegate.setCompatVectorFromResourcesEnabled(true); }
    TextView tvCompanyName,tvCompanyEmail;
    FragmentManager manager;
    private View navHeaderView;
    private Context context;
    private boolean isChecked = false;
    private Handler handler = new Handler();
    private int selectedNavId;
    private MenuItem menuLockIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Home");
        context = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        fab.setVisibility(View.INVISIBLE);

        if(savedInstanceState == null) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main, homeFragment,homeFragment.getTag()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                switch (selectedNavId)
                {
                    case R.id.nav_home: openHomeFragment(); break;
                    case R.id.nav_employee : checkEmployeesSettings(); break;
                    case R.id.nav_settings : openSettingTabActivity(); break;
                    case R.id.nav_syncing : openSyncingFragment(); break;
                    case R.id.nav_export_csv : openExportLogsDialog(); break;
                    case R.id.nav_approve_requests: openApproveRequests(); break;
                    case R.id.nav_audit_logs: openAuditLogsActivity(); break;
//                    case R.id.nav_unsyced_overtime : openUnsyncedOvertime(); break;
                }
            }
        };

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navHeaderView = navigationView.getHeaderView(0);

        tvCompanyName = navHeaderView.findViewById(R.id.tvCompanyName);
        tvCompanyEmail = navHeaderView.findViewById(R.id.tvCompanyEmail);
        TextView tvDeviceId = navHeaderView.findViewById(R.id.tvDeviceId);
        DeviceInformation deviceInformation = new DeviceInformation(context);
        tvDeviceId.setText("Device ID: " + deviceInformation.getDeviceId());

        checkUserSession();
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.content_main);

            if (f instanceof HomeFragment)
            {
                isAppLocked();
            } else
            {
                openHomeFragment();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        KioskMode kioskMode = KioskMode.getKioskMode();
        MenuItem checkable = menu.findItem(R.id.action_continuous_mode);
        menuLockIndicator = menu.findItem(R.id.action_is_locked);
        checkable.setChecked(UserSession.isContinousMode(context));

        if(kioskMode.isLocked(context))
        {
            menuLockIndicator.setVisible(true);
        }
        else
        {
            menuLockIndicator.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id)
        {
            case R.id.action_continuous_mode: updateContinuousMode(item); break;
            case R.id.action_settings : openSettingsActivity(); break;
            case R.id.action_logout : openLogoutDialog(); break;
            case R.id.action_kiosk_settings : showSettingsDialog(); break;

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        selectedNavId = id;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void checkUserSession()
    {
        String authToken = UserSession.getToken(this);
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        if(authToken.length() <= 0)
        {
            Intent login_intent = new Intent(this, IntroSigninActivity.class);
            startActivity(login_intent);
            this.finish();
        }
        else if( Employee.read(context, null, dbAdapter).size() == 0)
        {
            Intent intent = new Intent();
            intent.setClass(context, SyncingActivity.class);
            startActivity(intent);
        }
        else
        {
            tvCompanyName.setText(UserSession.getCompany(this));
            tvCompanyEmail.setText(UserSession.getCompanyEmail(this));

            ArrayList<EmployeeLogs> read = EmployeeLogs.read(context,true,0, 20);

            if(Utility.haveNetworkConnection(context) && read.size() > 0)
            {
                Intent service = new Intent(getApplicationContext(), SyncingService.class);
                service.putExtra("NOTIFY", false);
                startService(service);
            }
        }

        dbAdapter.close();
    }

    private void openLogoutDialog()
    {
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_LOGOUT", true);
        LogOutAuthenticationDialog logOutAuthenticationDialog = new LogOutAuthenticationDialog();
        logOutAuthenticationDialog.setArguments(bundle);
        logOutAuthenticationDialog.show(getSupportFragmentManager(),"");
    }

    private void updateContinuousMode(MenuItem item)
    {
        isChecked = !item.isChecked();
        item.setChecked(isChecked);

        UserSession.setContinousMode(context, isChecked);
    }

    private void openHomeFragment()
    {
        setTitle("Home");
        HomeFragment homeFragment = new HomeFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main,homeFragment,homeFragment.getTag()).commit();
    }

    private void openStudentSessionFragment()
    {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                        DiagnosticClass.readTable(context, "employee_logs", DatabaseFields.employeeLogFields);

                        EmployeeLogFragments employeeLogFragments = new EmployeeLogFragments();
                        employeeLogFragments.setEmployeeSystemId(Integer.parseInt(userInputDialogEditText.getText().toString()));
                        manager = getSupportFragmentManager();
                        manager.beginTransaction().replace(R.id.content_main,employeeLogFragments,employeeLogFragments.getTag()).commit();
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        alertDialogAndroid.show();
    }

    private void checkEmployeesSettings()
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
//        if (settings.getBoolean("EMPLOYEE_VISIBLE", false)) openEmployeesFragment(); else getEmployeeId();
        openEmployeesFragment();
    }

    private void openEmployeesFragment()
    {
        setTitle("Employees");
        EmployeeFragment employeeFragment = new EmployeeFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main,employeeFragment,employeeFragment.getTag()).commit();
    }

    private void openApproveRequests()
    {
        setTitle("Approve Requests");
        SelectRequestTypeFragment selectRequestTypeFragment = new SelectRequestTypeFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main, selectRequestTypeFragment, selectRequestTypeFragment.getTag()).commit();
    }

    private void getEmployeeId()
    {
        SystemIdDialog systemIdDialog = new SystemIdDialog();
        systemIdDialog.show(getSupportFragmentManager(), "System ID Dialog");
    }

    private void openSyncingFragment()
    {
        setTitle("Manual Syncing");
        SyncingFragment syncingFragment = new SyncingFragment();
        manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main,syncingFragment,syncingFragment.getTag()).commit();
    }

    private void openExportLogsDialog()
    {
        ExportLogsDialog exportLogsDialog = new ExportLogsDialog();
        exportLogsDialog.show(getSupportFragmentManager(), "export");
    }


//    private void openSettingsFragment()
//    {
//        SettingsFragment settingsFragment = new SettingsFragment();
//        manager = getSupportFragmentManager();
//        manager.beginTransaction().replace(R.id.content_main,settingsFragment,settingsFragment.getTag()).commit();
//    }

    private void openSettingsActivity()
    {
        Intent intent = new Intent(context, SettingsActivity.class);
        startActivity(intent);
    }

    private void openSettingTabActivity()
    {
        Intent intent = new Intent(context, SettingsTabActivity.class);
        startActivity(intent);
    }

    private void openAuditLogsActivity()
    {
        Intent intent = new Intent(context, AuditLogsActivity.class);
        startActivity(intent);
    }

    private void openUnsyncedOvertime()
    {
        Intent intent = new Intent(context, UnsycedOvertimeActivity.class);
        startActivity(intent);
    }

    private boolean isAppLocked()
    {
        if (!kioskMode.isLocked(context))
        {
            if(nameOfHomeApp().equals("com.yahshua.yahshuatimekeeperdmpi"))
            {
              Utility.openSettingsDialog(getResources().getString(R.string.unselect_3s_as_home_screen),"Set Home Screen", getSupportFragmentManager());
              return false;
            }
            else
            {
                PackageManager pm         = getPackageManager();
                Intent         homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                List<ResolveInfo> infoList = pm.queryIntentActivities(homeIntent, PackageManager.MATCH_DEFAULT_ONLY);
                // Scan the list to find the first match that isn't my own app
                for (ResolveInfo info : infoList)
                {
                    if (!"com.yahshua.yahshuatimekeeperdmpi".equals(info.activityInfo.packageName))
                    {
                        // This is the first match that isn't my package, so copy the
                        //  package and class names into to the HOME Intent
                        homeIntent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                        break;
                    }
                }
                // Launch the default HOME screen
                startActivity(homeIntent);
                finish();
                return true;
            }

        }
        else
        {
            Toasty.warning(context, getString(R.string.setting_device_locked), Toast.LENGTH_SHORT).show();
            return true;
        }

    }

    private String nameOfHomeApp()
    {
        try
        {
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_HOME);
            PackageManager pm = getPackageManager();
            final ResolveInfo mInfo = pm.resolveActivity(i, PackageManager.MATCH_DEFAULT_ONLY);
            return mInfo.activityInfo.packageName;
        }
        catch(Exception e)
        {
            return "";
        }
    }

   private void showSettingsDialog()
   {
       if(!kioskMode.isLocked(context) && !nameOfHomeApp().equals("com.yahshua.yahshuatimekeeperdmpi"))
       {
           Utility.openSettingsDialog(getResources().getString(R.string.set_3s_as_home_screen),"Set Home Screen", getSupportFragmentManager());
       }
       else
       {
            KioskSettingDialog settingFragment = new KioskSettingDialog();
            Bundle             args            = new Bundle();
            args.putBoolean(KioskSettingDialog.LOCKED_BUNDLE_KEY, kioskMode.isLocked(this));
            settingFragment.setArguments(args);
            settingFragment.show(getSupportFragmentManager(), settingFragment.getClass().getSimpleName());
            settingFragment.setActionHandler(new KioskSettingDialog.IActionHandler()
            {
                @Override
                public void isLocked(boolean isLocked)
                {
                    String msg = isLocked ? getString(R.string.setting_device_locked) : getString(R.string.setting_device_unlocked);
                    kioskMode.lockUnlock(context, isLocked);
                    Toasty.success(context, msg, Toast.LENGTH_LONG).show();

                    if(isLocked)
                    {
                        menuLockIndicator.setVisible(true);
                    }
                    else
                    {
                        menuLockIndicator.setVisible(false);
                    }
                }
            });
       }
   }
}
