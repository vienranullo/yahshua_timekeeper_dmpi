package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;


public class OvertimeAdapter extends ArrayAdapter<Overtime>
{

    private boolean showEmployeeName;

    public OvertimeAdapter(Context context, ArrayList<Overtime> overtimeArrayList, boolean showEmployeeName)
    {
        super(context, 0, overtimeArrayList);
        this.showEmployeeName = showEmployeeName;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        Overtime overtime = getItem(position);

        if (convertView == null) convertView = LayoutInflater.from(getContext()).inflate(R.layout.overtime_listrow, parent, false);

        TextView tvEmployeeName = (TextView) convertView.findViewById(R.id.tvEmployeeName);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        TextView tvHours = (TextView) convertView.findViewById(R.id.tvHours);
        TextView tvReason = (TextView) convertView.findViewById(R.id.tvReason);
        TextView tvLastSyncedDate = (TextView) convertView.findViewById(R.id.tvLastSyncedDate);

        tvDate.setText(DateToString.displayFormat(overtime.getDate()));
        tvReason.setText("Reason: " + overtime.getReason());

        // Hours
        double hoursDouble = overtime.getHours();
        int hoursInt;

        if (hoursDouble % 1 == 0) {
            hoursInt = (int) hoursDouble;
            tvHours.setText("Hours: " + String.valueOf(hoursInt));
        } else {
            tvHours.setText("Hours: " + String.valueOf(hoursDouble));
        }

        // Sync Status
        TextView tvSyncStatus = (TextView) convertView.findViewById(R.id.tvSyncStatus);
        String syncStatus;

        if (overtime.isSync()) {
            syncStatus = "Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.alreadySynced));
        } else {
            syncStatus = "Not Yet Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.notYetSynced));
        }

        if(overtime.getLastSyncedDate() != null)
        {
            tvLastSyncedDate.setVisibility(View.VISIBLE);

            if (overtime.isSync())
            {
                tvLastSyncedDate.setText("Upload Date: "+overtime.getLastSyncedDate());
            } else if (overtime.getLastSyncedDate().length() > 1) {
                tvLastSyncedDate.setTextColor(getContext().getResources().getColor(R.color.pending));
                tvLastSyncedDate.setText(overtime.getLastSyncedDate());
            } else {
                tvLastSyncedDate.setText(null);
            }


        }
        else
        {
            tvLastSyncedDate.setVisibility(View.GONE);
        }

        tvSyncStatus.setText(syncStatus);

        if (showEmployeeName)
        {
            tvEmployeeName.setVisibility(View.VISIBLE);
            tvEmployeeName.setText(overtime.getEmployeeName());
        }

        return convertView;
    }
}
