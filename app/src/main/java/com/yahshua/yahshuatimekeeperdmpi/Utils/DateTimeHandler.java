package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeHandler {

    final static DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String formatCal(Calendar cal)
    {
        return dateFormat.format(cal.getTime());
    }

    public static String formatDate(String str_date)
    {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date(str_date);
        return format1.format(date);
    }

    public static String formatTime(Date dateTIme)
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        return timeFormat.format(dateTIme);
    }

    public static String formatDateProper(String strDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("E, MMM d, yyyy");
        return dateFormat.format(convertStringtoDate2(strDate));
    }

    public static Date convertStringtoDate(String date)
    {
        try
        {
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
            return df.parse(date);

        } catch (Exception err)
        {
            System.out.println(err);
            return null;
        }
    }

    public static Date convertStringtoDate2(String strDate)
    {
        try
        {
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
            return originalFormat.parse(strDate);
        } catch (Exception err)
        {
            return null;
        }
    }

    public static Date convertStringtoDate3(String strDate)
    {
        try
        {
            DateFormat originalFormat = new SimpleDateFormat("MMM. dd, yyyy");
            return originalFormat.parse(strDate);
        } catch (Exception err)
        {
            return null;
        }
    }
    public static String convert12to42(String time)
    {
        try
        {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date date = parseFormat.parse(time);

            return displayFormat.format(date);

        } catch (Exception err)
        {
            return null;
        }
    }

    public static String convert24to12(String time)
    {
        try
        {
            DateFormat originalFormat = new SimpleDateFormat("HH:mm");
            Date dateTime = originalFormat.parse(time);

            return formatTime(dateTime);

        } catch (Exception err)
        {
            return null;
        }
    }

    public static String convertDatetoStringTime(Date date)
    {
        try
        {
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            return timeFormat.format(date);
        } catch (Exception err)
        {
            return null;
        }
    }

    public static String convertDatetoStringDate(Date date)
    {
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } catch (Exception err)
        {
            return null;
        }
    }


    public static String getTimeStamp()
    {
        // Return Example (24082017050509)
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        String format = simpleDateFormat.format(new Date());
        return format.replace("-","");
    }

     public static Date convertStringtoDateForError(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("MMM dd, yyyy   h:mm a");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDatetoStringDate1(Date date)
    {
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            return dateFormat.format(date);
        } catch (Exception err)
        {
            return null;
        }
    }

    public static Date convertHourToString(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("HH:mm");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

     public static String getTimeForError(Date date){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy   h:mm a");
            return simpleDateFormat.format(date);

        } catch (Exception err) {
            return null;
        }
    }


}
