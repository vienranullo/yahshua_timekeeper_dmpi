package com.yahshua.yahshuatimekeeperdmpi.httpRequests;

import android.content.Context;
import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnRestday;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DownloadWorkOnRestDayRequestsAsyncTask extends AsyncTask<Void, Void, Void>
{
    private WeakReference<Context> contextWeakReference;
    private AsyncResponseListener asyncResponseListener;
    private int errorCode = 1;
    private String error;
    private ArrayList<WorkOnRestday> workOnRestDayArrayList = new ArrayList<>();
    private JSONObject jsonObject;


    public DownloadWorkOnRestDayRequestsAsyncTask(Context context, AsyncResponseListener asyncResponseListener, JSONObject jsonObject)
    {
        this.asyncResponseListener = asyncResponseListener;
        this.contextWeakReference = new WeakReference<>(context);
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute()
    {
        try
        {
            // Check internet connection
            Context context = contextWeakReference.get();

            if (!Utility.haveNetworkConnection(context))
            {
                errorCode = 2;
                cancel(true);
            }
        }
        catch (Exception err)
        {
            error = "Error onPreExecute: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            Context context = contextWeakReference.get();

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(new String(responseBody));
                        setFields((JSONArray) jsonObject.get("records"));
                    }
                    catch (Exception err)
                    {
                        error = "Error asyncHttpResponseHandler onSuccess: \n" + err.toString();
                        cancel(true);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (statusCode == 0)
                        {
                            DownloadWorkOnRestDayRequestsAsyncTask.this.errorCode = 3;
                        }
                        else
                        {
                            DownloadWorkOnRestDayRequestsAsyncTask.this.errorCode = 4;
                            DownloadWorkOnRestDayRequestsAsyncTask.this.error = new String(responseBody);
                        }
                    }
                    catch (Exception err)
                    {
                        DownloadWorkOnRestDayRequestsAsyncTask.this.error = "Error asyncHttpResponseHandler onFailure: \n" + err.toString();
                    }

                    cancel(true);
                }
            };

            HttpProvider.postSync(context, "workon_restday/read/", new StringEntity(jsonObject.toString()), true, asyncHttpResponseHandler);
        }
        catch (Exception err)
        {
            error = "Error doInBackground: \n" + err.toString();
            cancel(true);
        }

        return null;
    }

    private void setFields(JSONArray workOnRestDayJSONArray)
    {
        try
        {
            for (int i = 0; i < workOnRestDayJSONArray.length(); i++)
            {
                JSONObject jsonObject = workOnRestDayJSONArray.getJSONObject(i);
                WorkOnRestday workOnRestday = new WorkOnRestday();

                workOnRestday.setId(jsonObject.getInt("id"));
                workOnRestday.setEmployeeId(jsonObject.getJSONObject("employee").getInt("id"));
                workOnRestday.setEmployeeName(jsonObject.getJSONObject("employee").getString("fullname"));
                workOnRestday.setHours(jsonObject.getDouble("hours"));
                workOnRestday.setReason(jsonObject.getString("reason"));
                workOnRestday.setRemarks(jsonObject.has("remarks") ? jsonObject.getString("remarks") : "");
                workOnRestday.setDate(DateStringToDate.dbFormat(jsonObject.getString("date")));
                workOnRestday.setRequestedDate(DateStringToDate.dateTimeDbFormat(jsonObject.getString("requested_date")));

                workOnRestDayArrayList.add(workOnRestday);
            }
        }
        catch (Exception err)
        {
            error = "Error setting fields: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected void onCancelled()
    {
        asyncResponseListener.onFailure(errorCode, error);
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        asyncResponseListener.onSuccess(workOnRestDayArrayList);
    }
}
