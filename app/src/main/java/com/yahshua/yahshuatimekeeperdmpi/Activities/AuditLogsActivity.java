package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.AuditLogsAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.AuditLogFilterDialogFragment;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.StringCompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;


import java.util.ArrayList;


public class AuditLogsActivity extends BaseActivity implements StringCompleteListener
{
    private AuditLogsAdapter auditLogsAdapter;
    private ArrayList<AuditLog> auditLogArrayList = new ArrayList<>();
    private RecyclerView rvAuditLogs;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            initializeViews();
            initializeData("ALL");
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error AuditLogsActivity onCreate: " + err.toString());
        }
    }

    private void initializeViews() throws Exception
    {
        try
        {
            setContentView(R.layout.audit_logs_activity);
            rvAuditLogs = findViewById(R.id.rvAuditLogs);
            initializeRecyclerViews();

            // Action bar
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) throw new Exception("ActionBar is null");
            actionBar.setTitle("Audit Logs");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: " + err.toString());
        }
    }

    private void initializeRecyclerViews() throws Exception
    {
        try
        {

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvAuditLogs.setLayoutManager(layoutManager);
            auditLogsAdapter = new AuditLogsAdapter(auditLogArrayList, this);
            rvAuditLogs.setAdapter(auditLogsAdapter);
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: \n" + err.toString());
        }
    }

    private void initializeData(String module) throws Exception
    {
        try
        {
            String query = "SELECT * FROM auditLog WHERE module = '" + module +"'";
            auditLogArrayList = AuditLog.load(this, module.equals("ALL") ? null : query);

            rvAuditLogs.setLayoutManager( new LinearLayoutManager(this));
            auditLogsAdapter = new AuditLogsAdapter(auditLogArrayList, this);
            rvAuditLogs.setAdapter(auditLogsAdapter);
             auditLogsAdapter.notifyDataSetChanged();
        }
        catch (Exception err)
        {
            Debugger.logD("Exception " + err.getMessage());
            throw new Exception("Error initializing data: " + err.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        try
        {
            getMenuInflater().inflate(R.menu.request_filter_menu, menu);

            // Hide searching temporarily
            MenuItem searchItem = menu.findItem(R.id.action_search);

            searchItem.setVisible(false);
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error AuditLogsActivity onCreateOptionsMenu: " + err.toString());
        }

        return super.onCreateOptionsMenu(menu);
    }

    // Override Function to Add Function when option menu is pressed by user
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_filter)
        {
            openAudiLogsFilterDialog();
        }

        return super.onOptionsItemSelected(item);
    }


    private void openAudiLogsFilterDialog()
    {
        AuditLogFilterDialogFragment auditLogFilterDialogFragment = new AuditLogFilterDialogFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        auditLogFilterDialogFragment.show(fragmentManager, "AUDIT LOGS");
    }

    @Override
    public void onComplete(String result)
    {
        try
        {
            initializeData(result);
        }
        catch (Exception err)
        {
            Debugger.logD("Exception " +err.getMessage());
        }
    }
}