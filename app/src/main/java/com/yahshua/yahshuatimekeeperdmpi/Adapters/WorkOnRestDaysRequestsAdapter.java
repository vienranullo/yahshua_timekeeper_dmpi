package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnRestDayApprovalRequestActivity;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnRestday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;


public class WorkOnRestDaysRequestsAdapter extends RecyclerView.Adapter<WorkOnRestDaysRequestsAdapter.ViewHolder>
{
    private ArrayList<WorkOnRestday> workOnRestDayArrayList;
    private Context context;


    public WorkOnRestDaysRequestsAdapter(Context context, ArrayList<WorkOnRestday> workOnRestDayArrayList)
    {
        this.context = context;
        this.workOnRestDayArrayList = workOnRestDayArrayList;
    }

    @NonNull
    @Override
    public WorkOnRestDaysRequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.work_on_rest_day_request_item, parent, false);
        return new ViewHolder(contactView);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEmployeeName, tvDate, tvHours, tvReason;
        CardView cvWorkOnRestDayRequest;

        ViewHolder(View itemView)
        {
            super(itemView);

            tvEmployeeName = itemView.findViewById(R.id.tvEmployeeName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvHours = itemView.findViewById(R.id.tvHours);
            tvReason = itemView.findViewById(R.id.tvReason);
            cvWorkOnRestDayRequest = itemView.findViewById(R.id.cvWorkOnRestDayRequest);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        WorkOnRestday workOnRestday = workOnRestDayArrayList.get(position);

        holder.tvEmployeeName.setText(workOnRestday.getEmployeeName());
        holder.tvDate.setText(DateToString.displayFormat(workOnRestday.getDate()));
        holder.tvHours.setText(workOnRestday.getHours() + " hrs.");
        holder.tvReason.setText(workOnRestday.getReason());

        holder.cvWorkOnRestDayRequest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent coursesActivityIntent = new Intent(context, WorkOnRestDayApprovalRequestActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("WORK_ON_REST_DAY", workOnRestDayArrayList.get(position));
                coursesActivityIntent.putExtras(bundle);

                context.startActivity(coursesActivityIntent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return workOnRestDayArrayList.size();
    }
}
