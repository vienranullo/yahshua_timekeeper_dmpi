package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import java.util.ArrayList;


public class AuditLogsAdapter extends RecyclerView.Adapter<AuditLogsAdapter.ViewHolder>
{
    private ArrayList<AuditLog> auditLogArrayList;
    private Context context;


    public AuditLogsAdapter(ArrayList<AuditLog> auditLogArrayList, Context context)
    {
        this.auditLogArrayList = auditLogArrayList;
        this.context = context;
        Debugger.printO(auditLogArrayList.size());
    }

    @NonNull
    @Override
    public AuditLogsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.audit_log_item, parent, false);
        return new ViewHolder(contactView);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvModule, tvDateTime, tvAction, tvStatus, tvEmployeeName;
        CardView cvAuditLog;

        ViewHolder(View itemView)
        {
            super(itemView);

            tvModule = itemView.findViewById(R.id.tvModule);
            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvAction = itemView.findViewById(R.id.tvAction);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvEmployeeName = itemView.findViewById(R.id.tvEmployeeName);
            cvAuditLog = itemView.findViewById(R.id.cvAuditLog);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        AuditLog auditLog = auditLogArrayList.get(position);

        holder.tvModule.setText(auditLog.getModule());
        holder.tvDateTime.setText(DateToString.dateTimeFormat2(auditLog.getDateTime()));
        holder.tvAction.setText(auditLog.getAction());

        if (!TextUtils.isEmpty(auditLog.getEmployeeName())) holder.tvEmployeeName.setText(auditLog.getEmployeeName());
        else holder.tvEmployeeName.setText("");

        if (!TextUtils.isEmpty(auditLog.getStatus())) holder.tvStatus.setText(auditLog.getStatus());
        else holder.tvStatus.setText("");

    }

    @Override
    public int getItemCount()
    {
        return auditLogArrayList.size();
    }
}
