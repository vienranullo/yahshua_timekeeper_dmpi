package com.yahshua.yahshuatimekeeperdmpi.Interfaces;

import android.content.DialogInterface;

public interface DialogAlertInterfaces {

    public abstract void PositiveMethod(DialogInterface dialog, int id);
    public abstract void NegativeMethod(DialogInterface dialog, int id);

}
