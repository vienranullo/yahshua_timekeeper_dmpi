package com.yahshua.yahshuatimekeeperdmpi.CustomUI;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

public class SurfaceViewSquare extends SurfaceView {
    public SurfaceViewSquare(Context context) {
        super(context);
    }

    public SurfaceViewSquare(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SurfaceViewSquare(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SurfaceViewSquare(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int size = width > height ? height : width;
        setMeasuredDimension(size, size); // make it square
    }
}

