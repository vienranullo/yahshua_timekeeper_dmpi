package com.yahshua.yahshuatimekeeperdmpi.Interfaces;

public interface AsyncResponseListener
{
    void onSuccess(Object object);
    void onFailure(int errorCode, String error);
}