package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;


public class LoadingDialogFragment extends DialogFragment
{
    private String message;
    private TextView tvLoading;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) message = getArguments().getString("MESSAGE");
        }
        catch (Exception err)
        {
            Utility.showError(getFragmentManager(), "Error LoadingDialogFragment onCreate: " + err.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        if (getDialog().getWindow() != null) getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.loading_dialog_fragment, container, false);
        tvLoading = view.findViewById(R.id.tvLoading);

        if (TextUtils.isEmpty(message)) message = "Loading...";
        tvLoading.setText(message);

        return view;
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params;
        if (getDialog().getWindow() != null) params = getDialog().getWindow().getAttributes(); else return;

        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    public void changeMessage(String message)
    {
        tvLoading.setText(message);
    }
}
