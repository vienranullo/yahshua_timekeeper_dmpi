package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class Workdays implements Parcelable
{
    private boolean isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, isSunday;
    private int employeeId, scheduleId;

    public Workdays(){}

    protected Workdays(Parcel in)
    {
        isMonday = in.readByte() != 0;
        isTuesday = in.readByte() != 0;
        isWednesday = in.readByte() != 0;
        isThursday = in.readByte() != 0;
        isFriday = in.readByte() != 0;
        isSaturday = in.readByte() != 0;
        isSunday = in.readByte() != 0;
        employeeId = in.readInt();
        scheduleId = in.readInt();
    }

    public static final Creator<Workdays> CREATOR = new Creator<Workdays>() {
        @Override
        public Workdays createFromParcel(Parcel in) { return new Workdays(in); }

        @Override
        public Workdays[] newArray(int size) { return new Workdays[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeByte((byte) (isMonday ? 1 : 0));
        dest.writeByte((byte) (isTuesday ? 1 : 0));
        dest.writeByte((byte) (isWednesday ? 1 : 0));
        dest.writeByte((byte) (isThursday ? 1 : 0));
        dest.writeByte((byte) (isFriday ? 1 : 0));
        dest.writeByte((byte) (isSaturday ? 1 : 0));
        dest.writeByte((byte) (isSunday ? 1 : 0));
        dest.writeInt(employeeId);
        dest.writeInt(scheduleId);
    }


    public boolean isMonday()
    {
        return isMonday;
    }

    public void setMonday(boolean monday)
    {
        isMonday = monday;
    }

    public boolean isTuesday()
    {
        return isTuesday;
    }

    public void setTuesday(boolean tuesday)
    {
        isTuesday = tuesday;
    }

    public boolean isWednesday()
    {
        return isWednesday;
    }

    public void setWednesday(boolean wednesday)
    {
        isWednesday = wednesday;
    }

    public boolean isThursday()
    {
        return isThursday;
    }

    public void setThursday(boolean thursday)
    {
        isThursday = thursday;
    }

    public boolean isFriday()
    {
        return isFriday;
    }

    public void setFriday(boolean friday)
    {
        isFriday = friday;
    }

    public boolean isSaturday()
    {
        return isSaturday;
    }

    public void setSaturday(boolean saturday)
    {
        isSaturday = saturday;
    }

    public boolean isSunday()
    {
        return isSunday;
    }

    public void setSunday(boolean sunday)
    {
        isSunday = sunday;
    }

    public int getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(int employee)
    {
        this.employeeId = employee;
    }

    public int getScheduleId()
    {
        return scheduleId;
    }

    public void setScheduleId(int schedule)
    {
        this.scheduleId = schedule;
    }

    public static void deleteAll(DbAdapter dbAdapter) throws SQLException
    {
        dbAdapter.getDb().delete("workDays", "", new String[]{});
    }

    public static Workdays get(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<Workdays> workdaysArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM workDays";

        try {
            if (searchQuery != null) selectQuery += " " + searchQuery;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    Workdays workdays = new Workdays();

                    workdays.setMonday(cursor.getInt(cursor.getColumnIndex("isMonday")) > 0);
                    workdays.setTuesday(cursor.getInt(cursor.getColumnIndex("isTuesday")) > 0);
                    workdays.setWednesday(cursor.getInt(cursor.getColumnIndex("isWednesday")) > 0);
                    workdays.setThursday(cursor.getInt(cursor.getColumnIndex("isThursday")) > 0);
                    workdays.setFriday(cursor.getInt(cursor.getColumnIndex("isFriday")) > 0);
                    workdays.setSaturday(cursor.getInt(cursor.getColumnIndex("isSaturday")) > 0);
                    workdays.setSunday(cursor.getInt(cursor.getColumnIndex("isSunday")) > 0);
                    workdays.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    workdays.setScheduleId(cursor.getInt(cursor.getColumnIndex("scheduleId")));

                    workdaysArrayList.add(workdays);

                } while (cursor.moveToNext());
            }

            if (workdaysArrayList.size() > 0) return workdaysArrayList.get(workdaysArrayList.size() - 1);
            else return new Workdays();

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Workdays get error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, "Database Error").show();
            }
            return workdaysArrayList.get(0);
        }
    }

    public boolean save( DbAdapter dbAdapter)
    {
        ContentValues contentValues = new ContentValues();
        try
        {
            contentValues.put("isMonday", isMonday());
            contentValues.put("isTuesday", isTuesday());
            contentValues.put("isWednesday", isWednesday());
            contentValues.put("isThursday", isThursday());
            contentValues.put("isFriday", isFriday());
            contentValues.put("isSaturday", isSaturday());
            contentValues.put("isSunday", isSunday());
            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("scheduleId", getScheduleId());

            dbAdapter.getDb().insertOrThrow("workDays", null, contentValues);
            return true;

        } catch (Exception err)
        {
            ErrorLogs.save("Workdays save error "+err.getMessage(), dbAdapter);
            return false;
        }
    }
}