package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yahshua.yahshuatimekeeperdmpi.Activities.EmployeeLogsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.TimeIn;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.InputChapaNoDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.LogOutAuthenticationDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.SyncAllDataDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.IDialogCloseInterface;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.StringCompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.KioskMode;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserPreferences;
import com.yahshua.yahshuatimekeeperdmpi.services.UpdateDeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ToastNotification;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class HomeFragment extends Fragment implements IDialogCloseInterface.onInputIdCompleteListener, StringCompleteListener
{
    //Context & View
    private Context context;
    private View view;

    //Widgets
    private TextView tvUnsyncedCount, tvDateSyncAll;
    private LinearLayout layoutSync;
    private Button btnTimeIn, btnTimeOut, btnViewLog, btnUploadAll, btnUpdateApp;

    //Data
    private DeviceSettings deviceSettings;
    private Employee employee;
    private KioskMode kioskMode = KioskMode.getKioskMode();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view    = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();

        initializeUI(view);
        loadDeviceSettings(getContext());
        countUnSyncedData();

        // Get version name
        try
        {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            TextView tvVersionName = view.findViewById(R.id.tvVersionName);
            tvVersionName.setText("Version: " + version);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Debugger.logD("Getting version Error: " + e.toString());
        }

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // Once the time in activity closed,
        // detect if timein is cancelled by user.
        if (resultCode == 0)
        {
            Toasty.warning(getContext(), "Log Cancelled").show();
        }

        countUnSyncedData();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        loadDeviceSettings(getContext());
        countUnSyncedData();
    }

    public void loadDeviceSettings(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String deviceSettingsJSON = sharedPreferences.getString("DEVICE_SETTINGS", "");

        if (!deviceSettingsJSON.equals(""))
        {
            Gson gson = new Gson();
            deviceSettings = gson.fromJson(deviceSettingsJSON, DeviceSettings.class);
            setVisibleMenuItems();
        }
        else
        {
            downloadDeviceSettings();
        }
    }

    private void downloadDeviceSettings()
    {
        // Show progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Please Wait..");
        progressDialog.setMessage("Updating device settings..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        final Handler handler = new Handler(Looper.getMainLooper())
        {
            @Override
            public void handleMessage(Message message)
            {
                progressDialog.dismiss();

                switch (message.what)
                {
                    // Success
                    case 1:
                        ToastNotification.success(getContext(), "Device Settings Updated");
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                        String deviceSettingsJSON = sharedPreferences.getString("DEVICE_SETTINGS", "");
                        Gson gson = new Gson();
                        deviceSettings = gson.fromJson(deviceSettingsJSON, DeviceSettings.class);
                        setVisibleMenuItems();
                        break;
                    // Error
                    case 2:
                        Utility.showError(getFragmentManager(), message.obj.toString());
                        break;
                }
            }
        };

        ThreadGroup threadGroup = new ThreadGroup("downloadDeviceSettings");
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    UpdateDeviceSettings.downloadDeviceSettings(getContext(), handler);
                }
                catch (Exception err)
                {
                    Utility.sendHandlerMessage(handler, 2, "Error Runnable: \n" + err.toString());
                }
            }
        };

        new Thread(threadGroup, runnable, "downloadDeviceSettings", 3000000).start();
    }

    // Initialize component from a layout file
    private void initializeUI(View view)
    {
        layoutSync    = view.findViewById(R.id.layout_no_sync);
        btnTimeIn     = view.findViewById(R.id.btnTimeIn);
        tvDateSyncAll = view.findViewById(R.id.tvDateSyncAll);
        btnUpdateApp  = view.findViewById(R.id.btnUpdateApp);

        btnTimeIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeIn();
            }
        });

        // Tme-out
        btnTimeOut = view.findViewById(R.id.btnTimeOut);
        btnTimeOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeOut();

            }
        });

        // Select Application
        btnViewLog = view.findViewById(R.id.btnViewLog);
        btnViewLog.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openChapaNoDialog();
            }
        });

        btnUploadAll = view.findViewById(R.id.btnUploadAll);

        btnUploadAll.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(Utility.haveNetworkConnection(context, getFragmentManager()))
                {
                    openSyncAllDialog();
                }
            }
        });

        btnUpdateApp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if(kioskMode.isLocked(getContext()))
                    {
                        openLogoutDialog();
                    }
                    else if(Utility.haveNetworkConnection(context))
                    {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                        startActivity(intent);
                    }
                    else
                    {
                        Toasty.warning(context, context.getString(R.string.internet_required)).show();
                    }
                }catch (Exception err)
                {
                    Toasty.warning(context, context.getString(R.string.server_error)).show();
                    Debugger.logD("btnUpdateApp onClick "+err.getMessage());
                }
            }
        });
    }

    private void openLogoutDialog()
    {
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_LOGOUT", false);
        LogOutAuthenticationDialog logOutAuthenticationDialog = new LogOutAuthenticationDialog();
        logOutAuthenticationDialog.setArguments(bundle);
        logOutAuthenticationDialog.show(getFragmentManager(), "");
    }

    private void openChapaNoDialog()
    {
        Bundle bundle = new Bundle();
        bundle.putString("APPLICATION_NAME", context.getString(R.string.view_logs));
        InputChapaNoDialog inputChapaNoDialog = new InputChapaNoDialog();
        inputChapaNoDialog.setArguments(bundle);
        inputChapaNoDialog.setTargetFragment(this, 100);
        inputChapaNoDialog.show(getFragmentManager(),"EMPLOYEE_SYSTEM_ID");
    }


    public void countUnSyncedData()
    {
        final ArrayList<EmployeeLogs> employeeLogs = EmployeeLogs.read(context,true,0, 20);

        tvUnsyncedCount = view.findViewById(R.id.tv_unsynced_count);

        if(UserPreferences.getDateSyncAll(context).equals(""))
        {
            tvDateSyncAll.setVisibility(View.GONE);
        }
        else
        {
            tvDateSyncAll.setText("Last upload in Time in - out logs   "+UserPreferences.getDateSyncAll(context));
            tvDateSyncAll.setVisibility(View.VISIBLE);
        }

        if(employeeLogs.size() == 0)
        {
            tvUnsyncedCount.setVisibility(View.GONE);
        }else{
            tvUnsyncedCount.setVisibility(View.VISIBLE);
        }

        tvUnsyncedCount.setText(employeeLogs.size()+"");
    }

    private void openSyncAllDialog()
    {
        if (EmployeeLogs.read(context,true,0, 20).size() == 0)
        {
            ToastNotification.info(context, "No time-in/out logs to upload");
        }
        else
        {
            SyncAllDataDialog uploadAllDialogFragment = new SyncAllDataDialog();
            uploadAllDialogFragment.setTargetFragment(this, 100);
            uploadAllDialogFragment.show(getFragmentManager(), "UploadAllTag");
            uploadAllDialogFragment.setCancelable(false);
        }
    }

    public void onFilterDone()
    {
        countUnSyncedData();
    }

    private void setVisibleMenuItems()
    {
        if (!deviceSettings.getVisibleMenuItems().isTimeIn()) btnTimeIn.setVisibility(View.GONE); else btnTimeIn.setVisibility(View.VISIBLE);
        if (!deviceSettings.getVisibleMenuItems().isTimeOut()) btnTimeOut.setVisibility(View.GONE); else btnTimeOut.setVisibility(View.VISIBLE);
        if (!deviceSettings.getVisibleMenuItems().isApplications()) btnViewLog.setVisibility(View.GONE); else btnViewLog.setVisibility(View.VISIBLE);
        if (!deviceSettings.getVisibleMenuItems().isUploadAll()) btnUploadAll.setVisibility(View.GONE); else btnUploadAll.setVisibility(View.VISIBLE);
    }

    private boolean checkIfEmployeesExists(DbAdapter dbAdapter)
    {
        return Employee.read(getContext(), null, dbAdapter).size() > 0;
    }

    private void timeIn()
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        if (!deviceSettings.isAutoDateTime())
        {
            if (checkIfEmployeesExists(dbAdapter)) {
                Intent intent = new Intent(getContext(), TimeIn.class);
                intent.putExtra("LOG_TYPE", "IN");
                startActivityForResult(intent, 101);
            } else {
                Toasty.warning(getContext(), getString(R.string.label_no_employees)).show();
            }
        }else{
            if((Utility.dateAndTimeDetection(context, getFragmentManager()))){
                if (checkIfEmployeesExists(dbAdapter)) {
                    Intent intent = new Intent(getContext(), TimeIn.class);
                    intent.putExtra("LOG_TYPE", "IN");
                    startActivityForResult(intent, 101);
                } else {
                    Toasty.warning(getContext(), getString(R.string.label_no_employees)).show();
                }
            }
        }
        dbAdapter.close();
    }

    private void timeOut()
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        if (!deviceSettings.isAutoDateTime())
        {
            if (checkIfEmployeesExists(dbAdapter)) {
                Intent intent = new Intent(getContext(), TimeIn.class);
                intent.putExtra("LOG_TYPE", "OUT");
                startActivityForResult(intent, 102);
            } else {
                Toasty.warning(getContext(), getString(R.string.label_no_employees)).show();
            }
        }else{
            if((Utility.dateAndTimeDetection(context, getFragmentManager()))){
                if (checkIfEmployeesExists(dbAdapter)) {
                    Intent intent = new Intent(getContext(), TimeIn.class);
                    intent.putExtra("LOG_TYPE", "OUT");
                    startActivityForResult(intent, 102);
                } else {
                    Toasty.warning(getContext(), getString(R.string.label_no_employees)).show();
                }
            }
        }

        dbAdapter.close();
    }

    @Override
    public void onInputComplete(Bundle bundle)
    {
        employee = bundle.getParcelable("EMPLOYEE_OBJECT");

        if(employee != null)
        {
            Intent intent = new Intent(context, EmployeeLogsActivity.class);
            intent.putExtra("EmployeeLogs", employee.getEmployeeid2());
            startActivity(intent);
        }
    }

    @Override
    public void onComplete(String result)
    {
        countUnSyncedData();
    }
    public void onPause()
    {
        super.onPause();
    }
    //End
}
