package com.yahshua.yahshuatimekeeperdmpi.DBClasses;

import java.util.ArrayList;
import java.util.Arrays;


public class DatabaseFields
{
    public static ArrayList<String> employeeFields = new ArrayList<>(
        Arrays.asList("ypo_id INTEGER",
                "employeeid2 INTEGER",
                "firstname TEXT",
                "middlename TEXT",
                "lastname TEXT",
                "employee_img TEXT",
                "company_id INT",
                "rfid TEXT"
            )
        );

    public static ArrayList<String> employeeLogFields = new ArrayList<>(
        Arrays.asList(
                "date DATE",
                "employee INTEGER",
                "log_img TEXT",
                "log_type TEXT",
                "log_time TEXT",
                "sync_id TEXT",
                "company_id INT",
                "is_sync INTEGER DEFAULT 0"
            )
        );

    static ArrayList<String> leaveTable = new ArrayList<>(
            Arrays.asList(
                    "employeeId INT",
                    "employeeName TEXT",
                    "day TEXT",
                    "dateFrom DATE",
                    "dateTo DATE",
                    "sync_id TEXT",
                    "company_id INT",
                    "is_sync INTEGER DEFAULT 0"
            )
    );

    static ArrayList<String> undertimeFields = new ArrayList<>(
            Arrays.asList(
                    "employeeId INT",
                    "employeeName TEXT",
                    "date DATE",
                    "hours DOUBLE",
                    "reason TEXT",
                    "requestedDate DATETIME DEFAULT (datetime('now', 'localtime'))",
                    "isSync BOOLEAN",
                    "company_id INT",
                    "status TEXT",
                    "remarks TEXT",
                    "lastSyncedDate DATE"
            )
    );

    static ArrayList<String> workOnRestdayFields = new ArrayList<>(
            Arrays.asList(
                    "employeeId INT",
                    "employeeName TEXT",
                    "date DATE",
                    "hours DOUBLE",
                    "reason TEXT",
                    "requestedDate DATETIME DEFAULT (datetime('now', 'localtime'))",
                    "isSync BOOLEAN",
                    "status TEXT",
                    "company_id INT",
                    "remarks TEXT",
                    "lastSyncedDate DATE"
            )
    );

    static ArrayList<String> workOnHolidayFields = new ArrayList<>(
            Arrays.asList(
                    "employeeId INT",
                    "employeeName TEXT",
                    "date DATE",
                    "hours DOUBLE",
                    "reason TEXT",
                    "requestedDate DATETIME DEFAULT (datetime('now', 'localtime'))",
                    "isSync BOOLEAN",
                    "status TEXT",
                    "company_id INT",
                    "remarks TEXT",
                    "lastSyncedDate DATE"
            )
    );

    static ArrayList<String> scheduleWorkdaysFields = new ArrayList<>(
            Arrays.asList(
                    "isMonday BOOLEAN",
                    "isTuesday BOOLEAN",
                    "isWednesday BOOLEAN",
                    "isThursday BOOLEAN",
                    "isFriday BOOLEAN",
                    "isSaturday BOOLEAN",
                    "isSunday BOOLEAN",
                    "employeeId INT",
                    "company_id INT",
                    "scheduleId INT"
            )
    );
}
