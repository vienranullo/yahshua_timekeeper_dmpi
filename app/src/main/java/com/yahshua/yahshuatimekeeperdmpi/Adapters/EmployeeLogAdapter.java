package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;

import java.util.ArrayList;


public class EmployeeLogAdapter extends ArrayAdapter<EmployeeLogs> {
    private final int mResource;
    private ArrayList<EmployeeLogs> memberList;
    private ImageView imageView;

    public EmployeeLogAdapter(@NonNull Context context, ArrayList<EmployeeLogs> list)
    {
        super(context, 0, list);
        memberList = list;
        mResource = 0;
    }

    @Override
    public int getPosition(@Nullable EmployeeLogs item)
    {
        int x = 0;
        for(EmployeeLogs student : memberList)
        {
            if(student.getId() == item.getId())
            {
                return x;
            }else{
                x++;
            }
        }

        return super.getPosition(item);
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension =  MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }


    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        try
        {
            final EmployeeLogs employeeLogs = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_employee_logs, parent, false);
            }

            imageView = convertView.findViewById(R.id.imageViewMember);
//            imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try
//                    {
//                        File myFile = new File(employeeLogs.getLogImg());
//
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        Uri uriForFile = FileProvider.getUriForFile(getContext(), "com.yahshua.yahshuatimekeeperdmpi.fileProvider", myFile);
//                        intent.setDataAndType(uriForFile, getMimeType(employeeLogs.getLogImg()));
//                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        v.getContext().startActivity(intent);
//
//                    } catch(Exception e)
//                    {
//                        ErrorLogs errorLogs=new ErrorLogs();
//
//                        String errorMessage = e.toString();
//                        errorLogs.setName("Employee Logs adapter error "+errorMessage);
//                        if(errorLogs.save(getContext())){
//                            Toasty.error(getContext(), e.toString()).show();
//                        }
//
//                    }
//                }
//            });

            TextView tvMemberName = convertView.findViewById(R.id.tvMemberName);
            TextView tvDate =  convertView.findViewById(R.id.tvDate);
            TextView tvTime =  convertView.findViewById(R.id.txTime);
            TextView tvTimeOut = convertView.findViewById(R.id.txTimeOut);

            tvMemberName.setText(employeeLogs.employeeObject.getFullName());
            tvDate.setText(employeeLogs.getDisplayDate());
            tvTime.setText("(" + employeeLogs.getLogType() +") " + employeeLogs.getDisplayLogTime());
            displayImage(employeeLogs);

            if(employeeLogs.isSync())
            {
                tvTimeOut.setText("SYNCED");
            }else{
                tvTimeOut.setText("NOT SYNCED");
            }

            return convertView;
        } catch (Exception err)
        {
            Toasters.ShowToast(getContext(), err.toString());
            return convertView;
        }


    }

    //Display list of image using Glide
    private void displayImage(EmployeeLogs employeeLogs)
    {
        RequestOptions myOption = new RequestOptions()
                .centerCrop()
                .circleCrop();

        Glide.with(getContext())
                .load(employeeLogs.getLogImg())
                .apply(myOption)
                .into(imageView);
    }

}
