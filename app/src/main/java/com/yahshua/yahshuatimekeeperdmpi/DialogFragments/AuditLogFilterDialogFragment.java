package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.yahshua.yahshuatimekeeperdmpi.Activities.AuditLogsActivity;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

public class AuditLogFilterDialogFragment extends DialogFragment
{
    // Context and Views
    private Context context;
    private View view;

    // Widget
    private Spinner spnModules;
    private Button btnFilter;

    private String selectedModules;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.dialog_frag_audit_log_filter, container, false);
        context = view.getContext();

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        initializeUI(view);
        return view;
    }

    private void initializeUI(View view)
    {
        spnModules = view.findViewById(R.id.spnModules);
        btnFilter  = view.findViewById(R.id.btnFilter);

        String[] modules = context.getResources().getStringArray(R.array.list_modules);
        ArrayAdapter<String> modulesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, modules);
        spnModules.setAdapter(modulesAdapter);

        spnModules.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedModules = spnModules.getSelectedItem().toString();
                Debugger.logD("selectedModules " +selectedModules);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);
        ((AuditLogsActivity) getActivity()).onComplete(selectedModules);
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,400);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

}
