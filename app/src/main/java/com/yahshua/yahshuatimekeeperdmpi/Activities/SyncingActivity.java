package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ToastNotification;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;

import es.dmoral.toasty.Toasty;
import io.netopen.hotbitmapgg.library.view.RingProgressBar;

public class SyncingActivity extends AppCompatActivity
{
    // Context
    private Context context;

    //Widgets
    private ImageView imgEmployee;
    private Button btnEmployees;
    private TextView tvEmployeeError,  tvEmployeeRequesting, tvEmployeeProgress;
    private RingProgressBar pbEmploy;

    //Data
    private Handler handler = new Handler();
    private boolean isRequesting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syncing);
        context = this;

        InitializeUI();
    }

    @Override
    public void onBackPressed()
    {
        if(!isRequesting)
        {
            super.onBackPressed();
        }
        else
        {
            Toasty.warning(context, context.getString(R.string.pls_wait)).show();
        }
    }

    private void InitializeUI()
    {
        btnEmployees           = findViewById(R.id.btnEmployees);
        pbEmploy               = findViewById(R.id.pbEmploy);
        tvEmployeeError        = findViewById(R.id.tvEmployeeError);
        imgEmployee            = findViewById(R.id.imgEmployee);
        tvEmployeeRequesting   = findViewById(R.id.tvEmployeeRequesting);
        tvEmployeeProgress     = findViewById(R.id.tvEmployeeProgress);

        btnEmployees.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!isRequesting)
                {
                    new UpdateData(context, handler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                else
                {
                    Toasty.warning(context, context.getString(R.string.pls_wait)).show();
                }
            }
        });
    }

    private class UpdateData extends AsyncTask<String, Integer, String>
    {

        private Context context;
        private String success = "";
        private Handler handler;

        public UpdateData(Context context, Handler handler)
        {
            this.context = context;
            this.handler = handler;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            isRequesting = true;
        }

        @Override
        protected String doInBackground(final String... strings)
        {
            try
            {
                TransactionSyncer uploadData = new TransactionSyncer(context);

                uploadData.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(final String fromWhere, String successMessage)
                    {
                        handler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if ("Employee".equals(fromWhere))
                                {
                                    btnEmployees.setText(context.getString(R.string.download));
                                    tvEmployeeError.setVisibility(View.VISIBLE);
                                    tvEmployeeRequesting.setVisibility(View.GONE);
                                    tvEmployeeError.setText(getString(R.string.download_complete));
                                    tvEmployeeError.setTextColor(getResources().getColor(R.color.colorAccent));
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(final int errorCode, final String errorMessage, final String fromWhere)
                    {
                        handler.post(new Runnable()
                        {

                            @Override
                            public void run()
                            {
                                if ("Employee".equals(fromWhere))
                                {
                                    tvEmployeeError.setText(errorMessage);
                                    tvEmployeeError.setVisibility(View.VISIBLE);
                                    tvEmployeeError.setTextColor(Color.RED);
                                }
                            }
                        });
                    }

                    @Override
                    public void onUpdate(final int max, final int progress,final String currentDownload)
                    {
                        handler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if ("Employee".equals(currentDownload))
                                {
                                    pbEmploy.setMax(max);
                                    pbEmploy.setProgress(progress);
                                    imgEmployee.setVisibility(View.GONE);
                                    tvEmployeeRequesting.setText(getString(R.string.saving));
                                    tvEmployeeProgress.setVisibility(View.VISIBLE);
                                    tvEmployeeProgress.setText((progress * 100) / max + "%");
                                }
                            }
                        });
                    }

                    @Override
                    public void showSpinner(final boolean isShow, final String currentDownload)
                    {
                        handler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                if ("Employee".equals(currentDownload))
                                {
                                    tvEmployeeRequesting.setText(getString(R.string.requesting));
                                    tvEmployeeRequesting.setVisibility(isShow ? View.VISIBLE : View.GONE);
                                    tvEmployeeError.setVisibility(isShow ? View.INVISIBLE : View.VISIBLE);
                                    if (!isShow)
                                    {
                                        imgEmployee.setVisibility(View.VISIBLE);
                                        tvEmployeeProgress.setVisibility(View.GONE);
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }
                });

                uploadData.downloadEmployee(true);

                return success;
            }
            catch (Exception err)
            {
                return success;
            }
        }

        @Override
        protected void onPostExecute(String success)
        {
            isRequesting = false;
            if (success.equals(""))
            {
                finish();
                ToastNotification.success(context, context.getString(R.string.successful_downloaded));
            }
            else
            {
                ToastNotification.warning(context, context.getString(R.string.failed_download));
            }
        }
    }
}
