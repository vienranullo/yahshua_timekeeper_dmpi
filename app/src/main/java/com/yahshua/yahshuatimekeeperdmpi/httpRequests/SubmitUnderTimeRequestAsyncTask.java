package com.yahshua.yahshuatimekeeperdmpi.httpRequests;

import android.content.Context;
import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class SubmitUnderTimeRequestAsyncTask extends AsyncTask<Void, Void, Void>
{
    private WeakReference<Context> contextWeakReference;
    private AsyncResponseListener asyncResponseListener;
    private int errorCode = 1;
    private String error;
    private JSONObject jsonObject;


    public SubmitUnderTimeRequestAsyncTask(Context context, AsyncResponseListener asyncResponseListener, JSONObject jsonObject)
    {
        this.asyncResponseListener = asyncResponseListener;
        this.jsonObject = jsonObject;
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected void onPreExecute()
    {
        try
        {
            // Check internet connection
            Context context = contextWeakReference.get();

            if (!Utility.haveNetworkConnection(context))
            {
                errorCode = 2;
                cancel(true);
            }
        }
        catch (Exception err)
        {
            error = "Error onPreExecute: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            Context context = contextWeakReference.get();

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        Debugger.logD("Success Response: " + new String(responseBody));
                    }
                    catch (Exception err)
                    {
                        error = "Error asyncHttpResponseHandler onSuccess: \n" + err.toString();
                        cancel(true);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (statusCode == 0)
                        {
                            SubmitUnderTimeRequestAsyncTask.this.errorCode = 3;
                        }
                        else
                        {
                            SubmitUnderTimeRequestAsyncTask.this.errorCode = 4;
                            SubmitUnderTimeRequestAsyncTask.this.error = new String(responseBody);
                        }
                    }
                    catch (Exception err)
                    {
                        SubmitUnderTimeRequestAsyncTask.this.error = "Error asyncHttpResponseHandler onFailure: \n" + err.toString();
                    }

                    cancel(true);
                }
            };

            HttpProvider.postSync(context, "undertime_requests/approve/", new StringEntity(jsonObject.toString()), true, asyncHttpResponseHandler);
        }
        catch (Exception err)
        {
            error = "Error doInBackground: \n" + err.toString();
            cancel(true);
        }

        return null;
    }

    @Override
    protected void onCancelled()
    {
        asyncResponseListener.onFailure(errorCode, error);
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        asyncResponseListener.onSuccess(null);
    }
}
