package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.ExistingLogDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.OnDialogComplete;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.BreakIntervalSettingsList;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.services.SyncingService;
import com.yahshua.yahshuatimekeeperdmpi.services.UpdateDeviceSettings;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Constants;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ImageHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.PopUpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.SettingsHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ToastNotification;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserPreferences;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class TimeIn extends BaseActivity implements SurfaceHolder.Callback, View.OnClickListener, OnDialogComplete {
    // Context
    private Context context;

    // Widgets
    private TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv0, tvClear, tvDone;
    private Employee selectedEmployee = new Employee();
    private TextView tvEmployeeName, tvLogType;
    private EditText etEmployeeCode;
    public SurfaceHolder sHolder;
    private SurfaceView sv;
    public Camera mCamera;

    // Data
    DecimalFormat df = new DecimalFormat("0000000");
    private String employeeSyncId;
    String logType;
    boolean camCondition = false;
    private File pictureFile;
    private StringBuilder sb;
    int minutesPassed = 0, hoursPassed = 0;
    private DeviceSettings deviceSettings;

    // Storage Permissions
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 1;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_in);


    }

    @Override
    protected void onStart()
    {
        super.onStart();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        initializeUI();
        loadDeviceSettings(context);

        logType = getIntent().getStringExtra("LOG_TYPE");

        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        final ArrayList<Employee> employeeList = Employee.read(this, null, dbAdapter);

        dbAdapter.close();
        getWindow().setFormat(PixelFormat.UNKNOWN);
        sv = findViewById(R.id.camerePreview);
        sHolder = sv.getHolder();
        sHolder.addCallback(this);
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        etEmployeeCode = findViewById(R.id.etStudentCode);
        tvEmployeeName = findViewById(R.id.txStudentName);
        tvLogType = findViewById(R.id.txLogType);
        tvLogType.setText("TIME " + logType);

        verifyStoragePermissions(this);

        etEmployeeCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String code = etEmployeeCode.getText().toString();
                tvDone.setTextColor(Color.WHITE);

                if (employeeList.size() > 0) {

                    for (Employee employee : employeeList) {
                        selectedEmployee = null;
                        tvEmployeeName.setText("...");

                        if (code.equals(String.valueOf(employee.getEmployeeid2()))) {
                            String displayName = employee.getFullName();

                            tvEmployeeName.setText(displayName);
                            selectedEmployee = employee;
                            tvDone.setTextColor(Color.GREEN);
                            break;
                        }
                    }

                } else {
                    Toasty.error(context, getString(R.string.label_no_employees), Toast.LENGTH_LONG).show();
                }
            }
        });

        etEmployeeCode.setEnabled(false);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == STORAGE_PERMISSION_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                restartCamera();
            } else {
                Toasty.warning(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        getTagInfo(intent);
    }

    //Time in using rfid
    private void getTagInfo(Intent intent)
    {
        try
        {
            DbAdapter dbAdapter = new DbAdapter(context);
            dbAdapter.open();

            // Get serial no.
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            byte[] tagInfo = tag.getId();
            int maxLength = tagInfo.length;
            sb = new StringBuilder();
            int count = 1;

            for (byte b : tagInfo)
            {
                sb.append(count < maxLength ? String.format("%02X:", b) : String.format("%02X", b));
                count++;
            }

            Debugger.logD(sb.toString());
            Employee employee = Employee.readEmployeeByRFID(context, sb.toString(), dbAdapter);

            if (employee != null) {

                tvEmployeeName.setText(employee.getFullName());
                etEmployeeCode.setText(String.valueOf(employee.getEmployeeid2()));
                selectedEmployee = employee;

                if (deviceSettings.isAutoDateTime())
                {
                   if( Utility.dateAndTimeDetection(context, getSupportFragmentManager())){
                       takePicture();
                   }
                }else {
                    takePicture();
                }

            } else {
                Toasty.warning(context, "Card is not registered", Toast.LENGTH_SHORT).show();
                tvEmployeeName.setText("");
                etEmployeeCode.setText("");
                selectedEmployee = null;
            }

            dbAdapter.close();
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error getting tag info: \n" + err.toString());
        }
    }


    public void loadDeviceSettings(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String deviceSettingsJSON = sharedPreferences.getString("DEVICE_SETTINGS", "");

        if (!deviceSettingsJSON.equals(""))
        {
            Gson gson = new Gson();
            deviceSettings = gson.fromJson(deviceSettingsJSON, DeviceSettings.class);
        }
        else
        {
            downloadDeviceSettings();
        }
    }

    private void downloadDeviceSettings()
    {
        // Show progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please Wait..");
        progressDialog.setMessage("Updating device settings..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        final Handler handler = new Handler(Looper.getMainLooper())
        {
            @Override
            public void handleMessage(Message message)
            {
                progressDialog.dismiss();

                switch (message.what)
                {
                    // Success
                    case 1:
                        ToastNotification.success(context, "Device Settings Updated");
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                        String deviceSettingsJSON = sharedPreferences.getString("DEVICE_SETTINGS", "");
                        Gson gson = new Gson();
                        deviceSettings = gson.fromJson(deviceSettingsJSON, DeviceSettings.class);
                        break;
                    // Error
                    case 2:
                        Utility.showError(getSupportFragmentManager(), message.obj.toString());
                        break;
                }
            }
        };

        ThreadGroup threadGroup = new ThreadGroup("downloadDeviceSettings");
        Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    UpdateDeviceSettings.downloadDeviceSettings(context, handler);
                }
                catch (Exception err)
                {
                    Utility.sendHandlerMessage(handler, 2, "Error Runnable: \n" + err.toString());
                }
            }
        };

        new Thread(threadGroup, runnable, "downloadDeviceSettings", 3000000).start();
    }

    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Return to home?", "Yes", "No");
    }

    // Initialize component from a layout file
    private void initializeUI() {
        tv0 = findViewById(R.id.t9_key_0);
        tv0.setOnClickListener(this);
        tv1 = findViewById(R.id.t9_key_1);
        tv1.setOnClickListener(this);
        tv2 = findViewById(R.id.t9_key_2);
        tv2.setOnClickListener(this);
        tv3 = findViewById(R.id.t9_key_3);
        tv3.setOnClickListener(this);
        tv4 = findViewById(R.id.t9_key_4);
        tv4.setOnClickListener(this);
        tv5 = findViewById(R.id.t9_key_5);
        tv5.setOnClickListener(this);
        tv6 = findViewById(R.id.t9_key_6);
        tv6.setOnClickListener(this);
        tv7 = findViewById(R.id.t9_key_7);
        tv7.setOnClickListener(this);
        tv8 = findViewById(R.id.t9_key_8);
        tv8.setOnClickListener(this);
        tv9 = findViewById(R.id.t9_key_9);
        tv9.setOnClickListener(this);

        tvClear = findViewById(R.id.t9_key_clear);
        tvClear.setOnClickListener(this);
        tvDone = findViewById(R.id.t9_key_done);
        tvDone.setOnClickListener(this);
    }

    private boolean checkExistingLog(Employee employee)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        tvDone.setEnabled(true);
        EmployeeLogs employeeLogs = employee.checkExistingLogs(logType, dbAdapter);

        if (employeeLogs != null)
        {
            employeeLogs.employeeObject.setFullName(employee.getFullName());
            Bundle bundle = new Bundle();
            bundle.putParcelable("EMPLOYEE_LOGS", employeeLogs);

            ExistingLogDialog existingLogDialog = new ExistingLogDialog();
            existingLogDialog.setArguments(bundle);
            existingLogDialog.show(getSupportFragmentManager(),"");

            return true;
        }

        dbAdapter.close();
        return false;
    }

    private boolean validateLog() {

        if (etEmployeeCode.getText().toString().equals("") || selectedEmployee == null || selectedEmployee.getEmployeeid2() == 0) {
            Toasters.ShowToast(context, "No Employee Selected");
            return false;
        }

        if (EmployeeLogs.checkIfIntervalNotPassed(context, selectedEmployee.getId(), DateTimeHandler.convertDatetoStringDate(new Date()))) {
            String message = "You need to wait for " + Constants.waitingMinutes + " minute";

            if (Constants.waitingMinutes > 1) {
                message += "s";
                Toasty.warning(context, message).show();
            }

            tvDone.setTextColor(Color.WHITE);
            return false;
        }
        return  true;
    }

    @SuppressWarnings("deprecation")
    private void takePicture()
    {

        if(SettingsHandler.haveBreakInterval(context)) {
            String timeMessage;
            if (mCamera != null && validateLog()) {

                if (logType.equals("OUT") && timeInterVal()) {
                    savingLog();
                } else if (logType.equals("IN") && !EmployeeLogs.checkOutInterval(context, selectedEmployee.getId(), DateTimeHandler.convertDatetoStringDate(new Date()))) {
                    savingLog();
                } else if (logType.equals("IN") && EmployeeLogs.checkOutInterval(context, selectedEmployee.getId(), DateTimeHandler.convertDatetoStringDate(new Date()))) {
                    String message = "You need to wait for " + Constants.waitingMinutes + " minute";

                    if (Constants.waitingMinutes > 1) {
                        message += "s";
                        Toasty.warning(context, message).show();
                    }
                } else {

                    if (hoursPassed == 0 && minutesPassed != 0) {
                        timeMessage = minutesPassed + " " + (minutesPassed == 1 ? "minute." : "minutes.");
                    } else if (hoursPassed != 0 && minutesPassed == 0) {
                        timeMessage = hoursPassed + " " + (hoursPassed == 1 ? "hour." : "hours.");
                    } else {
                        timeMessage = hoursPassed + (hoursPassed == 1 ? "hour and " : "hour and ") + minutesPassed + " " + (minutesPassed == 1 ? "minute." : "minutes.");
                    }

                    Toasty.warning(context, "You cannot time out this time, Wait after " + timeMessage, Toast.LENGTH_LONG).show();
                }
            }
        } else{
            if (mCamera != null && validateLog()) {
                savingLog();
            }
        }

    }

    private void savingLog(){

        mCamera.takePicture(new Camera.ShutterCallback() {
            @Override
            public void onShutter() {
                MediaActionSound sound = new MediaActionSound();
                sound.play(MediaActionSound.SHUTTER_CLICK);
            }
        }, null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                getCameraPath(ImageHandler.formatImage(data, sv));
                EmployeeLogs employeeLogs = new EmployeeLogs();
                employeeLogs.setEmployeeId(selectedEmployee.getId());
                employeeLogs.setLogImg(pictureFile.toString());
                employeeLogs.setLogType(logType);
                employeeSyncId = df.format(selectedEmployee.getId());
                employeeLogs.setSyncId(employeeSyncId + "__" + DateTimeHandler.getTimeStamp());
                employeeLogs.save(context);

                AuditLog.saveAuditLog(context,"TIME IN","IN","SUCCESS",selectedEmployee.getFullName());

                stopCamera(camera);

                if (UserPreferences.getAutoInstantSyncPreferences(context))
                {
                    ArrayList<EmployeeLogs> read = EmployeeLogs.read(context,true,0, 20);

                    if(Utility.haveNetworkConnection(context) && read.size() > 0)
                    {
                        Intent service = new Intent(getApplicationContext(), SyncingService.class);
                        service.putExtra("NOTIFY", false);
                        startService(service);
                    }
                }

                if (!UserSession.isContinousMode(context))
                {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    selectedEmployee = null;
                    etEmployeeCode.setText(null);
                    tvDone.setEnabled(true);
                    restartCamera();
                }
            }
        });
    }

    private boolean timeInterVal(){

        try {

            int largest = 0, currentTime = 0, currentMin, count = 0;
            String secondLargest = "", startTimeOut = "", endTimeOut = "", timeInterval;

            BreakIntervalSettingsList breakIntervalSettingsList1 = new BreakIntervalSettingsList(context);

            Calendar rightNow = Calendar.getInstance();
            Date Hour = rightNow.getTime();
            String currentHour = DateTimeHandler.convertDatetoStringDate1(Hour);

            for (int i = 0; i < breakIntervalSettingsList1.getArrayList(context).size(); i++) {

                Calendar cTime = Calendar.getInstance();
                Calendar sTime = Calendar.getInstance();
                cTime.setTime(DateTimeHandler.convertHourToString(currentHour));
                sTime.setTime(DateTimeHandler.convertHourToString(breakIntervalSettingsList1.getArrayList(context).get(i).getTimeOutStart()));

                currentTime = cTime.get(Calendar.HOUR_OF_DAY);
                currentMin = cTime.get(Calendar.MINUTE);
                int startTimeOutHour = sTime.get(Calendar.HOUR_OF_DAY);
                int startTimeOutMin = sTime.get(Calendar.MINUTE);


                if (currentTime == startTimeOutHour) {

                    startTimeOut = breakIntervalSettingsList1.getArrayList(context).get(i).getTimeOutStart();
                    endTimeOut = breakIntervalSettingsList1.getArrayList(context).get(i).getTimeOutEnd();
                    timeInterval = String.valueOf(breakIntervalSettingsList1.getArrayList(context).get(i).getInterval());

                    SettingsHandler.setMinutesInterval2(context, timeInterval);
                }

                if (startTimeOutHour > largest) {
                    largest = startTimeOutHour;
                }

                if(currentTime == startTimeOutHour && currentMin <= startTimeOutMin){
                    count++;
                    if(count == 1) {
                        secondLargest = breakIntervalSettingsList1.getArrayList(context).get(i).getTimeOutStart();
                    }
                }

                if(currentTime < startTimeOutHour){
                    count++;
                    if(count == 1) {
                        secondLargest = breakIntervalSettingsList1.getArrayList(context).get(i).getTimeOutStart();
                    }
                }

            }

            if(!secondLargest.equals("")) {

                SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");

                Date d1 = format1.parse(secondLargest);
                Date d2 = format1.parse(currentHour);

                long difference = d1.getTime() - d2.getTime();

                minutesPassed = (int) ((difference / (1000 * 60)) % 60);
                hoursPassed = (int) ((difference / (1000 * 60 * 60)) % 24);

            }

            Date endDate = DateTimeHandler.convertHourToString(endTimeOut);

            Date beforeDate = DateTimeHandler.convertHourToString(startTimeOut);

            Date inBeforeDate = DateTimeHandler.convertHourToString(currentHour);

            if(!secondLargest.equals("") && hoursPassed == 0 && minutesPassed == 0)  {

                return true;

            } if(!startTimeOut.equals("") && hoursPassed == 0 && minutesPassed == 0 ){

                return true;

            } if(!startTimeOut.equals("")){

                return inBeforeDate.after(beforeDate) && inBeforeDate.before(endDate);

            }
            else return currentTime > largest;

        }catch (Exception err){
            Debugger.logD("timeInterVal Exception err :"+ err.getMessage());
            return false;
        }
    }

    private static File getOutputMediaFile(Context context)
    {

        ContextWrapper contextWrapper = new ContextWrapper(context);
        File directory = contextWrapper.getDir("my_images", Context.MODE_PRIVATE);
        File mediaStorage = new File(directory, "3sImage");

        if (!mediaStorage.exists()) {
            if (!mediaStorage.mkdirs()) {
                Debugger.logD("CAMERA"+ "failed to create directory");
                return mediaStorage.getAbsoluteFile();
            }
        }

        // Create a media file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorage.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void getCameraPath(byte[] data)
    {
        pictureFile = getOutputMediaFile(context);
        if (pictureFile == null){
            Debugger.printO( "Error creating media file, check storage permissions: ");
            return;
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            Debugger.printO( "File not found: " + e.getMessage());
        } catch (IOException e) {
            Debugger.printO("Error accessing file: " + e.getMessage());
        }
    }

    private void stopCamera(Camera camera) {
        camera.stopPreview();

        if (camera != null) {
            camera.release();
            mCamera = null;
        }
    }

    public void verifyStoragePermissions(final Context context) {

        try{

            int permissionCheckStorage = ContextCompat.checkSelfPermission( context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCheckCamera = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);

            if (permissionCheckStorage != PackageManager.PERMISSION_GRANTED && permissionCheckCamera != PackageManager.PERMISSION_GRANTED) {

                // if storage request is denied
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("You need to give permission to access storage in order to work this feature.");

                    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    builder.setPositiveButton("GIVE PERMISSION", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            // Show permission request popup
                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    STORAGE_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.show();

                } //asking permission for first time
                else {
                    // Show permission request popup for the first time
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            STORAGE_PERMISSION_REQUEST_CODE);

                }

            }
        }catch (Exception err){
            Debugger.logD("verifyStoragePermissions Exception error: "+err);
        }
    }

    private void restartCamera() {
        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            mCamera.setDisplayOrientation(90);
        } catch (Exception err) {
            err.printStackTrace();
        }

        if (camCondition) {
            mCamera.stopPreview();
            camCondition = false;
        }

        if (mCamera != null)
            try {
                Camera.Parameters parameters = mCamera.getParameters();

                if (Pattern.compile(Pattern.quote("Progress Bicycle"), Pattern.CASE_INSENSITIVE).matcher(UserSession.getCompany(context)).find()) {
                    parameters.setExposureCompensation(parameters.getMaxExposureCompensation());
                    if (parameters.isAutoExposureLockSupported())
                        parameters.setAutoExposureLock(false);
                }

                mCamera.setParameters(parameters);
                mCamera.setPreviewDisplay(sHolder);
                mCamera.startPreview();
            } catch (IOException e) {
                Debugger.logD("restartCamera error: " + e.toString());
                e.printStackTrace();
            }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            mCamera.setDisplayOrientation(90);
        } catch (Exception err) {
            Debugger.logD("surfaceChanged error: " + err.toString());
            err.printStackTrace();
        }

        if (camCondition) {
            mCamera.stopPreview();
            camCondition = false;
        }

        if (mCamera != null)
            try {
                Camera.Parameters parameters = mCamera.getParameters();

                if (Pattern.compile(Pattern.quote("progress bicycle"), Pattern.CASE_INSENSITIVE).matcher(UserSession.getCompany(context)).find()) {
                    parameters.setExposureCompensation(parameters.getMaxExposureCompensation());
                    if (parameters.isAutoExposureLockSupported())
                        parameters.setAutoExposureLock(false);
                }

                mCamera.setParameters(parameters);
                mCamera.setPreviewDisplay(sHolder);
                mCamera.startPreview();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);

            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);

            mCamera.release();
            mCamera = null;
        }
    }

    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onClick(View v)
    {
        if (deviceSettings.isAutoDateTime())
        {
            if((Utility.dateAndTimeDetection(context, getSupportFragmentManager())) && v.getTag() != null && "number_button".equals(v.getTag())){
                etEmployeeCode.append(((TextView) v).getText().toString());
            }
        }else {
            etEmployeeCode.append(((TextView) v).getText().toString());
        }

        switch (v.getId()) {
            // handle clear button
            case R.id.t9_key_clear:
                etEmployeeCode.setText(null);
                tvDone.setEnabled(true);
                break;
            // handle done button
            case R.id.t9_key_done:
                tvDone.setEnabled(false);
                if (UserPreferences.getWarningExisting(context))
                {
                    if (!checkExistingLog(selectedEmployee) ){
                        if(!deviceSettings.isAutoDateTime()) {
                            takePicture();
                        }else {
                            if(Utility.dateAndTimeDetection(context, getSupportFragmentManager())) {
                                takePicture();
                            }
                        }
                    }
                } else {
                    if(!deviceSettings.isAutoDateTime()) {
                        takePicture();
                    }else {
                       if (Utility.dateAndTimeDetection(context, getSupportFragmentManager())){
                           takePicture();
                       }
                    }
                }

                tvDone.setEnabled(true);
                break;
        }

    }

    @Override
    public void onPositiveSelect() {
        if (logType.equals("IN")){
            logType = "OUT";
        } else if (logType.equals("OUT")){
            logType = "IN";
        }

        takePicture();
    }

    @Override
    public void onNegativeSelect() {
        takePicture();
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
//            readEmployeeLogs(logType);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }
}
