package com.yahshua.yahshuatimekeeperdmpi.Interfaces;

public interface HttpRequestListener
{
    void onSuccess(String fromWhere, String successMessage);
    void onFailure(int errorCode, String errorMessage, String fromWhere);
    void onStart(String fromWhere);
    void onFinish(String fromWhere);
    void onUpdate(int max, int progress, String currentDownload);
    void showSpinner(boolean isShow, String currentDownload);
}
