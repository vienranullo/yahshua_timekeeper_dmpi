package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class WorkOnHoliday extends Application implements Parcelable
{
    public WorkOnHoliday(){}


    protected WorkOnHoliday(Parcel in)
    {
        super.setId(in.readInt());
        super.setEmployeeId(in.readInt());
        super.setEmployeeName(in.readString());
        super.setReason(in.readString());
        super.setStatus(in.readString());
        super.setRemarks(in.readString());
        super.setHours(in.readDouble());
        super.setSync(in.readByte() != 0);
        super.setDate((java.util.Date) in.readSerializable());
        super.setRequestedDate((java.util.Date) in.readSerializable());
    }

    public static final Creator<WorkOnHoliday> CREATOR = new Creator<WorkOnHoliday>() {
        @Override
        public WorkOnHoliday createFromParcel(Parcel in) { return new WorkOnHoliday(in); }

        @Override
        public WorkOnHoliday[] newArray(int size) { return new WorkOnHoliday[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(super.getId());
        dest.writeInt(super.getEmployeeId());
        dest.writeString(super.getEmployeeName());
        dest.writeString(super.getReason());
        dest.writeString(super.getStatus());
        dest.writeString(super.getRemarks());
        dest.writeDouble(super.getHours());
        dest.writeByte((byte) (super.isSync() ? 1 : 0));
        dest.writeSerializable(super.getDate());
        dest.writeSerializable(super.getRequestedDate());
    }


    public static ArrayList<WorkOnHoliday> load(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<WorkOnHoliday> workOnHolidayArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM workOnHoliday";

        try {
            if (searchQuery != null) selectQuery += " " + searchQuery;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    WorkOnHoliday workOnHoliday = new WorkOnHoliday();
                    Date date, requestedDate;

                    date = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("date")));
                    requestedDate = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("requestedDate")));

                    workOnHoliday.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    workOnHoliday.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    workOnHoliday.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    workOnHoliday.setDate(date);
                    workOnHoliday.setHours(cursor.getDouble(cursor.getColumnIndex("hours")));
                    workOnHoliday.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    workOnHoliday.setRequestedDate(requestedDate);
                    workOnHoliday.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    workOnHoliday.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    workOnHoliday.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    workOnHolidayArrayList.add(workOnHoliday);

                } while (cursor.moveToNext());
            }

            return workOnHolidayArrayList;
        } catch (SQLiteException er)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = er.toString();
            errorLogs.setName("WorkOnHoliday load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return workOnHolidayArrayList;
        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnHoliday load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return workOnHolidayArrayList;
        }
    }

    public static int count(Context context, String countQuery) {
        int count = 0;

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            Cursor cursor = db.read(countQuery);

            if (cursor.moveToNext()) count = cursor.getInt(cursor.getColumnIndex("count"));

            return count;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnHoliday count error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return count;
        }
    }


    public boolean save(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ContentValues contentValues = new ContentValues();

        try {
            // Hours
            DecimalFormat decimalFormat = new DecimalFormat("#.######");
            contentValues.put("hours", Double.valueOf(decimalFormat.format(getHours())));

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("employeeName", getEmployeeName());
            contentValues.put("date", DateToString.dbFormat(getDate()));
            contentValues.put("reason", getReason());
            contentValues.put("isSync", false);
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (getId() > 0) {
                db.open();
                db.update(contentValues, "workOnHoliday", " id = ?", new String[]{ String.valueOf( getId()) });
            } else {
                contentValues.put("status", "pending");

                db.save(contentValues, "workOnHoliday");
            }

            return true;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnHoliday save error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, "Database Error").show();
            }
            return false;
        }
    }

    public boolean changeSyncStatus(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("isSync", "1");
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));

            return db.update(contentValues, "workOnHoliday", "id=" + this.getId(),null);

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnHoliday changeSyncStatus error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }


    public void delete(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);

        if (getId() > 0) db.delete("workOnHoliday","id = ?", new String[]{ String.valueOf(getId()) });
    }

    public static int getWorkonHolidayCount(Context context)
    {
        try {
            WorkOnHoliday workOnHoliday = new WorkOnHoliday();
            String selectQuery = "SELECT COUNT(id) AS count FROM workOnHoliday WHERE isSync = " + workOnHoliday.getEmployeeId();
            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            return count;
        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("getWorkonHolidayCount error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, "Database Error").show();
            }
            return 0;
        }

    }

}
