package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class LeaveRequest implements Parcelable {

    private int id;
    @SerializedName("employee_id")
    private int employeeId;
    @SerializedName("employeename")
    private String employeeName;
    private String reason;
    @SerializedName("leave_type")
    private String leaveType;
    private String status;
    @SerializedName("requested_date")
    private Date requestedDate;
    @SerializedName("days_leave")
    private int daysleave;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public int getDaysleave() {
        return daysleave;
    }

    public void setDaysleave(int daysleave) {
        this.daysleave = daysleave;
    }

    public LeaveRequest()
    {

    }

    protected LeaveRequest(Parcel in)
    {
        setId(in.readInt());
        setEmployeeId(in.readInt());
        setEmployeeName(in.readString());
        setReason(in.readString());
        setLeaveType(in.readString());
        setDaysleave(in.readInt());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(getId());
        parcel.writeInt(getEmployeeId());
        parcel.writeString(getEmployeeName());
        parcel.writeString(getReason());
        parcel.writeString(getLeaveType());
        parcel.writeInt(getDaysleave());
    }
}
