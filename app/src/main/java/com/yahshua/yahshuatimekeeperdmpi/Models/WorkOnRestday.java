package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class WorkOnRestday extends Application implements Parcelable
{
    public WorkOnRestday(){}

    protected WorkOnRestday(Parcel in)
    {
        super.setId(in.readInt());
        super.setEmployeeId(in.readInt());
        super.setEmployeeName(in.readString());
        super.setReason(in.readString());
        super.setStatus(in.readString());
        super.setRemarks(in.readString());
        super.setHours(in.readDouble());
        super.setSync(in.readByte() != 0);
        super.setDate((java.util.Date) in.readSerializable());
        super.setRequestedDate((java.util.Date) in.readSerializable());
    }

    public static final Creator<WorkOnRestday> CREATOR = new Creator<WorkOnRestday>()
    {
        @Override
        public WorkOnRestday createFromParcel(Parcel in) { return new WorkOnRestday(in); }

        @Override
        public WorkOnRestday[] newArray(int size) { return new WorkOnRestday[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(super.getId());
        dest.writeInt(super.getEmployeeId());
        dest.writeString(super.getEmployeeName());
        dest.writeString(super.getReason());
        dest.writeString(super.getStatus());
        dest.writeString(super.getRemarks());
        dest.writeDouble(super.getHours());
        dest.writeByte((byte) (super.isSync() ? 1 : 0));
        dest.writeSerializable(super.getDate());
        dest.writeSerializable(super.getRequestedDate());
    }

    public static ArrayList<WorkOnRestday> load(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<WorkOnRestday> workOnRestdayArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM workOnRestday";

        try {
            if (searchQuery != null) selectQuery += " " + searchQuery;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    WorkOnRestday workOnRestday = new WorkOnRestday();
                    Date date, requestedDate;

                    date = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("date")));
                    requestedDate = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("requestedDate")));

                    workOnRestday.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    workOnRestday.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    workOnRestday.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    workOnRestday.setDate(date);
                    workOnRestday.setHours(cursor.getDouble(cursor.getColumnIndex("hours")));
                    workOnRestday.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    workOnRestday.setRequestedDate(requestedDate);
                    workOnRestday.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    workOnRestday.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    workOnRestday.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    workOnRestdayArrayList.add(workOnRestday);

                } while (cursor.moveToNext());
            }

            return workOnRestdayArrayList;
        } catch (SQLiteException err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return workOnRestdayArrayList;
        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return workOnRestdayArrayList;
        }
    }

    public static int count(Context context, String countQuery) {
        int count = 0;

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            Cursor cursor = db.read(countQuery);

            if (cursor.moveToNext()) count = cursor.getInt(cursor.getColumnIndex("count"));

            return count;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday count error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return count;
        }
    }


    public boolean save(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ContentValues contentValues = new ContentValues();

        try {
            // Hours
            DecimalFormat decimalFormat = new DecimalFormat("#.######");
            contentValues.put("hours", Double.valueOf(decimalFormat.format(getHours())));

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("employeeName", getEmployeeName());
            contentValues.put("date", DateToString.dbFormat(getDate()));
            contentValues.put("reason", getReason());
            contentValues.put("isSync", false);
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (getId() > 0) {
                db.open();
                db.update(contentValues, "workOnRestday", " id = ?", new String[]{ String.valueOf( getId()) });
            } else {
                contentValues.put("status", "pending");

                db.save(contentValues, "workOnRestday");
            }

            return true;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday save error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public boolean changeSyncStatus(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            db.open();
            ContentValues contentValues = new ContentValues();

            contentValues.put("isSync", "1");
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));

            boolean result = db.update(contentValues, "workOnRestday", "id=" + this.getId(),null);
            return result;

        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday changeSyncStatus error "+errorMessage);
            if (errorLogs.save(context))
            {
                Debugger.logD("WorkOnRestday changeSyncStatus error: " + errorMessage);
            }
            return false;
        }
    }

    public void delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            if (getId() > 0) db.delete("workOnRestday","id = ?", new String[]{ String.valueOf(getId()) });
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("WorkOnRestday delete error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
        }
    }

    public static int getWorkonRestdayCount(Context context)
    {
        try
        {
            WorkOnRestday workOnRestday = new WorkOnRestday();
            String selectQuery = "SELECT COUNT(id) AS count FROM workOnRestday WHERE isSync = " + workOnRestday.getEmployeeId();
            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            return count;
        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("getWorkOnRestdayCount error "+errorMessage);
            if(errorLogs.save(context)){
                Toasty.error(context, "Database Error").show();
            }
            return 0;
        }
    }

}
