package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class AuditLog
{
    private int id, employeeId;
    private String module, action, status, errorMessage, previousValue, newValue, employeeName;
    private Date dateTime;


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(int employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getModule()
    {
        return module;
    }

    public void setModule(String module)
    {
        this.module = module;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getPreviousValue()
    {
        return previousValue;
    }

    public void setPreviousValue(String previousValue)
    {
        this.previousValue = previousValue;
    }

    public String getNewValue()
    {
        return newValue;
    }

    public void setNewValue(String newValue)
    {
        this.newValue = newValue;
    }

    public Date getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(Date dateTime)
    {
        this.dateTime = dateTime;
    }

    public String getEmployeeName()
    {
        return employeeName;
    }

    public void setEmployeeName(String employeeName)
    {
        this.employeeName = employeeName;
    }


    public static ArrayList<AuditLog> load(Context context, String searchQuery)
    {
        DbAdapter db = new DbAdapter(context);
        ArrayList<AuditLog> auditLogArrayList = new ArrayList<>();
        String selectQuery = (searchQuery != null && !searchQuery.isEmpty()) ? searchQuery : "SELECT * FROM auditLog" ;
        selectQuery += " ORDER BY dateTime DESC";

        try
        {
            Cursor cursor = db.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst())
            {
                do
                {
                    AuditLog auditLog = new AuditLog();

                    auditLog.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    auditLog.setModule(cursor.getString(cursor.getColumnIndex("module")));
                    auditLog.setAction(cursor.getString(cursor.getColumnIndex("action")));
                    auditLog.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    auditLog.setErrorMessage(cursor.getString(cursor.getColumnIndex("errorMessage")));
                    auditLog.setPreviousValue(cursor.getString(cursor.getColumnIndex("previousValue")));
                    auditLog.setNewValue(cursor.getString(cursor.getColumnIndex("newValue")));
                    auditLog.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));

                    Date dateTime = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("dateTime")));
                    auditLog.setDateTime(dateTime);

                    auditLogArrayList.add(auditLog);
                }
                while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

            return auditLogArrayList;
        }
        catch (Exception err)
        {
            ErrorLogs.save("AuditLog load error "+err.getMessage(), db);
            db.close();
            return auditLogArrayList;
        }
    }

    public boolean save( DbAdapter dbAdapter)
    {
        try
        {
            ContentValues contentValues = new ContentValues();

            // Set fields
            contentValues.put("module", getModule());
            contentValues.put("action", getAction());
            contentValues.put("status", getStatus());
            contentValues.put("errorMessage", getErrorMessage());
            contentValues.put("previousValue", getPreviousValue());
            contentValues.put("newValue", getNewValue());
            contentValues.put("employeeName", getEmployeeName());

            // Create or Update
            if (getId() > 0)
            {
                dbAdapter.getDb().update("auditLog", contentValues, " id = ?", new String[]{ String.valueOf( getId()) });
            }
            else
            {
                dbAdapter.getDb().insertOrThrow("auditLog", null, contentValues);
            }

            return true;
        }
        catch (Exception err)
        {
            ErrorLogs.save("AuditLog save error "+err.getMessage(), dbAdapter);
            return false;
        }
    }

    public static void saveAuditLog(Context context, String module, String action, String status,String employeeName)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        try
        {
            AuditLog auditLog = new AuditLog();

            auditLog.setModule(module);
            auditLog.setAction(action);
            auditLog.setStatus(status);
            auditLog.setEmployeeName(employeeName);

            auditLog.save(dbAdapter);
            dbAdapter.close();

        }
        catch (Exception err)
        {
            ErrorLogs.save("AuditLog saveAuditLog error "+err.getMessage(), dbAdapter);
            dbAdapter.close();
        }
    }
}
