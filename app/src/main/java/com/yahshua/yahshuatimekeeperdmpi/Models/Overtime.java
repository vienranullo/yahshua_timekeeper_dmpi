package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseHelper;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class Overtime extends Application implements Parcelable
{
    public Overtime() {}

    protected Overtime(Parcel in)
    {
        super.setId(in.readInt());
        super.setEmployeeId(in.readInt());
        super.setEmployeeName(in.readString());
        super.setReason(in.readString());
        super.setStatus(in.readString());
        super.setRemarks(in.readString());
        super.setHours(in.readDouble());
        super.setSync(in.readByte() != 0);
        super.setDate((java.util.Date) in.readSerializable());
        super.setRequestedDate((java.util.Date) in.readSerializable());
    }

    public static final Creator<Overtime> CREATOR = new Creator<Overtime>()
    {
        @Override
        public Overtime createFromParcel(Parcel in) { return new Overtime(in); }

        @Override
        public Overtime[] newArray(int size) {  return new Overtime[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(super.getId());
        dest.writeInt(super.getEmployeeId());
        dest.writeString(super.getEmployeeName());
        dest.writeString(super.getReason());
        dest.writeString(super.getStatus());
        dest.writeString(super.getRemarks());
        dest.writeDouble(super.getHours());
        dest.writeByte((byte) (super.isSync() ? 1 : 0));
        dest.writeSerializable(super.getDate());
        dest.writeSerializable(super.getRequestedDate());
    }


    // Read all unsynced overtime regardless of Employee
    public static ArrayList<Overtime> readUnsyncedRecords(Context context)
    {
        ArrayList<Overtime> overtimeArrayList = new ArrayList<>();
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery = "SELECT * FROM overtime WHERE isSync = 0";

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst())
            {
                do {
                    Overtime overtime = new Overtime();
                    Date date, requestedDate;

                    date = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("date")));
                    requestedDate = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("requestedDate")));

                    overtime.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    overtime.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    overtime.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    overtime.setDate(date);
                    overtime.setHours(cursor.getDouble(cursor.getColumnIndex("hours")));
                    overtime.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    overtime.setRequestedDate(requestedDate);
                    overtime.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    overtime.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    overtime.setRemarks(cursor.getString(cursor.getColumnIndex("remarks")));
                    overtime.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    overtimeArrayList.add(overtime);

                } while (cursor.moveToNext());
            }

            return overtimeArrayList;
        } catch (Exception err)
        {
            return overtimeArrayList;
        }
    }

    public static ArrayList<Overtime> load(Context context, String searchQuery)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ArrayList<Overtime> overtimeArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM overtime";

        try
        {
            if (searchQuery != null) selectQuery += " " + searchQuery;

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    Overtime overtime = new Overtime();
                    Date date, requestedDate;

                    date = DateStringToDate.dbFormat(cursor.getString(cursor.getColumnIndex("date")));
                    requestedDate = DateStringToDate.dateTimeDbFormat(cursor.getString(cursor.getColumnIndex("requestedDate")));

                    overtime.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    overtime.setEmployeeId(cursor.getInt(cursor.getColumnIndex("employeeId")));
                    overtime.setEmployeeName(cursor.getString(cursor.getColumnIndex("employeeName")));
                    overtime.setDate(date);
                    overtime.setHours(cursor.getDouble(cursor.getColumnIndex("hours")));
                    overtime.setReason(cursor.getString(cursor.getColumnIndex("reason")));
                    overtime.setRequestedDate(requestedDate);
                    overtime.setSync(cursor.getInt(cursor.getColumnIndex("isSync")) > 0);
                    overtime.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    overtime.setRemarks(cursor.getString(cursor.getColumnIndex("remarks")));
                    overtime.setLastSyncedDate(cursor.getString(cursor.getColumnIndex("lastSyncedDate")));

                    overtimeArrayList.add(overtime);

                } while (cursor.moveToNext());
            }

            return overtimeArrayList;

        } catch (SQLiteException err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return overtimeArrayList;
        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime load error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return overtimeArrayList;
        }
    }

    public static int count(Context context, String countQuery) {
        int count = 0;

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            Cursor cursor = db.read(countQuery);

            if (cursor.moveToNext()) count = cursor.getInt(cursor.getColumnIndex("count"));

            return count;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime count error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return count;
        }
    }

    public boolean save(Context context)
    {
        DatabaseAdapter db = new DatabaseAdapter(context);
        ContentValues contentValues = new ContentValues();

        try {
            // Hours
            DecimalFormat decimalFormat = new DecimalFormat("#.######");
            contentValues.put("hours", Double.valueOf(decimalFormat.format(getHours())));

            contentValues.put("employeeId", getEmployeeId());
            contentValues.put("employeeName", getEmployeeName());
            contentValues.put("date", DateToString.dbFormat(getDate()));
            contentValues.put("reason", getReason());
            contentValues.put("isSync", false);
            contentValues.put("remarks", getRemarks());
            contentValues.put("lastSyncedDate", "");
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (getId() > 0) {
                db.open();
                db.update(contentValues, "overtime", " id = ?", new String[]{ String.valueOf( getId()) });
            } else {
                contentValues.put("status", "pending");

                db.save(contentValues, "overtime");
            }

            return true;

        } catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime save error "+errorMessage);
            if(errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public boolean saveSyncErrorMessage(Context context)
    {
        try
        {
            DatabaseHelper databaseHelper = new DatabaseHelper(context, null);
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put("lastSyncedDate", getSyncErrorMessage());

            return db.update("overtime",contentValues, "id=" + this.getId(),null) == 0;
        } catch (Exception err)
        {
            return false;
        }
    }

    public boolean changeSyncStatus(Context context)
    {
        try
        {
            DatabaseHelper databaseHelper = new DatabaseHelper(context, null);
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("isSync", "1");
            contentValues.put("lastSyncedDate", DateToString.dateTimeFormat2(new Date()));
            return db.update("overtime",contentValues, "id=" + this.getId(),null) == 0;
        } catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime changeSyncStatus error "+errorMessage);
            if(errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return false;
        }
    }

    public void delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            if (getId() > 0) db.delete("overtime","id = ?", new String[]{ String.valueOf(getId()) });
        }
        catch (Exception err)
        {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("Overtime delete error "+errorMessage);
            if(errorLogs.save(context)) Toasty.error(context, "Database Error").show();
        }
    }

    public static int getOvertimeCount(Context context)
    {
        try {
            Overtime overtime = new Overtime();
            String selectQuery = "SELECT COUNT(id) AS count FROM overtime WHERE isSync = " + overtime.getEmployeeId();
            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            return count;
        }catch (Exception err) {
            ErrorLogs errorLogs = new ErrorLogs();
            String errorMessage = err.toString();
            errorLogs.setName("getOvertimeCount error "+errorMessage);
            if (errorLogs.save(context)) Toasty.error(context, "Database Error").show();
            return 0;
        }

    }

}
