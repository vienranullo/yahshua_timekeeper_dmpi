package com.yahshua.yahshuatimekeeperdmpi.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class Application
{
    private int id;
    @SerializedName("employee")
    private int employeeId;
    private String employeeName, reason, status, remarks;
    private double hours;
    @SerializedName("date1")
    private Date date;
    private Date requestedDate;
    @SerializedName("date")
    private String dateString;
    @SerializedName("requested_date")
    private String dateRequestedString;
    private boolean isSync;
    private String lastSyncedDate;
    private String syncErrorMessage;

    public String getSyncErrorMessage() {
        return syncErrorMessage;
    }

    public void setSyncErrorMessage(String syncErrorMessage) {
        this.syncErrorMessage = syncErrorMessage;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getDateRequestedString() {
        return dateRequestedString;
    }

    public void setDateRequestedString(String dateRequestedString) {
        this.dateRequestedString = dateRequestedString;
    }

    public String getLastSyncedDate()
    {
        return lastSyncedDate;
    }

    public void setLastSyncedDate(String lastSyncedDate)
    {
        this.lastSyncedDate = lastSyncedDate;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(int employeeId)
    {
        this.employeeId = employeeId;
    }

    public double getHours()
    {
        return hours;
    }

    public void setHours(double hours)
    {
        this.hours = hours;
    }

    public String getEmployeeName()
    {
        return employeeName;
    }

    public void setEmployeeName(String employeeName)
    {
        this.employeeName = employeeName;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public Date getRequestedDate()
    {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate)
    {
        this.requestedDate = requestedDate;
    }

    public boolean isSync()
    {
        return isSync;
    }

    public void setSync(boolean sync)
    {
        isSync = sync;
    }

    public String getRemarks()
    {
        return remarks;
    }

    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
}