package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.OvertimeAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;
import com.yahshua.yahshuatimekeeperdmpi.services.OvertimeUploader;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;

public class UnsycedOvertimeActivity extends AppCompatActivity implements CompleteListener
{
    private MenuItem menuSync;
    private OvertimeAdapter overtimeAdapter;
    private Context context;
    private ArrayList<Overtime> overtimeArrayList;
    private Overtime selectedOvertime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unsyced_overtime);
        context = this;

        setTitle("Unsynced Overtime Applications");

        loadData();
        initializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync, menu);
        menuSync = menu.findItem(R.id.action_sync);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_sync:
                uploadOvertime();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeUI()
    {
        ListView lvUnsyncedOvertime = findViewById(R.id.lvOvertime);

        overtimeAdapter = new OvertimeAdapter(context, overtimeArrayList, true);
        lvUnsyncedOvertime.setAdapter(overtimeAdapter);

        lvUnsyncedOvertime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedOvertime = overtimeAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Options");

                final String[] finalOptions;
                String[] optionUnSynced;
                String[] optionSynced;

                optionUnSynced = getResources().getStringArray(R.array.lv_options2);
                optionSynced = Arrays.copyOf(optionUnSynced, optionUnSynced.length - 1);

                finalOptions = selectedOvertime.isSync() ? optionSynced : optionUnSynced;

                builder.setItems(finalOptions, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        switch (finalOptions[i])
                        {
                            case "Delete":
                                confirmDelete();
                                break;
                        }
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        selectedOvertime = new Overtime();
                    }
                });

                builder.show();
            }
        });
    }

    private void loadData()
    {
        overtimeArrayList = Overtime.readUnsyncedRecords(context);
    }

    private void uploadOvertime()
    {
        if (Utility.haveNetworkConnection(context))
        {
            String searchQuery = "WHERE isSync = 0 AND company_id = " + UserSession.getCompanyId(context);

            if(Overtime.load(context, searchQuery).size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                Toasters.ShowLoadingSpinner(this);
                new OvertimeUploader(context, this).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }

    private void confirmDelete()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                deleteRecord();
            }
        };

        String message = "Delete Overtime Application on\n" + DateToString.displayFormat(selectedOvertime.getDate()) + "?";

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Confirm Delete");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, dialogClickListener);
        builder.setNegativeButton(android.R.string.no, null);

        builder.show();
    }

    private void deleteRecord()
    {
        selectedOvertime.delete(context);
        AuditLog.saveAuditLog(context,"OVERTIME","DELETE","SUCCESS",selectedOvertime.getEmployeeName());
        updateLeaveAdapter();
        Toasters.ShowToast(context, "Overtime Application Deleted");
    }

    private void updateLeaveAdapter()
    {
        loadData();
        overtimeAdapter.clear();
        overtimeAdapter.addAll(overtimeArrayList);
        overtimeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onComplete(Object object)
    {
        updateLeaveAdapter();
        Toasters.HideLoadingSpinner();
    }
}
