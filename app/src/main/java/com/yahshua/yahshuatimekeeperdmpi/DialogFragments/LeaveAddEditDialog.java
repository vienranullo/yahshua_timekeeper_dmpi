package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLeaveBalance;
import com.yahshua.yahshuatimekeeperdmpi.Models.Leave;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveType;
import com.yahshua.yahshuatimekeeperdmpi.Models.Workdays;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class LeaveAddEditDialog extends DialogFragment
{
    // View
    private View view;

    // Widget
    private EditText etEmployeeName, etReason;
    private Button btnSaveLeave;
    private ImageView imgClose;

    // Data
    private Workdays workdays = new Workdays();
    private String employeeName, day;
    private int employeeId;
    private Leave leave;

    private DialogInterface.OnDismissListener onDismissListener;

    // Leave types and balances
    private LinearLayout leaveBalancesLayout;
    private ArrayList<EmployeeLeaveBalance> employeeLeaveBalanceArrayList = new ArrayList<>();
    private Spinner spLeaveType;
    private ArrayList<LeaveType> leaveTypeArrayList = new ArrayList<>();
    private ArrayAdapter<String> leaveTypeAdapter;
    private String selectedLeaveType;

    // Whole and Half Day
    private RadioGroup rgDay;
    private RadioButton rbWholeDay, rbHalfDay;
    // Date and Time
    private LinearLayout datesLayout, halfDayDateLayout, timeLayout;
    private EditText etDateFrom, etDateTo, etHalfDayDate, etTimeFrom, etTimeTo;
    private Date dateFrom, dateTo, halfDayDate, dateTimeFrom, dateTimeTo;
    // Date
    private Calendar calendar = Calendar.getInstance();
    private String dateToUpdate;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    // Time
    private Calendar calendarForTime = Calendar.getInstance();
    private String timeToUpdate;
    private TimePickerDialog.OnTimeSetListener onTimeSetListener;


    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.leave_add_edit_dialog, container, false);

        initializeData();
        initializeUI();
        loadForm();

        return view;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    private void initializeData()
    {
        // Get Passed Data
        Bundle bundle = getArguments();

        if (bundle.size() > 0)
        {
            employeeId = bundle.getInt("EMPLOYEE_ID");
            employeeName = bundle.getString("EMPLOYEE_NAME");
            employeeLeaveBalanceArrayList = bundle.getParcelableArrayList("EMPLOYEE_LEAVE_BALANCES");
            leaveTypeArrayList = bundle.getParcelableArrayList("LEAVE_TYPES");
            workdays = bundle.getParcelable("WORKDAYS");

            if (bundle.containsKey("SELECTED_LEAVE"))
            {
                leave = bundle.getParcelable("SELECTED_LEAVE");
            }
        }
    }


    // Initialize component from a layout file
    private void initializeUI()
    {
        String dialogTitle = (leave != null) ? "Edit Leave Application" : "Create Leave Application";
        getDialog().setTitle(dialogTitle);

        etEmployeeName  = view.findViewById(R.id.etEmployeeName);
        etReason        = view.findViewById(R.id.etReason);
        imgClose        = view.findViewById(R.id.imgClose);

        initializeLeaveBalancesLayout();
        initializeDateAndTimeViews();
        initializeLeaveTypesSpinner();
        initializeRadioButtons();

        // Save Leave Button
        btnSaveLeave = view.findViewById(R.id.btnSaveLeave);
        btnSaveLeave.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if(leave != null)
                {
                    if(leave.isSync())
                    {
                        Toasty.warning(getContext(), getResources().getString(R.string.unable_to_edit)).show();
                    }
                    else
                    {
                        saveLeave();
                    }
                }
                else
                {
                    saveLeave();
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void initializeLeaveBalancesLayout()
    {
        leaveBalancesLayout = view.findViewById(R.id.leaveBalancesLayout);

        leaveBalancesLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout[] leaveBalanceLayouts = new LinearLayout[employeeLeaveBalanceArrayList.size()];

        for (int i = 0; i < leaveBalanceLayouts.length; i++)
        {
            // Set Leave Type Name
            TextView tvLeaveType = new TextView(getContext());

            tvLeaveType.setLayoutParams(new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvLeaveType.setTextSize(16);
            tvLeaveType.setText(employeeLeaveBalanceArrayList.get(i).getLeaveTypeName() + ":");

            // Set Leave Balance
            TextView tvLeaveBalance = new TextView(getContext());

            tvLeaveBalance.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvLeaveBalance.setTextSize(16);
            tvLeaveBalance.setText(Double.toString(employeeLeaveBalanceArrayList.get(i).getTotal()));

            // Leave Balance Row Layout
            leaveBalanceLayouts[i] = new LinearLayout(getContext());

            leaveBalanceLayouts[i].setOrientation(LinearLayout.HORIZONTAL);
            leaveBalanceLayouts[i].setVisibility(View.VISIBLE);
            leaveBalanceLayouts[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            leaveBalanceLayouts[i].setPadding(0, 10, 0, 10);

            leaveBalanceLayouts[i].addView(tvLeaveType);
            leaveBalanceLayouts[i].addView(tvLeaveBalance);

            leaveBalancesLayout.addView(leaveBalanceLayouts[i]);
        }
    }

    private void initializeDateAndTimeViews()
    {
        datesLayout = view.findViewById(R.id.datesLayout);
        halfDayDateLayout = view.findViewById(R.id.halfDayDateLayout);
        timeLayout = view.findViewById(R.id.timeLayout);

        // Date and Time Listeners
        onDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                calendar.set(year, month, dayOfMonth);
                updateDate();
            }
        };

        onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute)
            {
                calendarForTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendarForTime.set(Calendar.MINUTE, minute);
                updateTime();
            }
        };

        // Date From
        etDateFrom = view.findViewById(R.id.etDateFrom);

        etDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateToUpdate = "DATE-FROM";
                calendar.setTime(dateFrom);
                calendar.setTime(dateTo);
                new DatePickerDialog(getContext(), onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        // Date To
        etDateTo = view.findViewById(R.id.etDateTo);

        etDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateToUpdate = "DATE-TO";
                calendar.setTime(dateTo);
                new DatePickerDialog(getContext(), onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        // Half Day Date
        etHalfDayDate = (EditText) view.findViewById(R.id.etHalfDayDate);

        etHalfDayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateToUpdate = "HALF-DAY-DATE";
                calendar.setTime(halfDayDate);
                new DatePickerDialog(getContext(), onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        // Time From
        etTimeFrom = view.findViewById(R.id.etTimeFrom);

        etTimeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeToUpdate = "TIME-FROM";
                calendarForTime.setTime(dateTimeFrom);
                new TimePickerDialog(getContext(), onTimeSetListener, calendarForTime.get(Calendar.HOUR_OF_DAY), calendarForTime.get(Calendar.MINUTE), false).show();
            }
        });

        // Time To
        etTimeTo = view.findViewById(R.id.etTimeTo);

        etTimeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeToUpdate = "TIME-TO";
                calendarForTime.setTime(dateTimeTo);
                new TimePickerDialog(getContext(), onTimeSetListener, calendarForTime.get(Calendar.HOUR_OF_DAY), calendarForTime.get(Calendar.MINUTE), false).show();
            }
        });
    }

    private void initializeLeaveTypesSpinner()
    {
        spLeaveType = view.findViewById(R.id.spLeaveType);

        ArrayList<String> leaveTypeSpinnerItems = new ArrayList<>();

        for (LeaveType leaveType: leaveTypeArrayList)
        {
            leaveTypeSpinnerItems.add(leaveType.getName());
        }

        leaveTypeAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, leaveTypeSpinnerItems);

        spLeaveType.setAdapter(leaveTypeAdapter);

        spLeaveType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedLeaveType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });
    }

    private void initializeRadioButtons()
    {
        rgDay = view.findViewById(R.id.rgDay);

        rgDay.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) { checkDay(); }
        });

        rbWholeDay = view.findViewById(R.id.rbWholeDay);
        rbHalfDay = view.findViewById(R.id.rbHalfDay);

        checkDay();
    }

    private void updateDate()
    {
        switch (dateToUpdate) {
            case "DATE-FROM":
                dateFrom = calendar.getTime();
                dateTo = calendar.getTime();
                etDateFrom.setText(DateToString.formFormat(dateFrom));
                etDateTo.setText(DateToString.formFormat(dateTo));
                break;
            case "DATE-TO":
                dateTo = calendar.getTime();
                etDateTo.setText(DateToString.formFormat(dateTo));
                break;
            case "HALF-DAY-DATE":
                halfDayDate = calendar.getTime();
                etHalfDayDate.setText(DateToString.formFormat(halfDayDate));
                break;
        }
    }

    private void updateTime()
    {
        switch (timeToUpdate) {
            case "TIME-FROM":
                dateTimeFrom = calendarForTime.getTime();
                etTimeFrom.setText(DateToString.timeDisplayFormat(dateTimeFrom));
                break;
            case "TIME-TO":
                dateTimeTo = calendarForTime.getTime();
                etTimeTo.setText(DateToString.timeDisplayFormat(dateTimeTo));
                break;
        }
    }

    private void checkDay()
    {
        if (rbWholeDay.isChecked()) {
            day = "whole";
            datesLayout.setVisibility(View.VISIBLE);
            halfDayDateLayout.setVisibility(View.GONE);
            timeLayout.setVisibility(View.GONE);
        } else {
            day = "half";
            halfDayDateLayout.setVisibility(View.VISIBLE);
            timeLayout.setVisibility(View.VISIBLE);
            datesLayout.setVisibility(View.GONE);
        }
    }


    // Load form fields
    private void loadForm()
    {
        etEmployeeName.setText(employeeName);

        // Initialize Dates
        dateFrom = dateTo = halfDayDate = calendar.getTime();

        calendar.setTime(dateFrom);

        // Initialize Time From and Time To
        calendarForTime.set(Calendar.HOUR, 0);
        calendarForTime.set(Calendar.MINUTE, 0);
        calendarForTime.set(Calendar.AM_PM, 0);

        dateTimeFrom = dateTimeTo = calendarForTime.getTime();


        if (leave != null) {
            etReason.setText(leave.getReason());

            // Whole/Half Day
            switch (leave.getDay()) {
                case "whole":
                    rbWholeDay.setChecked(true);

                    dateFrom = leave.getDateFrom();
                    dateTo = leave.getDateTo();

                    break;

                case "half":
                    rbHalfDay.setChecked(true);

                    halfDayDate = leave.getHalfDayDate();

                    dateTimeFrom = DateStringToDate.timeDisplayFormat(leave.getTimeFrom());
                    dateTimeTo = DateStringToDate.timeDisplayFormat(leave.getTimeTo());

                    break;
            }

            // Leave Type Spinner
            for (int idx = 0; idx < leaveTypeArrayList.size(); idx++)
            {
                if (leaveTypeArrayList.get(idx).getId() == leave.getLeaveTypeId()) {
                    spLeaveType.setSelection(leaveTypeAdapter.getPosition(leaveTypeArrayList.get(idx).getName()));
                    break;
                }
            }

        } else rbWholeDay.setChecked(true);

        initializeWholeDayFields();
        initializeHalfDayFields();
    }

    private void initializeWholeDayFields()
    {
        etDateFrom.setText(DateToString.formFormat(dateFrom));
        etDateTo.setText(DateToString.formFormat(dateTo));
    }

    private void initializeHalfDayFields()
    {
        etHalfDayDate.setText(DateToString.formFormat(halfDayDate));

        etTimeFrom.setText(DateToString.timeDisplayFormat(dateTimeFrom));
        etTimeTo.setText(DateToString.timeDisplayFormat(dateTimeTo));
    }


    private void saveLeave()
    {
        if (leave == null)  leave = new Leave();
        double daysLeave;

        // Get Days of Leave
        Calendar calendarDayOfWeek = Calendar.getInstance();

        if (day.equals("whole")) {

            long diff = Math.abs(dateTo.getTime() - dateFrom.getTime());
            long diffDays = diff / (24 * 60 * 60 * 1000);
            daysLeave = (double) diffDays + 1;

            // Check for Day offs
            int dayOffs = 0;
            calendarDayOfWeek.setTime(dateFrom);

            if (diff == 0) {
                if (isDayOff(calendarDayOfWeek.get(Calendar.DAY_OF_WEEK))) daysLeave -= 1;
            } else {
                do {
                    if (isDayOff(calendarDayOfWeek.get(Calendar.DAY_OF_WEEK))) dayOffs += 1;
                    calendarDayOfWeek.add(Calendar.DATE, 1);

                } while (calendarDayOfWeek.getTime().getTime() <= dateTo.getTime());

                daysLeave -= dayOffs;
            }

            if (daysLeave == 0)
            {
                Toasty.error(getContext(), "Cannot Apply Leave on Rest Day").show();
                return;
            }
        }
        else
        {
            calendarDayOfWeek.setTime(halfDayDate);
            if (isDayOff(calendarDayOfWeek.get(Calendar.DAY_OF_WEEK)))
            {
                Toasty.error(getContext(), "Cannot Apply Leave on Rest Day").show();
                return;
            }

            daysLeave = 0.5;
        }

        // Get Selected Leave Type and Check balance
        int leaveTypeId = 0;
        String leaveTypeName = "";

        for (LeaveType leaveType: leaveTypeArrayList)
        {
            if (leaveType.getName().equals(selectedLeaveType))
            {
                leaveTypeId = leaveType.getId();
                leaveTypeName = leaveType.getName();

                if (!leaveType.isDeductible()) {
                    for (EmployeeLeaveBalance employeeLeaveBalance: employeeLeaveBalanceArrayList)
                    {
                        if (employeeLeaveBalance.getLeaveTypeId() == leaveTypeId)
                        {
                            if (daysLeave > employeeLeaveBalance.getTotal()) {
                                Toasters.ShowToast(getContext(), "Not Enough " + leaveType.getName() + " Leave Balance");
                                return;
                            }
                        }
                    }
                }

                break;
            }
        }

        // Validate Leave Application
        if (!isValid()) return;


        if (day.equals("whole")) {
            leave.setDateFrom(dateFrom);
            leave.setDateTo(dateTo);

        } else if (day.equals("half")) {
            leave.setHalfDayDate(halfDayDate);

            leave.setTimeFrom(DateToString.timeDbFormat(dateTimeFrom));
            leave.setTimeTo(DateToString.timeDbFormat(dateTimeTo));
        }

        // Saving
        leave.setEmployeeId(employeeId);
        leave.setEmployeeName(employeeName);
        leave.setDay(day);
        leave.setDaysLeave(daysLeave);
        leave.setLeaveTypeId(leaveTypeId);
        leave.setLeaveTypeName(leaveTypeName);
        leave.setReason(etReason.getText().toString());
        leave.setSync(false);
        leave.setStatus("pending");

        if (leave.save(getContext()))
        {

            String message = "Leave Application ";
            message += (leave.getId() > 0) ? "Edited" : "Created";

            Toasters.ShowToast(getContext(), message);
            AuditLog.saveAuditLog(getContext(),"LEAVE",""+message,"SUCCESS",""+employeeName);
            dismiss();
        }
    }



    private boolean isValid()
    {
        boolean isValid;
        String countQuery = "SELECT COUNT(*) AS count FROM leave WHERE employeeId = " + employeeId;

        if (leave != null && leave.getId() > 0) countQuery += " AND id != " + leave.getId() + " AND ";
        else countQuery += " AND ";

        try {
            // Empty fields
            String reason = etReason.getText().toString();

            if (reason.equals("")) throw new Exception("Please Enter A Reason");


            if (day.equals("whole")) {
                if (dateFrom.compareTo(dateTo) > 0) throw new Exception("Invalid Dates");

                String dateFromStr = DateToString.dbFormat(dateFrom);
                String dateToStr = DateToString.dbFormat(dateTo);

                countQuery += "( ( halfDayDate = '" + dateFromStr + "' OR halfDayDate = '" + dateToStr + "' ) " +
                                "OR ( dateFrom <= '" + dateFromStr + "' AND dateTo >= '" + dateFromStr + "' ) " +
                                "OR ( dateFrom <= '" + dateToStr + "' AND dateTo >= '" + dateToStr + "' ) ) " ;
            } else {
                if (dateTimeFrom.compareTo(dateTimeTo) > 0) throw new Exception("Invalid Time");

                String halfDayDateStr = DateToString.dbFormat(halfDayDate);

                countQuery += "( halfDayDate = '" + halfDayDateStr + "' OR ( dateFrom <= '" + halfDayDateStr + "' AND dateTo >= '" + halfDayDateStr + "' ) )";
            }

            if (Leave.count(getContext(), countQuery) > 0) throw new Exception("Already Requested");


            isValid = true;

        } catch (Exception err) {
            Toasters.ShowToast(getContext(), err.getMessage());
            isValid = false;
        }

        return isValid;
    }

    private boolean isDayOff(int dayOfWeek) {
        boolean isDayOff = false;

        switch (dayOfWeek) {
            case 1:
                if (!workdays.isSunday()) isDayOff = true;
                break;
            case 2:
                if (!workdays.isMonday()) isDayOff = true;
                break;
            case 3:
                if (!workdays.isTuesday()) isDayOff = true;
                break;
            case 4:
                if (!workdays.isWednesday()) isDayOff = true;
                break;
            case 5:
                if (!workdays.isThursday()) isDayOff = true;
                break;
            case 6:
                if (!workdays.isFriday()) isDayOff = true;
                break;
            case 7:
                if (!workdays.isSaturday()) isDayOff = true;
                break;
        }

        return isDayOff;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener)
    {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);
        if (onDismissListener != null) onDismissListener.onDismiss(dialog);
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}