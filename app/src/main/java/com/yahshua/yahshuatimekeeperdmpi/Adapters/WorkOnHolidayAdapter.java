package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;

public class WorkOnHolidayAdapter extends ArrayAdapter<WorkOnHoliday>
{
    public WorkOnHolidayAdapter(Context context, ArrayList<WorkOnHoliday> workOnHolidayArrayList)
    {
        super(context, 0, workOnHolidayArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        WorkOnHoliday workOnHoliday = getItem(position);

        if (convertView == null) convertView = LayoutInflater.from(getContext()).inflate(R.layout.workonholiday_listrow, parent, false);


        TextView tvDate = convertView.findViewById(R.id.tvDate);
        TextView tvHours = convertView.findViewById(R.id.tvHours);
        TextView tvReason = convertView.findViewById(R.id.tvReason);
        TextView tvLastSyncedDate = (TextView) convertView.findViewById(R.id.tvLastSyncedDate);

        tvDate.setText(DateToString.displayFormat(workOnHoliday.getDate()));
        tvReason.setText("Reason: " + workOnHoliday.getReason());

        // Hours
        double hoursDouble = workOnHoliday.getHours();
        int hoursInt;

        if (hoursDouble % 1 == 0) {
            hoursInt = (int) hoursDouble;
            tvHours.setText("Hours: " + String.valueOf(hoursInt));
        } else {
            tvHours.setText("Hours: " + String.valueOf(hoursDouble));
        }

        // Sync Status
        TextView tvSyncStatus = convertView.findViewById(R.id.tvSyncStatus);
        String syncStatus;

        if (workOnHoliday.isSync()) {
            syncStatus = "Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.alreadySynced));
        } else {
            syncStatus = "Not Yet Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.notYetSynced));
        }

        if(workOnHoliday.getLastSyncedDate() != null)
        {
            tvLastSyncedDate.setVisibility(View.VISIBLE);
            tvLastSyncedDate.setText("Upload Date: "+workOnHoliday.getLastSyncedDate());
        }
        else
        {
            tvLastSyncedDate.setVisibility(View.GONE);
        }

        tvSyncStatus.setText(syncStatus);


        return convertView;
    }
}
