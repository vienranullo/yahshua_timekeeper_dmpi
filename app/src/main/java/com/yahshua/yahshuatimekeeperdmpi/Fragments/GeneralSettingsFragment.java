package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.CheckUnsyncedItemsDialog;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.SelectApiUrlDialog;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import es.dmoral.toasty.Toasty;


public class GeneralSettingsFragment extends Fragment
{
    // Views and Context
    private Context context;
    private View view;

    // Widgets
    private Button btnDeveloperMode, btnDeleteDatabase, btnSelectApiUrl;

    // Data
    private boolean show;

    // View layout design
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_general_settings, container, false);
        context = getContext();

        initializeUI();
        return view;
    }

    // Initialize components from a layout file
    private void initializeUI()
    {
        // Buttons
        btnDeveloperMode = view.findViewById(R.id.btnDeveloperMode);
        btnDeveloperMode.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                promptPassword();
            }
        });

        btnDeleteDatabase = view.findViewById(R.id.btnDeleteDatabase);
        btnDeleteDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckUnsyncedItemsDialog checkUnsyncedItemsDialog = new CheckUnsyncedItemsDialog();
                checkUnsyncedItemsDialog.show(getActivity().getFragmentManager(),"");

            }
        });

        btnSelectApiUrl = view.findViewById(R.id.btnSelectApiUrl);
        btnSelectApiUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // Create password edit text
                final EditText etPassword = new EditText(context);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                etPassword.setLayoutParams(layoutParams);
                etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                // Create admin password dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Developer Password")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                try
                                {
                                    if (etPassword.getText().toString().equals("colgate"))
                                    {
                                        SelectApiUrlDialog selectApiUrlDialog = new SelectApiUrlDialog();
                                        selectApiUrlDialog.show(getFragmentManager(), "Select API");
                                    }
                                    else
                                    {
                                        Toasty.warning(context, "Invalid Password").show();
                                    }
                                }
                                catch (Exception err)
                                {
                                    Toasty.error(context, "Error showing developer password").show();
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null);

                builder.setView(etPassword);

                builder.create().show();
            }
        });
    }

    // Secure delete and recreate database
    private void promptPassword()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Title
        builder.setTitle("Enter Admin Password");

        // Content Area
        final EditText etWrongPassword = new EditText(getContext());

        etWrongPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        etWrongPassword.setLayoutParams(layoutParams);

        builder.setView(etWrongPassword);

        // Action Buttons
        builder.setPositiveButton("Okay", null);
        builder.setNegativeButton("Cancel", null);

        final AlertDialog alertDialog = builder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface)
            {
                Button okayBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);

                okayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        if (etWrongPassword.getText().toString().equals("123456"))
                        {
                            show = !show;
                            showDiagnosticInterface(show);
                            btnDeveloperMode.setVisibility(View.GONE);
                            alertDialog.dismiss();
                        } else {
                            etWrongPassword.setError("Wrong Password");
                        }
                    }
                });
            }
        });

        alertDialog.show();
    }

    // Hide delete and recreate database buttons
    private void showDiagnosticInterface(boolean show)
    {
        btnDeleteDatabase.setVisibility(show ? View.VISIBLE : View.GONE);
        btnSelectApiUrl.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}
