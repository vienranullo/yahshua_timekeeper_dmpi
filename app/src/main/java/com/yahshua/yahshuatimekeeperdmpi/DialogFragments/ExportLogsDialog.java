package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.support.v4.app.DialogFragment;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class ExportLogsDialog extends DialogFragment {

    // View
    private View view;
    private Context context;

    //Widgets
    private Button btnExportLogs, btnCancel;
    private ProgressBar progressBar;
    private CheckBox cbSync, cbUnSync;
    private EditText etDateFrom, etDateTo;
    private LinearLayout layoutExportButton, layoutProgressBar;

    //Data
    int nameCount = 0;
    private MediaPlayer mediaPlayer;
    private boolean isSync = true, isAllRecords = false;
    private ArrayList<EmployeeLogs> employeeLogsArrayList = new ArrayList<>();
    private String folderName = "DMPI Timekeeper", fileName = "", from = "", to = "";

    //Date
    private String dateToUpdate;
    private Date dateFrom, dateTo;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener onDateSetListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.export_logs_dialog, container, false);
        context = view.getContext();

        initializeUI();

        return view;
    }

    private void initializeUI() {

        cbSync = view.findViewById(R.id.cb_sync);
        cbUnSync = view.findViewById(R.id.cb_unsync);
        etDateTo = view.findViewById(R.id.et_date_to);
        progressBar = view.findViewById(R.id.pb_export);
        etDateFrom = view.findViewById(R.id.et_date_from);
        btnExportLogs = view.findViewById(R.id.btn_export);
        btnCancel = view.findViewById(R.id.btnCancel);
        layoutProgressBar = view.findViewById(R.id.layout_progess);
        layoutExportButton = view.findViewById(R.id.layout_export_button);

        cbSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    btnExportLogs.setEnabled(true);
                    isSync = true;
                }if(!cbUnSync.isChecked() && !cbSync.isChecked()){
                    btnExportLogs.setEnabled(false);
                }if(cbUnSync.isChecked() && cbSync.isChecked()){
                    isAllRecords = true;
                }if(!cbUnSync.isChecked() || !cbSync.isChecked()){
                    isAllRecords = false;
                }

            }
        });

        cbUnSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    btnExportLogs.setEnabled(true);
                    isSync = false;
                }if(!cbUnSync.isChecked() && !cbSync.isChecked()){
                    btnExportLogs.setEnabled(false);
                }if(cbUnSync.isChecked() && cbSync.isChecked()){
                    isAllRecords = true;
                }if(!cbUnSync.isChecked() || !cbSync.isChecked()){
                    isAllRecords = false;
                }
            }
        });



        // Date and Time Listeners
        onDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                calendar.set(year, month, dayOfMonth);
                updateDate();
            }

        };

        etDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                dateToUpdate = "DATE-FROM";
                calendar.setTime(dateFrom);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();

            }
        });

        etDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                dateToUpdate = "DATE-TO";
                calendar.setTime(dateTo);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        dateFrom = dateTo = calendar.getTime();
        calendar.setTime(dateFrom);

        btnExportLogs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 if (validate())
                 {
                     employeeLogsArrayList =  EmployeeLogs.readForExport(view.getContext(), Converter.BooleanToInt(isSync), isAllRecords, from, to);
                     if (employeeLogsArrayList.size() > 0)
                     {
                         new ExportLogsDialog.ExportEmployeeLogsTask(context).execute();
                     } else
                     {
                         Toasty.warning(context, "No logs for this date range", Toast.LENGTH_LONG).show();
                     }
                 }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        cbSync.setChecked(true);
        setDefaultDate();
    }

    private void setDefaultDate()
    {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, cal.getActualMinimum(Calendar.DATE));

        dateFrom = cal.getTime();
        dateTo = Calendar.getInstance().getTime();

        etDateFrom.setText(DateToString.formFormat(dateFrom));
        etDateTo.setText(DateToString.formFormat(dateTo));

    }

    private boolean validate(){

        if(!etDateFrom.getText().toString().equals("") && etDateTo.getText().toString().equals("")){
            etDateTo.setError(context.getString(R.string.error_field_required));
            etDateTo.requestFocus();
            return false;
        }else if(!etDateTo.getText().toString().equals("") && etDateFrom.getText().toString().equals("")){
            etDateFrom.setError(context.getString(R.string.error_field_required));
            etDateFrom.requestFocus();
            return false;
        }else if(!etDateTo.getText().toString().equals("") && !etDateFrom.getText().toString().equals("")){
            from = DateToString.dbQuery(dateFrom);
            to = DateToString.dbQuery(dateTo);
            return true;
        }else {
            from = "";
            to = "";
            return true;
        }

    }

    private void exportToCsv()
    {
        File directoryDownload = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File logDir = new File (directoryDownload, folderName); //Creates a new folder in DOWNLOAD directory
        logDir.mkdir();

        fileName = "Employeelogs.csv";

        File file = new File(logDir, fileName);

        //Increment file name
        while (file.exists()){
             fileName = "Employeelogs"+("("+nameCount++ +")")+".csv";
             file = new File(logDir, fileName);
        }

        employeeLogsArrayList =  EmployeeLogs.readForExport(view.getContext(), Converter.BooleanToInt(isSync), isAllRecords, from, to);
        try
        {
            FileOutputStream outputStream = null;
            outputStream = new FileOutputStream(file, true);

            outputStream.write(("NAME" + ",").getBytes());
            outputStream.write(("EMPLOYEE ID" + ",").getBytes());
            outputStream.write(("LOG TYPE" + ",").getBytes());
            outputStream.write(("LOG TIME" + ",").getBytes());
            outputStream.write(("DATE" + ",").getBytes());
            outputStream.write(("SYNC" + "\n").getBytes());

            for (EmployeeLogs employeeLogs : employeeLogsArrayList)
            {
                outputStream.write((employeeLogs.employeeObject.getFullName() + ",").getBytes());
                outputStream.write((employeeLogs.employeeObject.getEmployeeid2() + ",").getBytes());
                outputStream.write((employeeLogs.getLogType() + ",").getBytes());
                outputStream.write((employeeLogs.getLogTime() + ",").getBytes());
                outputStream.write((employeeLogs.getDate() + ",").getBytes());
                outputStream.write((Converter.IntToBoolean(employeeLogs.isSync()) + "\n").getBytes());
            }

            outputStream.close();

        } catch (Exception e)
        {
            Debugger.logD("Exception exportToCsv "+e.getMessage());
        }
    }

    private void showSpinner(boolean show){

        layoutExportButton.setVisibility(show ? View.GONE : View.VISIBLE);
        layoutProgressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    public  String getMimeType(String url) {
        String type = null;
        String extension =  MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }else{
            Toasty.warning(context, "No handler for this type of file.", Toast.LENGTH_SHORT).show();
            Debugger.logD("No handler for this type of file");
        }
        return type;
    }

    private void sendNotification(){

        String channelId = "channel-01";
        String channelName = "Channel Name";

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),folderName+"/"+fileName);

        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri uriForFile = FileProvider.getUriForFile(context, "com.yahshua.yahshuatimekeeperdmpi.fileProvider", file);
                        intent.setDataAndType(uriForFile, getMimeType(file.getPath()));
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification.Builder mNotify  = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Export Finished")
                .setContentText("File is located in Internal/Download/"+folderName+"/")
                .setContentIntent(pIntent)
                .setAutoCancel(true);

        mediaPlayer = MediaPlayer.create(context,  Settings.System.DEFAULT_NOTIFICATION_URI);
        mediaPlayer.start();
        mediaPlayer.setLooping(false);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            NotificationChannel nChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
            nChannel.enableLights(true);
            mNotify.setChannelId(channelId);
            mNotificationManager.createNotificationChannel(nChannel);
        }
        mNotificationManager.notify(0, mNotify.build());

    }

    private void updateDate()
    {
        switch (dateToUpdate) {
            case "DATE-FROM":
                dateFrom = calendar.getTime();
                etDateFrom.setText(DateToString.formFormat(dateFrom));
                break;
            case "DATE-TO":
                dateTo = calendar.getTime();
                etDateTo.setText(DateToString.formFormat(dateTo));
                break;

        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class ExportEmployeeLogsTask extends AsyncTask<String ,String, String> {

        private Context context;
        public ExportEmployeeLogsTask (Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            showSpinner(true);
            getDialog().setCancelable(false);
        }

        protected String doInBackground(final String... args){

            exportToCsv();

            return "";
        }

        @Override
        protected void onPostExecute(final String success) {

            showSpinner(false);
            getDialog().setCancelable(true);

            if (success.isEmpty()){
                Toasty.success(context, "Export successful!", Toast.LENGTH_LONG).show();
                dismiss();
                sendNotification();
                AuditLog.saveAuditLog(context,"EXPORT LOGS","EXPORT","SUCCESS","NONE");
            }
            else {
                Toasty.warning(context, "Export failed!", Toast.LENGTH_SHORT).show();
                AuditLog.saveAuditLog(context, "EXPORT LOGS", "EXPORT","FAILED","NONE");
            }
        }

    }


    @Override
    public void onResume()
    {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    public int getTheme()
    {
        return R.style.full_screen_dialog;
    }

}
