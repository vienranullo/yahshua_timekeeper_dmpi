package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.util.ArrayList;


public class DeductionType implements Parcelable
{
    private int id;
    private String name;
    @SerializedName("company_id")
    private int companyId;
    private boolean isSelected = false;

    public boolean isSelected()
    {
        return isSelected;
    }

    public void setSelected(boolean selected)
    {
        isSelected = selected;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public static ArrayList<DeductionType> read(Context context)
    {
        ArrayList<DeductionType> deductionTypeArrayList = new ArrayList<>();
        try
        {
            String selectQuery = "SELECT * FROM deductionTypes WHERE company_id = " + UserSession.getCompanyId(context);

            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst())
            {
                do
                {
                    DeductionType deductionType = new DeductionType();

                    deductionType.setId(Converter.ObjectInt(cursor, "deduction_type_id"));
                    deductionType.setName(Converter.ObjectToString(cursor, "name"));
                    deductionType.setCompanyId(Converter.ObjectInt(cursor, "company_id"));

                    deductionTypeArrayList.add(deductionType);
                } while (cursor.moveToNext());
            }

            return deductionTypeArrayList;
        }
        catch (Exception err)
        {
//            ErrorLogs.save(context, "DeductionType read "+err.getMessage());
            return deductionTypeArrayList;
        }
    }

    public DeductionType getInstance(Context context, int deductionTypeId)
    {
        try
        {
            String selectQuery = "SELECT * FROM deductionTypes WHERE deduction_type_id = " + deductionTypeId;

            DatabaseAdapter db = new DatabaseAdapter(context);
            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst())
            {
                setId(Converter.ObjectInt(cursor, "deduction_type_id"));
                setName(Converter.ObjectToString(cursor, "name"));
                setCompanyId(Converter.ObjectInt(cursor, "company_id"));

                return this;
            } else{
                return null;
            }
        } catch (Exception err)
        {
//            ErrorLogs.save(context, "DeductionType getInstance "+err.getMessage());
            return null;
        }
    }

    public boolean save(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            ContentValues contentValues = new ContentValues();

            contentValues.put("deduction_type_id", getId());
            contentValues.put("name", getName());
            contentValues.put("company_id", UserSession.getCompanyId(context));

            if (this.getInstance(context, getId()) != null)
            {
                return db.update(contentValues, "deductionTypes", " deduction_type_id = ?", new String[]{ String.valueOf(getId()) });
            } else
            {
                return db.save(contentValues, "deductionTypes");
            }

        } catch (Exception err)
        {
//            ErrorLogs.save(context, "DeductionType save "+err.getMessage());
            return false;
        }
    }

    public static boolean delete(Context context)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            db.delete("deductionTypes",null,null);
        }
        catch (Exception err)
        {
//            ErrorLogs.save(context, "DeductionType delete "+err.getMessage());
            return false;
        }
        return true;
    }

    protected DeductionType(Parcel in)
    {
        id = in.readInt();
        name = in.readString();
    }

    public DeductionType() {
    }

    public static final Creator<DeductionType> CREATOR = new Creator<DeductionType>()
    {
        @Override
        public DeductionType createFromParcel(Parcel in)
        {
            return new DeductionType(in);
        }

        @Override
        public DeductionType[] newArray(int size)
        {
            return new DeductionType[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(name);
    }
}
