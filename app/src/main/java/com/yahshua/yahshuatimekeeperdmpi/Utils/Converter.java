package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.database.Cursor;
import java.text.DecimalFormat;
import java.util.Date;


public class Converter
{
    public static double ObjectToDouble(Cursor cursor, String fieldName)
    {
        try
        {
            return cursor.getDouble(cursor.getColumnIndex(fieldName));
        } catch (NumberFormatException err)
        {
            return 0;
        }
    }

    public static int ObjectInt(Cursor cursor, String fieldName)
    {
        try
        {
            String sValue = cursor.getString(cursor.getColumnIndex(fieldName));
            if (sValue == null || sValue.isEmpty()) return 0;
            return Integer.parseInt(sValue);
        } catch (NumberFormatException err)
        {
            return 0;
        }
    }

    public static String ObjectToString(Cursor cursor, String fieldName)
    {
        try
        {
            return cursor.getString(cursor.getColumnIndex(fieldName));
        } catch (Exception err)
        {
            return "";
        }
    }

    public static Date ObjectToDate(Cursor cursor, String fieldName)
    {
        try
        {
            return DateTimeHandler.convertStringtoDate(ObjectToString(cursor,fieldName));
        } catch (Exception err)
        {
            return null;
        }
    }

    public static boolean ObjectToBoolean(Cursor cursor, String fieldName)
    {
        try
        {
            int result = ObjectInt(cursor,fieldName);
            return result == 1;
        } catch (Exception err)
        {
            return false;
        }
    }

    public static String IntToBoolean(boolean boolValue)
    {
        try
        {
            return boolValue ? "True" : "False";
        } catch (Exception err)
        {
            return "";
        }
    }

    public static int BooleanToInt(boolean boolValue)
    {
        try
        {
            return boolValue ? 1 : 0;
        } catch (Exception err)
        {
            return 0;
        }
    }

    public static boolean IntToBoolean(int intValue)
    {
        return intValue == 1;
    }

    public static String ConvertCurrencyDisplay(double amount)
    {
        String countDecimal = String.valueOf(amount);
        String addZero = "0";
        int finalCount = countDecimal.substring(countDecimal.indexOf(".") + 1).length();
        StringBuilder total = new StringBuilder();

        for (int i = 0; i < finalCount; i++)
        {
            total.append(addZero);
        }

        if(total.toString().length() == 1)
        {
            total.append("0");
        }

        DecimalFormat formatter = new DecimalFormat("#,###."+total);
        return formatter.format(amount);
    }

    public static String addZeroes(String val)
    {
        if (val.equals(".00")) val = "00" + val;
        return val;
    }


}
