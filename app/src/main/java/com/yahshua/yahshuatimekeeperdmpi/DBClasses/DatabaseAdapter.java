package com.yahshua.yahshuatimekeeperdmpi.DBClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;


public class DatabaseAdapter
{
    public static SQLiteDatabase db;
    private final Context context;
    private DatabaseHelper dbHelper;

    public DatabaseAdapter(Context _context)
    {

        context = _context;
        dbHelper = new DatabaseHelper(context, null);
    }

    public DatabaseAdapter open() throws SQLException
    {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public static void close()
    {
        db.close();
    }


    public Cursor read(String query)
    {
        return open().getDatabaseInstance().rawQuery(query, null);
    }

    public SQLiteDatabase getDatabaseInstance()
    {
        return db;
    }


    public boolean save(ContentValues contentValues, String table)
    {
        try
        {
            open();

            if (db.insert(table, null, contentValues) == -1) {
                throw new SQLException("INSERT ERROR");
            }

            close();

            return true;

        } catch (SQLException err) {
            Log.e("ERROR SQL", err.toString());
            return false;
        }
    }

    public boolean save2(ContentValues contentValues, String table) {
        try{
            open();

            db.insertOrThrow(table, null, contentValues);

            close();

            return true;

        } catch (SQLException err) {
            Debugger.printO("Error Saving: " + err.toString());
            return false;
        }
    }

    public int delete(String table, String where, String[] Args) {
        open();

        int numberOFEntriesDeleted = db.delete(table, where, Args);

        close();

        return numberOFEntriesDeleted;
    }

    public boolean update(ContentValues contentValues, String table, String where, String[] args)
    {
        db.update(table,contentValues, where, args);
        return true;
    }

}