package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class DeviceSettings
{
    private boolean autoDateTime;
    private visibleMenuItems visibleMenuItems;
    @SerializedName("break_interval_settings_list")
    ArrayList<BreakIntervalSettingsList> breakIntervalSettingsListArrayList;

    public ArrayList<BreakIntervalSettingsList> getBreakIntervalSettingsListArrayList() {
        return breakIntervalSettingsListArrayList;
    }

    public void setBreakIntervalSettingsListArrayList(ArrayList<BreakIntervalSettingsList> breakIntervalSettingsListArrayList) {
        this.breakIntervalSettingsListArrayList = breakIntervalSettingsListArrayList;
    }

    public boolean isAutoDateTime()
    {
        return autoDateTime;
    }

    public void setAutoDateTime(boolean autoDateTime)
    {
        this.autoDateTime = autoDateTime;
    }

    public DeviceSettings.visibleMenuItems getVisibleMenuItems()
    {
        return visibleMenuItems;
    }

    public void setVisibleMenuItems(DeviceSettings.visibleMenuItems visibleMenuItems)
    {
        this.visibleMenuItems = visibleMenuItems;
    }

    public class visibleMenuItems
    {
        private boolean timeIn;
        private boolean timeOut;
        private boolean uploadAll;
        private boolean applications;
        private boolean leave;
        private boolean overtime;
        private boolean underTime;
        private boolean workOnRestDay;
        private boolean workOnHoliday;

        public boolean isTimeIn()
        {
            return timeIn;
        }

        public void setTimeIn(boolean timeIn)
        {
            this.timeIn = timeIn;
        }

        public boolean isTimeOut()
        {
            return timeOut;
        }

        public void setTimeOut(boolean timeOut)
        {
            this.timeOut = timeOut;
        }

        public boolean isUploadAll()
        {
            return uploadAll;
        }

        public void setUploadAll(boolean uploadAll)
        {
            this.uploadAll = uploadAll;
        }

        public boolean isApplications()
        {
            return applications;
        }

        public void setApplications(boolean applications)
        {
            this.applications = applications;
        }

        public boolean isLeave()
        {
            return leave;
        }

        public void setLeave(boolean leave)
        {
            this.leave = leave;
        }

        public boolean isOvertime()
        {
            return overtime;
        }

        public void setOvertime(boolean overtime)
        {
            this.overtime = overtime;
        }

        public boolean isUnderTime()
        {
            return underTime;
        }

        public void setUnderTime(boolean underTime)
        {
            this.underTime = underTime;
        }

        public boolean isWorkOnRestDay()
        {
            return workOnRestDay;
        }

        public void setWorkOnRestDay(boolean workOnRestDay)
        {
            this.workOnRestDay = workOnRestDay;
        }

        public boolean isWorkOnHoliday()
        {
            return workOnHoliday;
        }

        public void setWorkOnHoliday(boolean workOnHoliday)
        {
            this.workOnHoliday = workOnHoliday;
        }
    }

    public DeviceSettings()
    {
        this.visibleMenuItems = new visibleMenuItems();
    }

    public void save(Context context) throws Exception
    {
        try
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            Gson gson = new Gson();
            String deviceSettingString = gson.toJson(this);

            editor.putString("DEVICE_SETTINGS", deviceSettingString);
            editor.apply();

            saveBreakTime(context);
        }
        catch (Exception err)
        {
            throw new Exception("Error Saving Device Settings: \n" + err.toString());
        }
    }

    public void saveBreakTime(Context context) throws Exception{

        try {

            for (BreakIntervalSettingsList breakInterval : breakIntervalSettingsListArrayList){

                breakInterval.save(context, breakIntervalSettingsListArrayList);
            }
        }

        catch (Exception err)
        {
            throw new Exception("Error saveBreakTime  Fields: \n" + err.toString());
        }
    }
}