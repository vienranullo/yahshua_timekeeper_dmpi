package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.DialogAlertInterfaces;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.PopUpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserPreferences;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

public class SettingsFragment extends Fragment
{
    private View view;
    private Context context;
    private Switch aSwitchAutoSync;
    private Button btnClearsynclogs;
    private Spinner spLogInterval;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        context = getContext();
        initializeUI(view);
        readSettings();

        return view;
    }

    private void initializeUI(View view)
    {
        aSwitchAutoSync = view.findViewById(R.id.swAutoSync);
        aSwitchAutoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                UserPreferences.setAutoInstantSyncPreferences(context, aSwitchAutoSync.isChecked());
            }
        });

        btnClearsynclogs = view.findViewById(R.id.btnClearsyncedlogs);
        btnClearsynclogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpProvider.getConfirmDialog(context,"Are you sure?", "this will clear all Synced Logs", "Yes", "No", false,
                        new DialogAlertInterfaces() {

                            @Override
                            public void PositiveMethod(final DialogInterface dialog, final int id) {

                                EmployeeLogs.clearSyncedLogs(context);
                                Toasters.ShowSnackBar(getView(), "Synced Logs Cleared");
                            }

                            @Override
                            public void NegativeMethod(DialogInterface dialog, int id) {
                            }
                        });
            }
        });

        spLogInterval = view.findViewById(R.id.spLogInterval);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
                getActivity(), R.array.pref_log_interval_titles, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLogInterval.setAdapter(adapter2);

        spLogInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());  //context
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.putString("log_interval",spLogInterval.getSelectedItem().toString());
                prefEditor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });

        String log_interval=PreferenceManager
                .getDefaultSharedPreferences(getActivity().getBaseContext())
                .getString("log_interval","");

        for(int i=0; i<4; i++)
            if(log_interval.equals(spLogInterval.getItemAtPosition(i).toString())){
                spLogInterval.setSelection(i);
                break;
            }
    }

    private void readSettings()
    {
        aSwitchAutoSync.setChecked(UserPreferences.getAutoInstantSyncPreferences(context));

    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End

}
