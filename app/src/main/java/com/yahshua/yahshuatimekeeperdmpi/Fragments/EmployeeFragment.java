package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.EmployeeAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.LogOutAuthenticationDialog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;

/**
 * Created by AlfredVanz on 8/22/2017.
 */

public class EmployeeFragment extends Fragment
{

    // Views and Context
    private Context context;
    private View view;

    // Widgets
    private ListView lvEmployees;
    private SearchView etSearchView;

    // Adapter
    private EmployeeAdapter employeeAdapter;

    // Data
    private ArrayList<Employee> employeeList = new ArrayList<>();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_employee, container, false);
        context = view.getContext();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        initializeUI(view);
        readEmployees(null);
    }

    // Initialize component from a layout file
    private void initializeUI(View view)
    {

        // SearchView
        etSearchView = view.findViewById(R.id.etSearchView);

        etSearchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String searchValue)
            {
                readEmployees(searchValue);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String searchValue)
            {
                readEmployees(searchValue);
                return true;
            }
        });

        // ListView
        lvEmployees = view.findViewById(R.id.lvEmployee);
        employeeAdapter = new EmployeeAdapter(view.getContext(),employeeList );
        lvEmployees.setAdapter(employeeAdapter);
    }

    // Read data from database
    private void readEmployees(String searchValue)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        String searchQuery = null;

        if (searchValue != null) searchQuery = " WHERE (employeeid2 LIKE '%" + searchValue + "%' OR firstname LIKE '%" + searchValue + "%' OR lastname LIKE '%" + searchValue + "%')";

        employeeList = Employee.read(view.getContext(), searchQuery, dbAdapter);
        employeeAdapter.clear();
        employeeAdapter.addAll(employeeList);
        employeeAdapter.notifyDataSetChanged();

        dbAdapter.close();
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}