package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Debug;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class Employee implements Parcelable {

    private int id;
    @SerializedName("employeeid")
    private int employeeid2;
    private String firstname;
    private String lastname;
    private String middlename;
    private String employeeImage;
    private String fullName;
    private String rfid;
    @SerializedName("leave_type_balances")
    private ArrayList<LeaveTypeBalances> leaveTypeBalances = new ArrayList<>();

    public String getEmployeeImage() {
        return employeeImage;
    }

    public String getFullName2()
    {
        return this.fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }
    public void setEmployeeImage(String employeeImage) {
        this.employeeImage = employeeImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeid2() {
        return employeeid2;
    }

    public void setEmployeeid2(int employeeid2) {
        this.employeeid2 = employeeid2;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getFullName()
    {
        return this.firstname + " " + this.lastname;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public ArrayList<LeaveTypeBalances> getLeaveTypeBalances()
    {
        return leaveTypeBalances;
    }

    public void setLeaveTypeBalances(ArrayList<LeaveTypeBalances> leaveTypeBalances)
    {
        this.leaveTypeBalances = leaveTypeBalances;
    }

    public Employee(){}

    public EmployeeLogs checkExistingLogs(String logType, DbAdapter dbAdapter)
    {
        try
        {
            EmployeeLogs employeeLog = null;
            Date today = new Date();

            String selectQuery = "SELECT * FROM employee_logs WHERE employee = " + getId();
            selectQuery += " AND date = '" + DateTimeHandler.convertDatetoStringDate(today) + "'";
            selectQuery += " AND log_type = '" + logType + "'" ;

            Cursor cursor = dbAdapter.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()){
                do {
                    employeeLog = new EmployeeLogs();
                    employeeLog.setLogTime(Converter.ObjectToString(cursor,"log_time"));
                    employeeLog.setDate(Converter.ObjectToString(cursor,"date"));
                    employeeLog.setLogImg(Converter.ObjectToString(cursor,"log_img"));
                    employeeLog.setLogType(Converter.ObjectToString(cursor,"log_type"));

                } while (cursor.moveToNext());
            }

            cursor.close();

            return employeeLog;
        } catch (Exception err)
        {
            ErrorLogs.save( "EmployeeLogs checkExistingLogs error "+err.getMessage(), dbAdapter);
            return null;
        }
    }

    public static ArrayList<Employee> read(Context context, String searchQuery, DbAdapter dbAdapter)
    {
        ArrayList<Employee> employeeArrayList = new ArrayList<>();
        try
        {
            String selectQuery;
            if (searchQuery != null) {
                selectQuery = "SELECT * FROM employee "  + searchQuery + " AND company_id = "+ UserSession.getCompanyId(context)+ " ORDER BY id DESC";
            } else {
                selectQuery = "SELECT * FROM employee WHERE company_id = "+ UserSession.getCompanyId(context)+ " ORDER BY employeeid2 DESC";
            }

            Cursor cursor = dbAdapter.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Employee employee = new Employee();

                    employee.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("ypo_id"))));
                    employee.setFirstname(cursor.getString(cursor.getColumnIndex("firstname")));
                    employee.setMiddlename(cursor.getString(cursor.getColumnIndex("middlename")));
                    employee.setLastname(cursor.getString(cursor.getColumnIndex("lastname")));
                    employee.setEmployeeid2(Integer.parseInt(cursor.getString(cursor.getColumnIndex("employeeid2"))));
                    employee.setRfid(cursor.getString(cursor.getColumnIndex("rfid")));
                    employeeArrayList.add(employee);

                } while (cursor.moveToNext());
            }

            cursor.close();

            return employeeArrayList;
        }
        catch (Exception err)
        {
            ErrorLogs.save("Employee read "+ err.getMessage(), dbAdapter);
            Debugger.logD("Employee read "+err.getMessage());
            return employeeArrayList;
        }
    }

    public static Employee readEmployeeByRFID(Context context, String RFID, DbAdapter dbAdapter)
    {
        try
        {
            String selectQuery;

            selectQuery = "SELECT * FROM employee WHERE rfid LIKE '%"+RFID+"%' AND company_id = "+ UserSession.getCompanyId(context);

            Cursor cursor = dbAdapter.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Employee employee = new Employee();

                    employee.setEmployeeid2(cursor.getInt(cursor.getColumnIndex("employeeid2")));
                    employee.setId(cursor.getInt(cursor.getColumnIndex("ypo_id")));
                    employee.setFirstname(cursor.getString(cursor.getColumnIndex("firstname")));
                    employee.setLastname(cursor.getString(cursor.getColumnIndex("lastname")));
                    employee.setRfid(cursor.getString(cursor.getColumnIndex("rfid")));

                    return employee;

                } while (cursor.moveToNext());
            }

            cursor.close();
            return null;

        } catch (Exception err)
        {
            ErrorLogs.save("Employee readEmployeeByRFID "+ err.getMessage(), dbAdapter);
            return null;
        }
    }

    public boolean save(Context context, DbAdapter dbAdapter)
    {
        try
        {
            ContentValues contentValues = new ContentValues();

            contentValues.put("ypo_id", getId());
            contentValues.put("employeeid2", getEmployeeid2());
            contentValues.put("firstname", getFirstname());
            contentValues.put("lastname", getLastname());
            contentValues.put("middlename", getMiddlename());
            contentValues.put("employee_img",getEmployeeImage());
            contentValues.put("rfid",getRfid());
            contentValues.put("company_id", UserSession.getCompanyId(context));

            dbAdapter.getDb().insertOrThrow("employee", null, contentValues);

            return true;

        }
        catch (Exception err)
        {
            ErrorLogs.save("Employee save error "+err.getMessage(), dbAdapter);
            return false;
        }
    }

    public static void delete(DbAdapter dbAdapter) throws SQLException
    {
        dbAdapter.getDb().delete("employee", "", new String[]{});
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(employeeid2);
        dest.writeString(firstname);
        dest.writeString(middlename);
        dest.writeString(lastname);
        dest.writeString(employeeImage);
    }

    protected Employee(Parcel in)
    {
        id = in.readInt();
        employeeid2 = in.readInt();
        firstname = in.readString();
        middlename = in.readString();
        lastname = in.readString();
        employeeImage = in.readString();
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>()
    {
        @Override
        public Employee createFromParcel(Parcel in)
        {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size)
        {
            return new Employee[size];
        }
    };
}
