package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.OvertimeRequestsAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.RequestsFilterDialogFragment;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;
import com.yahshua.yahshuatimekeeperdmpi.httpRequests.DownloadOvertimeRequestsAsyncTask;

import org.json.JSONObject;

import java.util.ArrayList;


public class OvertimeRequestsActivity extends BaseActivity
{
    private ArrayList<Overtime> overtimeArrayList = new ArrayList<>();
    private OvertimeRequestsAdapter overtimeRequestsAdapter;
    private ProgressBar pbDownloadOvertimes;
    private RecyclerView rvOvertimeRequests;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            initializeViews();
            initializeData(null);
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error OvertimeRequestsActivity onCreate: \n" + err.toString());
        }
    }

    private void initializeViews() throws Exception
    {
        try
        {
            setContentView(R.layout.overtime_requests_activity);
            initializeRecyclerViews();

            // Progress bar
            pbDownloadOvertimes = findViewById(R.id.pbDownloadOvertimes);
            pbDownloadOvertimes.setVisibility(View.VISIBLE);

            // Title
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) throw new Exception("ActionBar is null");
            actionBar.setTitle("Overtime Requests");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: \n" + err.toString());
        }
    }

    private void initializeRecyclerViews() throws Exception
    {
        try
        {
            rvOvertimeRequests = findViewById(R.id.rvOvertimeRequests);
            rvOvertimeRequests.setVisibility(View.GONE);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvOvertimeRequests.setLayoutManager(layoutManager);

            overtimeRequestsAdapter = new OvertimeRequestsAdapter(this, overtimeArrayList);
            rvOvertimeRequests.setAdapter(overtimeRequestsAdapter);
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: \n" + err.toString());
        }
    }

    private void initializeData(JSONObject jsonObject) throws Exception
    {
        try
        {
            // Download overtime requests
            AsyncResponseListener asyncResponseListener = new AsyncResponseListener()
            {
                @Override
                public void onSuccess(Object object)
                {
                    overtimeArrayList.clear();
                    overtimeArrayList.addAll((ArrayList<Overtime>) object);
                    overtimeRequestsAdapter.notifyDataSetChanged();

                    pbDownloadOvertimes.setVisibility(View.GONE);
                    rvOvertimeRequests.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(int errorCode, String error)
                {
                    switch (errorCode)
                    {
                        case 2:
                            Utility.showNoNetworkConnectionDialog(OvertimeRequestsActivity.this);
                            break;
                        case 3:
                            Utility.showFailedToConnectToServerDialog(OvertimeRequestsActivity.this);
                            break;
                        case 4:
                            Utility.showServerErrorDialog(OvertimeRequestsActivity.this, error);
                            break;
                        default:
                            Utility.showError(getSupportFragmentManager(), "Error DownloadOvertimeRequestsAsyncTask: \n" + error);
                    }

                    pbDownloadOvertimes.setVisibility(View.GONE);
                }
            };

            // Default filter
            if (jsonObject == null)
            {
                jsonObject = new JSONObject();
                jsonObject.put("status", "Pending");
            }

            new DownloadOvertimeRequestsAsyncTask(this, asyncResponseListener, jsonObject).execute();
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing data: \n" + err.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        try
        {
            getMenuInflater().inflate(R.menu.request_filter_menu, menu);
            initializeSearchView(menu);
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error OvertimeRequestsActivity onCreateOptionsMenu: " + err.toString());
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void initializeSearchView(Menu menu) throws Exception
    {
        try
        {
            MenuItem searchItem = menu.findItem(R.id.action_search);

            // Expand and collapse listener
            MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener()
            {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item)
                {
                    try
                    {
                        rvOvertimeRequests.setVisibility(View.GONE);
                        pbDownloadOvertimes.setVisibility(View.VISIBLE);
                        initializeData(null);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager(), "Error onMenuItemActionCollapse: " + err.toString());
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item)
                {
                    return true;
                }
            };

            searchItem.setOnActionExpandListener(expandListener);

            // Query listener
            SearchView searchView = (SearchView) searchItem.getActionView();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
            {
                @Override
                public boolean onQueryTextChange(String newText)
                {
                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query)
                {
                    try
                    {
                        rvOvertimeRequests.setVisibility(View.GONE);
                        pbDownloadOvertimes.setVisibility(View.VISIBLE);

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("search_text", query);
                        jsonObject.put("status", "Pending");

                        initializeData(jsonObject);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager() ,"Error onQueryTextSubmit: " + err.toString());
                    }
                    return false;
                }
            });
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing searchView: " + err.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        try
        {
            switch (item.getItemId())
            {
                case android.R.id.home:
                    finish();
                    return true;
                case R.id.action_filter:
                    showFilterDialog();
                    break;
            }
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error OvertimeRequestsActivity onOptionsItemSelected: " + err.toString());
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog()
    {
        try
        {
            RequestsFilterDialogFragment requestsFilterDialogFragment = new RequestsFilterDialogFragment();

            requestsFilterDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {}
            });

            requestsFilterDialogFragment.setOnCompleteListener(new CompleteListener()
            {
                @Override
                public void onComplete(Object object)
                {
                    try
                    {
                        rvOvertimeRequests.setVisibility(View.GONE);
                        pbDownloadOvertimes.setVisibility(View.VISIBLE);
                        initializeData((JSONObject) object);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager(), "Error onComplete: " + err.toString());
                    }
                }
            });

            requestsFilterDialogFragment.show(getSupportFragmentManager(), "REQUEST_FILTER_DIALOG");
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error showing filters dialog: " + err.toString());
        }
    }
}
