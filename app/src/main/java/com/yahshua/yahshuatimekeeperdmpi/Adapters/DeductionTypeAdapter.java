package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.DeductionType;
import com.yahshua.yahshuatimekeeperdmpi.R;

import java.util.ArrayList;

public class DeductionTypeAdapter extends ArrayAdapter<DeductionType>
{
    private final int mResource;
    private ArrayList<DeductionType> deductionTypeArrayList;
    private FragmentManager fragmentManager;

    public DeductionTypeAdapter(@NonNull Context context, ArrayList<DeductionType> list)
    {
        super(context, 0, list);
        deductionTypeArrayList = list;
        mResource = 0;
        this.fragmentManager = fragmentManager;
    }

    @Override @NonNull
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        final DeductionType deductionType = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_deduction_type, parent, false);
        }

        final CheckedTextView checkedTextView = convertView.findViewById(R.id.tvSelected);

        assert deductionType != null;
        checkedTextView.setChecked(deductionType.isSelected());
        checkedTextView.setText(deductionType.getName());
        checkedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearCheckedItems();
                checkedTextView.toggle();
                deductionTypeArrayList.get(position).setSelected(checkedTextView.isChecked());
            }
        });

        return convertView;
    }

    private void clearCheckedItems()
    {
        for (int i = 0; i < this.getCount(); i++)
        {
            DeductionType object = this.getItem(i);
            assert object != null;
            object.setSelected(false);
        }

        notifyDataSetChanged();
    }
}

