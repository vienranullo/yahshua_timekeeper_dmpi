package com.yahshua.yahshuatimekeeperdmpi.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DateStringToDate
{
    public static Date dbFormat(String dateStr)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d", Locale.US);
        Date date = new Date();

        try {
            date = dateFormat.parse(dateStr);
        } catch (ParseException err) {
            Debugger.logD(err.toString());
        }

        return date;
    }

    public static Date timeDisplayFormat(String timeStr)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a", Locale.US);
        Date dateTime = new Date();
        Calendar calendarForTime = Calendar.getInstance();

        String[] timeArr = timeStr.split(":");

        calendarForTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArr[0]));
        calendarForTime.set(Calendar.MINUTE, Integer.parseInt(timeArr[1]));

        try {
            dateTime = dateFormat.parse(dateFormat.format(calendarForTime.getTime()));
        } catch (ParseException err) {
            Debugger.logD(err.toString());
        }

        return dateTime;
    }

    public static Date dateTimeDbFormat(String dateTimeStr) {
        SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyy-M-d HH:mm:ss", Locale.US);
        Date dateTime = new Date();

        try {
            dateTime = datetimeFormat.parse(dateTimeStr);
        } catch (ParseException err) {
            Debugger.logD(err.toString());
        }

        return dateTime;
    }

    public static Date dateTimeDbFormat2(String dateTimeStr)
    {
        SimpleDateFormat datetimeFormat = new SimpleDateFormat("MMM. dd, yyyy", Locale.US);
        Date dateTime = new Date();

        try
        {
            dateTime = datetimeFormat.parse(dateTimeStr);
        } catch (ParseException err) {
            Debugger.logD(err.toString());
        }

        return dateTime;
    }
}
