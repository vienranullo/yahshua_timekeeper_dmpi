package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;


public class SelectApiUrlDialog extends DialogFragment
{
    private SharedPreferences settings;
    private String selectedApi;
    private String localApiUrl;
    private ArrayList<String> apiArrayList = new ArrayList<>();

    private TextView tvApiLabel;
    private Spinner spApi;
    private ArrayAdapter<String> apiAdapter;
    private TextView tvUrlLabel;
    private EditText etUrl;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Add values to api spinner
        apiArrayList.add("LIVE");
        apiArrayList.add("LOCAL");

        // Get settings
        settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        selectedApi = settings.getString("SELECTED_API", "LIVE");
        localApiUrl = settings.getString("LOCAL_API_URL", "");

        // Create dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select API");

        // Parent layout
        LinearLayout parentLayout = new LinearLayout(getContext());
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        parentLayout.setOrientation(LinearLayout.VERTICAL);
        parentLayout.setPadding(50, 50, 50, 50);

        // First horizontal layout
        LinearLayout layout1 = new LinearLayout(getContext());
        layout1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 2.0f));
        layout1.setOrientation(LinearLayout.HORIZONTAL);

        // API label
        tvApiLabel = new TextView(getContext());

        LinearLayout.LayoutParams apiLabelLayoutParams = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        apiLabelLayoutParams.weight = 0.5f;
        tvApiLabel.setLayoutParams(apiLabelLayoutParams);

        tvApiLabel.setTextSize(18);
        tvApiLabel.setText("API:");

        // API spinner
        spApi = new Spinner(getContext());

        LinearLayout.LayoutParams apiSpinnerLayoutParams = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        apiSpinnerLayoutParams.weight = 1.5f;
        spApi.setLayoutParams(apiSpinnerLayoutParams);

        apiAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, apiArrayList);
        spApi.setAdapter(apiAdapter);

        spApi.setSelection(apiArrayList.indexOf(selectedApi));

        spApi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedApi = apiArrayList.get(position);
                if (selectedApi.equals("LIVE")) etUrl.setEnabled(false); else etUrl.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){}
        });


        layout1.addView(tvApiLabel);
        layout1.addView(spApi);
        parentLayout.addView(layout1);

        // Second horizontal layout
        LinearLayout layout2 = new LinearLayout(getContext());
        layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 2.0f));
        layout2.setOrientation(LinearLayout.HORIZONTAL);

        // URL label
        tvUrlLabel = new TextView(getContext());

        LinearLayout.LayoutParams urlLabelLayoutParams = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        urlLabelLayoutParams.weight = 0.5f;
        tvUrlLabel.setLayoutParams(urlLabelLayoutParams);

        tvUrlLabel.setTextSize(18);
        tvUrlLabel.setText("URL:");

        // Local URL
        etUrl = new EditText(getContext());

        LinearLayout.LayoutParams urlLayoutParams = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        urlLayoutParams.weight = 1.5f;
        etUrl.setLayoutParams(urlLayoutParams);

        etUrl.setText(localApiUrl);
        if (selectedApi.equals("LIVE")) etUrl.setEnabled(false);


        layout2.addView(tvUrlLabel);
        layout2.addView(etUrl);
        parentLayout.addView(layout2);

        builder.setView(parentLayout);

        // Action Buttons
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (selectedApi.equals("LOCAL"))
                {
                    localApiUrl = etUrl.getText().toString();
                }

                SharedPreferences.Editor editor = settings.edit();
                editor.putString("SELECTED_API", selectedApi);
                editor.putString("LOCAL_API_URL", localApiUrl);
                editor.apply();

                Toasty.success(getContext(), "Saved").show();
            }
        });
        builder.setNegativeButton("Cancel", null);


        return builder.create();
    }
}
