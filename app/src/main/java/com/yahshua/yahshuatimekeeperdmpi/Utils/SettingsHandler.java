package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class SettingsHandler {

    public static int getMinutesInterval(Context context)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(prefs.getString("log_interval","5"));
    }

    public static void setMinutesInterval(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("log_interval", "5");
        editor.commit();
    }

    public static int getMinutesInterval2(Context context)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(prefs.getString("interval","0"));
    }

    public static void setMinutesInterval2(Context context, String interval)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("interval", interval);
        editor.commit();
    }

    public static void isHaveBreakInterval(Context context, Boolean isHaveBreakInterval)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("HAVE_BREAK_INTERVAL", isHaveBreakInterval);
        editor.commit();
    }

    public static boolean haveBreakInterval(Context context)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("HAVE_BREAK_INTERVAL",false);
    }

}
