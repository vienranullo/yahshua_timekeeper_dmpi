package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.Leave;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;


public class LeaveAdapter extends ArrayAdapter<Leave>
{
    public LeaveAdapter(Context context, ArrayList<Leave> leaveArrayList)
    {
        super(context, 0, leaveArrayList);
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Leave leave = getItem(position);

        if (convertView == null) convertView = LayoutInflater.from(getContext()).inflate(R.layout.leave_listrow, parent, false);


        TextView tvHalfDayDate = (TextView) convertView.findViewById(R.id.tvHalfDayDate);
        TextView tvHalfDayTimeRange = (TextView) convertView.findViewById(R.id.tvHalfDayTimeRange);
        TextView tvDaysLeave = (TextView) convertView.findViewById(R.id.tvDaysLeave);
        TextView tvLeaveDates = (TextView) convertView.findViewById(R.id.tvLeaveDates);
        TextView tvLastSyncedDate = (TextView) convertView.findViewById(R.id.tvLastSyncedDate);

        tvHalfDayDate.setVisibility(View.VISIBLE);
        tvHalfDayTimeRange.setVisibility(View.VISIBLE);
        tvDaysLeave.setVisibility(View.VISIBLE);
        tvLeaveDates.setVisibility(View.VISIBLE);


        if (leave.getDay().equals("whole")) {
            // Days Leave
            tvDaysLeave = (TextView) convertView.findViewById(R.id.tvDaysLeave);
            int daysLeave = 0;

            if (leave.getDaysLeave() % 1 == 0) daysLeave = (int) leave.getDaysLeave();

            String dayStr = (daysLeave == 1) ? " Day" : " Days";

            tvDaysLeave.setText(Integer.toString(daysLeave) + dayStr);

            // Dates
            tvLeaveDates = (TextView) convertView.findViewById(R.id.tvLeaveDates);
            tvLeaveDates.setText(Utility.dateFromDateToDisplay(leave.getDateFrom(), leave.getDateTo()));

            // Hide Views
            tvHalfDayDate.setVisibility(View.GONE);
            tvHalfDayTimeRange.setVisibility(View.GONE);

        } else if (leave.getDay().equals("half")) {
            // Date
            tvHalfDayDate = (TextView) convertView.findViewById(R.id.tvHalfDayDate);
            tvHalfDayDate.setText(Utility.dateDisplay(leave.getHalfDayDate()));

            // Time Range
            tvHalfDayTimeRange = (TextView) convertView.findViewById(R.id.tvHalfDayTimeRange);

            String timeFrom = DateToString.timeDisplayFormat(DateStringToDate.timeDisplayFormat(leave.getTimeFrom()));
            String timeTo = DateToString.timeDisplayFormat(DateStringToDate.timeDisplayFormat(leave.getTimeTo()));

            tvHalfDayTimeRange.setText("From " + timeFrom + " to " + timeTo);

            // Hide Views
            tvDaysLeave.setVisibility(View.GONE);
            tvLeaveDates.setVisibility(View.GONE);
        }

        // Sync Status
        TextView tvSyncStatus = (TextView) convertView.findViewById(R.id.tvSyncStatus);
        String syncStatus;

        if (leave.isSync())
        {
            syncStatus = "Already Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.alreadySynced));

        }
        else
        {
            syncStatus = "Not Yet Synced";
            tvSyncStatus.setTextColor(getContext().getResources().getColor(R.color.notYetSynced));
        }

        if(leave.getLastSyncedDate() != null)
        {
            tvLastSyncedDate.setVisibility(View.VISIBLE);
            tvLastSyncedDate.setText("Upload Date: "+leave.getLastSyncedDate());
        }
        else
        {
            tvLastSyncedDate.setVisibility(View.GONE);
        }

        tvSyncStatus.setText(syncStatus);

        // Leave Type
        TextView tvLeaveType = (TextView) convertView.findViewById(R.id.tvLeaveType);
        tvLeaveType.setText("Leave Type: " + leave.getLeaveTypeName());

        return convertView;
    }
}
