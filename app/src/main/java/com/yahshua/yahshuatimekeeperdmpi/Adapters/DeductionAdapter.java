package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeDeduction;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;

import java.util.ArrayList;

public class DeductionAdapter extends RecyclerView.Adapter<DeductionAdapter.ViewHolder>
{
    private ArrayList<EmployeeDeduction> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    public DeductionAdapter(Context context, ArrayList<EmployeeDeduction> data)
    {
        this.mInflater = LayoutInflater.from(context);
        this.mData     = data;
        this.context   = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.listrow_deduction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        final EmployeeDeduction employeeDeduction = mData.get(position);

        int synced   = context.getResources().getColor(R.color.alreadySynced);
        int unSynced = context.getResources().getColor(R.color.unSynced);

        holder.tvStatus.setTextColor(employeeDeduction.isSynced() ? synced : unSynced);
        holder.tvStatus.setText(employeeDeduction.getStatus());
        holder.tvStartDate.setText(employeeDeduction.getDateStart());
        holder.tvName.setText(employeeDeduction.getDeductType().getName());
        holder.tvAmount.setText(Converter.addZeroes(Converter.ConvertCurrencyDisplay(employeeDeduction.getAmount())));
        holder.tvAmountPerDeduct.setText(Converter.addZeroes(Converter.ConvertCurrencyDisplay(employeeDeduction.getAmountPerDeduct())));
        holder.tvReason.setText(employeeDeduction.getRemarks());

        if(employeeDeduction.getLastSyncedDate() != null)
        {
            holder.tvLastSyncedDate.setVisibility(View.VISIBLE);
            holder.tvLastSyncedDate.setText("Upload Date: "+employeeDeduction.getLastSyncedDate());
        }
        else
        {
            holder.tvLastSyncedDate.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<EmployeeDeduction> getData()
    {
        return mData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener
    {

        CardView cardView;
        TextView tvStatus, tvStartDate, tvAmount, tvName, tvAmountPerDeduct, tvReason, tvLastSyncedDate;

        ViewHolder(View itemView)
        {
            super(itemView);
            cardView          = itemView.findViewById(R.id.cv_customer);
            tvStatus          = itemView.findViewById(R.id.tvStatus);
            tvStartDate       = itemView.findViewById(R.id.tvStartDate);
            tvAmount          = itemView.findViewById(R.id.tvAmount);
            tvName            = itemView.findViewById(R.id.tvName);
            tvAmountPerDeduct = itemView.findViewById(R.id.tvAmountPerDeduct);
            tvReason          = itemView.findViewById(R.id.tvReason);
            tvLastSyncedDate  = itemView.findViewById(R.id.tvLastSyncedDate);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }


    public EmployeeDeduction getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener)
    {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener
    {
        void onItemClick(View view, int position);
    }
}
