package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.UndertimeAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.UndertimeAddEditDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.Undertime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;


public class UndertimeApplicationsActivity extends BaseActivity
{
    // Context
    private Context context = this;

    // Widget
    private ListView  lvUndertime;
    private MenuItem menuSync;

    // Adapter
    private UndertimeAdapter undertimeAdapter;

    // ArrayList and Data
    private ArrayList<Undertime> undertimeArrayList = new ArrayList<>();
    private Undertime selectedUndertime;
    private String employeeName;
    private Employee employee;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.undertime_applications_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        employee = getIntent().getParcelableExtra("EMPLOYEE");

        String middleInitial = (employee.getMiddlename() != null &&  employee.getMiddlename().length() > 0) ? employee.getMiddlename().charAt(0) + ". " : "";
        employeeName = employee.getFirstname() + " " + middleInitial + employee.getLastname();

    }

    private void initializeData()
    {
        selectedUndertime = new Undertime();

        String searchQuery = "WHERE employeeId = " + employee.getId() +" ORDER BY id DESC ";
        undertimeArrayList = Undertime.load(context, searchQuery);

        showSyncIcon();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync, menu);

        menuSync = menu.findItem(R.id.action_sync);

        initializeData();
        initializeUI();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_sync:
                uploadUndertime();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSyncIcon()
    {
        try
        {
            String searchUnderTime = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<Undertime> undertimeArrayList = Undertime.load(context, searchUnderTime);

            menuSync.setVisible(undertimeArrayList.size() > 0);

        }catch (Exception err)
        {
            Debugger.logD("Exception showSyncIcon err "+err);
        }
    }

    private void uploadUndertime()
    {
        if (Utility.haveNetworkConnection(context))
        {
            String searchUnderTime = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<Undertime> undertimeArrayList = Undertime.load(context, searchUnderTime);

            if(undertimeArrayList.size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                new UndertimeApplicationsActivity.UploadUndertimeApplication(context).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }

    // Initialize component from a layout file
    private void initializeUI()
    {
        // Set Title on Action Bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle("Undertime Applications");
        actionBar.setSubtitle(employeeName);

        // List View
        lvUndertime = findViewById(R.id.lvUndertime);
        undertimeAdapter = new UndertimeAdapter(this, undertimeArrayList);

        lvUndertime.setAdapter(undertimeAdapter);

        lvUndertime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
            {
                selectedUndertime = undertimeAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Options");

                final String[] finalOptions;
                String[] optionUnSynced;
                String[] optionSynced;

                optionUnSynced = getResources().getStringArray(R.array.lv_option3);
                optionSynced = Arrays.copyOf(optionUnSynced, optionUnSynced.length - 1);

                finalOptions = selectedUndertime.isSync() ? optionSynced : optionUnSynced;

                builder.setItems(finalOptions, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        switch (finalOptions[i])
                        {
                            case "View/Edit":
                                showUndertimeAddEditDialog();
                                break;
                            case "Upload":
                                uploadUndertime();
                                break;
                            case "Delete":
                                confirmDelete();
                                break;
                        }
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        selectedUndertime = new Undertime();
                    }
                });

                builder.show();
            }
        });

        // Floating Action Button
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUndertimeAddEditDialog();
            }
        });
    }

    // Undertime add edit dialog
    private void showUndertimeAddEditDialog()
    {
        UndertimeAddEditDialog undertimeAddEditDialog = new UndertimeAddEditDialog();
        Bundle dialogArgs = new Bundle();

        dialogArgs.putInt("EMPLOYEE_ID", employee.getId());
        dialogArgs.putString("EMPLOYEE_NAME", employeeName);

        if (selectedUndertime.getId() > 0) dialogArgs.putParcelable("SELECTED_UNDERTIME", selectedUndertime);

        undertimeAddEditDialog.setArguments(dialogArgs);

        undertimeAddEditDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                initializeData();
                updateLeaveAdapter();
            }
        });

        undertimeAddEditDialog.show(getSupportFragmentManager(), "Create/Edit Undertime");
    }

    private void updateLeaveAdapter()
    {
        undertimeAdapter.clear();
        undertimeAdapter.addAll(undertimeArrayList);
        undertimeAdapter.notifyDataSetChanged();
    }

    private void confirmDelete()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                deleteRecord();
            }
        };

        String message = "Delete Undertime Application on\n" + DateToString.displayFormat(selectedUndertime.getDate()) + "?";

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Confirm Delete");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, dialogClickListener);
        builder.setNegativeButton(android.R.string.no, null);

        builder.show();
    }

    private void deleteRecord()
    {
        selectedUndertime.delete(context);
        initializeData();
        updateLeaveAdapter();
        Toasters.ShowToast(context, "Undertime Application Deleted");
        AuditLog.saveAuditLog(context,"UNDERTIME","DELETE","SUCCESS",employeeName);
    }

    private class UploadUndertimeApplication extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadUndertimeApplication(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", true);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message += "success";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message +=  errorMessage;
                    }

                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.loadUnderTimeRequests(employee.getId());

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, "Successfully uploaded").show();
            }
            else
            {
                Toasty.warning(context, "Failed to upload ").show();
            }

            Toasters.HideLoadingSpinner();
            initializeData();
            initializeUI();
        }
    }
    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End
}
