package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.DatePickerDialog;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.LoadingDialogFragment;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;
import com.yahshua.yahshuatimekeeperdmpi.httpRequests.SubmitWorkOnHolidayRequestAsyncTask;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class WorkOnHolidayApprovalRequestActivity extends BaseActivity
{
    private WorkOnHoliday workOnHoliday;
    private boolean isRecommended;
    private boolean isApproved;
    private LoadingDialogFragment loadingDialogFragment = new LoadingDialogFragment();
    private RadioGroup rgApprove;
    private RadioGroup rgRecommend;

    // Date
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    private Calendar calendar = Calendar.getInstance();
    private Date date;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            workOnHoliday = getIntent().getParcelableExtra("WORK_ON_HOLIDAY");
            date = workOnHoliday.getDate();
            initializeViews();
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error WorkOnHolidayApprovalRequestActivity onCreate: \n " + err.toString());
        }
    }

    private void initializeViews() throws Exception
    {
        try
        {
            setContentView(R.layout.work_on_holiday_approval_request_activity);
            initializeDateEditText();

            // Title
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) throw new Exception("ActionBar is null");
            actionBar.setTitle("Work on Holiday Request");
            actionBar.setDisplayHomeAsUpEnabled(true);

            // Employee name
            TextView tvEmployeeName = findViewById(R.id.tvEmployeeName);
            tvEmployeeName.setText(workOnHoliday.getEmployeeName());

            // Hours
            TextView tvHours = findViewById(R.id.tvHours);
            tvHours.setText(workOnHoliday.getHours() + "");

            // Reason
            TextView tvReason = findViewById(R.id.tvReason);
            tvReason.setText(workOnHoliday.getReason());

            // Recommend radioGroup
            rgRecommend = findViewById(R.id.rgRecommend);
            rgRecommend.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i)
                {
                    isRecommended = (i == R.id.rbRecommended);
                }
            });

            // Approve radioGroup
            rgApprove = findViewById(R.id.rgApprove);
            rgApprove.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i)
                {
                    isApproved = (i == R.id.rbApproved);
                }
            });

            // Application date
            TextView tvApplicationDate = findViewById(R.id.tvApplicationDate);
            tvApplicationDate.setText(DateToString.dateTimeFormat2(workOnHoliday.getRequestedDate()));
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: \n" + err.toString());
        }
    }

    private void initializeDateEditText() throws Exception
    {
        try
        {
            final EditText etDate = findViewById(R.id.etDate);
            etDate.setText(DateToString.displayFormat(workOnHoliday.getDate()));

            onDateSetListener = new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                {
                    calendar.set(year, month, dayOfMonth);
                    date = calendar.getTime();
                    etDate.setText(DateToString.displayFormat(date));
                }
            };

            etDate.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    new DatePickerDialog(WorkOnHolidayApprovalRequestActivity.this, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing date editText: " + err.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.request_approval_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_submit:
                submit();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void submit()
    {
        try
        {
            showLoadingDialog();

            // Validation
            if (rgApprove.getCheckedRadioButtonId() == -1 && rgRecommend.getCheckedRadioButtonId() == -1)
            {
                Toasty.info(this, "Please indicate recommendation or approval").show();
                loadingDialogFragment.dismiss();
                return;
            }

            // Set fields
            JSONObject jsonObject = new JSONObject();
            TextInputEditText tieRecommendRemarks = findViewById(R.id.tieRecommendRemarks);
            TextInputEditText tieApproveRemarks = findViewById(R.id.tieApproveRemarks);
            EditText etAdjustHours = findViewById(R.id.etAdjustHours);
            workOnHoliday.setDate(date);

            jsonObject.put("id", workOnHoliday.getId());
            jsonObject.put("employee_id", workOnHoliday.getEmployeeId());
            jsonObject.put("requested_date", DateToString.dbFormat(workOnHoliday.getRequestedDate()));
            jsonObject.put("date", DateToString.dbFormat(workOnHoliday.getDate()));
            jsonObject.put("hours", workOnHoliday.getHours());
            jsonObject.put("is_recommended", isRecommended);
            jsonObject.put("is_approved", isApproved);
            jsonObject.put("remarks", workOnHoliday.getRemarks());
            jsonObject.put("status", isApproved ? "approved" : "disapproved");
            jsonObject.put("reason", workOnHoliday.getReason());

            if (!TextUtils.isEmpty(tieApproveRemarks.getText().toString()))
            {
                jsonObject.put("approved_remarks", tieApproveRemarks.getText().toString());
            }

            if (!TextUtils.isEmpty(tieApproveRemarks.getText().toString()))
            {
                jsonObject.put("recommended_remarks", tieRecommendRemarks.getText().toString());
            }

            if (!TextUtils.isEmpty(etAdjustHours.getText().toString()))
            {
                jsonObject.put("adjust_hours", Double.parseDouble(etAdjustHours.getText().toString()));
            }

            // Download work on holiday requests
            AsyncResponseListener asyncResponseListener = new AsyncResponseListener()
            {
                @Override
                public void onSuccess(Object object)
                {
                    Toasty.success(WorkOnHolidayApprovalRequestActivity.this, "Success").show();
                    loadingDialogFragment.dismiss();
                    finish();
                }

                @Override
                public void onFailure(int errorCode, String error)
                {
                    switch (errorCode)
                    {
                        case 2:
                            Utility.showNoNetworkConnectionDialog(WorkOnHolidayApprovalRequestActivity.this);
                            break;
                        case 3:
                            Utility.showFailedToConnectToServerDialog(WorkOnHolidayApprovalRequestActivity.this);
                            break;
                        case 4:
                            Utility.showServerErrorDialog(WorkOnHolidayApprovalRequestActivity.this, error);
                            break;
                        default:
                            Utility.showError(getSupportFragmentManager(), "Error SubmitWorkOnHolidayRequestAsyncTask: \n" + error);
                    }

                    loadingDialogFragment.dismiss();
                }
            };

            new SubmitWorkOnHolidayRequestAsyncTask(this, asyncResponseListener, jsonObject).execute();
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error submitting: \n" + err.toString());
            loadingDialogFragment.dismiss();
        }
    }

    private void showLoadingDialog() throws Exception
    {
        try
        {
            loadingDialogFragment.show(getSupportFragmentManager(), "LOADING_DIALOG");
            loadingDialogFragment.setCancelable(false);
        }
        catch (Exception err)
        {
            throw new Exception("Error showing loading dialog: " + err.toString());
        }
    }
}
