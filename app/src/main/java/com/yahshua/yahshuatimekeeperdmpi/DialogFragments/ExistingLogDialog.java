package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Activities.TimeIn;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ImageHandler;

public class ExistingLogDialog extends DialogFragment
{
    // Widget
    private EditText etSystemId;
    // Data
    private int selectedApplication;
    private View view;
    private Context context;
    private Bundle bundle;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        bundle = getArguments();

        view = inflater.inflate(R.layout.dialog_existing_logs,container,false);
        context = getContext();
        initializeUI();
        return view;
    }

    private void initializeUI()
    {
        EmployeeLogs employeeLogs = bundle.getParcelable("EMPLOYEE_LOGS");

        ImageView logImg = view.findViewById(R.id.logImg);
        TextView tvName = view.findViewById(R.id.tvName);
        TextView tvDate = view.findViewById(R.id.tvDate);
        TextView tvTime = view.findViewById(R.id.tvTime);
        TextView tvType = view.findViewById(R.id.tvType);

        byte[] bitmapData = new byte[0];

        try
        {
            bitmapData = ImageHandler.getImageByPath(employeeLogs.getLogImg());
        }
        catch (Exception err)
        {
            Debugger.logD(err.toString());
        }

        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);

        logImg.setImageBitmap(bitmap);
        tvName.setText(employeeLogs.employeeObject.getFullName2());
        tvDate.setText(employeeLogs.getDate());
        tvTime.setText(employeeLogs.getLogTime());
        tvType.setText(employeeLogs.getLogType());

        Button btnContinue = view.findViewById(R.id.btnContinue);
        Button btnChangeLog = view.findViewById(R.id.btnChangeLog);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TimeIn)getActivity()).onNegativeSelect();
                dismiss();
            }
        });

        btnChangeLog.setText("Change Type to " + reverseType(employeeLogs.getLogType()) + " and Save Log");

        btnChangeLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TimeIn)getActivity()).onPositiveSelect();
                dismiss();
            }
        });

    }

    private String reverseType(String logType)
    {
        return logType.equals("IN") ? "OUT" : "IN";
    }






}

