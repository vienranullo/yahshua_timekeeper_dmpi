package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.google.gson.Gson;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeviceSettings;

import java.util.ArrayList;


public class SelectApplicationDialog extends DialogFragment
{
    private DeviceSettings deviceSettings;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ArrayList<String> applicationArray1 = new ArrayList<>();
        loadDeviceSettings(getContext());

        if (deviceSettings.getVisibleMenuItems().isLeave()) applicationArray1.add("Leave");
        if (deviceSettings.getVisibleMenuItems().isOvertime()) applicationArray1.add("Overtime");
        if (deviceSettings.getVisibleMenuItems().isUnderTime()) applicationArray1.add("Undertime");
        if (deviceSettings.getVisibleMenuItems().isWorkOnRestDay()) applicationArray1.add("Work on Restday");
        if (deviceSettings.getVisibleMenuItems().isWorkOnHoliday()) applicationArray1.add("Work on Holiday");

        applicationArray1.add("Cash Advance");

        String applicationArray[] = new String[applicationArray1.size()];

        for (int i = 0; i < applicationArray1.size(); i++)
        {
            applicationArray[i] = applicationArray1.get(i);
        }

        builder.setTitle("Select Application")
                .setItems(applicationArray, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        SystemIdDialog systemIdDialog = new SystemIdDialog();

                        Bundle dialogArgs = new Bundle();
                        dialogArgs.putInt("selectedApplication", which);

                        systemIdDialog.setArguments(dialogArgs);

                        systemIdDialog.show(getFragmentManager(), "System ID Dialog");
                    }
                });

        return builder.create();
    }

    public void loadDeviceSettings(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String deviceSettingsJSON = sharedPreferences.getString("DEVICE_SETTINGS", "");
        Gson gson = new Gson();
        deviceSettings = gson.fromJson(deviceSettingsJSON, DeviceSettings.class);
    }
}
