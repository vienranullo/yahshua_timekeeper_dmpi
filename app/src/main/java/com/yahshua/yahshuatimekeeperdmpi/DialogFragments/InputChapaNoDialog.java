package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Fragments.HomeFragment;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;

import java.util.ArrayList;

public class InputChapaNoDialog extends DialogFragment
{
    //Context
    private Context context;
    private View view;

    //Widgets
    private TextView tvTitle;
    private EditText etEmployeeId;
    private Button btnOk, btnCancel;

    //Variable
    private Employee employee;
    private String applicationName;
    private boolean isCorrect= false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.input_chapa_no, container, false);
        context = view.getContext();

        Bundle bundle = getArguments();

        if(bundle != null)
        {
            applicationName = bundle.getString("APPLICATION_NAME");
        }

        initializeUI();

        return view;
    }

    private void initializeUI(){

        btnOk = view.findViewById(R.id.btn_ok);
        tvTitle = view.findViewById(R.id.tv_title);
        btnCancel = view.findViewById(R.id.btn_cancel);
        etEmployeeId = view.findViewById(R.id.et_employee_id);

        tvTitle.setText(applicationName);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if(getEmployee(Integer.parseInt(etEmployeeId.getText().toString()))){
                        isCorrect = true;
                        dismiss();
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private boolean getEmployee(int systemId)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        String searchQuery = "WHERE employeeid2 = " + systemId;

        ArrayList<Employee> employeeArrayList = Employee.read(getContext(), searchQuery, dbAdapter);

        dbAdapter.close();
        if (employeeArrayList.size() == 1)
        {
            employee = employeeArrayList.get(0);
            return  true;
        } else {
            etEmployeeId.setError(getString(R.string.invalid_system_id));
            return  false;
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {

        if (isCorrect)
        {
            Bundle bundle = new Bundle();
            bundle.putParcelable("EMPLOYEE_OBJECT", employee);

            if(applicationName.equals("View Logs"))
            {
                ((HomeFragment) getTargetFragment()).onInputComplete(bundle);
            }
        }
    }

    private boolean validate(){

        if(etEmployeeId.getText().toString().matches("")){
            etEmployeeId.setError(getString(R.string.error_field_required));
            etEmployeeId.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
        onDestroy();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        dismiss();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}