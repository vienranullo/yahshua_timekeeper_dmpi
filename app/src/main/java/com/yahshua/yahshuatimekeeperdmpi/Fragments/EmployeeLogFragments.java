package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Adapters.EmployeeLogAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Syncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;


public class EmployeeLogFragments extends Fragment
{

    // Views and Context
    private View view;
    private Context context;

    // Widget
    private ListView lvStudentSession;
    private TextView tvResultMessage;
    private EditText etStudent;

    // Adapter
    private EmployeeLogAdapter studentSessionAdapter;

    // ArrayList and Data
    private ArrayList<EmployeeLogs> studentSessions;
    private EmployeeLogs selectedEmployeeLog;
    private int employeeSystemId;
    private PopupMenu popup;

    public int getEmployeeSystemId() {
        return employeeSystemId;
    }

    public void setEmployeeSystemId(int employeeSystemId) {
        this.employeeSystemId = employeeSystemId;
    }

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_employee_logs, container, false);
        context = getContext();
        readSessions();

        initializeUI(view);

        return view;
    }

    // Initialize component from a layout file
    private void initializeUI(View view)
    {
        try
        {
            // TextView
            tvResultMessage = (TextView) view.findViewById(R.id.tvResultMessage);

            // ListView
            lvStudentSession = (ListView) view.findViewById(R.id.lvEmployeeLogs);
            studentSessionAdapter = new EmployeeLogAdapter(view.getContext(),studentSessions);
            lvStudentSession.setAdapter(studentSessionAdapter);

            lvStudentSession.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    try
                    {
                        selectedEmployeeLog = (EmployeeLogs) (lvStudentSession.getItemAtPosition(position));

                        popup = new PopupMenu(getActivity(), view);
                        popup.getMenuInflater().inflate(R.menu.popup_logs, popup.getMenu());

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {

                                switch(item.getItemId()){

                                    case R.id.action_sync_item :
                                        syncEmployeeLog();
                                        break;

                                }

                                return true;
                            }
                        });

                        popup.show();

                        return true;
                    } catch (Exception err)
                    {
                        return false;
                    }
                }
            });

            if(studentSessions.size() == 0)
            {
                tvResultMessage.setVisibility(View.VISIBLE);
            }else
            {
                tvResultMessage.setVisibility(View.GONE);
            }



        } catch (Exception err)
        {
            Toasters.ShowToast(context, err.toString());
        }
    }

    private void readSessions()
    {
        try
        {
            studentSessions = EmployeeLogs.read(view.getContext(), false, getEmployeeSystemId(), 0);

        } catch (Exception err)
        {
            Toasters.ShowToast(context, err.toString());
        }
    }

    private void syncEmployeeLog()
    {
        try
        {
            Toasters.ShowLoadingSpinner(context);
            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    readSessions();
                    studentSessionAdapter = new EmployeeLogAdapter(view.getContext(),studentSessions);
                    studentSessionAdapter.notifyDataSetChanged();
                    lvStudentSession.setAdapter(studentSessionAdapter);
                    selectedEmployeeLog.convertSyncStatus(getContext());

                    Toasters.HideLoadingSpinner();
                    Toasters.ShowToast(getContext(), "Log Synced!");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        String str = new String(responseBody, "UTF-8");
                        Toasters.HideLoadingSpinner();
                        Toasters.ShowToast(getContext(), "Synced Failed " + str);
                    } catch (Exception err)
                    {
                        Toasters.ShowSnackBar(getView(), err.toString());
                    }
                }
            };

            Syncer.syncIndividualLogs(getContext(), selectedEmployeeLog, asyncHttpResponseHandler);

        } catch (Exception err)
        {
            Toasters.ShowSnackBar(getView(), err.toString());
        }
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}