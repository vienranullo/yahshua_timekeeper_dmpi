package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Fragments.HomeFragment;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserPreferences;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.Date;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class SyncAllDataDialog extends DialogFragment
{
    // Context
    private Context context;

    //View
    private View view;

    //Widgets
    private Button btnCancel;
    private LinearLayout llTitle;
    private TextView tvErrorMessage;

    //Data
    private UploadLogs uploadLogs = new UploadLogs();
    private String errorMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.dialog_fragment_upload_all, container, false);
        context = view.getContext();

        initializeUI();
        return view;
    }

    private void initializeUI()
    {
        btnCancel       = view.findViewById(R.id.btnCancel);
        llTitle         = view.findViewById(R.id.llTitle);
        tvErrorMessage  = view.findViewById(R.id.tvErrorMessage);

        tvErrorMessage.setVisibility(View.GONE);
        uploadLogs.execute();

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    if(uploadLogs.getStatus() == AsyncTask.Status.RUNNING)
                    {
                        uploadLogs.cancel(true);
                        dismiss();
                    }
                    else
                    {
                        errorMessage = "";
                        llTitle.setVisibility(View.VISIBLE);
                        tvErrorMessage.setVisibility(View.GONE);
                        btnCancel.setText(getString(R.string.cancel));
                        uploadLogs = new UploadLogs();
                        uploadLogs.execute();
                    }

                }catch (Exception err)
                {
                    Debugger.logD("btnCancel "+err.getMessage());
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);

        assert getTargetFragment() != null;
        UserPreferences.setDateSyncAll(context, DateToString.dateTimeFormat2(new Date()));

         ((HomeFragment) getTargetFragment()).onComplete("");
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadLogs extends AsyncTask<String, Integer, String>
    {
        private String success = "";

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer uploadData = new TransactionSyncer(getContext());

                uploadData.setOnRequestListener(new HttpRequestListener()
                {

                    @Override
                    public void onSuccess(String fromWhere, String successMessage)
                    {
                        success = "";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        success = errorMessage;
                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                uploadData.uploadEmployeeLogs();

            }
            catch (Exception err)
            {
                return success += "FAILED";
            }

            return success;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s)
        {
            try
            {
                if(success.equals(""))
                {
                    Toasty.success(context, context.getResources().getString(R.string.successful_upload)).show();
                    dismiss();
                }
                else
                {
                    errorMessage = success;
                    llTitle.setVisibility(View.GONE);
                    tvErrorMessage.setText(errorMessage);
                    tvErrorMessage.setVisibility(View.VISIBLE);
                    btnCancel.setText(Objects.requireNonNull(getContext()).getString(R.string.retry));
                }

                super.onPostExecute(s);
            }catch (Exception err)
            {
                Debugger.logD("onPostExecute "+err.getMessage());
                Toasty.warning(context, context.getResources().getString(R.string.server_error)).show();
            }
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
            HttpProvider.cancelRequest(context);
            dismiss();
        }
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        super.onResume();
    }

    // Function to full screen the dialog fragment
    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
