package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.yahshua.yahshuatimekeeperdmpi.Activities.SyncingActivity;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import es.dmoral.toasty.Toasty;


public class SyncingFragment extends Fragment
{
    // Views and Context
    private View view;
    private Context context;

    // Widget
    Button btnUploadEmployeeLogs, btnSyncLeave, btnSyncOverTime, btnSyncUndertime, btnSyncWorkOnRestday, btnSyncWorkOnHoliday,btnDownloadEmployees, btnSyncCashAdvance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_syncing, container, false);
        initializeUI(view);

        context = getContext();

        return view;
    }

    // Initialize component from a layout file
    private void initializeUI(View view)
    {
        // Buttons
        btnUploadEmployeeLogs = view.findViewById(R.id.btnUploadEmployeeLogs);
        btnSyncLeave          = view.findViewById(R.id.btnSyncLeave);
        btnSyncOverTime       = view.findViewById(R.id.btnSyncOverTime);
        btnSyncUndertime      = view.findViewById(R.id.btnSyncUndertime);
        btnSyncWorkOnRestday  = view.findViewById(R.id.btnSyncWorkOnRestday);
        btnSyncWorkOnHoliday  = view.findViewById(R.id.btnSyncWorkOnHoliday);
        btnDownloadEmployees  = view.findViewById(R.id.btnDownloadEmployees);
        btnSyncCashAdvance    = view.findViewById(R.id.btnSyncCashAdvance);

        btnUploadEmployeeLogs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(Utility.haveNetworkConnection(context, getFragmentManager()))
                {
                    if(Utility.haveNetworkConnection(context))
                    {
                        if( EmployeeLogs.read(context,true,0, 20).size() == 0)
                        {
                            Toasty.warning(context, getResources().getString(R.string.no_logs_to_sync)).show();
                        }
                        else
                        {
                            new SyncingFragment.UploadApplication(context).execute();
                        }
                    }
                    else
                    {
                        Toasty.warning(context, context.getString(R.string.internet_required)).show();
                    }
                }
            }
        });

        btnDownloadEmployees.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent();
                intent.setClass(context, SyncingActivity.class);
                startActivity(intent);
            }
        });
    }

    private class UploadApplication extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadApplication(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", false);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message = " success ";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message =  errorMessage;
                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.uploadEmployeeLogs();

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, context.getString(R.string.successful_upload)).show();
            }
            else
            {
                Toasty.warning(context, context.getString(R.string.failed_upload)).show();
            }

            Toasters.HideLoadingSpinner();
            Debugger.logD(message);
        }
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}