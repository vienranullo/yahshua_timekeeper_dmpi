package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.DeductionTypeDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeDeduction;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class EmployeeDeductionRequestsActivity extends BaseActivity implements CompleteListener
{
    // Context
    private Context context = this;

    //Data
    private Employee  employee;
    private EmployeeDeduction deduction;
    private String    selectedSchedule = null, dateRequested = "";
    private int       deductionTypeId = 0;
    private boolean isEdit;

    //Widgets
    private TextView tvName, tvDeductionType, tvSchedule;
    private CardView cvDeductionType;
    private CardView cvSchedule;
    private LinearLayout llSchedule;
    private ImageView imgSchedule;
    private RadioGroup rgTerm;
    private RadioButton rbWeekly, rbQuincenal, rbMonthly;
    private EditText etAmount, etAmountPerDeduction, etStartDate, etEndDate, etReason;
    private MenuItem menuSave;

    //Date
    private String dateToUpdate;
    private Date dateStart, dateEnd;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener onDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deduction_form_activity);

        employee = getIntent().getParcelableExtra("EMPLOYEE");
        isEdit = getIntent().getBooleanExtra("IS_EDIT", false);
        deduction = getIntent().getParcelableExtra("DEDUCTION");

        initializeUI();

        if(isEdit)
        {
            loadEditForm();
        }

        setTitle("Fill Up Form");
    }

    private void initializeUI()
    {
        tvName               = findViewById(R.id.tv_name);
        etAmount             = findViewById(R.id.etAmount);
        tvSchedule           = findViewById(R.id.tv_schedule);
        cvDeductionType      = findViewById(R.id.cv_deduction_type);
        cvSchedule           = findViewById(R.id.cv_schedule);
        tvDeductionType      = findViewById(R.id.tv_deduction_type);
        etAmountPerDeduction = findViewById(R.id.etAmountPerDeduction);
        llSchedule           = findViewById(R.id.llSchedule);
        imgSchedule          = findViewById(R.id.imgSchedule);
        rgTerm               = findViewById(R.id.rgTerm);
        rbWeekly             = findViewById(R.id.rbWeekly);
        rbQuincenal          = findViewById(R.id.rbQuincenal);
        rbMonthly            = findViewById(R.id.rbMonthly);
        etStartDate          = findViewById(R.id.etStartDate);
        etEndDate            = findViewById(R.id.etEndDate);
        etReason             = findViewById(R.id.etReason);

        tvName.setText(employee.getFullName());
        llSchedule.setVisibility(View.GONE);

        cvDeductionType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               openDeductionType();
            }
        });

        cvSchedule.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(llSchedule.getVisibility() == View.GONE)
                {
                    llSchedule.setVisibility(View.VISIBLE);
                    imgSchedule.setImageResource(R.drawable.ic_arrow_up);
                }
                else
                {
                    llSchedule.setVisibility(View.GONE);
                    imgSchedule.setImageResource(R.drawable.ic_arrow_down);
                }

            }
        });

        rbWeekly.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectedSchedule = "weekly";
                tvSchedule.setText(getString(R.string.weekly));
                computeEndDate();
            }
        });

        rbQuincenal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectedSchedule = "quincena";
                tvSchedule.setText(R.string.quincenal);
                computeEndDate();
            }
        });

        rbMonthly.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectedSchedule = "monthly";
                tvSchedule.setText(R.string.monthly);
                computeEndDate();
            }
        });

        etAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) llSchedule.setVisibility(View.GONE);
            }
        });

        etAmountPerDeduction.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) llSchedule.setVisibility(View.GONE);
            }
        });

        etAmount.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                try
                {
                    String text = charSequence.toString();

                    if (text.contains(".") && text.substring(text.indexOf(".") + 1).length() > 2)
                    {
                        etAmount.setText(text.substring(0, text.length() - 1));
                        etAmount.setSelection(etAmount.getText().length());
                    }

                }catch (Exception err)
                {
                    Debugger.logD(err.getMessage());
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.length() > 0)
                {
                    computeEndDate();
                }

                //Put comma during typing
                etAmount.removeTextChangedListener(this);
                try
                {
                    String originalString = s.toString();

                    Long longVal;

                    if (originalString.contains(","))
                    {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longVal = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longVal);

                    //setting text after format to EditText
                    etAmount.setText(formattedString);
                    etAmount.setSelection(etAmount.getText().length());

                } catch (NumberFormatException nfe)
                {
                    nfe.printStackTrace();
                }

                etAmount.addTextChangedListener(this);
            }
        });

        etAmountPerDeduction.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                String text = charSequence.toString();
                if (text.contains(".") && text.substring(text.indexOf(".") + 1).length() > 2)
                {
                    etAmountPerDeduction.setText(text.substring(0, text.length() - 1));
                    etAmountPerDeduction.setSelection(etAmountPerDeduction.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.length() > 0)
                {
                    computeEndDate();
                }

                //Put comma during typing
                etAmountPerDeduction.removeTextChangedListener(this);
                try
                {
                    String originalString = s.toString();

                    Long longVal;

                    if (originalString.contains(","))
                    {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longVal = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longVal);

                    //setting text after format to EditText
                    etAmountPerDeduction.setText(formattedString);
                    etAmountPerDeduction.setSelection(etAmountPerDeduction.getText().length());

                } catch (NumberFormatException nfe)
                {
                    nfe.printStackTrace();
                }

                etAmountPerDeduction.addTextChangedListener(this);
            }
        });

        etStartDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
                dateToUpdate = "DATE-FROM";
                calendar.setTime(dateStart);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        etStartDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                {
                    hideKeyboard();
                    dateToUpdate = "DATE-FROM";
                    calendar.setTime(dateStart);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.show();
                }
            }
        });

        etStartDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && selectedSchedule != null)
                    computeEndDate();
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard();
            }
        });

        // Date and Time Listeners
        onDateSetListener = new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                calendar.set(year, month, dayOfMonth);
                updateDate();
            }

        };

        etReason.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            llSchedule.setVisibility(View.GONE);
                        }
                    });
            }
        });

        setDefaultDate();
    }

    private void loadEditForm()
    {
        switch (deduction.getTerm())
        {
            case "weekly":
                selectedSchedule  = "weekly";
                tvSchedule.setText(getString(R.string.weekly));
                break;

            case "quincena":
                selectedSchedule  = "quincena";
                tvSchedule.setText(getString(R.string.quincenal));
                break;

            case "monthly":
                selectedSchedule  = "monthly";
                tvSchedule.setText(getString(R.string.monthly));
                break;
        }

        tvName.setText(employee.getFullName());
        etReason.setText(deduction.getRemarks());
        etEndDate.setText(deduction.getDateEnd());
        dateRequested = deduction.getDateRequested();
        deductionTypeId = deduction.getDeductionTypeId();
        etAmount.setText(Converter.ConvertCurrencyDisplay(deduction.getAmount()));
        tvDeductionType.setText(deduction.getDeductType().getName());
        etAmountPerDeduction.setText(Converter.ConvertCurrencyDisplay(deduction.getAmountPerDeduct()));

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateTimeHandler.convertStringtoDate3(deduction.getDateStart()));

        dateStart = calendar.getTime();
        etStartDate.setText((DateToString.customDisplay(dateStart)));

        computeEndDate();

        //Disable Widgets
        if(deduction.isSynced())
        {
            etAmount.setEnabled(false);
            etReason.setEnabled(false);
            cvSchedule.setEnabled(false);
            etStartDate.setEnabled(false);
            cvDeductionType.setEnabled(false);
            etAmountPerDeduction.setEnabled(false);
            etReason.setTextColor(ContextCompat.getColor(context, R.color.black));
            etAmount.setTextColor(ContextCompat.getColor(context, R.color.black));
            etStartDate.setTextColor(ContextCompat.getColor(context, R.color.black));
            etAmountPerDeduction.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
        else
        {
            etAmount.setEnabled(true);
            etReason.setEnabled(true);
            cvSchedule.setEnabled(true);
            etStartDate.setEnabled(true);
            cvDeductionType.setEnabled(true);
            etAmountPerDeduction.setEnabled(true);
        }
    }

    private void setDefaultDate()
    {
        dateStart = Calendar.getInstance().getTime();
        dateEnd = Calendar.getInstance().getTime();

        etStartDate.setText(DateToString.customDisplay(dateStart));
        etEndDate.setText(DateToString.customDisplay(dateEnd));
    }

    private void updateDate()
    {
        switch (dateToUpdate) {
            case "DATE-FROM":
                dateStart = calendar.getTime();
                etStartDate.setText(DateToString.customDisplay(dateStart));
                break;
            case "DATE-TO":
                dateEnd = calendar.getTime();
                etEndDate.setText(DateToString.customDisplay(dateEnd));
                break;
        }
    }


    private void openDeductionType()
    {
        DeductionTypeDialog deductionTypeDialog = new DeductionTypeDialog();
        deductionTypeDialog.show(getFragmentManager(), "filter dialog");
    }

    @Override
    public void onComplete(Object object)
    {
        try
        {
            Gson gson = new Gson();
            String json = gson.toJson(object);

            JSONObject jsonObject = new JSONObject(json);

            deductionTypeId = jsonObject.getInt("id");
            tvDeductionType.setText(jsonObject.get("name").toString());
            tvDeductionType.setError(null);

        }catch (Exception err)
        {
            Debugger.logD("onComplete "+err.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        menuSave = menu.findItem(R.id.action_save);

        if( isEdit && deduction.isSynced())
        {
            menuSave.setVisible(false);
        }
        else
        {
            menuSave.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_save)
        {
            if(validate()) saveToDisk();
        }
        return super.onOptionsItemSelected(item);
    }


    public void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean validate()
    {
        if(tvDeductionType.getText().equals(""))
        {
            tvDeductionType.setError(getString(R.string.error_field_required));
            tvDeductionType.requestFocus();
            return false;
        }
        else if(tvName.getText().equals(""))
        {
            tvName.setError(getString(R.string.error_field_required));
            tvName.requestFocus();
            return false;
        }
        else if(tvSchedule.getText().equals(""))
        {
            tvSchedule.setError(getString(R.string.error_field_required));
            tvSchedule.requestFocus();
            return false;
        }
        else if(etAmount.getText().toString().equals(""))
        {
            etAmount.setError(getString(R.string.error_field_required));
            etAmount.requestFocus();
            return false;
        }
        else if(etAmountPerDeduction.getText().toString().equals(""))
        {
            etAmountPerDeduction.setError(getString(R.string.error_field_required));
            etAmountPerDeduction.requestFocus();
            return false;
        }
        else if(etStartDate.getText().toString().equals(""))
        {
            etStartDate.setError(getString(R.string.error_field_required));
            etStartDate.requestFocus();
            return false;
        }
        else if(etEndDate.getText().toString().equals(""))
        {
            etEndDate.setError(getString(R.string.error_field_required));
            etEndDate.requestFocus();
        }
        else if(etReason.getText().toString().equals(""))
        {
            etReason.setError(getString(R.string.error_field_required));
            etReason.requestFocus();
            return false;
        }
        else if( isEdit && deduction.isSynced())
        {
            Toasty.warning(context ,getString(R.string.cannot_edit_transaction)).show();
            return false;
        }

        double amount = Double.parseDouble(etAmount.getText().toString().replace(",",""));
        double amountPerDeduct = Double.parseDouble(etAmountPerDeduction.getText().toString().replace(",",""));

         if( amountPerDeduct <= 0)
        {
            etAmountPerDeduction.setError(getResources().getString(R.string.input_valid));
            etAmountPerDeduction.requestFocus();
            return false;
        }

        else if( amount <= 0)
        {
            etAmount.setError(getResources().getString(R.string.input_valid));
            etAmount.requestFocus();
            return false;
        }

        dateRequested = DateToString.dbQuery(new Date());
        return  true;
    }

    private boolean isAmountFieldsValid()
    {
        try
        {
            etAmount.setError(null);
            etAmountPerDeduction.setError(null);

            if (etAmount.getText().length() == 0)
            {
                etAmount.setError("Please enter amount");
                return false;
            }

            if (etAmountPerDeduction.getText().length() == 0)
            {
                etAmountPerDeduction.setError("Please enter amount");
                return false;
            }

            return true;

        }catch (Exception err)
        {
            Debugger.logD("isAmountFieldsValid "+err.getMessage());
            return false;
        }
    }

    private void computeEndDate()
    {
        try
        {
            if (isAmountFieldsValid() && selectedSchedule != null)
            {
                switch (selectedSchedule)
                {
                    case "weekly":
                        getWeeklyTerm();
                        break;
                    case "quincena":
                        getQuincenal();
                        break;
                    case "monthly":
                        getMonthly();
                        break;
                }
            }
        }catch (Exception err)
        {
            Debugger.logD("computeEndDate "+err.getMessage());
        }
    }

    private void getWeeklyTerm()
    {
        try
        {
            Double amount = Double.parseDouble(etAmount.getText().toString().replace(",",""));
            Double amountDeduction = Double.parseDouble(etAmountPerDeduction.getText().toString().replace(",",""));

            int numWeeks = (int) Math.round((amount / amountDeduction));

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateStart);
            calendar.add(Calendar.WEEK_OF_MONTH, numWeeks);
            dateEnd = calendar.getTime();

            etEndDate.setText(DateToString.customDisplay(calendar.getTime()));
            dateEnd = calendar.getTime();

        } catch (Exception err)
        {
            Toasty.warning(context, "Unable to compute End Date").show();
        }
    }

    private void getQuincenal()
    {
        try
        {
            Double amount           = Double.parseDouble(etAmount.getText().toString().replace(",",""));
            Double amountDeduction  = Double.parseDouble(etAmountPerDeduction.getText().toString().replace(",",""));

            Calendar startDate = Calendar.getInstance();
            startDate.setTime(dateStart);

            int currentDay        = startDate.get(Calendar.DATE);
            int getLastDayofMonth = startDate.getMaximum(Calendar.DATE);

            Calendar startDeduction = Calendar.getInstance();

            if (currentDay <= 15)
            {
                startDeduction.set(Calendar.DATE, 15);
            } else
            {
                startDeduction.set(Calendar.DATE, getLastDayofMonth);
            }

            while(amount > 0)
            {
                currentDay = startDeduction.get(Calendar.DATE);
                getLastDayofMonth = startDeduction.getActualMaximum(Calendar.DAY_OF_MONTH);

                if (currentDay == 15)
                {
                    int daysDifference = getLastDayofMonth - currentDay;
                    startDeduction.add(Calendar.DATE, daysDifference);

                } else if (currentDay == getLastDayofMonth)
                {
                    startDeduction.add(Calendar.DATE, 15);
                }

                amount -= amountDeduction;
            }
            dateEnd = startDeduction.getTime();
            etEndDate.setText(DateToString.customDisplay(startDeduction.getTime()));
            dateEnd = startDeduction.getTime();

        } catch (Exception err)
        {

        }
    }

    private void getMonthly()
    {
        try
        {
            Double amount = Double.parseDouble(etAmount.getText().toString().replace(",",""));
            Double amountDeduction = Double.parseDouble(etAmountPerDeduction.getText().toString().replace(",",""));

            int counter = 0;
            while (amount > 0)
            {
                amount -= amountDeduction;
                counter++;
            }

            Calendar endDate = Calendar.getInstance();
            endDate.setTime(dateStart);
            endDate.add(Calendar.MONTH, counter);
            dateEnd = endDate.getTime();
            etEndDate.setText(DateToString.customDisplay(endDate.getTime()));
            dateEnd = endDate.getTime();

        } catch (Exception err)
        {

        }
    }

    private void saveToDisk()
    {
        try
        {
            EmployeeDeduction employeeDeduction = new EmployeeDeduction();

            employeeDeduction.setTerm(selectedSchedule);
            employeeDeduction.setPaymentTerm("regular");
            employeeDeduction.setEmployeeId(employee.getId());
            employeeDeduction.setDateRequested(dateRequested);
            employeeDeduction.setDeductionTypeId(deductionTypeId);
            employeeDeduction.setRemarks(etReason.getText().toString());
            employeeDeduction.setDateEnd(DateToString.dbQuery(dateEnd));
            employeeDeduction.setDateStart(DateToString.displayFormat(dateStart));
            employeeDeduction.setAmount(Double.parseDouble(etAmount.getText().toString().replace(",","")));
            employeeDeduction.setAmountPerDeduct(Double.parseDouble(etAmountPerDeduction.getText().toString().replace(",","")));

            if(isEdit)
            {
                employeeDeduction.save(context, deduction.getId());
                Toasty.success(context, "Successfully Edited").show();
                finish();
            }
            else
            {
                employeeDeduction.save(context, 0);
                Toasty.success(context, "Successfully Created").show();
                finish();
            }

        } catch (Exception err)
        {
            Debugger.logD("saveToDisk "+err.getMessage());
        }
    }

}
