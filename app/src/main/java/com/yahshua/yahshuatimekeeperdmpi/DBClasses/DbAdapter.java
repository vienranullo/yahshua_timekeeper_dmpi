package com.yahshua.yahshuatimekeeperdmpi.DBClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DbAdapter extends DatabaseManager
{

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "db_session";

    private String employee, employeeLogs,
            createLeaveTableStmt, createEmployeeLeaveBalanceTableStmt, createLeaveTypeTableStmt,
            createOvertimeTableStmt, createUndertimeTableStmt,
            createWorkOnRestdayTableStmt, createWorkOnHolidayTableStmt,
            createWorkdaysTableStmt, createErrorLogsTable, createAuditLogsTable, createEmployeeDeduction,
            createDeductionType;

    public DbAdapter(Context context)
    {
        super(context, DATABASE_NAME, DATABASE_VERSION);
    }

    private void createTables()
    {
        employee = "CREATE TABLE IF NOT EXISTS employee (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "ypo_id INTEGER, employeeid2 INTEGER, firstname TEXT, middlename TEXT, lastname TEXT, employee_img TEXT, company_id INT, rfid TEXT)";

        employeeLogs = "CREATE TABLE IF NOT EXISTS employee_logs (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "date DATE, employee INTEGER, log_img TEXT, log_type TEXT, log_time TEXT, sync_id TEXT, company_id INT, is_sync INTEGER DEFAULT 0)";

        createUndertimeTableStmt = "CREATE TABLE IF NOT EXISTS undertime (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "employeeId INT, employeeName TEXT, date DATE, hours DOUBLE, reason TEXT, requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), "+
                "isSync BOOLEAN, company_id INT, status TEXT, remarks TEXT, lastSyncedDate DATE)";

        createWorkOnRestdayTableStmt = "CREATE TABLE IF NOT EXISTS workOnRestday (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "employeeId INT, employeeName TEXT, date DATE, hours DOUBLE, reason TEXT, requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), "+
                "isSync BOOLEAN, status TEXT, company_id INT, remarks TEXT, lastSyncedDate DATE)";

        createWorkOnHolidayTableStmt = "CREATE TABLE IF NOT EXISTS workOnHoliday (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "employeeId INT, employeeName TEXT, date DATE, hours DOUBLE, reason TEXT, requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), "+
                "isSync BOOLEAN , status TEXT, company_id INT,remarks TEXT,lastSyncedDate DATE)";

        createWorkdaysTableStmt = "CREATE TABLE IF NOT EXISTS workDays (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "isMonday BOOLEAN,isTuesday BOOLEAN, isWednesday BOOLEAN, isThursday BOOLEAN, isFriday BOOLEAN, isSaturday BOOLEAN, isSunday BOOLEAN, "+
                "employeeId INT, company_id INT, scheduleId INT)";

                createLeaveTableStmt = "CREATE TABLE IF NOT EXISTS leave (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "employeeId INT, employeeName TEXT, day TEXT, dateFrom DATE, " +
                "dateTo DATE, halfDayDate DATE, timeFrom TEXT, timeTo TEXT, " +
                "daysLeave DOUBLE, leaveTypeId INT, leaveTypeName TEXT, reason TEXT, " +
                "requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), isSync BOOLEAN, company_id INT, " +
                "status TEXT, lastSyncedDate DATE)";

        createEmployeeLeaveBalanceTableStmt = "CREATE TABLE IF NOT EXISTS employeeLeaveBalance (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "employeeId INT, leaveTypeId INT, company_id INT, leaveTypeName TEXT, total DOUBLE)";

        createLeaveTypeTableStmt = "CREATE TABLE IF NOT EXISTS leaveType (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, company_id INT, isDeductible BOOLEAN)";

        createOvertimeTableStmt = "CREATE TABLE IF NOT EXISTS overtime (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "employeeId INT, employeeName TEXT, date DATE, hours DOUBLE, " +
                "requestedDate DATETIME DEFAULT (datetime('now', 'localtime')), " +
                "reason TEXT, isSync BOOLEAN, company_id INT, status TEXT, remarks TEXT, lastSyncedDate DATE)";

        createErrorLogsTable = "CREATE TABLE IF NOT EXISTS errorLogs (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "errorlogsId INT, error_logs TEXT, date DATE) ";

        createAuditLogsTable = "CREATE TABLE IF NOT EXISTS auditLog (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "module TEXT, dateTime DATETIME DEFAULT (datetime('now', 'localtime')), " +
                "action TEXT, employeeId INT, employeeName TEXT, status TEXT, errorMessage TEXT, " +
                "previousValue TEXT, newValue TEXT)";

        createEmployeeDeduction = "CREATE TABLE IF NOT EXISTS employeeDeductions (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "deduction_type_id INT, amount NUMERIC, date_requested DATE, remarks TEXT, status TEXT, term TEXT," +
                "amount_per_deduction NUMERIC, date_start DATE, date_end DATE, regular_id INT, employee_id INT, is_synced INT DEFAULT 0, company_id INT, lastSyncedDate DATE)";

        createDeductionType = "CREATE TABLE IF NOT EXISTS deductionTypes (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "deduction_type_id INT, name TEXT, company_id INT)";

    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        createTables();

        db.execSQL(employee);
        db.execSQL(employeeLogs);
        db.execSQL(createLeaveTableStmt);
        db.execSQL(createEmployeeLeaveBalanceTableStmt);
        db.execSQL(createLeaveTypeTableStmt);
        db.execSQL(createOvertimeTableStmt);
        db.execSQL(createUndertimeTableStmt);
        db.execSQL(createWorkOnRestdayTableStmt);
        db.execSQL(createWorkOnHolidayTableStmt);
        db.execSQL(createWorkdaysTableStmt);
        db.execSQL(createErrorLogsTable);
        db.execSQL(createAuditLogsTable);
        db.execSQL(createEmployeeDeduction);
        db.execSQL(createDeductionType);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
