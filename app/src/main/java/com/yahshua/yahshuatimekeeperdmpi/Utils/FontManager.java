package com.yahshua.yahshuatimekeeperdmpi.Utils;


public class FontManager {

    private static final String CUSTOM_FONT_PATH = "fontawesome-webfont.ttf";

    public static final char SIGNIN = '\uf090';
    public static final char SIGNOUT = '\uF08b';
    public static final char PERSONS = '\uf0c0';
    public static final char CHILD = '\uf1ae';
    public static final char BOOK = '\uf02d';
    public static final char PLUS = '\uf067';
    public static final char PENCIL = '\uf040';
    public static final char NOTES = '\uf24a';
    public static final char COMMENT = '\uf075';
    public static final char CLOUD_UPLOAD = '\uf0ee';


//    public static FontDrawable getSignInDrawable(Context context, char unicode)
//    {
//        FontDrawable fontDrawable = new FontDrawable.Builder(context, unicode, CUSTOM_FONT_PATH)
//                .setSizeDp(1)
//                .setPaddingDp(50)
//                .setColor(Color.BLACK)
//                .build();
//
//        return fontDrawable;
//    }
//
//    public static FontDrawable getFontDrawable(Context context, char unicode, int sizeDp, int paddingDp, String color)
//    {
//        Color.parseColor(color);
//
//        FontDrawable fontDrawable = new FontDrawable.Builder(context, unicode, CUSTOM_FONT_PATH)
//                .setSizeDp(sizeDp)
//                .setPaddingDp(paddingDp)
//                .setColor(Color.parseColor(color))
//                .build();
//
//        return fontDrawable;
//    }


}