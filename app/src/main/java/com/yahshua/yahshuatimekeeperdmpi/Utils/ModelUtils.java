package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;

import java.util.ArrayList;


public class ModelUtils {


    public static long getLastId(Context context, String table)
    {
        try
        {
            long lastId = 0;
            DatabaseAdapter db = new DatabaseAdapter(context);
            String query = "SELECT id from " + table + " order by id DESC limit 1";

            db = db.open();
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                lastId = cursor.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }

            db.close();

            return lastId;

        } catch (Exception err)
        {
            return 0;
        }
    }

    public static String getNextCode(Context context, String table)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);

            String lastCode = "";
            String query = "SELECT code FROM " + table + " ORDER BY id DESC LIMIT 1";

            db = db.open();
            Cursor cursor = db.open().getDatabaseInstance().rawQuery(query, null);

            if (cursor != null && cursor.moveToFirst()) {
                lastCode = cursor.getString(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }
            Log.d("DDD", lastCode);
            db.close();

            return incrementCode(lastCode);

        } catch (Exception err)
        {
            System.out.println("sds" + err);
            return null;
        }
    }

    public static String incrementCode(String code)
    {
        System.out.println("YEST " + code.length());
        if(code != null && code.length() > 0)
        {
            int lastCode = Integer.parseInt(code);
            lastCode += 1;

            return String.format("%04d", lastCode);
        }else{
            return "0001";
        }
    }

    public static String buildFields(ArrayList<String> tableFields)
    {
        String finalString = "";
        for(String field : tableFields)
        {
            finalString = finalString.concat(field + ",");
        }

        finalString = finalString.substring(0, finalString.length() - 1);
        return finalString;
    }

    public static int getInt(String field)
    {
        return 0;
    }

}
