package com.yahshua.yahshuatimekeeperdmpi.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeductionType;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DownloadDeductionTypeService extends Service
{
    private Context context;

    @Override
    public void onCreate()
    {
        context = getApplicationContext();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        downloadDeductionType();
        return START_STICKY;
    }

    private void downloadDeductionType()
    {
        try
        {
            HttpProvider.get(context, "other_deduction_types/read/", null, new JsonHttpResponseHandler(){

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        super.onSuccess(statusCode, headers, response);
                        DeductionType.delete(context);

                        ArrayList<DeductionType> deductionTypeArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<List<DeductionType>>(){}.getType());

                        for(DeductionType deductionType : deductionTypeArrayList)
                        {
                            deductionType.save(context);
                        }

                    } catch (Exception err)
                    {
                        Debugger.logD("onSuccess err "+err.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    try
                    {
                       Debugger.logD("onFailure1 "+responseString);
                    }
                    catch (Exception err)
                    {
                        Debugger.logD("Exception "+err.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    try
                    {
                        Debugger.logD("onFailure2 "+errorResponse);
                    }
                    catch (Exception err)
                    {
                        Debugger.logD("Exception2 "+err.getMessage());
                    }
                }
            });


        } catch (Exception err)
        {
            Debugger.logD(err.toString());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
