package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.WorkOnHolidayRequestsAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.RequestsFilterDialogFragment;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;
import com.yahshua.yahshuatimekeeperdmpi.httpRequests.DownloadWorkOnHolidayRequestsAsyncTask;

import org.json.JSONObject;

import java.util.ArrayList;


public class WorkOnHolidayRequestsActivity extends BaseActivity
{
    private ArrayList<WorkOnHoliday> workOnHolidayArrayList = new ArrayList<>();
    private WorkOnHolidayRequestsAdapter workOnHolidayRequestsAdapter;
    private ProgressBar pbDownloadWorkOnHolidays;
    private RecyclerView rvWorkOnHolidayRequests;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            initializeViews();
            initializeData(null);
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error WorkOnHolidayRequestsActivity onCreate: \n" + err.toString());
        }
    }private void initializeViews() throws Exception
{
    try
    {
        setContentView(R.layout.work_on_holiday_requests_activity);
        initializeRecyclerViews();

        // Progress bar
        pbDownloadWorkOnHolidays = findViewById(R.id.pbDownloadWorkOnHolidays);
        pbDownloadWorkOnHolidays.setVisibility(View.VISIBLE);

        // Title
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) throw new Exception("ActionBar is null");
        actionBar.setTitle("Work on Holiday Requests");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    catch (Exception err)
    {
        throw new Exception("Error initializing views: \n" + err.toString());
    }
}

    private void initializeRecyclerViews() throws Exception
    {
        try
        {
            rvWorkOnHolidayRequests = findViewById(R.id.rvWorkOnHolidayRequests);
            rvWorkOnHolidayRequests.setVisibility(View.GONE);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvWorkOnHolidayRequests.setLayoutManager(layoutManager);

            workOnHolidayRequestsAdapter = new WorkOnHolidayRequestsAdapter(this, workOnHolidayArrayList);
            rvWorkOnHolidayRequests.setAdapter(workOnHolidayRequestsAdapter);
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: \n" + err.toString());
        }
    }

    private void initializeData(JSONObject jsonObject) throws Exception
    {
        try
        {
            // Download work on holiday requests
            AsyncResponseListener asyncResponseListener = new AsyncResponseListener()
            {
                @Override
                public void onSuccess(Object object)
                {
                    workOnHolidayArrayList.clear();
                    workOnHolidayArrayList.addAll((ArrayList<WorkOnHoliday>) object);
                    workOnHolidayRequestsAdapter.notifyDataSetChanged();

                    pbDownloadWorkOnHolidays.setVisibility(View.GONE);
                    rvWorkOnHolidayRequests.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(int errorCode, String error)
                {
                    switch (errorCode)
                    {
                        case 2:
                            Utility.showNoNetworkConnectionDialog(WorkOnHolidayRequestsActivity.this);
                            break;
                        case 3:
                            Utility.showFailedToConnectToServerDialog(WorkOnHolidayRequestsActivity.this);
                            break;
                        case 4:
                            Utility.showServerErrorDialog(WorkOnHolidayRequestsActivity.this, error);
                            break;
                        default:
                            Utility.showError(getSupportFragmentManager(), "Error DownloadWorkOnHolidayRequestsAsyncTask: \n" + error);
                    }

                    pbDownloadWorkOnHolidays.setVisibility(View.GONE);
                }
            };

            // Default filter
            if (jsonObject == null)
            {
                jsonObject = new JSONObject();
                jsonObject.put("status", "Pending");
            }

            new DownloadWorkOnHolidayRequestsAsyncTask(this, asyncResponseListener, jsonObject).execute();
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing data: \n" + err.toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        try
        {
            getMenuInflater().inflate(R.menu.request_filter_menu, menu);
            initializeSearchView(menu);
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error WorkOnHolidayRequestsActivity onCreateOptionsMenu: " + err.toString());
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void initializeSearchView(Menu menu) throws Exception
    {
        try
        {
            MenuItem searchItem = menu.findItem(R.id.action_search);

            // Expand and collapse listener
            MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener()
            {
                @Override
                public boolean onMenuItemActionCollapse(MenuItem item)
                {
                    try
                    {
                        rvWorkOnHolidayRequests.setVisibility(View.GONE);
                        pbDownloadWorkOnHolidays.setVisibility(View.VISIBLE);
                        initializeData(null);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager(), "Error onMenuItemActionCollapse: " + err.toString());
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionExpand(MenuItem item)
                {
                    return true;
                }
            };

            searchItem.setOnActionExpandListener(expandListener);

            // Query listener
            SearchView searchView = (SearchView) searchItem.getActionView();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
            {
                @Override
                public boolean onQueryTextChange(String newText)
                {
                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query)
                {
                    try
                    {
                        rvWorkOnHolidayRequests.setVisibility(View.GONE);
                        pbDownloadWorkOnHolidays.setVisibility(View.VISIBLE);

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("search_text", query);
                        jsonObject.put("status", "Pending");

                        initializeData(jsonObject);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager() ,"Error onQueryTextSubmit: " + err.toString());
                    }
                    return false;
                }
            });
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing searchView: " + err.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        try
        {
            switch (item.getItemId())
            {
                case android.R.id.home:
                    finish();
                    return true;
                case R.id.action_filter:
                    showFilterDialog();
                    break;
            }
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error WorkOnHolidayRequestsActivity onOptionsItemSelected: " + err.toString());
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog()
    {
        try
        {
            RequestsFilterDialogFragment requestsFilterDialogFragment = new RequestsFilterDialogFragment();

            requestsFilterDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {}
            });

            requestsFilterDialogFragment.setOnCompleteListener(new CompleteListener()
            {
                @Override
                public void onComplete(Object object)
                {
                    try
                    {
                        rvWorkOnHolidayRequests.setVisibility(View.GONE);
                        pbDownloadWorkOnHolidays.setVisibility(View.VISIBLE);
                        initializeData((JSONObject) object);
                    }
                    catch (Exception err)
                    {
                        Utility.showError(getSupportFragmentManager(), "Error onComplete: " + err.toString());
                    }
                }
            });

            requestsFilterDialogFragment.show(getSupportFragmentManager(), "REQUEST_FILTER_DIALOG");
        }
        catch (Exception err)
        {
            Utility.showError(getSupportFragmentManager(), "Error showing filters dialog: " + err.toString());
        }
    }
}
