package com.yahshua.yahshuatimekeeperdmpi.services;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class OvertimeUploader extends AsyncTask
{

    private WeakReference<Context> contextRef;
    private CompleteListener completeListener;

    public OvertimeUploader(Context context, CompleteListener completeListener) {
        contextRef = new WeakReference<>(context);
        this.completeListener = completeListener;
    }

    @Override
    protected Object doInBackground(Object[] objects)
    {
        Context context = contextRef.get();

        String searchQuery = "WHERE isSync = 0 AND company_id = " + UserSession.getCompanyId(context);
        ArrayList<Overtime> overtimeArrayList = Overtime.load(context, searchQuery);

        // Tempo Fix
        if (overtimeArrayList.size() == 0)
        {
            searchQuery = "WHERE isSync = 0";
            overtimeArrayList = Overtime.load(context, searchQuery);
        }

        for (final Overtime overtime : overtimeArrayList)
        {
            uploadOvertime(overtime, context);
        }


        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Toasty.success(contextRef.get(), "Overtime Uploading Finished.").show();

        if (completeListener != null)
        {
            completeListener.onComplete(null);
        }

    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    private void uploadOvertime(final Overtime overtime, final Context context)
    {
        try
        {
            overtime.setDateString(DateToString.dbFormat(overtime.getDate()));
            overtime.setDateRequestedString(DateToString.dateTimeFormat(overtime.getRequestedDate()));
            overtime.setEmployeeName("");

            String json = new Gson().toJson(overtime);
            StringEntity entity = new StringEntity(json);

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    overtime.changeSyncStatus(context);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        Debugger.logD("Failed");
                        String str = "";

                        if (responseBody!= null && responseBody.length > 0)
                        {
                            str = new String(responseBody, "UTF-8");
                        }

                        JSONObject obj = new JSONObject(str);

                        if (obj.has("description"))
                        {
                            overtime.setSyncErrorMessage(obj.getString("description"));
                            overtime.saveSyncErrorMessage(context);
                        }

                        AuditLog.saveAuditLog(context,"MANUAL SYNCING - OVERTIME","SYNC","FAILED","NONE");

                    } catch (Exception err) {

                    }
                }

            };

            HttpProvider.postSync(context, "overtime-application/save/", entity, false, asyncHttpResponseHandler);
        } catch (Exception err)
        {

        }
    }
}
