package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Activities.OvertimeApprovalRequestActivity;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;


public class OvertimeRequestsAdapter extends RecyclerView.Adapter<OvertimeRequestsAdapter.ViewHolder>
{
    private ArrayList<Overtime> overtimeArrayList;
    private Context context;


    public OvertimeRequestsAdapter(Context context, ArrayList<Overtime> overtimeArrayList)
    {
        this.context = context;
        this.overtimeArrayList = overtimeArrayList;
    }

    @NonNull
    @Override
    public OvertimeRequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.overtime_request_item, parent, false);
        return new ViewHolder(contactView);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEmployeeName, tvDate, tvHours, tvReason;
        CardView cvOvertimeRequest;

        ViewHolder(View itemView)
        {
            super(itemView);

            tvEmployeeName = itemView.findViewById(R.id.tvEmployeeName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvHours = itemView.findViewById(R.id.tvHours);
            tvReason = itemView.findViewById(R.id.tvReason);
            cvOvertimeRequest = itemView.findViewById(R.id.cvOvertimeRequest);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        Overtime overtime = overtimeArrayList.get(position);

        holder.tvEmployeeName.setText(overtime.getEmployeeName());
        holder.tvDate.setText(DateToString.displayFormat(overtime.getDate()));
        holder.tvHours.setText(overtime.getHours() + " hrs.");
        holder.tvReason.setText(overtime.getReason());

        holder.cvOvertimeRequest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent coursesActivityIntent = new Intent(context, OvertimeApprovalRequestActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("OVERTIME", overtimeArrayList.get(position));
                coursesActivityIntent.putExtras(bundle);

                context.startActivity(coursesActivityIntent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return overtimeArrayList.size();
    }
}
