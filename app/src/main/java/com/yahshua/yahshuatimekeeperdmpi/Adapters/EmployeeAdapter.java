package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;

import java.util.ArrayList;

/**
 * Created by AlfredVanz on 8/22/2017.
 */

public class EmployeeAdapter extends ArrayAdapter<Employee> {
    private final int mResource;
    private ArrayList<Employee> memberList;

    public EmployeeAdapter(@NonNull Context context, ArrayList<Employee> list)
    {
        super(context, 0, list);
        memberList = list;
        mResource = 0;
    }

    @Override
    public int getPosition(@Nullable Employee item)
    {
        int x = 0;
        for(Employee student : memberList)
        {
            if(student.getId() == item.getId())
            {
                return x;
            }else{
                x++;
            }
        }

        return super.getPosition(item);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Employee employee = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listrow_employee, parent, false);
        }

        TextView tvCode = (TextView) convertView.findViewById(R.id.tvCode);
        tvCode.setText(" " + employee.getEmployeeid2());
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        tvName.setText(" " + employee.getFullName());
        return convertView;
    }

}

