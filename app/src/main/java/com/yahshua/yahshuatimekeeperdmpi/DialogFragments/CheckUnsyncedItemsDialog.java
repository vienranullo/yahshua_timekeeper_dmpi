package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.PopUpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;

import es.dmoral.toasty.Toasty;

public class CheckUnsyncedItemsDialog extends DialogFragment
{
    // Context and Views
    private View view;
    private Context context;

    // Widgets
    private TextView tvEmployeeLogsCount;
    private Button btnRecreate, btnSync;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.dialog_check_unsynced,container,false);
        context = getContext();
        initializeUI();
        return view;
    }

    private void initializeUI()
    {
        tvEmployeeLogsCount = view.findViewById(R.id.tvEmployeeLogs);
        tvEmployeeLogsCount.setText("" + EmployeeLogs.getUnSyncLogsCount(context));

        btnRecreate = view.findViewById(R.id.btnRecreate);
        btnRecreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteDb();
            }
        });

        btnSync = view.findViewById(R.id.btnSync);
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                new DownloadFilesTask().execute(null, null, null);
            }
        });
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, String>
    {
        private String message = "";

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {

                    @Override
                    public void onSuccess(String fromWhere, String successMessage)
                    {

                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {

                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.uploadEmployeeLogs();

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPreExecute() {
            Toasters.ShowLoadingSpinner(context);
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected void onPostExecute(String s)
        {
            Toasters.HideLoadingSpinner();
            deleteDatabase();
        }
    }



    // Deleting database
    public boolean deleteDatabase()
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        boolean d = getActivity().deleteDatabase(dbAdapter.DATABASE_NAME);
        dbAdapter.close();

        restartApplication();
        return d;
    }

    public void restartApplication()
    {
        Activity activity = getActivity();
        final PackageManager pm = context.getPackageManager();
        final Intent intent = pm.getLaunchIntentForPackage(context.getPackageName());
        activity.finishAffinity();
        activity.startActivity(intent);
        System.exit(0);
    }

    private void confirmDeleteDb()
    {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteDatabase();
                        Toasty.success(getActivity(), "Database recreated.\nPlease sync employees!", Toast.LENGTH_LONG).show();
                        dismiss();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(getActivity(), dialogClickListener,"Recreate will delete all records in this device.", "Yes", "No");
    }

    @Override
    public void onResume()
    {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

}
