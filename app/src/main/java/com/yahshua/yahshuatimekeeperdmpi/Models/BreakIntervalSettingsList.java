package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class BreakIntervalSettingsList {

    @SerializedName("server_id")
    private int serverId;
    @SerializedName("device_settings_server_id")
    private int deviceSettingsServerId;
    @SerializedName("time_out_start")
    private String timeOutStart;
    @SerializedName("time_out_end")
    private String timeOutEnd;
    private int interval;

    public BreakIntervalSettingsList(){

    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getDeviceSettingsServerId() {
        return deviceSettingsServerId;
    }

    public void setDeviceSettingsServerId(int deviceSettingsServerId) {
        this.deviceSettingsServerId = deviceSettingsServerId;
    }

    public String getTimeOutStart() {
        return timeOutStart;
    }

    public void setTimeOutStart(String timeOutStart) {
        this.timeOutStart = timeOutStart;
    }

    public String getTimeOutEnd() {
        return timeOutEnd;
    }

    public void setTimeOutEnd(String timeOutEnd) {
        this.timeOutEnd = timeOutEnd;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public void save(Context context, ArrayList<BreakIntervalSettingsList> list) throws Exception{

        try
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            Gson gson = new Gson();
            String timeInterval = gson.toJson(list);

            editor.putString("TIME_INTERVAL", timeInterval);
            editor.apply();
        }
        catch (Exception err)
        {
            throw new Exception("Error Saving Device Settings: \n" + err.toString());
        }

    }

    public BreakIntervalSettingsList(Context context)
    {
        this.timeOutEnd = getTimeOutEnd();
        this.timeOutStart = getTimeOutStart();
        this.deviceSettingsServerId = getDeviceSettingsServerId();
        this.serverId = getServerId();
        this.interval = getInterval();
    }

    public ArrayList<BreakIntervalSettingsList> getArrayList(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("TIME_INTERVAL", null);
        Type type = new TypeToken<ArrayList<BreakIntervalSettingsList>>() {}.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public String toString() {
        return   "{serverId=" + serverId + ", deviceSettingsServerId=" + deviceSettingsServerId + ", timeOutStart='" + timeOutStart + '\'' + ", timeOutEnd='" + timeOutEnd + '\'' + ", interval=" + interval + '}';
    }
}
