package com.yahshua.yahshuatimekeeperdmpi.Models;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Converter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Constants;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ModelUtils;
import com.yahshua.yahshuatimekeeperdmpi.Utils.SettingsHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Syncer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class EmployeeLogs implements Parcelable {

    private int id;
    @SerializedName("employee")
    private int employeeId;
    private String date;
    @SerializedName("log_time")
    private String logTime;
    @SerializedName("log_img")
    private String logImg;
    @SerializedName("log_type")
    private String logType;
    @SerializedName("sync_id")
    private String syncId;
    private boolean isSync;

    public Employee employeeObject = new Employee();

    public EmployeeLogs(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLogTime() {
        return logTime;
    }

    public String getDisplayDate()
    {
        return DateTimeHandler.formatDateProper(date);
    }

    public String getDisplayLogTime()
    {
        // Displays Time as AM:PM Format instead of 24 HOUR Format
        return DateTimeHandler.convert24to12(logTime);
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getLogImg() {
        return logImg;
    }

    public void setLogImg(String logImg) {
        this.logImg = logImg;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getSyncId() {
        return syncId;
    }

    public void setSyncId(String syncId) {
        this.syncId = syncId;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public void syncLog(final Context context, AsyncHttpResponseHandler asyncHttpResponseHandler)
    {
        try
        {
            Syncer.syncIndividualLogs(context, this, asyncHttpResponseHandler);
        } catch (Exception err)
        {
            Toasty.error(context, err.toString()).show();
        }
    }

    public static void delete(DbAdapter dbAdapter) throws SQLException
    {
        dbAdapter.getDb().delete("employee_logs", "", new String[]{});
    }

    public static void clearSyncedLogs(Context context)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();

        dbAdapter.getDb().delete("employee_logs", "is_sync=1",null);

        dbAdapter.close();
    }

    public static boolean checkOutInterval(Context context, int studentId, String dateString)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();
        try
        {
            String query = "SELECT log_time FROM employee_logs WHERE employee=" + studentId + " AND date ='" + dateString +"' ORDER BY id DESC LIMIT 1";

            Cursor cursor = dbAdapter.getDb().rawQuery(query, null);

            int minutesPassed, hoursPassed, intervalMinutes;
            intervalMinutes = SettingsHandler.getMinutesInterval2(context);

            if(cursor.moveToFirst())
            {
                do {

                    String logTime = DateTimeHandler.convert12to42(cursor.getString(cursor.getColumnIndex("log_time")));
                    SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");

                    Date d1 = format1.parse(logTime);
                    Date d2 = new Date();

                    long difference = d2.getTime() - d1.getTime();

                    minutesPassed = (int) ((difference / (1000*60)) % 60);
                    hoursPassed = (int) ((difference / (1000*60*60)) % 24);

                } while (cursor.moveToNext());

                Constants.waitingMinutes = intervalMinutes - minutesPassed;

                cursor.close();
                dbAdapter.close();
                return hoursPassed == 0 && minutesPassed < intervalMinutes;

            }else{

                dbAdapter.close();
                // Return true if no record as of today to let user log in
                return false;
            }

        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs checkOutInterval error "+err.getMessage(), dbAdapter);
            dbAdapter.close();
            return false;
        }

    }

    public static boolean checkIfIntervalNotPassed(Context context, int studentId, String dateString)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();
        try
        {
            String query = "SELECT log_time FROM employee_logs WHERE employee=" + studentId + " AND date ='" + dateString +"' ORDER BY id DESC LIMIT 1";
            Cursor cursor = dbAdapter.getDb().rawQuery(query, null);
            int minutesPassed, hoursPassed, intervalMinutes;
            intervalMinutes = SettingsHandler.getMinutesInterval(context);

            if(cursor.moveToFirst())
            {
                do {

                    String logTime = DateTimeHandler.convert12to42(cursor.getString(cursor.getColumnIndex("log_time")));
                    SimpleDateFormat format1 = new SimpleDateFormat("HH:mm");

                    Date d1 = format1.parse(logTime);
                    Date d2 = new Date();

                    long difference = d2.getTime() - d1.getTime();

                    minutesPassed = (int) ((difference / (1000*60)) % 60);
                    hoursPassed = (int) ((difference / (1000*60*60)) % 24);

                } while (cursor.moveToNext());

                Constants.waitingMinutes = intervalMinutes - minutesPassed;

                cursor.close();
                dbAdapter.close();
                return hoursPassed == 0 && minutesPassed < intervalMinutes;

            }else{

                // Return true if no record as of today to let user log in
                dbAdapter.close();
                return false;
            }

        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs checkIfIntervalNotPassed error "+err.getMessage(), dbAdapter);
            dbAdapter.close();
            return false;
        }
    }

    public static ArrayList<EmployeeLogs> read(boolean forSync, String logType, int employeeId,int limitCount, DbAdapter dbAdapter)
    {
        ArrayList<EmployeeLogs> employeeLogsArrayList = new ArrayList<>();
        try
        {
            ArrayList<String> fields = new ArrayList<String>();

            fields.add("employee_logs.id");
            fields.add("employee");
            fields.add("log_img");
            fields.add("log_type");
            fields.add("log_time");
            fields.add("date");
            fields.add("sync_id");
            fields.add("is_sync");

            if(!forSync)
            {
                fields.add("employee_table.firstname as firstname");
                fields.add("employee_table.lastname as lastname");
            }

            String selectQuery = "SELECT " + ModelUtils.buildFields(fields) + " FROM employee_logs";

            if(forSync) {
                selectQuery += " WHERE is_sync = 0 LIMIT " + limitCount;
            }else if(employeeId > 0)
            {
                selectQuery += " LEFT JOIN employee as employee_table ON employee_logs.employee = employee_table.ypo_id ";
                selectQuery += " WHERE employee_table.employeeid2=" + employeeId + " AND employee_logs.log_type='" + logType +"'";
            }else{
                selectQuery += " LEFT JOIN employee as employee_table ON employee_logs.employee = employee_table.ypo_id";
            }

            Cursor cursor = dbAdapter.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {

                    EmployeeLogs employeeLogs = new EmployeeLogs();

                    employeeLogs.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))));
                    employeeLogs.setEmployeeId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("employee"))));
                    employeeLogs.setLogImg(cursor.getString(cursor.getColumnIndex("log_img")));
                    employeeLogs.setLogType(cursor.getString(cursor.getColumnIndex("log_type")));
                    employeeLogs.setLogTime(DateTimeHandler.convert12to42(cursor.getString(cursor.getColumnIndex("log_time"))));
                    employeeLogs.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    employeeLogs.setSyncId(cursor.getString(cursor.getColumnIndex("sync_id")));

                    if(!forSync)
                    {
                        employeeLogs.employeeObject.setFirstname(cursor.getString(cursor.getColumnIndex("firstname")));
                        employeeLogs.employeeObject.setLastname(cursor.getString(cursor.getColumnIndex("lastname")));

                        int isSync = Integer.parseInt(cursor.getString(cursor.getColumnIndex("is_sync")));
                        if(isSync == 1)
                        {
                            employeeLogs.setSync(true);
                        }else{
                            employeeLogs.setSync(false);
                        }
                    }

                    employeeLogsArrayList.add(employeeLogs);

                } while (cursor.moveToNext());
            }

            cursor.close();

            return employeeLogsArrayList;
        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs read error "+err.getMessage(), dbAdapter);
            return employeeLogsArrayList;
        }
    }

    public static ArrayList<EmployeeLogs> read(Context context, boolean forSync, int employeeId,int limitCount)
    {
        ArrayList<EmployeeLogs> employeeLogsArrayList = new ArrayList<>();
        DbAdapter db = new DbAdapter(context);
        db.open();

        try
        {
            ArrayList<String> fields = new ArrayList<String>();

            fields.add("employee_logs.id");
            fields.add("employee");
            fields.add("log_img");
            fields.add("log_type");
            fields.add("log_time");
            fields.add("date");
            fields.add("sync_id");
            fields.add("is_sync");

            if(!forSync)
            {
                fields.add("employee_table.firstname as firstname");
                fields.add("employee_table.lastname as lastname");
            }

            String selectQuery = "SELECT " + ModelUtils.buildFields(fields) + " FROM employee_logs";

            if(forSync) {
                selectQuery += " WHERE is_sync = 0 LIMIT " + limitCount;
            }else if(employeeId > 0)
            {
                selectQuery += " LEFT JOIN employee as employee_table ON employee_logs.employee = employee_table.ypo_id ";
                selectQuery += " WHERE employee_table.employeeid2=" + employeeId;
            }else{
                selectQuery += " LEFT JOIN employee as employee_table ON employee_logs.employee = employee_table.ypo_id";
            }

            Cursor cursor = db.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {

                    EmployeeLogs employeeLogs = new EmployeeLogs();

                    employeeLogs.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))));
                    employeeLogs.setEmployeeId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("employee"))));
                    employeeLogs.setLogImg(cursor.getString(cursor.getColumnIndex("log_img")));
                    employeeLogs.setLogType(cursor.getString(cursor.getColumnIndex("log_type")));
                    employeeLogs.setLogTime(DateTimeHandler.convert12to42(cursor.getString(cursor.getColumnIndex("log_time"))));
                    employeeLogs.setDate(cursor.getString(cursor.getColumnIndex("date")));
                    employeeLogs.setSyncId(cursor.getString(cursor.getColumnIndex("sync_id")));

                    if(!forSync)
                    {
                        employeeLogs.employeeObject.setFirstname(cursor.getString(cursor.getColumnIndex("firstname")));
                        employeeLogs.employeeObject.setLastname(cursor.getString(cursor.getColumnIndex("lastname")));

                        int isSync = Integer.parseInt(cursor.getString(cursor.getColumnIndex("is_sync")));
                        if(isSync == 1)
                        {
                            employeeLogs.setSync(true);
                        }else{
                            employeeLogs.setSync(false);
                        }
                    }

                    employeeLogsArrayList.add(employeeLogs);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();

            return employeeLogsArrayList;
        }
        catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs read Error "+err.getMessage(),db );
            db.close();
            return employeeLogsArrayList;
        }
    }

    public static ArrayList<EmployeeLogs> readForExport(Context context,int isSynced, boolean isAllLogs, String dateFrom, String dateTo)
    {
        ArrayList<EmployeeLogs> employeeLogsArrayList = new ArrayList<>();
        DbAdapter db = new DbAdapter(context);
        db.open();

        try
        {
            ArrayList<String> fields = new ArrayList<String>();

            fields.add("employee_logs.id");
            fields.add("employee");
            fields.add("log_type");
            fields.add("log_time");
            fields.add("date");
            fields.add("is_sync");

            fields.add("employee_table.firstname as firstname");
            fields.add("employee_table.lastname as lastname");
            fields.add("employee_table.employeeid2 as employeeid2");

            String selectQuery = "SELECT " + ModelUtils.buildFields(fields) + " FROM employee_logs";

            selectQuery += " LEFT JOIN employee as employee_table ON employee_logs.employee = employee_table.ypo_id";

            if(!isAllLogs && !dateFrom.equals("")){
                selectQuery += " WHERE is_sync = " + isSynced +" AND date >= '" + dateFrom + "' AND date <= '" + dateTo + "'";
            } else if(isAllLogs && !dateFrom.equals("")){
                selectQuery += " WHERE date >= '" + dateFrom + "' AND date <= '" + dateTo + "'";
            }else if(!isAllLogs && dateFrom.equals("")){
                selectQuery += " WHERE is_sync = " + isSynced ;
            }

            Cursor cursor = db.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {

                    EmployeeLogs employeeLogs = new EmployeeLogs();

                    employeeLogs.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))));
                    employeeLogs.setEmployeeId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("employee"))));
                    employeeLogs.setLogType(cursor.getString(cursor.getColumnIndex("log_type")));
                    employeeLogs.setLogTime(DateTimeHandler.convert12to42(cursor.getString(cursor.getColumnIndex("log_time"))));
                    employeeLogs.setDate(cursor.getString(cursor.getColumnIndex("date")));
//
                    employeeLogs.employeeObject.setFirstname(cursor.getString(cursor.getColumnIndex("firstname")));
                    employeeLogs.employeeObject.setLastname(cursor.getString(cursor.getColumnIndex("lastname")));
                    employeeLogs.employeeObject.setEmployeeid2(Converter.ObjectInt(cursor,"employeeid2"));

                    int isSync = Integer.parseInt(cursor.getString(cursor.getColumnIndex("is_sync")));
                    if(isSync == 1)
                    {
                        employeeLogs.setSync(true);
                    }else{
                        employeeLogs.setSync(false);
                    }

                    employeeLogsArrayList.add(employeeLogs);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
            return employeeLogsArrayList;
        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs readForExport Error "+err.getMessage(),db );
            db.close();
            return employeeLogsArrayList;
        }
    }

    public void convertSyncStatus(Context context)
    {
        DbAdapter dbAdapter = new DbAdapter(context);
        dbAdapter.open();
        try
        {
            ContentValues studentSessionValues = new ContentValues();
            studentSessionValues.put("is_sync", "1");
            dbAdapter.getDb().update("employee_logs",studentSessionValues, "id = "+ getId(),null);

        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs convertSyncStatus error "+err.getMessage(), dbAdapter);
            dbAdapter.close();
        }
    }

    public boolean save(Context context)
    {
        DbAdapter db = new DbAdapter(context);
        db.open();

        try
        {
            ContentValues employeeLog = new ContentValues();
            Date today = new Date();

            employeeLog.put("date",     DateTimeHandler.convertDatetoStringDate(today));
            employeeLog.put("employee", this.getEmployeeId());
            employeeLog.put("log_img",  this.logImg);
            employeeLog.put("log_time", DateTimeHandler.formatTime(today));
            employeeLog.put("log_type", this.logType);
            employeeLog.put("sync_id",  this.getSyncId());

            db.getDb().insertOrThrow("employee_logs",null, employeeLog);
            db.close();
            return true;

        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs save error "+err.getMessage(), db);
            db.close();
            return false;
        }
    }

    public static int getUnSyncLogsCount(Context context)
    {
        DbAdapter db = new DbAdapter(context);
        db.open();

        try
        {
            EmployeeLogs employeeLogs = new EmployeeLogs();
            String selectQuery = "SELECT COUNT(id) AS count FROM employee_logs WHERE is_sync = " + employeeLogs.getEmployeeId();

            Cursor cursor = db.getDb().rawQuery(selectQuery, null);
            int count = 0;
            if (cursor.moveToFirst()) {
                do {
                    count = Integer.parseInt(cursor.getString(cursor.getColumnIndex("count")));
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
            return count;
        } catch (Exception err)
        {
            ErrorLogs.save("EmployeeLogs getUnSyncLogsCount error "+err.getMessage(), db);
            db.close();
            return 0;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(logImg);
        dest.writeString(logType);
        dest.writeString(logTime);
        dest.writeString(date);
    }

    protected EmployeeLogs(Parcel in)
    {
        id = in.readInt();
        logImg = in.readString();
        logType = in.readString();
        logTime = in.readString();
        date = in.readString();
    }

    public static final Creator<EmployeeLogs> CREATOR = new Creator<EmployeeLogs>()
    {
        @Override
        public EmployeeLogs createFromParcel(Parcel in)
        {
            return new EmployeeLogs(in);
        }

        @Override
        public EmployeeLogs[] newArray(int size)
        {
            return new EmployeeLogs[size];
        }
    };

}
