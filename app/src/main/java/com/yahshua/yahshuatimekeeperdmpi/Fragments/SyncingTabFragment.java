package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserPreferences;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;


public class SyncingTabFragment extends Fragment
{
    // Views and Context
    private View view;
    private Context context;

    // Widgets
    private Switch swSyncPicture,swDeleteSync, swWarningExisting;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_syncing_tab, container, false);
        context = getContext();

        initializeUI(view);
        readSettings();

        return view;
    }

    // Initialize component from a layout file
    private void initializeUI(View view)
    {
        // Switch Buttons
        swSyncPicture = view.findViewById(R.id.swSyncPictures);
        swSyncPicture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                UserPreferences.setAutoInstantSyncPreferences(context, swSyncPicture.isChecked());
            }
        });

        swDeleteSync = view.findViewById(R.id.swDeleteSync);
        swDeleteSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                UserPreferences.setDeleteAfterSyncingPreferences(context, swDeleteSync.isChecked());
            }
        });

        swWarningExisting = view.findViewById(R.id.swWarningExisting);
        swWarningExisting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                UserPreferences.setDeleteAfterSyncingPreferences(context, swWarningExisting.isChecked());
            }
        });
    }

    private void readSettings()
    {
        swSyncPicture.setChecked(UserPreferences.getAutoInstantSyncPreferences(context));
        swDeleteSync.setChecked(UserPreferences.getDeleteAfterSyncingPreferences(context));
        swWarningExisting.setChecked(UserPreferences.getWarningExisting(context));
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(getContext(), getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(getActivity(), pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(getActivity());
        }
    }
    //End
}
