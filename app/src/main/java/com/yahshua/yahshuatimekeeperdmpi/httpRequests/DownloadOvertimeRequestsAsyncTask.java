package com.yahshua.yahshuatimekeeperdmpi.httpRequests;

import android.content.Context;
import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.Overtime;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DownloadOvertimeRequestsAsyncTask extends AsyncTask<Void, Void, Void>
{
    private WeakReference<Context> contextWeakReference;
    private AsyncResponseListener asyncResponseListener;
    private int errorCode = 1;
    private String error;
    private ArrayList<Overtime> overtimeArrayList = new ArrayList<>();
    private JSONObject jsonObject;


    public DownloadOvertimeRequestsAsyncTask(Context context, AsyncResponseListener asyncResponseListener, JSONObject jsonObject)
    {
        this.asyncResponseListener = asyncResponseListener;
        this.contextWeakReference = new WeakReference<>(context);
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute()
    {
        try
        {
            // Check internet connection
            Context context = contextWeakReference.get();

            if (!Utility.haveNetworkConnection(context))
            {
                errorCode = 2;
                cancel(true);
            }
        }
        catch (Exception err)
        {
            error = "Error onPreExecute: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            Context context = contextWeakReference.get();

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(new String(responseBody));
                        setFields((JSONArray) jsonObject.get("records"));
                    }
                    catch (Exception err)
                    {
                        error = "Error asyncHttpResponseHandler onSuccess: \n" + err.toString();
                        cancel(true);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (statusCode == 0)
                        {
                            DownloadOvertimeRequestsAsyncTask.this.errorCode = 3;
                        }
                        else
                        {
                            DownloadOvertimeRequestsAsyncTask.this.errorCode = 4;
                            DownloadOvertimeRequestsAsyncTask.this.error = new String(responseBody);
                        }
                    }
                    catch (Exception err)
                    {
                        DownloadOvertimeRequestsAsyncTask.this.error = "Error asyncHttpResponseHandler onFailure: \n" + err.toString();
                    }

                    cancel(true);
                }
            };

            HttpProvider.postSync(context, "overtime_requests/read/", new StringEntity(jsonObject.toString()), true, asyncHttpResponseHandler);
        }
        catch (Exception err)
        {
            error = "Error doInBackground: \n" + err.toString();
            cancel(true);
        }

        return null;
    }

    private void setFields(JSONArray overtimeJSONArray)
    {
        try
        {
            for (int i = 0; i < overtimeJSONArray.length(); i++)
            {
                JSONObject jsonObject = overtimeJSONArray.getJSONObject(i);
                Overtime overtime = new Overtime();

                overtime.setId(jsonObject.getInt("id"));
                overtime.setEmployeeId(jsonObject.getJSONObject("employee").getInt("id"));
                overtime.setEmployeeName(jsonObject.getJSONObject("employee").getString("fullname"));
                overtime.setHours(jsonObject.getDouble("hours"));
                overtime.setReason(jsonObject.getString("reason"));
                overtime.setRemarks(jsonObject.has("remarks") ? jsonObject.getString("remarks") : "");
                overtime.setDate(DateStringToDate.dbFormat(jsonObject.getString("date")));
                overtime.setRequestedDate(DateStringToDate.dateTimeDbFormat(jsonObject.getString("requested_date")));

                overtimeArrayList.add(overtime);
            }
        }
        catch (Exception err)
        {
            error = "Error setting fields: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected void onCancelled()
    {
        asyncResponseListener.onFailure(errorCode, error);
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        asyncResponseListener.onSuccess(overtimeArrayList);
    }
}
