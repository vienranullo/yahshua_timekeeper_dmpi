package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.os.Bundle;

import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveRequest;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

public class LeaveRequestApprovalActivity extends BaseActivity
{
    private LeaveRequest leaveRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request_approval);
        setTitle("Leave Request");
        leaveRequest = getIntent().getParcelableExtra("LEAVE_REQUEST");
        Debugger.printO(leaveRequest);
    }
}
