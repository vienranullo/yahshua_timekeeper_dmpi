package com.yahshua.yahshuatimekeeperdmpi.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateToString
{
    private static SimpleDateFormat initializeFormat(String dateFormat)
    {
        return new SimpleDateFormat(dateFormat, Locale.US);
    }

    public static String dbFormat(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("yyyy-M-d");
        return dateFormat.format(date);
    }

    public static String dbQuery(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public static String displayFormat(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("MMM. dd, yyyy");
        return dateFormat.format(date);
    }

    public static String customDisplay(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("MMMM dd, yyyy");
        return dateFormat.format(date);
    }

    public static String displayFormat2(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("MMM. dd");
        return dateFormat.format(date);
    }

    public static String formFormat(Date date)
    {
        SimpleDateFormat dateFormat = initializeFormat("MM/dd/yy");
        return dateFormat.format(date);
    }

    // Convert Date Instance to 24 Hour Format
    // Returns a String
    public static String timeDbFormat(Date date)
    {
        SimpleDateFormat timeFormat = initializeFormat("H:m");
        return timeFormat.format(date);
    }

    public static String timeDisplayFormat(Date date)
    {
        SimpleDateFormat timeFormat = initializeFormat("h:mm a");
        return timeFormat.format(date);
    }

    public static String dateTimeFormat(Date dateTime)
    {
        SimpleDateFormat dateTimeFormat = initializeFormat("yyyy-M-d HH:mm:ss");
        return dateTimeFormat.format(dateTime);
    }

    public static String dateTimeFormat2(Date dateTime)
    {
        SimpleDateFormat dateTimeFormat = initializeFormat("MMM. dd, yyyy hh:mm a");
        return dateTimeFormat.format(dateTime);
    }
}
