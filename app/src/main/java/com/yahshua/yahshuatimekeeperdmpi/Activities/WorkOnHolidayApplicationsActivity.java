package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.WorkOnHolidayAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DialogFragments.WorkOnHolidayAddEditDialog;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import java.util.Arrays;

import es.dmoral.toasty.Toasty;


public class WorkOnHolidayApplicationsActivity extends BaseActivity
{
    // Context
    private Context context = this;

    // Widget
    private ListView   lvWorkOnHoliday;
    private MenuItem   menuSync;
    // Adapter
    private WorkOnHolidayAdapter workOnHolidayAdapter;

    // ArrayList and Data
    private ArrayList<WorkOnHoliday> workOnHolidayArrayList = new ArrayList<>();
    private WorkOnHoliday selectedWorkOnHoliday;
    private String employeeName;
    private Employee employee;

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workonholiday_applications_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        employee = getIntent().getParcelableExtra("EMPLOYEE");

        String middleInitial = (employee.getMiddlename() != null &&  employee.getMiddlename().length() > 0) ? employee.getMiddlename().charAt(0) + ". " : "";
        employeeName = employee.getFirstname() + " " + middleInitial + employee.getLastname();
    }

    private void initializeData()
    {
        selectedWorkOnHoliday = new WorkOnHoliday();

        String searchQuery = "WHERE employeeId = " + employee.getId() +" ORDER BY id DESC ";
        workOnHolidayArrayList = WorkOnHoliday.load(context, searchQuery);
        showSyncIcon();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_sync, menu);

        menuSync = menu.findItem(R.id.action_sync);

        initializeData();
        initializeUI();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch(id)
        {
            case R.id.action_sync:
                uploadHoliday();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSyncIcon()
    {
        try
        {
            String searchQueryHoliday = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<WorkOnHoliday> workOnHolidayArrayList = WorkOnHoliday.load(context, searchQueryHoliday);

            menuSync.setVisible(workOnHolidayArrayList.size() > 0);

        }catch (Exception err)
        {
            Debugger.logD("Exception showSyncIcon err "+err);
        }
    }

    private void uploadHoliday()
    {
        if (Utility.haveNetworkConnection(context))
        {
            String searchQueryHoliday = "WHERE isSync = 0 and company_id = " + UserSession.getCompanyId(context);
            final ArrayList<WorkOnHoliday> workOnHolidayArrayList = WorkOnHoliday.load(context, searchQueryHoliday);

            if(workOnHolidayArrayList.size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                new WorkOnHolidayApplicationsActivity.UploadHoliday(context).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }
    // Initialize component from a layout file
    private void initializeUI()
    {
        // Set Title on Action Bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle("Work on Holiday Applications");
        actionBar.setSubtitle(employeeName);

        // List View
        lvWorkOnHoliday = findViewById(R.id.lvWorkOnHoliday);
        workOnHolidayAdapter = new WorkOnHolidayAdapter(this, workOnHolidayArrayList);

        lvWorkOnHoliday.setAdapter(workOnHolidayAdapter);

        lvWorkOnHoliday.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
            {
                selectedWorkOnHoliday = workOnHolidayAdapter.getItem(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Options");

                final String[] finalOptions;
                String[] optionUnSynced;
                String[] optionSynced;

                optionUnSynced = getResources().getStringArray(R.array.lv_option3);
                optionSynced = Arrays.copyOf(optionUnSynced, optionUnSynced.length - 1);

                finalOptions = selectedWorkOnHoliday.isSync() ? optionSynced : optionUnSynced;

                builder.setItems(finalOptions, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        switch (finalOptions[i])
                        {
                            case "View/Edit":
                                showWorkOnHolidayAddEditDialog();
                                break;
                            case "Upload":
                                uploadHoliday();
                                break;
                            case "Delete":
                                confirmDelete();
                                break;
                        }
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        selectedWorkOnHoliday = new WorkOnHoliday();
                    }
                });

                builder.show();
            }
        });

        // Floating Action Button
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWorkOnHolidayAddEditDialog();
            }
        });
    }

    // Work on holiday add edit dialog
    private void showWorkOnHolidayAddEditDialog()
    {
        WorkOnHolidayAddEditDialog workOnHolidayAddEditDialog = new WorkOnHolidayAddEditDialog();
        Bundle dialogArgs = new Bundle();

        dialogArgs.putInt("EMPLOYEE_ID", employee.getId());
        dialogArgs.putString("EMPLOYEE_NAME", employeeName);

        if (selectedWorkOnHoliday.getId() > 0) dialogArgs.putParcelable("SELECTED_WORKONHOLIDAY", selectedWorkOnHoliday);

        workOnHolidayAddEditDialog.setArguments(dialogArgs);

        workOnHolidayAddEditDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                initializeData();
                updateLeaveAdapter();
            }
        });

        workOnHolidayAddEditDialog.show(getSupportFragmentManager(), "Create/Edit Work on Holiday");
    }

    private void updateLeaveAdapter()
    {
        workOnHolidayAdapter.clear();
        workOnHolidayAdapter.addAll(workOnHolidayArrayList);
        workOnHolidayAdapter.notifyDataSetChanged();
    }

    private void confirmDelete()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                deleteRecord();
            }
        };

        String message = "Delete Work on Holiday Application on\n" + DateToString.displayFormat(selectedWorkOnHoliday.getDate()) + "?";

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Confirm Delete");
        builder.setMessage(message);
        builder.setPositiveButton(android.R.string.yes, dialogClickListener);
        builder.setNegativeButton(android.R.string.no, null);

        builder.show();
    }

    private void deleteRecord()
    {
        selectedWorkOnHoliday.delete(context);
        AuditLog.saveAuditLog(context,"WORK ON HOLIDAY","DELETE","SUCCESS",employeeName);
        initializeData();
        updateLeaveAdapter();
        Toasters.ShowToast(context, "Work on Holiday Application Deleted");
    }

    private class UploadHoliday extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadHoliday(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", true);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message += "success";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message +=  errorMessage;
                    }
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }
                });

                transactionSyncer.loadWorkOnHolidayRequests(employee.getId());

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, "Successfully uploaded").show();
            }
            else
            {
                Toasty.warning(context, "Failed to upload ").show();
            }

            Toasters.HideLoadingSpinner();
            initializeData();
            initializeUI();
        }
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End
}
