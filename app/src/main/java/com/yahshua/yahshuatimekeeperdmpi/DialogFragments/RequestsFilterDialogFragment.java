package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.yahshua.yahshuatimekeeperdmpi.Interfaces.CompleteListener;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;


public class RequestsFilterDialogFragment extends DialogFragment
{
    private Context context;
    private View view;
    private JSONObject filtersJsonObject = new JSONObject();
    private TextInputEditText tieSearchText;
    private String status;
    private DialogInterface.OnDismissListener onDismissListener;
    private CompleteListener completeListener;

    // Dates
    private EditText etRequestedDateFrom, etRequestedDateTo, etAppliedDateFrom, etAppliedDateTo;
    private DatePickerDialog.OnDateSetListener onDateSetListener, onDateSetListener2, onDateSetListener3, onDateSetListener4;
    private Calendar calendar = Calendar.getInstance();
    private Date requestedDateFrom, requestedDateTo, appliedDateFrom, appliedDateTo;


    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        this.context = context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        try
        {
            LayoutInflater inflater = requireActivity().getLayoutInflater();
            view = inflater.inflate(R.layout.requests_filter_dialog, null);
            initializeViews();

            builder.setView(view)
                    .setTitle("Filters")
                    .setPositiveButton("Search", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int id)
                        {
                            getFilters();
                        }
                    })
                    .setNegativeButton(R.string.cancel, null);
        }
        catch (Exception err)
        {
            Utility.showError(getFragmentManager(), "Error RequestsFilterDialogFragment onCreateDialog: " + err.toString());
        }

        return builder.create();
    }

    private void initializeViews() throws Exception
    {
        try
        {
            tieSearchText = view.findViewById(R.id.tieSearchText);
            initializeStatusSpinner();
            initializeDates();
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing views: " + err.toString());
        }
    }

    private void initializeStatusSpinner() throws Exception
    {
        try
        {
            Spinner spStatus = view.findViewById(R.id.spStatus);

            // Set items
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.request_status_array, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spStatus.setAdapter(adapter);

            // Select listener
            spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                {
                    status = adapterView.getItemAtPosition(i).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView)
                {
                    status = adapterView.getItemAtPosition(0).toString();
                }
            });
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing spinner: " + err.toString());
        }
    }

    private void initializeDates() throws Exception
    {
        try
        {
            // Requested date from
            etRequestedDateFrom = view.findViewById(R.id.etRequestedDateFrom);

            onDateSetListener = new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                {
                    calendar.set(year, month, dayOfMonth);
                    requestedDateFrom = calendar.getTime();
                    etRequestedDateFrom.setText(DateToString.formFormat(requestedDateFrom));
                }
            };

            etRequestedDateFrom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            // Requested date to
            etRequestedDateTo = view.findViewById(R.id.etRequestedDateTo);

            onDateSetListener2 = new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                {
                    calendar.set(year, month, dayOfMonth);
                    requestedDateTo = calendar.getTime();
                    etRequestedDateTo.setText(DateToString.formFormat(requestedDateTo));
                }
            };

            etRequestedDateTo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    new DatePickerDialog(context, onDateSetListener2, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            // Applied date from
            etAppliedDateFrom = view.findViewById(R.id.etAppliedDateFrom);

            onDateSetListener3 = new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                {
                    calendar.set(year, month, dayOfMonth);
                    appliedDateFrom = calendar.getTime();
                    etAppliedDateFrom.setText(DateToString.formFormat(appliedDateFrom));
                }
            };

            etAppliedDateFrom.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    new DatePickerDialog(context, onDateSetListener3, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            // Applied date to
            etAppliedDateTo = view.findViewById(R.id.etAppliedDateTo);

            onDateSetListener4 = new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                {
                    calendar.set(year, month, dayOfMonth);
                    appliedDateTo = calendar.getTime();
                    etAppliedDateTo.setText(DateToString.formFormat(appliedDateTo));
                }
            };

            etAppliedDateTo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    new DatePickerDialog(context, onDateSetListener4, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });
        }
        catch (Exception err)
        {
            throw new Exception("Error initializing dates: " + err.toString());
        }
    }

    private void getFilters()
    {
        try
        {
            if (!status.equals("All")) filtersJsonObject.put("status", status);

            if (!(TextUtils.isEmpty(tieSearchText.getText().toString())))
            {
                filtersJsonObject.put("search_text", tieSearchText.getText().toString());
            }

            if (requestedDateTo != null && requestedDateFrom != null)
            {
                filtersJsonObject.put("requested_date_from", DateToString.dbFormat(requestedDateFrom));
                filtersJsonObject.put("requested_date_to", DateToString.dbFormat(requestedDateTo));
            }
            else if (requestedDateFrom != null)
            {
                Toasty.error(context, "Application Date To is required: ").show();
            }
            else if (requestedDateTo != null)
            {
                Toasty.error(context, "Application Date From is required: ").show();
            }

            if (requestedDateTo != null && requestedDateFrom != null)
            {
                filtersJsonObject.put("applied_overtime_date_from", DateToString.dbFormat(appliedDateFrom));
                filtersJsonObject.put("applied_overtime_date_to", DateToString.dbFormat(appliedDateTo));
            }
            else if (requestedDateFrom != null)
            {
                Toasty.error(context, "Requested Date To is required: ").show();
            }
            else if (requestedDateTo != null)
            {
                Toasty.error(context, "Requested Date From is required: ").show();
            }

            Debugger.printO(filtersJsonObject);

            completeListener.onComplete(filtersJsonObject);
            dismiss();
        }
        catch (Exception err)
        {
            Utility.showError(getFragmentManager(), "Error getting filters: " + err.toString());
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener)
    {
        this.onDismissListener = onDismissListener;
    }

    public void setOnCompleteListener(CompleteListener completeListener)
    {
        this.completeListener = completeListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);
        if (onDismissListener != null) onDismissListener.onDismiss(dialog);
    }
}
