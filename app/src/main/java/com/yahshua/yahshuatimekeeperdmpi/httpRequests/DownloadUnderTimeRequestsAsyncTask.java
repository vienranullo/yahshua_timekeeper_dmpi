package com.yahshua.yahshuatimekeeperdmpi.httpRequests;

import android.content.Context;
import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.Undertime;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DownloadUnderTimeRequestsAsyncTask extends AsyncTask<Void, Void, Void>
{
    private WeakReference<Context> contextWeakReference;
    private AsyncResponseListener asyncResponseListener;
    private int errorCode = 1;
    private String error;
    private ArrayList<Undertime> underTimeArrayList = new ArrayList<>();
    private JSONObject jsonObject;


    public DownloadUnderTimeRequestsAsyncTask(Context context, AsyncResponseListener asyncResponseListener, JSONObject jsonObject)
    {
        this.asyncResponseListener = asyncResponseListener;
        this.contextWeakReference = new WeakReference<>(context);
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute()
    {
        try
        {
            // Check internet connection
            Context context = contextWeakReference.get();

            if (!Utility.haveNetworkConnection(context))
            {
                errorCode = 2;
                cancel(true);
            }
        }
        catch (Exception err)
        {
            error = "Error onPreExecute: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            Context context = contextWeakReference.get();

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(new String(responseBody));
                        setFields((JSONArray) jsonObject.get("records"));
                    }
                    catch (Exception err)
                    {
                        error = "Error asyncHttpResponseHandler onSuccess: \n" + err.toString();
                        cancel(true);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (statusCode == 0)
                        {
                            DownloadUnderTimeRequestsAsyncTask.this.errorCode = 3;
                        }
                        else
                        {
                            DownloadUnderTimeRequestsAsyncTask.this.errorCode = 4;
                            DownloadUnderTimeRequestsAsyncTask.this.error = new String(responseBody);
                        }
                    }
                    catch (Exception err)
                    {
                        DownloadUnderTimeRequestsAsyncTask.this.error = "Error asyncHttpResponseHandler onFailure: \n" + err.toString();
                    }

                    cancel(true);
                }
            };

            HttpProvider.postSync(context, "undertime_requests/read/", new StringEntity(jsonObject.toString()), true, asyncHttpResponseHandler);
        }
        catch (Exception err)
        {
            error = "Error doInBackground: \n" + err.toString();
            cancel(true);
        }

        return null;
    }

    private void setFields(JSONArray undertimeJSONArray)
    {
        try
        {
            for (int i = 0; i < undertimeJSONArray.length(); i++)
            {
                JSONObject jsonObject = undertimeJSONArray.getJSONObject(i);
                Undertime underTime = new Undertime();

                underTime.setId(jsonObject.getInt("id"));
                underTime.setEmployeeId(jsonObject.getJSONObject("employee").getInt("id"));
                underTime.setEmployeeName(jsonObject.getJSONObject("employee").getString("fullname"));
                underTime.setHours(jsonObject.getDouble("hours"));
                underTime.setReason(jsonObject.getString("reason"));
                underTime.setRemarks(jsonObject.has("remarks") ? jsonObject.getString("remarks") : "");
                underTime.setDate(DateStringToDate.dbFormat(jsonObject.getString("date")));
                underTime.setRequestedDate(DateStringToDate.dateTimeDbFormat(jsonObject.getString("requested_date")));

                underTimeArrayList.add(underTime);
            }
        }
        catch (Exception err)
        {
            error = "Error setting fields: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected void onCancelled()
    {
        asyncResponseListener.onFailure(errorCode, error);
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        asyncResponseListener.onSuccess(underTimeArrayList);
    }
}
