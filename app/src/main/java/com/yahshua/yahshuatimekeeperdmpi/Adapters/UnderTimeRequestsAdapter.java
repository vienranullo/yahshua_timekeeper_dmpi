package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Activities.UnderTimeApprovalRequestActivity;
import com.yahshua.yahshuatimekeeperdmpi.Models.Undertime;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;

import java.util.ArrayList;


public class UnderTimeRequestsAdapter extends RecyclerView.Adapter<UnderTimeRequestsAdapter.ViewHolder>
{
    private ArrayList<Undertime> underTimeArrayList;
    private Context context;


    public UnderTimeRequestsAdapter(ArrayList<Undertime> underTimeArrayList, Context context)
    {
        this.underTimeArrayList = underTimeArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public UnderTimeRequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.under_time_request_item, parent, false);
        return new ViewHolder(contactView);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvEmployeeName, tvDate, tvHours, tvReason;
        CardView cvUnderTimeRequest;

        ViewHolder(View itemView)
        {
            super(itemView);

            tvEmployeeName = itemView.findViewById(R.id.tvEmployeeName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvHours = itemView.findViewById(R.id.tvHours);
            tvReason = itemView.findViewById(R.id.tvReason);
            cvUnderTimeRequest = itemView.findViewById(R.id.cvUnderTimeRequest);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {
        Undertime undertime = underTimeArrayList.get(position);

        holder.tvEmployeeName.setText(undertime.getEmployeeName());
        holder.tvDate.setText(DateToString.displayFormat(undertime.getDate()));
        holder.tvHours.setText(undertime.getHours() + " hrs.");
        holder.tvReason.setText(undertime.getReason());

        holder.cvUnderTimeRequest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent coursesActivityIntent = new Intent(context, UnderTimeApprovalRequestActivity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelable("UNDER_TIME", underTimeArrayList.get(position));
                coursesActivityIntent.putExtras(bundle);

                context.startActivity(coursesActivityIntent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return underTimeArrayList.size();
    }
}
