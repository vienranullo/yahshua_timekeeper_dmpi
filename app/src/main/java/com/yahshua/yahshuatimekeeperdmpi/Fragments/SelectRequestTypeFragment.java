package com.yahshua.yahshuatimekeeperdmpi.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yahshua.yahshuatimekeeperdmpi.Activities.LeaveRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.OvertimeRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.UnderTimeRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnHolidayRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnRestDayRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;


public class SelectRequestTypeFragment extends Fragment
{
    private View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        try
        {
            view = inflater.inflate(R.layout.select_request_type_fragment, container, false);
            initializeViews();
        }
        catch (Exception err)
        {
            Utility.showError(getFragmentManager(), "Error SelectRequestTypeFragment onCreateView: \n" + err.toString());
        }

        return view;
    }

    private void initializeViews()
    {
        try
        {
            // Leave
            View llLeave = view.findViewById(R.id.llLeave);
            llLeave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), LeaveRequestsActivity.class);
                    startActivity(intent);
                }
            });

            // Overtime
            View llOvertime = view.findViewById(R.id.llOvertime);
            llOvertime.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(getContext(), OvertimeRequestsActivity.class);
                    startActivity(intent);
                }
            });

            // Under time
            View llUnderTime = view.findViewById(R.id.llUnderTime);
            llUnderTime.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(getContext(), UnderTimeRequestsActivity.class);
                    startActivity(intent);
                }
            });

            // Work on rest day
            View llWorkOnRestDay = view.findViewById(R.id.llWorkOnRestDay);
            llWorkOnRestDay.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(getContext(), WorkOnRestDayRequestsActivity.class);
                    startActivity(intent);
                }
            });

            // Work on rest day
            View llWorkOnHoliday = view.findViewById(R.id.llWorkOnHoliday);
            llWorkOnHoliday.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(getContext(), WorkOnHolidayRequestsActivity.class);
                    startActivity(intent);
                }
            });
        }
        catch (Exception err)
        {
            Utility.showError(getFragmentManager(), "Error initializing views: \n" + err.toString());
        }
    }
}