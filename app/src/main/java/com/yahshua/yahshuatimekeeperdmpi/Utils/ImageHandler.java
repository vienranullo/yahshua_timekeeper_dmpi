package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.view.SurfaceView;

import java.io.ByteArrayOutputStream;
import java.io.File;


public class ImageHandler {

    public static byte[] formatImage(byte[] imageData, SurfaceView imagePreview)
    {
        try
        {
            Matrix mat = new Matrix();
            mat.postRotate(270);

            Bitmap bmp = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);

            int width = bmp.getWidth();
            int height = bmp.getHeight();

            float scaleWidth = ((float) 250 ) / width;
            float scaleHeight = ((float) 188) / height;

            mat.postScale(scaleWidth, scaleHeight);

            Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, width, height, mat, false);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 95, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        } catch (Exception err)
        {
            return null;
        }
    }

    public static byte[] getImageByPath(String path) throws Exception
    {
        try
        {
            Bitmap bm = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            return baos.toByteArray();
        }
        catch (Exception err)
        {
            throw new Exception("Error getting image: \n" + err.toString());
        }
    }

    public static byte[] convertFileAsBase64()
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return byteArray;
    }

    public static Bitmap getScaledBitmap(String imageString, int newSize) {

        byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inInputShareable = true;
        options.inPurgeable = true;

        BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        if ((options.outWidth == -1) || (options.outHeight == -1))
            return null;

        int originalSize = (options.outHeight > options.outWidth) ? options.outHeight
                : options.outWidth;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / newSize;

        Bitmap scaledBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, opts);
        decodedString = null;

        return scaledBitmap;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String res, int reqWidth, int reqHeight) {

        byte[] decodedString = Base64.decode(res, Base64.DEFAULT);

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize)
    {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static boolean fileExists(Context context, String fileName)
    {
        ContextWrapper contextWrapper = new ContextWrapper(context);

        File myImagesDirectory = contextWrapper.getDir("my_images", 0);
        File subDirectory = new File(myImagesDirectory, "3sImage");

        if (!subDirectory.exists()) return false;

        File file = new File(subDirectory, fileName);
        return file.exists();
    }
}
