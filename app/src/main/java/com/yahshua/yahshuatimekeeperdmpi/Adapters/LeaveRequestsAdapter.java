package com.yahshua.yahshuatimekeeperdmpi.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveRequest;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;

import java.util.ArrayList;

public class LeaveRequestsAdapter extends RecyclerView.Adapter<LeaveRequestsAdapter.ViewHolder> {

    private ArrayList<LeaveRequest> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Fragment fragment;
    private Context context;

    public LeaveRequestsAdapter(Context context,  ArrayList<LeaveRequest> data)
    {
        this.mInflater  = LayoutInflater.from(context);
        this.mData      = data;
        this.context    = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = mInflater.inflate(R.layout.listrow_leave_request, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        final LeaveRequest leaveRequest;

        leaveRequest = mData.get(position);

        holder.tvEmployeeName.setText(leaveRequest.getEmployeeName());
        holder.tvReason.setText(leaveRequest.getReason());
        holder.tvRequestedDate.setText(DateTimeHandler.convertDatetoStringDate(leaveRequest.getRequestedDate()));
        holder.tvLeaveType.setText(leaveRequest.getLeaveType());
        holder.tvDaysLeave.setText("Days Requested: " + leaveRequest.getDaysleave() );
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<LeaveRequest> getData() {
        return mData;
    }

    public void clearData() { this.mData = new ArrayList<>(); }

    public void addData(ArrayList<LeaveRequest> leaveRequests)
    {
        this.mData = leaveRequests;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener
    {
        CardView cardView;
        TextView tvRequestedDate;
        TextView tvDaysLeave;
        TextView paperClip;
        TextView tvEmployeeName;
        TextView tvInvoiceDate;
        TextView tvLeaveType;
        ImageView ivPaid, ivSync, ivWarning;
        TextView tvReason;
        View layoutSyncErrorMessageContainer;

        ViewHolder(View itemView) {
            super(itemView);
            cardView        = itemView.findViewById(R.id.cv_invoice);
            tvRequestedDate = itemView.findViewById(R.id.tvRequestedDate);
            tvDaysLeave     = itemView.findViewById(R.id.tvDaysLeave);
            paperClip       = itemView.findViewById(R.id.tv_paperClip);
            tvEmployeeName  = itemView.findViewById(R.id.tv_customer);
            tvInvoiceDate   = itemView.findViewById(R.id.tv_invoice_date);
            tvLeaveType     = itemView.findViewById(R.id.tvLeaveType);

            ivPaid = itemView.findViewById(R.id.iv_InvoicePaid);
            ivSync = itemView.findViewById(R.id.iv_InvoiceSync);
            ivWarning = itemView.findViewById(R.id.iv_InvoiceWarning);

            tvReason = itemView.findViewById(R.id.tvReason);
            layoutSyncErrorMessageContainer = itemView.findViewById(R.id.layoutSyncErrorMessageContainer);

            cardView.setOnClickListener(this);
            cardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }

    public LeaveRequest getItem(int id) {
        return mData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
