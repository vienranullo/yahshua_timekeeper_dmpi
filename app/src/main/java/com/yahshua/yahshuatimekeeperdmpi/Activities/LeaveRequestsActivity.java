package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Adapters.LeaveRequestsAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.LeaveRequest;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class LeaveRequestsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, LeaveRequestsAdapter.ItemClickListener {

    // Widgets
    private Button btnFilter;
    private MenuItem menuSync;
    private SearchView searchView;
    private RecyclerView rcInvoice;
    private SwipeRefreshLayout swipeLayout;

    private Context context;
    private LeaveRequestsAdapter leaveRequestsAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private LeaveRequest selectedLeaveRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_requests);
        setTitle("Leave Requests");
        context = this;

        initializeView();
        readRecords();
    }

    private void initializeView()
    {

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(android.R.color.holo_red_dark),
                getResources().getColor(android.R.color.holo_blue_dark),
                getResources().getColor(android.R.color.holo_orange_dark));

        searchView = findViewById(R.id.etSearchView);
        btnFilter = findViewById(R.id.btnFilter);
        rcInvoice = findViewById(R.id.rc_invoice);

        layoutManager = new LinearLayoutManager(this);
        rcInvoice.setLayoutManager(layoutManager);

        leaveRequestsAdapter = new LeaveRequestsAdapter(context, new ArrayList<LeaveRequest>(){});
        leaveRequestsAdapter.setClickListener(this);
        rcInvoice.setAdapter(leaveRequestsAdapter);
    }

    private void readRecords()
    {
        try
        {

            HttpProvider.post(this, "leave_requests/read/", null, null, new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    try
                    {
                        super.onSuccess(statusCode, headers, response);

                        ArrayList<LeaveRequest> leaveRequestArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<ArrayList<LeaveRequest>>() {
                        }.getType());

                        leaveRequestsAdapter.clearData();
                        leaveRequestsAdapter.addData(leaveRequestArrayList);
                        leaveRequestsAdapter.notifyDataSetChanged();

                        swipeLayout.setRefreshing(false);

                    } catch (Exception err)
                    {
                        Debugger.logD(err.toString());

                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }
            });

        } catch (Exception err)
        {

        }
    }


    @Override
    public void onRefresh()
    {
        readRecords();
    }

    @Override
    public void onItemClick(View view, int position)
    {
        selectedLeaveRequest = leaveRequestsAdapter.getItem(position);
        Intent intent = new Intent(context, LeaveRequestApprovalActivity.class);
        intent.putExtra("LEAVE_REQUEST", selectedLeaveRequest);
        startActivity(intent);
    }
}
