package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;


public class DeviceInformation
{
    private String deviceType;
    private String deviceId;
    private String model;
    private String brand;
    private String hostName;

    public String getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    public String getDeviceId()
    {
        return deviceId;
    }

    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getDeviceHostName()
    {
        try
        {
            BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
            String deviceName = myDevice.getName();
            return deviceName;
        } catch (Exception err)
        {
            return "Unable to get device name, check bluetooth permission of the device";
        }
    }

    public DeviceInformation(Context context)
    {
        this.deviceType = "ANDROID";
        this.deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        this.model = Build.MODEL;
        this.brand = Build.MANUFACTURER;
    }
}