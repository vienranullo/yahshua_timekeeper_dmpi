package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Activities.EmployeeDeductionRequestsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Adapters.DeductionTypeAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.DeductionType;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class DeductionTypeDialog extends DialogFragment
{
    //Views
    private Context context;
    private View view;

    //Widgets
    private ListView listView;
    private LinearLayout layoutButton, layoutProgress, layoutInclude;
    private ProgressBar progressBar;
    private Button btnConfirm;
    private ImageView imgSync, imgClose;
    private TextView tvErrorMessage;

    //Data
    private DeductionType selectedType;
    private ArrayList<DeductionType> deductionTypeArrayList = new ArrayList<>();
    private boolean isSelect;

    //Adapter
    private DeductionTypeAdapter deductionTypeAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view    = inflater.inflate(R.layout.dialog_deduction_type, container, false);
        context = view.getContext();

        initializeUI();

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    private void initializeUI()
    {
        btnConfirm      = view.findViewById(R.id.btn_confirm);
        listView        = view.findViewById(R.id.lv_deduction_type);
        layoutButton    = view.findViewById(R.id.layoutButton);
        layoutProgress  = view.findViewById(R.id.layout_progress);
        progressBar     = view.findViewById(R.id.pb_deduction_type);
        layoutInclude   = view.findViewById(R.id.layoutInclude);
        imgSync         = view.findViewById(R.id.imgSync);
        tvErrorMessage  = view.findViewById(R.id.tvErrorMessage);
        imgClose        = view.findViewById(R.id.imgClose);

        tvErrorMessage.setVisibility(View.GONE);
        imgSync.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                downloadDeductionType();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                isSelect = false;
                dismiss();
            }
        });
        deductionTypeAdapter = new DeductionTypeAdapter(context, deductionTypeArrayList);
        listView.setAdapter(deductionTypeAdapter);

        btnConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                isSelect = true;
                confirmSelected();
            }
        });

        requestDeductionType();
    }

    private void readRecords(ArrayList<DeductionType> deductionTypes)
    {
        deductionTypeAdapter.clear();
        deductionTypeAdapter.addAll(deductionTypes);
        deductionTypeAdapter.notifyDataSetChanged();
    }

    private boolean confirmSelected()
    {
        int count = 0;

        for(DeductionType deductionType : deductionTypeArrayList)
        {
            if(deductionType.isSelected())
            {
                selectedType = deductionType;
                count++;
            }
        }

        if(count > 1)
        {
            Toasty.warning(context, "Select Only One").show();
            return  false;
        }
        dismiss();
        return true;
    }

    private void showProgress(boolean isShow)
    {
        layoutProgress.setVisibility(isShow ? View.VISIBLE : View.GONE);
        layoutButton.setVisibility(isShow ? View.GONE : View.VISIBLE);
        layoutInclude.setVisibility(isShow ? View.GONE : View.VISIBLE);
    }

    private void requestDeductionType()
    {
        readRecords(DeductionType.read(context));
        showProgress(false);
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);

        if(isSelect)
        {
            ((EmployeeDeductionRequestsActivity) getActivity()).onComplete(selectedType);
        }
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    private void downloadDeductionType()
    {
        try
        {
            HttpProvider.get(context, "other_deduction_types/read/", null, new AsyncHttpResponseHandler(){

                @Override
                public void onStart()
                {
                    super.onStart();
                    showProgress(true);
                    catchError("");
                }

                @Override
                public void onFinish()
                {
                    super.onFinish();
                    showProgress(false);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        JSONObject response = new JSONObject(new String(responseBody));

                        DeductionType.delete(context);
                        ArrayList<DeductionType> deductionTypeArrayList = new Gson().fromJson(response.getJSONArray("records").toString(), new TypeToken<List<DeductionType>>(){}.getType());

                        for(DeductionType deductionType : deductionTypeArrayList)
                        {
                            deductionType.save(context);
                        }

                        readRecords(DeductionType.read(context));

                    } catch (Exception err)
                    {
                        catchError(err.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        String responseString = new String(responseBody);

                        catchError(statusCode == 404 ? getString(R.string.return_404) : responseString);
                    }
                    catch (Exception e)
                    {
                      catchError(e.getMessage());
                    }
                }
            });

        } catch (Exception err)
        {
            catchError(err.toString());
        }
    }

    private void catchError(String err)
    {
        tvErrorMessage.setText(err);
        tvErrorMessage.setVisibility(err.equals("") ? View.GONE : View.VISIBLE);
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
