package com.yahshua.yahshuatimekeeperdmpi.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserPreferences {

    public static boolean getAutoInstantSyncPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("instant_syncing", true);
    }

    public static boolean setAutoInstantSyncPreferences(Context context, boolean value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("instant_syncing", value);
        return editor.commit();
    }

    public static boolean getDeleteAfterSyncingPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("delete_syncing", false);
    }

    public static boolean getWarningExisting(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("delete_syncing", false);
    }

    public static boolean setDeleteAfterSyncingPreferences(Context context, boolean value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("delete_syncing", value);
        return editor.commit();
    }

    public static boolean setWarningExisting(Context context, boolean value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("warn_existing", value);
        return editor.commit();
    }

    public static boolean setDateSyncAll(Context context, String value)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("date_now", value);
        return editor.commit();
    }

    public static String getDateSyncAll(Context context)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("date_now", "");
    }
}