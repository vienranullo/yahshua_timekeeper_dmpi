package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.DeductionAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeDeduction;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;
import es.dmoral.toasty.Toasty;

public class EmployeeDeductionCreateActivity extends BaseActivity implements DeductionAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener
{
    // Context
    private Context context = this;

    //Data
    private Employee employee;
    private String employeeName;
    private View noInternetIndicator;
    private EmployeeDeduction selectedEmployeeDeduction;


    //Widgets
    private Menu menu;
    private RecyclerView         recyclerView;
    private FloatingActionButton fab;
    private SwipeRefreshLayout   swipeLayout;
    private TextView             tvItemCount;
    private Button               btnWIfi, btnCellular;

    //Adapter
    private DeductionAdapter deductionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deduction_activity);

        employee = getIntent().getParcelableExtra("EMPLOYEE");

        String middleInitial = (employee.getMiddlename() != null &&  employee.getMiddlename().length() > 0) ? employee.getMiddlename().charAt(0) + ". " : "";
        employeeName = employee.getFirstname() + " " + middleInitial + employee.getLastname();

        setTitle(employeeName);

        initializeUI();
    }

    private void initializeUI()
    {
        btnWIfi         = findViewById(R.id.btnWifiSettings);
        btnCellular     = findViewById(R.id.btnCellularSettings);
        fab             = findViewById(R.id.fab_deduction);
        recyclerView    = findViewById(R.id.rc_deduction);
        swipeLayout     = findViewById(R.id.swipe_container);
        tvItemCount     = findViewById(R.id.tvItemCount);
        noInternetIndicator = findViewById(R.id.viewNoInternet);

        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openDeductionForm(false);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0)
                {
                    fab.setVisibility(View.GONE);
                }
                else
                {
                    fab.setVisibility(View.VISIBLE);
                }
            }
        });


        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                                         getResources().getColor(R.color.colorAccent),
                                         getResources().getColor(android.R.color.holo_red_dark),
                                         getResources().getColor(android.R.color.holo_blue_dark),
                                         getResources().getColor(android.R.color.holo_orange_dark));
        swipeLayout.setProgressViewOffset(false,
                                          getResources().getDimensionPixelSize(R.dimen.refresher_offset),
                                          getResources().getDimensionPixelSize(R.dimen.refresher_offset_end));

        btnWIfi.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        btnCellular.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    private void readRecords(ArrayList<EmployeeDeduction> employeeDeductionArrayList)
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        deductionAdapter = new DeductionAdapter(context, employeeDeductionArrayList);
        deductionAdapter.setClickListener(this);
        recyclerView.setAdapter(deductionAdapter);
        recyclerView.setVisibility(View.VISIBLE);
        noInternetIndicator.setVisibility(View.GONE);
        tvItemCount.setText("List "+ employeeDeductionArrayList.size());

        if(employeeDeductionArrayList.size() > 5)
        {
            tvItemCount.setVisibility(View.VISIBLE);
        }
        else
        {
            tvItemCount.setVisibility(View.GONE);
        }

    }

    private void openDeductionForm(boolean isEdit)
    {
        Intent intent = new Intent();
        intent.setClass(context, EmployeeDeductionRequestsActivity.class);
        intent.putExtra("EMPLOYEE", employee);
        intent.putExtra("DEDUCTION", selectedEmployeeDeduction);
        intent.putExtra("IS_EDIT", isEdit);
        startActivity(intent);
    }

    @Override
    public void onItemClick(View view, int position)
    {
        selectedEmployeeDeduction = deductionAdapter.getItem(position);

        final String[] finalOptions;
        String[] optionUnSynced;
        String[] optionSynced;

        optionUnSynced = getResources().getStringArray(R.array.lv_option3);
        optionSynced = getResources().getStringArray(R.array.lv_option4);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");

        finalOptions = selectedEmployeeDeduction.isSynced() ? optionSynced : optionUnSynced;

        builder.setItems(finalOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (finalOptions[i])
                {
                    case "View":
                        openDeductionForm(true);
                        break;
                    case "View/Edit":
                         openDeductionForm(true);
                        break;
                    case "Upload":
                        uploadDeduction();
                        break;
                    case "Delete":
                        deleteCashAdvance();
                        break;
                }
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                selectedEmployeeDeduction = new EmployeeDeduction();
            }
        });

        builder.show();
    }

    private void deleteCashAdvance()
    {
        selectedEmployeeDeduction.delete(context);
        Toasty.success(context, "Successfully Deleted").show();
        onRefresh();
    }

    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        onRefresh();
    }

    @Override
    public void onRefresh()
    {
        readRecords(EmployeeDeduction.read(context, employee.getId(), false, null));
        swipeLayout.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.application_menu, menu);
        this.menu = menu;

        final MenuItem searchViewItem = menu.findItem(R.id.action_search);

        final SearchView searchViewAndroidActionBar = (SearchView) searchViewItem.getActionView();
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                readRecords(EmployeeDeduction.read(context, employee.getId(), false, query));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                readRecords(EmployeeDeduction.read(context, employee.getId(), false, newText));
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch(id)
        {
            case R.id.action_sync:
                uploadDeduction();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void uploadDeduction()
    {
        if(Utility.haveNetworkConnection(context))
        {
            final ArrayList<EmployeeDeduction> employeeDeductionArrayList = EmployeeDeduction.read(context, employee.getId(), true, null);

            if(employeeDeductionArrayList.size() == 0)
            {
                Toasty.warning(context, getResources().getString(R.string.no_data_to_upload)).show();
            }
            else
            {
                new EmployeeDeductionCreateActivity.UploadCashAdvanceApplication(context).execute();
            }
        }
        else
        {
            Toasty.warning(context, getResources().getString(R.string.internet_required)).show();
        }
    }

    //Hide item in Action bar
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

//        menuAdd = menu.findItem(R.id.action_add);
//        menuSync = menu.findItem(R.id.action_sync);
//        showSyncIcon();
        return  super.onPrepareOptionsMenu(menu);
    }

    private class UploadCashAdvanceApplication extends AsyncTask<String, Integer, String>
    {
        private Context context;
        private String message = "";

        public UploadCashAdvanceApplication(Context context)
        {
            this.context = context;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toasters.ShowProgressSpinner(context, "Uploading...", true);
        }

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer transactionSyncer = new TransactionSyncer(context);
                transactionSyncer.setOnRequestListener(new HttpRequestListener()
                {
                    @Override
                    public void onSuccess(String fromWhere, final String successMessage)
                    {
                        message += " success ";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        message +=  errorMessage;
                    }
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {


                    }
                });

                transactionSyncer.uploadEmployeeDeduction(employee.getId());

            }catch (Exception err)
            {
                message += " failed " +err.getMessage();
                return message;
            }

            return message;
        }

        @Override
        protected void onPostExecute(String success)
        {
            if(message.contains("success"))
            {
                Toasty.success(context, "Successfully uploaded").show();
            }
            else
            {
                Toasty.warning(context, "Failed to upload ").show();
            }

            Toasters.HideLoadingSpinner();
            onRefresh();
            Debugger.logD(message);
        }
    }
}
