package com.yahshua.yahshuatimekeeperdmpi.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.HttpRequestListener;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.TransactionSyncer;

public class SyncingService extends Service
{
    private Context context;

    @Override
    public void onCreate()
    {
        context = getApplicationContext();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        new SyncingService.UploadLogs().execute();
        return START_STICKY;
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadLogs extends AsyncTask<String, Integer, String>
    {
        private String success = "";

        @Override
        protected String doInBackground(String... strings)
        {
            try
            {
                TransactionSyncer uploadData = new TransactionSyncer(context);

                uploadData.setOnRequestListener(new HttpRequestListener()
                {

                    @Override
                    public void onSuccess(String fromWhere, String successMessage)
                    {
                        success = "";
                    }

                    @Override
                    public void onFailure(int errorCode, String errorMessage, String fromWhere)
                    {
                        success = errorMessage;
                    }

                    @Override
                    public void onStart(String fromWhere)
                    {

                    }

                    @Override
                    public void onFinish(String fromWhere)
                    {

                    }

                    @Override
                    public void onUpdate(int max, int progress, String currentDownload)
                    {

                    }

                    @Override
                    public void showSpinner(boolean isShow, String currentDownload)
                    {

                    }

                });

                uploadData.uploadEmployeeLogs();
            }
            catch (Exception err)
            {
                return success += "FAILED";
            }

            return success;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String message)
        {
            stopSelf();
            Debugger.logD("message "+message);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

