package com.yahshua.yahshuatimekeeperdmpi.Utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.annotations.SerializedName;
import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;


public class UserSession {

    @SerializedName("company_name")
    private String companyName;
    @SerializedName("token")
    private String authToken;
    @SerializedName("emp_id_digits")
    private int empIdDigits;
    private String email;
    @SerializedName("company_id")
    private int companyId;
    private static Context context;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static String getToken(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("AUTHTOKEN", "");
    }

    public static String getCompany(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_NAME", "No Company");
    }

    public static String getCompanyEmail(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COMPANY_EMAIL", "No Company Email");
    }

    public static int getEmployeeIdDigits(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("EMP_ID_DIGITS", 0);
    }

    public int getCompanyId() {
        return companyId;
    }

    public static int getCompanyId(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("COMPANY_ID", 0);
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public static boolean clearSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }

    public boolean saveUserSession(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("COMPANY_NAME", companyName);
        editor.putString("AUTHTOKEN", authToken);
        editor.putString("COMPANY_EMAIL", getEmail());
        editor.putInt("COMPANY_ID", getCompanyId());
        editor.putInt("EMP_ID_DIGITS", empIdDigits);
        return editor.commit();
    }

    // System Preference

    public static boolean isContinousMode(Context context)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getBoolean("CONTINOUS_MODE", false);
    }

    public static boolean setContinousMode(Context context1, boolean isChecked)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context1);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("CONTINOUS_MODE", isChecked);

        context = context1;
        AuditLog.saveAuditLog(context,"CONTINOUS_MODE","CONTINOUS_MODE","SUCCESS","NONE");
        return editor.commit();
    }
}
