package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class KioskSettingDialog extends DialogFragment
{

    //Context
    private Context context;
    private View    view;

    //Widgets
    private Button      btnLockUnlock;
    private TextView    etPassword, etAuthenticating;
    private ProgressBar progressBar;
    private ImageView   imgClose;

    //Data
    private int btnText;
    private boolean isLocked;
    private String getTextPassword = "";
    public static final String LOCKED_BUNDLE_KEY = "locked_bundle_key";
    public static final String KIOSK_MODE_PASSWORD = "kiosk";
    private boolean isSuccess = false;
    private IActionHandler actionHandler;
    private String errorMessage = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.setting_fragment, container, false);
    context = view.getContext();

    Bundle bundle = getArguments();
    isLocked = bundle.getBoolean(LOCKED_BUNDLE_KEY);
    initializeUI();

    return view;
}

    private void initializeUI()
    {

    imgClose = view.findViewById(R.id.img_close);
    btnLockUnlock = view.findViewById(R.id.btn_lock_unlock);
    etPassword = view.findViewById(R.id.enter_password);
    progressBar = view.findViewById(R.id.spinner_bar);
    etAuthenticating = view.findViewById(R.id.tv_authenticating);

    btnText   = isLocked ? R.string.setting_unlock_device : R.string.setting_lock_device;
    btnLockUnlock.setText(btnText);

    btnLockUnlock.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            getTextPassword = etPassword.getText().toString();

            if(!Utility.haveNetworkConnection(context))
            {
                etPassword.setError(getString(R.string.internet_required));
                etPassword.requestFocus();
            }
            else if(!getTextPassword.equals(""))
            {
                authenticateKioskMode(getTextPassword);
            }
            else
            {
                etPassword.setError(getString(R.string.error_field_required));
                etPassword.requestFocus();
            }
        }
    });

    imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

    }

    public void setActionHandler(IActionHandler actionHandler)
    {
    this.actionHandler = actionHandler;
    }

    public interface IActionHandler
    {
        void isLocked(boolean isLocked);
    }

    private void showSpinner(boolean show)
    {

    btnLockUnlock.setVisibility(show ? View.GONE : View.VISIBLE);
    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    etAuthenticating.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void hideKeyboard()
    {
        InputMethodManager imm = (InputMethodManager)etPassword.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void authenticateKioskMode(String password)
    {
        try
        {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("username", UserSession.getCompanyEmail(context));
            jsonParams.put("password", password);
            StringEntity entity = new StringEntity(jsonParams.toString());

            HttpProvider.post(context, "api-auth/", entity, "application/json", new AsyncHttpResponseHandler()
            {
                @Override
                public void onStart()
                {
                    super.onStart();
                    showSpinner(true);
                    hideKeyboard();
                }

                @Override
                public void onFinish()
                {
                    super.onFinish();
                    showSpinner(false);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    if (actionHandler != null)
                    {
                        actionHandler.isLocked(!isLocked);
                        dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        String response = new String(responseBody);
                        if(response.contains("Invalid"))
                        {
                            displayError(getString(R.string.error_incorrect_password));
                        }
                        else
                        {
                            displayError(response);
                        }

                    }catch (Exception err)
                    {
                        displayError(err.getMessage());
                    }
                }
            });

        }catch (Exception err)
        {
            Debugger.logD("Exception requestLogout "+err.getMessage());
        }
    }

    private void displayError(String err)
    {
        etPassword.setError(err);
        etPassword.requestFocus();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }
}
