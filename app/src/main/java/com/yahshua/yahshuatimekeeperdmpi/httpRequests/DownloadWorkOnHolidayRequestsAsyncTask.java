package com.yahshua.yahshuatimekeeperdmpi.httpRequests;

import android.content.Context;
import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yahshua.yahshuatimekeeperdmpi.Interfaces.AsyncResponseListener;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateStringToDate;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;
import com.yahshua.yahshuatimekeeperdmpi.Utils.HttpProvider;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DownloadWorkOnHolidayRequestsAsyncTask extends AsyncTask<Void, Void, Void>
{
    private WeakReference<Context> contextWeakReference;
    private AsyncResponseListener asyncResponseListener;
    private int errorCode = 1;
    private String error;
    private ArrayList<WorkOnHoliday> workOnHolidayArrayList = new ArrayList<>();
    private JSONObject jsonObject;


    public DownloadWorkOnHolidayRequestsAsyncTask(Context context, AsyncResponseListener asyncResponseListener, JSONObject jsonObject)
    {
        this.asyncResponseListener = asyncResponseListener;
        this.contextWeakReference = new WeakReference<>(context);
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute()
    {
        try
        {
            // Check internet connection
            Context context = contextWeakReference.get();

            if (!Utility.haveNetworkConnection(context))
            {
                errorCode = 2;
                cancel(true);
            }
        }
        catch (Exception err)
        {
            error = "Error onPreExecute: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        try
        {
            Context context = contextWeakReference.get();

            AsyncHttpResponseHandler asyncHttpResponseHandler = new AsyncHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(new String(responseBody));
                        setFields((JSONArray) jsonObject.get("records"));
                    }
                    catch (Exception err)
                    {
                        error = "Error asyncHttpResponseHandler onSuccess: \n" + err.toString();
                        cancel(true);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                {
                    try
                    {
                        if (statusCode == 0)
                        {
                            DownloadWorkOnHolidayRequestsAsyncTask.this.errorCode = 3;
                        }
                        else
                        {
                            DownloadWorkOnHolidayRequestsAsyncTask.this.errorCode = 4;
                            DownloadWorkOnHolidayRequestsAsyncTask.this.error = new String(responseBody);
                        }
                    }
                    catch (Exception err)
                    {
                        DownloadWorkOnHolidayRequestsAsyncTask.this.error = "Error asyncHttpResponseHandler onFailure: \n" + err.toString();
                    }

                    cancel(true);
                }
            };

            Debugger.printO("==========");
            Debugger.printO(jsonObject);
            Debugger.printO("==========");

            HttpProvider.postSync(context, "workon_holiday/read/", new StringEntity(jsonObject.toString()), true, asyncHttpResponseHandler);
        }
        catch (Exception err)
        {
            error = "Error doInBackground: \n" + err.toString();
            cancel(true);
        }

        return null;
    }

    private void setFields(JSONArray workOnHolidayJSONArray)
    {
        try
        {
            for (int i = 0; i < workOnHolidayJSONArray.length(); i++)
            {
                JSONObject jsonObject = workOnHolidayJSONArray.getJSONObject(i);
                WorkOnHoliday workOnHoliday = new WorkOnHoliday();

                workOnHoliday.setId(jsonObject.getInt("id"));
                workOnHoliday.setEmployeeId(jsonObject.getJSONObject("employee").getInt("id"));
                workOnHoliday.setEmployeeName(jsonObject.getJSONObject("employee").getString("fullname"));
                workOnHoliday.setHours(jsonObject.getDouble("hours"));
                workOnHoliday.setReason(jsonObject.getString("reason"));
                workOnHoliday.setRemarks(jsonObject.has("remarks") ? jsonObject.getString("remarks") : "");
                workOnHoliday.setDate(DateStringToDate.dbFormat(jsonObject.getString("date")));
                workOnHoliday.setRequestedDate(DateStringToDate.dateTimeDbFormat(jsonObject.getString("requested_date")));

                workOnHolidayArrayList.add(workOnHoliday);
            }
        }
        catch (Exception err)
        {
            error = "Error setting fields: \n" + err.toString();
            cancel(true);
        }
    }

    @Override
    protected void onCancelled()
    {
        asyncResponseListener.onFailure(errorCode, error);
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        asyncResponseListener.onSuccess(workOnHolidayArrayList);
    }
}
