package com.yahshua.yahshuatimekeeperdmpi.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateTimeHandler;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Debugger;

import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class ErrorLogs {

    private int id;
    private String errorLogs;
    private Date date;

    public int getId()
    {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName() {
        return errorLogs;
    }

    public void setName(String errorLogs) {
        this.errorLogs = errorLogs;
    }
   public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public static ArrayList<ErrorLogs> read(Context context)
    {
        ArrayList<ErrorLogs> errorLogsArrayList = new ArrayList<ErrorLogs>();
        try
        {   DbAdapter  db = new DbAdapter(context);
            db.open();

            String selectQuery = "SELECT * FROM errorLogs ORDER BY id DESC";
            Cursor cursor = db.getDb().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    ErrorLogs errorLogs = new ErrorLogs();

                    errorLogs.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    errorLogs.setName(cursor.getString(cursor.getColumnIndex("error_logs")));
                    errorLogs.setDate((DateTimeHandler.convertStringtoDateForError(cursor.getString(cursor.getColumnIndex("date")))));

                    errorLogsArrayList.add(errorLogs);

                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
            return errorLogsArrayList;

        } catch (Exception err)
        {
            Debugger.logD("ErrorLogs read "+err.getMessage());
            return errorLogsArrayList;
        }
    }

    public boolean save(Context context)
    {
        try
        {
            DbAdapter db = new DbAdapter(context);
            db.open();

            ContentValues contentValues = new ContentValues();

            Date today = new Date();

            contentValues.put("error_logs", getName());
            contentValues.put("date",DateTimeHandler.getTimeForError(today));

            db.getDb().insertOrThrow("errorLogs", null, contentValues);

            db.close();
            return true;
        } catch (Exception err)
        {
            Debugger.logD("ErrorLogs save "+err.getMessage());
            return false;
        }
    }


    public static boolean save(String errorMessage, DbAdapter dbAdapter)
    {
        try
        {
            ContentValues contentValues = new ContentValues();
            Date today = new Date();

            contentValues.put("error_logs", errorMessage);
            contentValues.put("date",DateTimeHandler.getTimeForError(today));

            dbAdapter.getDb().insertOrThrow("errorLogs", null, contentValues);

            return true;
        } catch (Exception err)
        {
            Debugger.logD("ErrorLogs  : "+err.getMessage());
            return false;
        }
    }
}
