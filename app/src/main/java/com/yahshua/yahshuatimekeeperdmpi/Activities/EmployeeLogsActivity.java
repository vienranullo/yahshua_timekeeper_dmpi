package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.Adapters.EmployeeLogAdapter;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.EmployeeLogs;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;

public class EmployeeLogsActivity extends BaseActivity
 {
    // Context
     private Context context;

     // Widgets
    private ListView lv_employeeLogs;
    private Button btnIN;
    private Button btnOUT;
    private TextView tvnoLogs;

    // Adapter
    private EmployeeLogAdapter employeeLogsAdapter ;

    // ArrayList and Data
    private ArrayList<EmployeeLogs> employeeLogsArrayList = new ArrayList <>();
    private EmployeeLogs employeeLogs = new EmployeeLogs();

     // Handles Interface and Initialization Functions
     // Parameters - Default
     // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_logs);
        context = this;

        InitializeUI();

        setTitle("Employee Logs");
    }

     // Initialize component from a layout file
     private void InitializeUI()
    {

        // TextView
        tvnoLogs = findViewById(R.id.tvnoLogs);

        // ListView
        lv_employeeLogs = findViewById(R.id.lv_employeeLogs);
        employeeLogsAdapter = new EmployeeLogAdapter(context,employeeLogsArrayList);
        lv_employeeLogs.setAdapter(employeeLogsAdapter);
        lv_employeeLogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                employeeLogs = employeeLogsAdapter.getItem(position);

                Intent intent = new Intent(context,ViewLogsActivity.class);

                intent.putExtra("Image",employeeLogs.getLogImg());
                intent.putExtra("Name",employeeLogs.employeeObject.getFullName());
                intent.putExtra("Code", employeeLogs.getSyncId());
                intent.putExtra("Date", employeeLogs.getDisplayDate());
                intent.putExtra("LogType",employeeLogs.getLogType());
                intent.putExtra("Time",employeeLogs.getLogTime());
                startActivity(intent);
            }
        });

        // Buttons
        btnIN = findViewById(R.id.btnIN);
        readRecords("IN");
        btnIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                readRecords("IN");
            }
        });

        btnOUT = findViewById(R.id.btnOUT);
        btnOUT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                readRecords("OUT");

            }
        });

    }

    // Method to set no logs found when there is no records on the list
     private void noLogsFound()
     {

         if (employeeLogsArrayList.size() == 0 ) {
             tvnoLogs.setVisibility(View.VISIBLE);
             lv_employeeLogs.setVisibility(View.GONE);
         } else {
             tvnoLogs.setVisibility(View.GONE);
             lv_employeeLogs.setVisibility(View.VISIBLE);
         }
     }

     // Read records from database
     private void readRecords(String logType)
     {
         DbAdapter dbAdapter = new DbAdapter(context);
         dbAdapter.open();

         // Get employee id from selectedEmployee in employeeFragment
         Intent intent = getIntent();
         int employeeId = intent.getExtras().getInt("EmployeeLogs");

         employeeLogsArrayList = EmployeeLogs.read(false, logType, employeeId, 0, dbAdapter);

         noLogsFound();

         employeeLogsAdapter.clear();
         employeeLogsAdapter.addAll(employeeLogsArrayList);
         employeeLogsAdapter.notifyDataSetChanged();

         dbAdapter.close();
     }

     //Disable reading of rfid to avoid error
     public void onResume()
     {
         super.onResume();

         if (Utility.isNfcSupported(context))
         {
             NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
             PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
             nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
         }
     }

     public void onPause()
     {
         super.onPause();

         if (Utility.isNfcSupported(context))
         {
             NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
             nfcAdapter.disableForegroundDispatch(this);
         }
     }

     public void onNewIntent(Intent intent)
     {
         if (Utility.isNfcSupported(context))
         {
             if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                 // drop NFC events
             }
         }
     }
     //End

 }