package com.yahshua.yahshuatimekeeperdmpi.Utils;


import android.content.Context;
import android.database.Cursor;

import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DatabaseAdapter;

import java.util.ArrayList;

public class DiagnosticClass {


    public static void readTable(Context context, String tableName, ArrayList<String> fields)
    {
        try
        {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String pattern = "([A-Z])";
            String selectQuery = "SELECT * FROM " + tableName;
            Cursor cursor = db.read(selectQuery);

            StringBuilder stringBuilder = new StringBuilder();
            for(String field : fields)
            {
                String fieldName = field.replaceAll(pattern, "");
                fieldName = fieldName.replaceAll("\\s+","");
                stringBuilder.append(fieldName);
                stringBuilder.append(" | ");
            }

            Debugger.logD(stringBuilder.toString());

            if (cursor.moveToFirst()) {
                do {

                    StringBuilder stringBuilder1 = new StringBuilder();
                    for (String field : fields)
                    {
                        String fieldName = field.replaceAll(pattern, "");
                        fieldName = fieldName.replaceAll("\\s+","");
                        stringBuilder1.append(Converter.ObjectToString(cursor, fieldName ));
                        stringBuilder1.append(" | ");
                    }

                    Debugger.logD(stringBuilder1.toString());

                } while (cursor.moveToNext());
            }

            cursor.close();

        } catch (Exception err)
        {
            Debugger.logD(err.toString());
        }
    }

}
