package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.yahshua.yahshuatimekeeperdmpi.Activities.EmployeeDeductionCreateActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.EmployeeLogsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.LeaveApplicationsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.OvertimeApplicationsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.UndertimeApplicationsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnHolidayApplicationsActivity;
import com.yahshua.yahshuatimekeeperdmpi.Activities.WorkOnRestdayApplicationsActivity;
import com.yahshua.yahshuatimekeeperdmpi.DBClasses.DbAdapter;
import com.yahshua.yahshuatimekeeperdmpi.Models.Employee;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Toasters;
import com.yahshua.yahshuatimekeeperdmpi.Utils.UserSession;

import java.util.ArrayList;


public class SystemIdDialog extends DialogFragment
{
    private EditText etSystemId;
    private int selectedApplication;
    Bundle bundle;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        bundle = getArguments();

        if (bundle != null)
        {
            if (bundle.containsKey("selectedApplication")) selectedApplication = bundle.getInt("selectedApplication");
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Title
        builder.setTitle(getString(R.string.input_chapa_no));

        // Content Area
        etSystemId = new EditText(getContext());

        etSystemId.setInputType(InputType.TYPE_CLASS_NUMBER);

        // System ID Digits limit
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(6);
        etSystemId.setFilters(filterArray);

        etSystemId.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus) getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        etSystemId.setLayoutParams(layoutParams);

        builder.setView(etSystemId);

        // Action Buttons
        builder.setPositiveButton("Okay", null);
        builder.setNegativeButton("Cancel", null);

        final AlertDialog alertDialog = builder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            @Override
            public void onShow(DialogInterface dialogInterface)
            {
                Button okayBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                okayBtn.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(validate()) getEmployee(Integer.parseInt(etSystemId.getText().toString()), alertDialog);
                    }
                });
            }
        });


        return alertDialog;
    }

    private boolean validate(){

        if(etSystemId.getText().toString().equals("")){
            etSystemId.setError(getContext().getString(R.string.error_field_required));
            etSystemId.requestFocus();
            return false;
        }else{
            return  true;
        }

    }

    private void getEmployee(int systemId, AlertDialog alertDialog)
    {
        DbAdapter dbAdapter = new DbAdapter(getContext());
        dbAdapter.open();

        // Check if System ID exists and Get Employee
        String searchQuery = "WHERE employeeid2 = " + systemId;
        ArrayList<Employee> employeeArrayList = Employee.read(getContext(), searchQuery, dbAdapter);
        Employee employee;

        dbAdapter.close();
        if (employeeArrayList.size() == 1)
        {
            employee = employeeArrayList.get(0);
        }
        else
        {
            Toasters.ShowToast(getContext(), "Invalid CHAPA No.");
            return;
        }

        // View employee logs
        if (bundle == null)
        {
            Intent intent = new Intent(getContext(), EmployeeLogsActivity.class);
            intent.putExtra("EmployeeLogs", employee.getEmployeeid2());
            startActivity(intent);
        }
        // Select application
        else if (bundle.containsKey("selectedApplication"))
        {
            Intent intent = new Intent();

            switch (selectedApplication) {
                case 0:
                    intent = new Intent(getActivity(), LeaveApplicationsActivity.class);
                    break;
                case 1:
                    intent = new Intent(getActivity(), OvertimeApplicationsActivity.class);
                    break;
                case 2:
                    intent = new Intent(getActivity(), UndertimeApplicationsActivity.class);
                    break;
                case 3:
                    intent = new Intent(getActivity(), WorkOnRestdayApplicationsActivity.class);
                    break;
                case 4:
                    intent = new Intent(getActivity(), WorkOnHolidayApplicationsActivity.class);
                    break;
                case 5:
                    intent = new Intent(getActivity(), EmployeeDeductionCreateActivity.class);
                    break;
            }

            intent.putExtra("EMPLOYEE", employee);
            startActivity(intent);
        }

        alertDialog.dismiss();
    }
}