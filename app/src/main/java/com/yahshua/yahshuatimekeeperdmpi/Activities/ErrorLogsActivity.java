package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.widget.ListView;

import com.yahshua.yahshuatimekeeperdmpi.Models.ErrorLogs;
import com.yahshua.yahshuatimekeeperdmpi.Adapters.ErrorLogsAdapter;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

import java.util.ArrayList;

public class ErrorLogsActivity extends BaseActivity
{
    // Context
    private Context context;

    // Widget
    private ListView lv_errorLogs;

    //Adapter
    private ErrorLogsAdapter errorLogsAdapter;

    // Data

   private ArrayList<ErrorLogs> errorLogsArrayList = new ArrayList<>();

    // Handles Interface and Initialization Functions
    // Parameters - Default
    // Return - View mag display sa layout
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_logs);
        context = this;

        InitializeUI();
        setTitle("Error Logs");
    }

    // Initialize component from a layout file
    private void InitializeUI()
    {
        // ListView
        lv_errorLogs = (ListView) findViewById(R.id.lv_errorLogs);
        errorLogsAdapter = new ErrorLogsAdapter(this,errorLogsArrayList);
        lv_errorLogs.setAdapter(errorLogsAdapter);
        readRecords();
    }

    private void readRecords()
    {
      errorLogsArrayList = ErrorLogs.read(context);
      errorLogsAdapter.clear();
      errorLogsAdapter.addAll(errorLogsArrayList);
      errorLogsAdapter.notifyDataSetChanged();
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End

}
