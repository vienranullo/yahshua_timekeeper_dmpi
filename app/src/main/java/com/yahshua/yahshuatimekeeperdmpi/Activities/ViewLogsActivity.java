package com.yahshua.yahshuatimekeeperdmpi.Activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.Utility;

public class ViewLogsActivity extends BaseActivity
{
    // Context
    private Context context;

    // Widgets
    private ImageView logImage;
    private TextView tvCode,tvName,tvDate,tvTime;
    private String fullName, code, date, logType, time;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);
        context = this;

        setTitle("Image Log Details");

        uri = Uri.parse(getIntent().getExtras().getString("Image"));
        fullName = getIntent().getStringExtra("Name");
        code     = getIntent().getStringExtra("Code");
        date     = getIntent().getStringExtra("Date");
        logType  = getIntent().getStringExtra("LogType");
        time     = getIntent().getStringExtra("Time");

        initializeUI();
    }

    private void initializeUI()
    {
        logImage = findViewById(R.id.logImage);
        tvCode = findViewById(R.id.tvCode);
        tvName = findViewById(R.id.tvName);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);

        logImage.setImageURI(uri);
        tvName.setText(fullName);
        tvCode.setText(code);
        tvDate.setText(date);
        tvTime.setText(String.format("(%s) %s", logType, time));
    }

    //Disable reading of rfid to avoid error
    public void onResume()
    {
        super.onResume();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

    public void onPause()
    {
        super.onPause();

        if (Utility.isNfcSupported(context))
        {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    public void onNewIntent(Intent intent)
    {
        if (Utility.isNfcSupported(context))
        {
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                // drop NFC events
            }
        }
    }
    //End

}
