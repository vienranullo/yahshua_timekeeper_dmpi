package com.yahshua.yahshuatimekeeperdmpi.DialogFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.yahshua.yahshuatimekeeperdmpi.Models.AuditLog;
import com.yahshua.yahshuatimekeeperdmpi.Models.WorkOnHoliday;
import com.yahshua.yahshuatimekeeperdmpi.R;
import com.yahshua.yahshuatimekeeperdmpi.Utils.DateToString;
import com.yahshua.yahshuatimekeeperdmpi.Utils.ToastNotification;

import java.util.Calendar;
import java.util.Date;


public class WorkOnHolidayAddEditDialog extends DialogFragment
{
    private Context context;
    private WorkOnHoliday workOnHoliday = new WorkOnHoliday();
    private WorkOnHoliday oldWorkOnHoliday = new WorkOnHoliday();
    private View view;
    private DialogInterface.OnDismissListener onDismissListener;

    // Employee
    private int       employeeId;
    private String    employeeName;
    private EditText  etEmployeeName;
    private ImageView imgClose;

    // Date
    private EditText etDate;
    private DatePickerDialog.OnDateSetListener onDateSetListener;
    private Calendar calendar = Calendar.getInstance();
    private Date date;

    // Hours
    private EditText etHours;
    private String hoursStr;

    // Minutes
    private EditText etMinutes;
    private int minutes;

    // Reason
    private EditText etReason;
    private String reason;


    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        initializeData();

        view = inflater.inflate(R.layout.workonholiday_add_edit_dialog, container, false);
        initializeUI();
        loadForm();

        return view;
    }

    private void initializeData()
    {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.size() > 0)
        {
            employeeId = bundle.getInt("EMPLOYEE_ID");
            employeeName = bundle.getString("EMPLOYEE_NAME");
            if (bundle.containsKey("SELECTED_WORKONHOLIDAY"))
            {
                workOnHoliday = bundle.getParcelable("SELECTED_WORKONHOLIDAY");
                oldWorkOnHoliday = bundle.getParcelable("SELECTED_WORKONHOLIDAY");
            }
        }
    }

    private void initializeUI()
    {
        etEmployeeName = view.findViewById(R.id.etEmployeeName);
        etHours        = view.findViewById(R.id.etHours);
        etMinutes      = view.findViewById(R.id.etMinutes);
        etReason       = view.findViewById(R.id.etReason);
        imgClose       = view.findViewById(R.id.imgClose);

        setTitle();
        initializeDateEditText();
        initializeSaveWorkOnHolidayButton();

        imgClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void setTitle()
    {
        String dialogTitle = (workOnHoliday.getId() > 0) ? "Edit Work on Holiday" : "Apply for Work on Holiday";
        getDialog().setTitle(dialogTitle);
    }

    private void initializeDateEditText()
    {
        etDate = view.findViewById(R.id.etDate);

        onDateSetListener = new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
            {
                calendar.set(year, month, dayOfMonth);
                date = calendar.getTime();
                etDate.setText(DateToString.formFormat(date));
            }
        };

        etDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new DatePickerDialog(context, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void initializeSaveWorkOnHolidayButton()
    {
        Button btnSaveWorkOnHoliday = view.findViewById(R.id.btnSaveWorkOnHoliday);
        btnSaveWorkOnHoliday.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                validateForm();
            }
        });
    }

    private void validateForm()
    {
        // Hours
        hoursStr = etHours.getText().toString();
        if (hoursStr.isEmpty())
        {
            ToastNotification.error(context, "Please enter hours");
            return;
        }

        // Minutes
        String minutesStr = etMinutes.getText().toString();
        if (minutesStr.isEmpty())
        {
            ToastNotification.error(context, "Please enter minutes");
            return;
        }

        minutes = Integer.parseInt(minutesStr);
        if (minutes > 59)
        {
            ToastNotification.error(context, "Invalid minutes");
            return;
        }

        // Reason
        reason = etReason.getText().toString();
        if (reason.isEmpty())
        {
            ToastNotification.error(context, "Please enter reason");
            return;
        }
        if(workOnHoliday.isSync())
        {
            ToastNotification.warning(context, getResources().getString(R.string.unable_to_edit));
            return;
        }

        saveWorkOnHoliday();
    }

    private void saveWorkOnHoliday()
    {
        double hours = Integer.parseInt(hoursStr);
        hours +=  ((double) minutes * 100 / 60) / 100;

        workOnHoliday.setEmployeeId(employeeId);
        workOnHoliday.setEmployeeName(employeeName);
        workOnHoliday.setDate(date);
        workOnHoliday.setHours(hours);
        workOnHoliday.setReason(reason);

        if (workOnHoliday.save(context))
        {
            String message = "Work on Holiday Application ";
            message += (workOnHoliday.getId() > 0) ? "Edited!" : "Created!";
            ToastNotification.success(getContext(), message);
            AuditLog.saveAuditLog(context,"WORK ON HOLIDAY","CREATE/EDIT","SUCCESS",employeeName);
            dismiss();
        }
    }

    private void loadForm()
    {
        etEmployeeName.setText(employeeName);

        if (workOnHoliday.getId() > 0)
        {
            date = workOnHoliday.getDate();
            etReason.setText(workOnHoliday.getReason());

            // Hours and minutes
            double hours = workOnHoliday.getHours();
            int hoursInt = (int) hours;
            double minutesDouble = hours - hoursInt;
            String minutesStr = Integer.toString((int) (((minutesDouble * 100) * 60) / 100));

            etMinutes.setText(minutesStr);
            etHours.setText(hoursInt + "");
        }
        else
        {
            date = calendar.getTime();
        }

        etDate.setText(DateToString.formFormat(date));
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener)
    {
        this.onDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);
        if (onDismissListener != null) onDismissListener.onDismiss(dialog);
    }

    @Override
    public void onResume()
    {
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        super.onResume();
    }

    public int getTheme() {
        return R.style.full_screen_dialog;
    }
}
